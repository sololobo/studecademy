import React from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import  './components/includes/css/main.css';
import  './components/includes/css/login.css';
import  './components/includes/css/style.css';
import  './components/includes/css/style.min.css';

import './components/includes/vendors/css/font-awesome.min.css';
import './components/includes/vendors/css/simple-line-icons.min.css';

import './components/includes/vendors/css/font-awesome.css.map';
import './components/includes/vendors/fonts/stylesheet.css';
import Footer from './components/footerComponent/footer';
import Header from  './components/dashboardHeader/header';
import Index from './components/indexComponent/indexComponent';
import Login from './components/loginComponent/login';
import Register from './components/registerComponent/register';
import Registerorg from './components/registerorgComponent/registerorg.js';
import Forgetpassword from './components/forgetPassword/forgetPassword';
import Orgredirection from './components/organisation/redirectionModule/redirection';
import Studentredirection from './components/student/redirectionModule/redirection';
import Teacherredirection from './components/teacher/redirectionModule/redirection';
import Control from './components/controler/controlerarea/controlerarea';
import Verified from './components/mailverify/verified';

import Examheader from './components/examHeader/header';
import Examfooter from './components/examFooter/footer';
import Startexam from './components/student/examModule/StartExam';
function App() {
  return (

    <Router >
      <Switch>
    <Route path='/' exact >
      <Index ></Index>
      </Route>

      <Route path='/registerorg' >
      <Registerorg></Registerorg>
      </Route>

      <Route path='/verified' >
      <Verified></Verified>
      </Route>

      <Route path='/register' >
      <Register></Register>
      </Route>

      <Route path='/login' >
      <Login></Login>
      </Route>

      <Route path='/forgetpass' >
      <Forgetpassword></Forgetpassword>
      </Route>

      <Route path='/organisation/dashboard' >
      <Header ></Header>
      <Orgredirection></Orgredirection>
      <Footer></Footer>
      </Route>

      <Route path='/student/dashboard' >
      <Header></Header>
      <Studentredirection></Studentredirection>
      <Footer></Footer>
      </Route>

      <Route path='/teacher/dashboard' >
      <Header></Header>
      <Teacherredirection></Teacherredirection>
      <Footer></Footer>
      </Route>


      <Route path='/controlpanel' >
      <Header></Header>
      <Control></Control>
      <Footer></Footer>
      </Route>

      <Route path='/startexam' >
      <Examheader></Examheader>
      <Startexam></Startexam>
      <Examfooter></Examfooter>
      </Route>

      </Switch>

    </Router>

  );
}

export default App;
