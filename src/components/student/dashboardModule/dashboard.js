import React, { Component } from 'react';
import $ from 'jquery';
import './dashboard.css';
import  './todo.css';


class Dashboard extends Component {

    render() {

        return (
            <div className="container-fluid">
                <div className="animated fadeIn">
                    {/*--=====Dasboard header Graphs=======*/}
                    <h2 className="font-weight-bold text-center">Welcome Back Somnath!</h2>
                    {/*--/Analytics*/}
                    <div className="row mt-4">
                        <div className="col-6 col-lg-3">
                            <div className="card ">
                                <div className="card-body p-3 d-flex align-items-center">
                                    <i className="icon-docs bg-danger p-3 font-2xl mr-3" />
                                    <div>
                                        <div className="text-value-sm text-danger">300</div>
                                        <div className="text-muted text-uppercase font-weight-bold small">Total Test
                Uploaded</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-6 col-lg-3">
                            <div className="card">
                                <div className="card-body p-3 d-flex align-items-center">
                                    <i className="icon-user bg-warning p-3 font-2xl mr-3" />
                                    <div>
                                        <div className="text-value-sm text-warning">300</div>
                                        <div className="text-muted text-uppercase font-weight-bold small">Total Assignment
              </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-6 col-lg-3">
                            <div className="card">
                                <div className="card-body p-3 d-flex align-items-center">
                                    <i className="icon-bell bg-success text-white p-3 font-2xl mr-3" />
                                    <div>
                                        <div className="text-value-sm text-success">150</div>
                                        <div className="text-muted text-uppercase font-weight-bold small">Test Submitted
              </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-6 col-lg-3">
                            <div className="card">
                                <div className="card-body p-3 d-flex align-items-center">
                                    <i className="icon-docs text-white bg-primary p-3 font-2xl mr-3" />
                                    <div>
                                        <div className="text-value-sm text-danger">200</div>
                                        <div className="text-muted text-uppercase font-weight-bold small">Not Submitting
                Test</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*--/row-*/}
                    {/*-===Routine*/}
                    <div className="card">
                        <div className="card-body">
                            <div className="table-responsive">
                                <table className="table  table-bordered" cellSpacing={0}>
                                    <thead>
                                        <tr className="text-center text-muted">
                                            <th> Time </th>
                                            <th />
                                            <th />
                                            <th />
                                            <th />
                                            <th />
                                            <th />
                                            <th />
                                            <th />
                                        </tr>
                                    </thead>
                                    <tbody className="text-center">
                                        <tr>
                                            <th className="text-muted text-center text-uppercase" rowSpan={3}>Monday</th>
                                            <th>Physics</th>
                                            <th>Chemistry</th>
                                            <th>--</th>
                                            <th>Mathematics</th>
                                            <th>Electronics</th>
                                            <th>Bengali</th>
                                            <th>--</th>
                                            <th>English</th>
                                        </tr>
                                        <tr>
                                            <th />
                                            <th>A. Sharma</th>
                                            <th>B. Sharma</th>
                                            <th>--</th>
                                            <th>C.Sharma</th>
                                            <th>C.Sharma</th>
                                            <th>C.Sharma</th>
                                            <th>-</th>
                                        </tr>
                                        <tr>
                                            <th />
                                            <th>R203</th>
                                            <th>R203</th>
                                            <th>--</th>
                                            <th>R203</th>
                                            <th>R203</th>
                                            <th>R203</th>
                                            <th>-</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {/*--=======/routine====*/}
                    {/*- event section-*/}
                    <div className="row">
                        <div className="col-md-6 col-xs-12">
                            {/*--notice--*/}
                            <div className="card">
                                <div className="card-header text-muted">
                                    <b>Notifications</b>
                                </div>
                                <div className="card-body scrollbar events-scroll">
                                    <div className="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>New Physics Assignement</strong> has updated. <a href="#"> View Now</a>
                                        <button type="button btn btn-primary" className="close" data-dismiss="alert" aria-label="Close">
                                            <i className="icon-close" />
                                        </button>
                                    </div>
                                    <div className="alert alert-danger alert-dismissible fade show" role="alert">
                                        <strong>Holy guacamole!</strong> You should check in on some of those fields
              below.
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                            <i className="icon-close" />
                                        </button>
                                    </div>
                                    <div className="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong>Holy guacamole!</strong> You should check in on some of those fields
              below.
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                            <i className="icon-close" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {/*--/to do list--*/}
                        </div>
                        {/*--/col-*/}
                        <div className="col-md-6 col-xs-12">
                            {/*--to do list--*/}
                            <div className="card">
                                <div className="card-header text-muted">
                                    <b> To Do Lists</b>
                                </div>
                                <div className="card-body scrollbar events-scroll">
                                    <div className="add-items d-flex"> <input type="text" className="form-control todo-list-input" style={{ width: '75%' }} placeholder="What do you need to do today?" /> <button className="add btn btn-primary font-weight-bold todo-list-add-btn" style={{ width: '25%', height: '20%', borderRadius: 'none !important' }}>Add</button>
                                    </div>
                                    <div className="list-wrapper">
                                        <ul className="d-flex flex-column-reverse todo-list">
                                            <li>
                                                <div className="form-check "> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> For what reason would it
                      be advisable. <i className="input-helper" /></label> <i className=" remove mdi mdi-close-circle-outline" /></div>
                                            </li>
                                            <li className="completed">
                                                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> For what reason
                      would it be advisable for me to think. <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                                            </li>
                                            <li>
                                                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> it be advisable for me to
                      think about business content? <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                                            </li>
                                            <li>
                                                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print Statements all <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                                            </li>
                                            <li className="completed">
                                                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> Call Rampbo <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                                            </li>
                                            <li>
                                                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print bills <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/*--/to do list--*/}
                        </div>
                        {/*--/col-*/}
                    </div>
                    {/*-/event section-*/}
                </div>
            </div>

        );


    }
}
export default Dashboard