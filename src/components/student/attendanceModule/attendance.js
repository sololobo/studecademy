import React, { Component } from 'react';


class attendance extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*===Body Content Goes Here===*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Attendance Report</span>
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <b>Select Month:</b>
                            <select name id className="form-control-sm">
                                <option value selected> --- Select Month---</option>
                                <option value> January</option>
                                <option value>February</option>
                                <option value>March</option>
                                <option value>April</option>
                                <option value>May</option>
                                <option value>June</option>
                                <option value>July</option>
                                <option value>August</option>
                                <option value>September</option>
                                <option value>October</option>
                                <option value>November</option>
                                <option value>December</option>
                            </select> &nbsp;
        <b>Select Subject:</b>
                            <select name id className="form-control-sm">
                                <option value selected> --- Select Subject---</option>
                                <option value> Subject1</option>
                                <option value>Subject2</option>
                                <option value>Subject 2</option>
                            </select>
                            <button className="btn btn-sm btn-primary" type="submit"> Submit Now</button>
                        </div>
                        {/*-/Filter-*/}
                        {/*-Status of Attendance*/}
                        <div className="row">
                            <div className="col-sm-6 col-md-3">
                                <div className="card alert-blue">
                                    <div className="card-body">
                                        <div className="h1 text-muted text-right mb-4">
                                            <i className="icon-badge" />
                                        </div>
                                        <div className="h4 mb-0">250</div>
                                        <small className="text-muted text-uppercase font-weight-bold">Total Class Held</small>
                                        <div className="progress progress-xs mt-3 mb-0">
                                            <div className="progress-bar bg-warning" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*/.col*/}
                            <div className="col-sm-6 col-md-3">
                                <div className="card alert-blue">
                                    <div className="card-body">
                                        <div className="h1 text-muted text-right mb-4">
                                            <i className="icon-badge" />
                                        </div>
                                        <div className="h4 mb-0">200</div>
                                        <small className="text-muted text-uppercase font-weight-bold">Present</small>
                                        <div className="progress progress-xs mt-3 mb-0">
                                            <div className="progress-bar bg-warning" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*/.col*/}
                            <div className="col-sm-6 col-md-3">
                                <div className="card alert-blue">
                                    <div className="card-body">
                                        <div className="h1 text-muted text-right mb-4">
                                            <i className="icon-pie-chart" />
                                        </div>
                                        <div className="h4 mb-0">28</div>
                                        <small className="text-muted text-uppercase font-weight-bold">Absent</small>
                                        <div className="progress progress-xs mt-3 mb-0">
                                            <div className="progress-bar" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*/.col*/}
                            <div className="col-sm-6 col-md-3">
                                <div className="card alert-blue">
                                    <div className="card-body">
                                        <div className="h1 text-muted text-right mb-4">
                                            <i className="icon-speedometer" />
                                        </div>
                                        <div className="h4 mb-0">24%</div>
                                        <small className="text-muted text-uppercase font-weight-bold">Overall Attendance</small>
                                        <div className="progress progress-xs mt-3 mb-0">
                                            <div className="progress-bar bg-danger" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*/.col*/}
                        </div>
                        {/*/Status of Attendance*/}
                        {/*--attendance--*/}
                        <table className="table table-bordered table-responsive">
                            <thead className="text-center">
                                <tr>
                                    <th>Subject</th>
                                    <th>01</th>
                                    <th>02</th>
                                    <th>03</th>
                                    <th>04</th>
                                    <th>05</th>
                                    <th>06</th>
                                    <th>07</th>
                                    <th>08</th>
                                    <th>09</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <th>23</th>
                                    <th>24</th>
                                    <th>25</th>
                                    <th>26</th>
                                    <th>27</th>
                                    <th>28</th>
                                    <th>29</th>
                                    <th>30</th>
                                    <th>31</th>
                                </tr>
                            </thead>
                            <tbody className="text-center">
                                <tr>
                                    <th>Physics</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>Chemistry</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>Biology</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>Bengali</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>English</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>Physical Science</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                            </tbody>
                        </table>
                        {/*--/attendance--*/}
                    </div>
                </div>
                {/*-====Body Content Goes Here===*/}
            </div>

        );


    }
}
export default attendance