import React, { Component } from 'react';
import './studentProfile.css';
import Cookies from 'js-cookie';
import logo from '../../../media/features/books.png';
import swal from 'sweetalert';
import $ from 'jquery';

class footerComponent extends Component {
    constructor(props) {
        super(props);


        this.state = {
            Students: []
        }

    }
    componentDidMount() {




        let url = global.API + "/studapi/public/api/viewastudent";
        fetch(url, {
            method: 'POST', 
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'student_i_d': Cookies.get('studentid') })
        })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    Students: json
                })
            });



    }
    changePass() {
        //let email = document.getElementById('email1').innerHTML;
        //let email = document.getElementById('username').defaultValue;
        let email = Cookies.get('username')
        let pwd = document.getElementById('newpass').defaultValue;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }
                                else {
                                    swal("not saved", {
                                        icon: "warning",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }

                            }));
                }

            });

    }

    submit(students) {
        this.state = {
            studentID: students.studentID,
            sectionID: students.sectionsID,
            rollNo: document.getElementById('studentprofilerollNo').value,
            studentName: document.getElementById('studentprofilestudentName').value,
            contactNo: document.getElementById('studentprofilecontactNo').value,
            gender: document.getElementById('studentprofilegender').value,
            dateOfBirth: document.getElementById('studentprofiledateOfBirth').value,
            dateOfAdmission: document.getElementById('studentprofiledateOfAdmission').value,
            fatherName: document.getElementById('studentprofilefatherName').value,
            fatherContactNo: document.getElementById('studentprofilefatherContactNo').value,
            fatherEmailID: document.getElementById('studentprofilefatherEmailID').value,
            motherName: document.getElementById('studentprofilemotherName').value,
            motherContactNo: document.getElementById('studentprofilemotherContactNo').value,
            motherEmailID: document.getElementById('studentprofilemotherEmailID').value,
            guardianName: document.getElementById('studentprofileguardianName').value,
            guardianContactNo:document.getElementById('studentprofileguardianContactNo').value,
            guardianEmail:document.getElementById('studentprofileguardianEmail').value,
            address1:document.getElementById('studentprofileaddress1').value,
            address2:document.getElementById('studentprofileaddress2').value,
            city:document.getElementById('studentprofilecity').value,
            state:document.getElementById('studentprofilestate').value,
            country:document.getElementById('studentprofilecountry').value,
            pinCode:document.getElementById('studentprofilepinCode').value,
           // desig:document.getElementById('editBookSessionName').value
         }
        console.log(this.state);

        let url = global.API + "/studapi/public/api/updatestudent";
        let data = this.state;
        // if (this.state.logo == null)
        //     this.state.logo = this.props.undata.logo;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        if (document.getElementById('studentprofilePic').files.length == 0)
        //console.log(orga.Logo+"hi");
            fd.append('profilePic', students.profilePic);
        else
            fd.append('profile_pic', document.getElementById('studentprofilePic').files[0]);

        if (document.getElementById('studentprofileCover').files.length == 0)
        
            fd.append('coverPhoto', students.coverPhoto);
        else
            fd.append('coverPhoto', document.getElementById('studentprofileCover').files[0]);
        
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Profile Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.componentDidMount()

                }
                else {
                    swal({
                        title: "Error",
                        text: "Profile Not Updated",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
        console.log(data);
        
    }

    render() {
        var { isLoaded, Students } = this.state;

        return (
            <div className="fadeIn animated">
                <span className="h3 font-weight-bold mb-3">My Profile:</span>
                {Students.map(students => (
                    <div className="card mt-2">
                        <div className="card-header card-header1 h-100 ">
                            <img src={global.img + students.coverPhoto} alt="" className="cover-photo-my-profile" id="cover-photo" /> 
                            
                             
                             <img src={global.img + students.profilePic} alt="profile picture" className="profile-photo-my-profile" id="profile-photo" />
                       </div>
                        <div className="card-body">
                            <div className="pt-2 pb-2 pl-5 pr-5 border mb-5 mt-5">
                                <span className="border-title h3 font-weight-bold">About Myself:</span>
                                <div className="row mt-3">
                                <div className="col-md-4">
                                <div className="form-group ">
                                            <label for="" className="mb-2 font-weight-bold mt-2">Students ID:</label>
                                            <input type="text" name="" id="" defaultValue={students.studentID} disabled
                                                className="form-control  bg-white" />
                                        </div> 
                                </div>
                                <div className="col-md-4"> 
                                <div className="form-group">
                                            <label for="" className="mb-2 font-weight-bold mt-2">Name:</label>
                                            <input type="text" name="" id="studentprofilestudentName" defaultValue={students.studentName} disabled
                                                className="form-control  bg-white" />
                                        </div>
                                </div>
                                <div className="col-md-4"> 
                                <div className="form-group">
                                            <label for="" className="mb-2 font-weight-bold mt-2">Students roll:</label>
                                            <input type="text" name="" id="studentprofilerollNo" defaultValue={students.rollNo} disabled
                                                className="form-control  bg-white" />
                                        </div> 
                                </div>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-md-6">
                                       
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Contact Number:</label>
                                            <input type="text" name="" id="studentprofilecontactNo" defaultValue={students.contactNo} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Gender:</label>
                                            <input type="text" name="" id="studentprofilegender" defaultValue={students.gender} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                      
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Contact Email:</label>
                                            <input type="email" name="" id="email1" defaultValue={students.emailID} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Date of Birth:</label>
                                            <input type="text" name="" id="studentprofiledateOfBirth" defaultValue={students.dateOfBirth} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pt-2 pb-2 pl-5 pr-5 mb-5 border">
                                <span className="border-title h3 font-weight-bold">Academic Details:</span>
                                <div className="row mt-3">
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Date of Admission:</label>
                                            <input type="text" name="" id="studentprofiledateOfAdmission" defaultValue={students.dateOfAdmission} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for=""
                                                className="mb-2 font-weight-bold col-md-5 mt-2">className/Deparment:</label>
                                            <input type="text" name="" id="studentprofiledepartmentName" defaultValue={students.departmentName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Name of Institute:</label>
                                            <input type="text" name="" id="studentinsName" defaultValue={students.studentsName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Current
                                        Section/Semester:</label>
                                            <input type="email" name="" id="studentintName" defaultValue={students.studentName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="pt-2 pb-2 pl-5 pr-5 mb-5 border">
                                <span className="border-title h3 font-weight-bold">Personal Details:</span>
                                <div className="row mt-3">
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Father's Name:</label>
                                            <input type="text" name="" id="studentprofilefatherName" defaultValue={students.fatherName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Father's Contact
                                        Number:</label>
                                            <input type="text" name="" id="studentprofilefatherContactNo" defaultValue={students.fatherContactNo} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Father's Email:</label>
                                            <input type="text" name="" id="studentprofilefatherEmailID" defaultValue={students.fatherEmailID} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Mother's Name:</label>
                                            <input type="text" name="" id="studentprofilemotherName" defaultValue={students.motherName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Mother's Contact
                                        Number:</label>
                                            <input type="text" name="" id="studentprofilemotherContactNo" defaultValue={students.motherContactNo} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Mother's Email:</label>
                                            <input type="text" name="" id="studentprofilemotherEmailID" defaultValue={students.motherEmailID} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Guardian's Name:</label>
                                            <input type="text" name="" id="studentprofileguardianName" defaultValue={students.guardianName} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Guardian's Contact
                                        Number:</label>
                                            <input type="text" name="" id="studentprofileguardianContactNo" defaultValue={students.guardianContactNo} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Guardian's Email:</label>
                                            <input type="text" name="" id="studentprofileguardianEmail" defaultValue={students.guardianEmail} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address:</label>
                                            <input type="text" name="" id="studentprofileaddress1" defaultValue={students.address1} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address 2:</label>
                                            <input type="text" name="" id="studentprofileaddress2" defaultValue={students.address2} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">City:</label>
                                            <input type="text" name="" id="studentprofilecity" defaultValue={students.city} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">State:</label>
                                            <input type="text" name="" id="studentprofilestate" defaultValue={students.state} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Country:</label>
                                            <input type="text" name="" id="studentprofilecountry" defaultValue={students.country} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">PIN Code:</label>
                                            <input type="text" name="" id="studentprofilepinCode" defaultValue={students.pinCode} disabled
                                                className="form-control col-md-7  bg-white" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Profile
                                        Picture:</label>
                                            <input type="file" name="" id="studentprofilePic" className="form-control col-md-7"
                                                />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Cover
                                        Photo:</label>
                                            <input type="file" name="" id="studentprofileCover" className="form-control col-md-7"
                                                 />
                                        </div>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-footer">
                            <button type="submit" data-toggle="modal" data-target="#resetPassword" className="btn btn-success float-left text-white"><i className="icon-key"></i> Edit Password</button>
                            <button type="submit" onClick={() => { this.submit(students) }} className="btn btn-primary float-right text-white">Update and Save</button>

                        </div>
                           {/*-modal for reset Password*/}
                <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Reset Password</span></h4>
                                    <hr />
                                    <div className="form-group">
                                        <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                                        <input type="password" className="form-control-sm" id="newpass" />
                                    </div>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" id="e" onClick={() => this.changePass()} className="btn btn-warning">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for reset Password */}

                    </div>
                ))}
            </div>

        );


    }
}
export default footerComponent