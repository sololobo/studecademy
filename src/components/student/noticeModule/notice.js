import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Viewnotice from './view'
import Listnotice from './list'

class Noticeboard extends Component {
    constructor(props) {
        super(props);
        this.viewnotice = this.viewnotice.bind(this);
        this.listnotice = this.listnotice.bind(this);
    }
    viewnotice() {

        ReactDOM.render(<Viewnotice />, document.getElementById('contain2'))

    }
    listnotice() {

        ReactDOM.render(<Listnotice />, document.getElementById('contain2'))

    }
    render() {

        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-header">
                        <h2 className="card-text text-uppercase"><b> Noticeboard </b></h2>
                    </div>
                    <div className="card-body">
                        <div className="email-app mb-4">
                            <nav>
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a className="nav-link border" ><i className="icon-envelope-letter" />Notices<span className="badge badge-danger badge-pill">4</span></a>
                                    </li>
                                </ul>
                            </nav>
                            <div id="contain2">
                                <Listnotice></Listnotice>
                            </div>
                       </div>
                    </div>
                </div>
            </div>

        );


    }
}
export default Noticeboard