import React, { Component } from 'react';
import Side from '../sidebarModule/side'
import Profile from '../profileModule/studentProfile'
class studentDashboard extends Component {

    render() {

        return (
            <div className="app-body" >
                <Side></Side>
                <main class="main">


                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>

                    </ol>

                    <div class="container-fluid" id="contain1">
                        <Profile></Profile>
                    </div>

                </main>

            </div>

        );


    }
}
export default studentDashboard