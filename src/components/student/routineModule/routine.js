import React, { Component } from 'react';


class view extends Component {

    render() {

        return (
            <div className="fadeIn animated">
  {/*-===Routine*/}
  <div className="card">
    <div className="card-header">
      <b>Class</b> 
      <select name id className="form-control-sm">
        <option value>class 4</option>
        <option value disabled>Class 3</option>
      </select>
    </div>
    <div className="card-body">
      <div className="table-responsive">
        <table className="table  table-bordered" cellSpacing={0}>
          <thead>
            <tr className="text-center text-muted"> 
              <th> Time </th>
              <th />
              <th />
              <th />
              <th />
              <th />
              <th />
              <th />
              <th />
              <th />
            </tr>
          </thead>
          <tbody className="text-center">
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Monday</th>
              <th>Physics</th>
              <th>Chemistry</th>
              <th>--</th>
              <th>Mathematics</th>
              <th className="bg-secondary"> Break </th>
              <th>Electronics</th>
              <th>Bengali</th>
              <th>--</th>
              <th>English</th>
            </tr>
            <tr>
              <th />
              <th>A. Sharma</th>
              <th>B. Sharma</th>
              <th>--</th>
              <th className="bg-secondary"> Break </th>
              <th>C.Sharma</th>
              <th>C.Sharma</th>
              <th>C.Sharma</th>
              <th>-</th>
            </tr>
            <tr>
              <th />
              <th>R203</th>
              <th>R203</th>
              <th>--</th>
              <th className="bg-secondary"> Break </th>
              <th>R203</th>
              <th>R203</th>
              <th>R203</th>
              <th>-</th>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {/*--=======/routine====*/}
</div>


        );


    }
}
export default view