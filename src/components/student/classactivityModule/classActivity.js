import React, { Component } from 'react';


class classActivity extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*-main content*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Class Activity</span>
                    </div>
                    <div className="card-body">
                        <div>
                            {/*filter*/}
                            <b>Select Subject:</b>
                            <select name id className="form-control-sm">
                                <option value>subject</option>
                                <option value>subject 2</option>
                            </select>
                            <b>Select date:</b>
                            <input type="date" name id className="form-control-sm" />
                            {/*/filter*/}
                        </div>
                        <table className="table mt-2 table-bordered">
                            <thead>
                                <tr className="bg-light">
                                    <th>Name of the Activity</th>
                                    <th>Subject</th>
                                    <th>Published by</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="#" data-target="#viewActivity" data-toggle="modal">Activity title</a></td>
                                    <td>Physics</td>
                                    <td>Teacher 1</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {/*/Main Content*/}
                {/*-modal for view activity*/}
                <div className="modal fade" id="viewActivity" tabIndex={-1} role="dialog" aria-labelledby="modelTitleviewActivity" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold">View Activity</span></h4>
                                    <hr />
                                    <fieldset>
                                        <label htmlFor className="mb-0 font-weight-bold">Published on:</label>
                                        <p>Date</p>
                                        <label htmlFor className="mb-0 font-weight-bold">Title of the Activity</label>
                                        <p>Title of activity</p>
                                        <label htmlFor className="font-weight-bold mb-0">Share Today's Class activity</label>
                                        <p>Today's Activity</p>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for view activity */}

            </div>

        );


    }
}
export default classActivity