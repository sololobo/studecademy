
import React, { Component } from 'react';
import Cookies from 'js-cookie';


class StartExam extends Component {

    render() {

        return (

            <div className="animated fadeIn">
                <p className="text-center h2 font-weight-bold uppercase text-dark"><i className="icon-clock small" /> Left: <span id="timer" className="text-danger" /></p>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-8 col-sm-12 col-xs-12">
                            <div className="card">
                                <div className="card-header">
                                    {/*-------Exam number and info--*/}
                                    <b>Question Number:</b> 10 / 30
            <div className="float-right">
                                        <a><b>Maximum Marks:</b>  1</a> |
              <a><b>Negative Marks:</b> 2</a>
                                    </div>
                                </div>
                                <div className="card-body">
                                    {/*--------question----*/}
                                    <h6 className="question-main">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h6>
                                    <hr />
                                    {/*----answer*/}
                                    <div className="question card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <span className="question-text-option">A) </span><input type="radio" className="question-btn form-check-input" defaultValue="A" /><span className="question-text"> Tiger</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="question card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <span className="question-text-option">B) </span><input type="radio" className="question-btn form-check-input" defaultValue="B" /><span className="question-text"> Lion</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="question card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <span className="question-text-option">C) </span><input type="radio" className="question-btn form-check-input" defaultValue="C" /><span className="question-text"> Select all above the question</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="question card-body">
                                        <div className="row">
                                            <div className="col-12">
                                                <span className="question-text-option">D) </span><input type="radio" className="question-btn form-check-input" defaultValue="d" /><span className="question-text"> Answer is not listed</span>
                                            </div>
                                        </div>
                                    </div>{/*----/answer---*/}
                                    <div className="float-right">
                                        <button className="btn btn-warning">Mark for Review</button>
                                        <button className="btn btn-danger">Skip &amp; Next</button>
                                        <button className="btn btn-primary">Save &amp; Next</button>
                                    </div>
                                </div>{/*-------/question body*/}
                            </div>{/*-------/card question*/}
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12">{/*-------exam sidebar---*/}
                            <div className="card">
                                <div className="card-header">
                                    <b>Select Topic</b>
                                    <div className="card-actions">
                                        <a href="#"><i className="icon-calculator" /></a>
                                        <a href="#"><i className="icon-question" /></a>
                                    </div>
                                </div>
                                <div className="card-body exam-widget">
                                    {/*---------Tab exam list------*/}
                                    <div id="accordion" role="tablist">
                                        {/*---------Exam topic 1--------*/}
                                        <div className="card exam-widget-inner">
                                            <div className="card-header" role="tab" id="headingOne">
                                                <h5 className="mb-0 exam-chapter">
                                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Chapter 1</a>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" className="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div className="card-body exam-widget">
                                                    <span className="attempt-view">1</span>
                                                    <span className="attempt-view">2</span>
                                                    <span className="attempt-view">3</span>
                                                    <span className="attempt-view">4</span>
                                                    <span className="attempt-view">5</span>
                                                    <span className="attempt-view">6</span>
                                                    <span className="attempt-view bg-danger">7</span>
                                                    <span className="attempt-view">8</span>
                                                    <span className="attempt-view">9</span>
                                                    <span className="attempt-view bg-success">10</span>
                                                    <span className="attempt-view bg-warning">11</span>
                                                    <span className="attempt-view">12</span>
                                                    <span className="attempt-view">13</span>
                                                    <span className="attempt-view">14</span>
                                                    <span className="attempt-view bg-warning">15</span>
                                                    <span className="attempt-view bg-success">16</span>
                                                    <span className="attempt-view bg-warning">17</span>
                                                    <span className="attempt-view">18</span>
                                                    <span className="attempt-view">19</span>
                                                    <span className="attempt-view">20</span>
                                                    <span className="attempt-view bg-success">21</span>
                                                    <span className="attempt-view bg-warning">22</span>
                                                    <span className="attempt-view">23</span>
                                                    <span className="attempt-view bg-success">24</span>
                                                    <span className="attempt-view bg-warning">25</span>
                                                    <span className="attempt-view">26</span>
                                                    <span className="attempt-view bg-success">27</span>
                                                    <span className="attempt-view bg-warning">28</span>
                                                    <span className="attempt-view">29</span>
                                                    <span className="attempt-view bg-success">30</span>
                                                </div>
                                            </div>
                                        </div>{/*-------/eaxm topic 1----*/}
                                        {/*---------Exam topic 2--------*/}
                                        <div className="card exam-widget-inner">
                                            <div className="card-header" role="tab" id="exam-topic">
                                                <h5 className="mb-0 exam-chapter">
                                                    <a data-toggle="collapse" href="#exam-topic-2" aria-expanded="true" aria-controls="exam-topic-2"> Chapter 1</a>
                                                </h5>
                                            </div>
                                            <div id="exam-topic-2" className="collapse" role="tabpanel" aria-labelledby="exam-topic-2" data-parent="#accordion">
                                                <div className="card-body exam-widget">
                                                    <span className="attempt-view">1</span> <span className="attempt-view">2</span>
                                                    <span className="attempt-view">3</span>
                                                    <span className="attempt-view">4</span>
                                                    <span className="attempt-view">5</span>
                                                    <span className="attempt-view">6</span>
                                                    <span className="attempt-view bg-danger">7</span>
                                                    <span className="attempt-view">8</span>
                                                    <span className="attempt-view">9</span>
                                                    <span className="attempt-view bg-success">10</span>
                                                    <span className="attempt-view bg-warning">11</span>
                                                    <span className="attempt-view">12</span>
                                                    <span className="attempt-view">13</span>
                                                    <span className="attempt-view">14</span>
                                                    <span className="attempt-view bg-warning">15</span>
                                                    <span className="attempt-view bg-success">16</span>
                                                    <span className="attempt-view bg-warning">17</span>
                                                    <span className="attempt-view">18</span>
                                                    <span className="attempt-view">19</span>
                                                    <span className="attempt-view">20</span>
                                                    <span className="attempt-view bg-success">21</span>
                                                    <span className="attempt-view bg-warning">22</span>
                                                    <span className="attempt-view">23</span>
                                                    <span className="attempt-view bg-success">24</span>
                                                    <span className="attempt-view bg-warning">25</span>
                                                    <span className="attempt-view">26</span>
                                                    <span className="attempt-view bg-success">27</span>
                                                    <span className="attempt-view bg-warning">28</span>
                                                    <span className="attempt-view">29</span>
                                                    <span className="attempt-view bg-success">30</span>
                                                </div>
                                            </div>
                                        </div>{/*-------/eaxm topic 2----*/}
                                    </div>
                                </div>
                                <div className="card-footer exam-widget-footer">
                                    <p><span className="circle bg-gray"> </span> <b>Your don't attempt question yet</b></p>
                                    <p><span className="circle bg-success"> </span> <b>You answer this question</b></p>
                                    <p><span className="circle bg-warning"> </span> <b>You mark this question for review</b></p>
                                    <p><span className="circle bg-danger"> </span> <b>You skip this answer</b></p>
                                </div>
                            </div>
                            <button className="btn btn-dark btn-finish" data-toggle="modal" data-target="#finishModalCenter">Finish Exam</button>
                        </div>
                    </div>
                    {/* Modal for password */}
                    <div className="modal fade" id="finishModalCenter" tabIndex={-1} role="dialog" aria-labelledby="finishModalCenterTitle" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="modal-body text-center">
                                    <a href="#" className="btn btn-primary">Go to Analytics</a>
                                    <a href="#" className="btn btn-light">View Solution</a>
                                </div>
                            </div>
                        </div>
                    </div>{/*-----/modal password--*/}
                </div>{/*-----/row-*/}
            </div>


        );


    }
}
export default StartExam