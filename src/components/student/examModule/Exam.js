
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Examresult from './Result'


class Exam extends Component {
    constructor(props) {
        super(props);
        this.examsresult = this.examsresult.bind(this);
    }
    examsresult() {
        ReactDOM.render(<Examresult />, document.getElementById('contain1'))
    }
    render() {

        return (
            <div className="fadeIn animated">
                <div className="row">
                    <div className="col-md-12 col-xs-12 col-sm-12">
                        <h3><b>Filter:</b></h3>
                        <div className="card">
                            <div className="row">
                                <div className="col-md-3 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold"> Select Exam Category: </label>
                                        <select className="form-control">
                                            <option> Academic</option>
                                            <option> Competetive</option>
                                            <option> Personal Gromming</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold"> Select Category:</label>
                                        <select className="form-control">
                                            <option> Mathematics</option>
                                            <option> Physics</option>
                                            <option> Chemistry </option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold"> Select Chapter:</label>
                                        <select className="form-control">
                                            <option> Chap 1</option>
                                            <option> Chap 2</option>
                                            <option> Chap 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-3 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <div className="card-text">
                                            <button className="btn-primary btn form-control mt-3"><i className="icon-check" />&nbsp; Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*--------/row*/}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12 col-xs-12 col-sm-12">
                        {/*-------/card-----*/}
                        <h2><b>List of Exams:</b></h2>
                        <div className="card">
                            {/*----- card for exam tabs*/}
                            <div className="card-body">
                                <div id="accordion" role="tablist">
                                    {/*----------------- test 1 start----------*/}
                                    <div className="card">
                                        <div className="card-header text-card-header" role="tab" id="test-1">
                                            <h5>
                                                <div className="row">
                                                    <div className="col-md-8 col-xs-12 col-sm-12">
                                                        <a data-toggle="collapse" href="#test1" aria-expanded="true" aria-controls="collapseOne">
                                                            Microprocessor 8086 chips and its data sjgjhs jghrgh
                        <span className="badge badge-danger badge-pill">NEW</span>
                                                        </a>
                                                    </div>
                                                    <div className="col-md-2 col-xs-12 col-sm-12">
                                                        <button className=" btn-success mt-1 btn form-control" data-toggle="modal" data-target="#examModalCenter"><i className="icon-paper-plane" /> Start Now</button>
                                                    </div>
                                                    <div className="col-md-2 col-xs-12 col-sm-12">
                                                        <button onClick={this.examsresult} className=" btn-secondary btn mt-1 form-control" ><i className="icon-layers" /> View Result</button>
                                                    </div>
                                                </div>
                                            </h5>
                                        </div>
                                        {/*--------/card-header--*/}
                                        <div id="test1" className="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h4><b>Course Objective:</b></h4>
                                                        <ul>
                                                            <li> Knowledge of web content</li>
                                                            <li> Improve of physics</li>
                                                            <li> Development of keyskill</li>
                                                        </ul>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <h4><b>Exam Details:</b></h4>
                                                        <ul>
                                                            <li><b> Last Date of Submission:</b> 20/09/2019</li>
                                                            <li><b> Time:</b> 40 mins</li>
                                                            <li><b> List of Question:</b> 20</li>
                                                            <li><b> Total Mark:</b> 50</li>
                                                            <li><b> Pass Mark:</b> 20</li>
                                                        </ul>
                                                    </div>
                                                    {/*--/col--*/}
                                                </div>
                                                {/*--------/row*/}
                                                <h5><b>Important Details of this Exam</b></h5>
                  This is the Practice Exam. You can Attempt multiple time for this exam. All your records will
                  be counted and monitored by your mentor. Try not to cheat. Your Practice will make you faster
                  for your skill.
                </div>
                                        </div>
                                    </div>
                                    {/*----------------- test 1 end----------*/}
                                    {/*----------------- test 2 start----------*/}
                                    <div className="card">
                                        <div className="card-header text-card-header" role="tab" id="test-2">
                                            <h5>
                                                <div className="row">
                                                    <div className="col-md-8 col-xs-12 col-sm-12">
                                                        <a data-toggle="collapse" href="#test2" aria-expanded="true" aria-controls="collapseOne">
                                                            Microprocessor 8086 chips and its data
                      </a>
                                                    </div>
                                                    <div className="col-md-2 col-xs-12 col-sm-12">
                                                        <button className=" btn-success mt-1 btn form-control" data-toggle="modal" data-target="#examModalCenter"><i className="icon-refresh" /> Retake </button>
                                                    </div>
                                                    <div className="col-md-2 col-xs-12 col-sm-12">
                                                        <button className=" btn-secondary btn mt-1 form-control"><i className="icon-layers" disabled="disabled"/> View Result</button>
                                                    </div>
                                                </div>
                                            </h5>
                                        </div>
                                        {/*--------/card-header--*/}
                                        <div id="test2" className="collapse " role="tabpanel" aria-labelledby="test2" data-parent="#accordion">
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h4><b>Course Objective:</b></h4>
                                                        <ul>
                                                            <li> Knowledge of web content</li>
                                                            <li> Improve of physics</li>
                                                            <li> Development of keyskill</li>
                                                        </ul>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <h4><b>Exam Details:</b></h4>
                                                        <ul>
                                                            <li><b> Last Date of Submission:</b> 20/09/2019</li>
                                                            <li><b> Time:</b> 40 mins</li>
                                                            <li><b> List of Question:</b> 20</li>
                                                            <li><b> Total Mark:</b> 50</li>
                                                            <li><b> Pass Mark:</b> 20</li>
                                                        </ul>
                                                    </div>
                                                    {/*--/col--*/}
                                                </div>
                                                {/*--------/row*/}
                                                <h5><b>Important Details of this Exam</b></h5>
                  This is the Practice Exam. You can Attempt multiple time for this exam. All your records will
                  be counted and monitored by your mentor. Try not to cheat. Your Practice will make you faster
                  for your skill.
                </div>
                                        </div>
                                    </div>
                                    {/*----------------- test 2 end----------*/}
                                </div>
                                {/*--pagination*/}
                                <nav aria-label="Page navigation">
                                    <ul className="pagination justify-content-center">
                                        <li className="page-item disabled">
                                            <a className="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">«</span>
                                                <span className="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        <li className="page-item"><a className="page-link" href="#">1</a></li>
                                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                                        <li className="page-item">
                                            <a className="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">»</span>
                                                <span className="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                                {/*-----------/pagination--*/}
                            </div>
                        </div>
                        {/*-----/card------*/}
                    </div>
                    {/*-------/col-----*/}
                </div>
                {/*--------/row-----*/}

                {/* Modal for exam info */}
                <div className="modal fade" id="examModalCenter" tabIndex={-1} role="dialog" aria-labelledby="examModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="examModalLongTitle">Read Instruction Carefully</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="modal-exam" style={{ overflow: 'auto' }}>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    <ul> You have to choose
            <li>option lorem dkfsdk kvavakv</li>
                                        <li>option lorem dkfsdk kvavakv</li>
                                        <li>option lorem dkfsdk kvavakv</li>
                                        <li>option lorem dkfsdk kvavakv</li>
                                    </ul>
                                    <i>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</i>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <a href="/startexam" > <button type="button" className="btn btn-primary">Start Now</button></a>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-----/modal exam info--*/}

            </div>



        );


    }
}
export default Exam