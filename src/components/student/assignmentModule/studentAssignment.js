import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assignment from './AssignmentView'
import Assignmentcheck from './assignmentCheck'
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
class studentAssignment extends Component {
    constructor(props) {
        super(props);
        this.viewAssignment = this.viewAssignment.bind(this);




        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            assignmentList: [],
            student_i_d: Cookies.get('studentid'),
            submission_u_r_l:"",
            searchString : "",
        }
        this.state.assignmentSearchList = []
    }
    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'studentID': Cookies.get('studentid') })

        };


        let url = global.API + "/studapi/public/api/viewassignstu";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    assignmentList: json
                })

            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });




    }

    viewAssignment(assignment) {

        ReactDOM.render(<Assignment assignment={assignment}/>, document.getElementById('contain1'))

    }
    checkAssignment(assignment) {
        let props = {
            assignmentview:assignment,
            fromAssignmentList:this.props.data
            }

        ReactDOM.render(<Assignmentcheck data={props} />, document.getElementById('contain1'))

    }

    setAssignmentID(obj){
        this.setState({ assignment_i_d: obj.assignment.assignmentID })




    }

    submitAssignment(){

        console.log(this.state);
        let url = global.API + "/studapi/public/api/assignmentsubmit";
        let data = this.state;
        const fd = new FormData();
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Answer uploaded Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    $('#uploadAnswer').modal('hide');
                    this.componentDidMount();
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Assignment already submitted",
                        icon: "warning",
                        button: "OK",
                    });
                    $('#uploadAnswer').modal('hide');
                }
            })
        })


    }
    render() {
        var { isLoaded, assignmentList, unitList, sessonList1, classList, semesterList, sectionList, subjectList } = this.state;
        if(this.state.searchString.replace(/\s/g, '') != ''){
           this.assignmentSearchList = SearchForObjectsWithName(assignmentList, this.state.searchString)
      }else{
          this.assignmentSearchList = assignmentList
      }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">

                        <span className="h3 font-weight-bold">Assignments/Homework</span>
                        <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                    </div>
                    <div className="card-body">

                        <hr />
                        <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0">Name</th>
                                        <th className="border-0">Subject's Name</th>
                                        <th className="border-0">Published on</th>
                                        <th className="border-0">Published by</th>
                                        <th className="border-0">Last Submission Date</th>
                                        <th className="border-0">Published Till</th>
                                        <th className="border-0">Marks Alloted</th>
                                        <th className="border-0">Your Marks</th>
                                        <th className="border-0">Status</th>
                                        <th className="border-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.assignmentSearchList.map(assignment => (
                                        <tr>
                                            <td className="border-0"><a href={global.img + assignment.assignmentURL} download=""  target="_blank">{assignment.assignmentName}</a></td>

                                            <td className="border-0">{assignment.subjectName}</td>
                                            <td className="border-0">{assignment.publishedOn}</td>
                                            <td className="border-0">{assignment.teacherName ? assignment.teacherName : assignment.OrganisationName}</td>
                                            <td className="border-0">{assignment.lastSubmissionDate}</td>
                                            <td className="border-0">{assignment.publishedTill}</td>
                                            <td className="border-0">{assignment.maximumMarks}</td>
                                            <td className="border-0">{assignment.marksObtained ? assignment.marksObtained : "---"}</td>
                                            <td className="border-0"><span className="badge-light badge badge-pill">{assignment.status}</span></td>
                                            <td className="border-0">
                                                <span className="h3"><a  onClick={() => { this.setState({ assignment_i_d: assignment.assignmentID }) }} data-toggle="modal" data-target="#uploadAnswer" data-placement="top" title="Upload Assignment/Homework"><i className="icon-cloud-upload font-weight-bold"></i> </a></span>
                                                <span className="h3"><a onClick={() => this.viewAssignment(assignment)} data-placement="top" title="View Uploaded Document"><i className="icon-eye font-weight-bold text-dark"></i> </a></span>
                                                <span className="h3"><a href={(assignment.submissionURL && assignment.solutionURL) ? global.img + assignment.solutionURL : "#"} download="" data-placement="top" title="Download Solution"><i className="icon-cloud-download font-weight-bold text-success"></i> </a></span>
                                                <span className="h3"><a onClick={()=>this.checkAssignment(assignment)} title="View Checked Copy"><i className="icon-doc font-weight-bold text-primary"></i> </a></span>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {/*-modal for upload  Result*/}
                <div className="modal fade" id="uploadAnswer" tabIndex={-1} role="dialog" aria-labelledby="modelTitleuploadAnswer" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Upload Answer for Your Homework/Assignment</span></h4>
                                    <p>
                                    </p><div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold">Upload only .pdf or .docx file</label>
                                        <input type="file" name="submission_u_r_l" onChange={(data) => { this.setState({ submission_u_r_l: data.target.files[0] }) }}className="form-control" />
                                    </div>
                                    <p />
                                    <hr />
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => this.submitAssignment()} className="btn btn-primary text-white">Upload Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for upload Result*/}
            </div>
        );


    }
}
export default studentAssignment
