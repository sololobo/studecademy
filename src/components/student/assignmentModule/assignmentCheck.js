import React, { Component } from 'react';

class StudentsAssignmentCheck extends Component {


    render() {

        return (

            <div className="fadeIn animated">
                <div className="row">
                    <div className="col-md-8 col-xl-8 col-sm-12 ">
                        <div className="card">
                            <div className="card-header bg-white">
                                <div className="row">
                                    <div className="col-6">
                                        <h5><b>Title:</b> {this.props.data.assignmentview.assignmentName}</h5>
                                    </div>
                                    <div className="col-6">
                                        <h5><b> Sub: </b> {this.props.data.assignmentview.subjectName}</h5>
                                    </div>
                                </div>
                            
                                <h5><b>Name of the Topic:</b>   {this.props.data.assignmentview.chapterNames}</h5>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">
                                <div className="row">
                                    <div className="col-md-12">
                                   </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <embed
                                src={`https://docs.google.com/gview?url=${global.img + this.props.data.assignmentview.submissionURL}&embedded=true`}
                                    //src={global.img + this.props.data.assignmentview.submissionURL}
                                    type="application/pdf" type="" width="100%" height="480px" />
                                   
                            </div>
                            <div className="card-footer">
                            <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">

                                    <label for="" className="mb-0 font-weight-bold">Maximum Marks</label>
                                    <input type="number" defaultValue={this.props.data.assignmentview.maximumMarks} disabled name="addr1"  className="form-control" />

                                   
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Marks Obtained:</label>

                                    <input type="number" id="marks" disabled defaultValue={this.props.data.assignmentview.marksObtained}  className="form-control" />

                             
                                </div>
                            </div>
                        </div>
                       
                              <div className="form-group">
                                    <label for="" className="font-weight-bold"> Review </label>
                                    <textarea disabled className="form-control" name="" id="" rows="3"></textarea>
                                    <label for="" className="font-weight-bold"> Review - Checked by : {this.props.data.assignmentview.teacherName ? this.props.data.assignmentview.teacherName : this.props.data.assignmentview.OrganisationName}</label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-xl-4 col-sm-12">


                        <div className="card">
                            <div className="card-header">
                                Question Paper <a href="#" className="float-right btn btn-light" data-toggle="tooltip"
                                    title="Zoom to Show Question Paper"><i className="fa fa-search-plus"></i></a>
                            </div>
                            <div className="card-body">
                                <div className="q-view">
                                    <embed
                                    src={`https://docs.google.com/gview?url=${global.img + this.props.data.assignmentview.assignmentURL}&embedded=true`}
                                      //  src={global.img + this.props.data.fromAssignmentList.assignmentURL}
                                        type="application/pdf" type="" width="100%" height="480px" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

              

            </div>
        );


    }
}
export default StudentsAssignmentCheck