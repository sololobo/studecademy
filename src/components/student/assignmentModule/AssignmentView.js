import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './AssignmentView.css';
import Cookies from 'js-cookie';
import Assignment from './studentAssignment'

class AssignmentView extends Component {
    constructor(props) {
        super(props);
        
        this.listAssignment = this.listAssignment.bind(this);

        this.state = {
            unitview: [],
            assignment: [],
            isLoaded: false
        }

    }
    componentDidMount() {




        let url = global.API + "/studapi/public/api/viewanunit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unitID': this.props.unid })
        })
            .then(res => res.json())
            .then(json => {
                console.log(json)
                this.setState({
                    isLoaded: true,
                    assignment: json,

                })
            });



    }
    listAssignment() {

        ReactDOM.render(<Assignment />, document.getElementById('contain1'))

    }


    render() {
        var { isLoaded, assignment } = this.state;
        
        
        return (
            <div className="fadeIn animated">
                
                    <div className="card">

                        <div className="card-header">
                            <span className="font-weight-bold text-primary mb-0 h3">{this.props.assignment.assignmentName}</span>
                            <a onClick={this.listAssignment} className="float-right mt-2"><i className="icon-action-undo"></i> Back to Assignment List</a>
                        </div>
                        <div className="card-body pdf-view">
                            <embed
                              src={`https://docs.google.com/gview?url=${global.img + this.props.assignment.submissionURL}&embedded=true`}
                                    
                              type="application/pdf" type="" width="100%" height="600px" />
                        </div>

                    </div>
             
            </div>
        );


    }
}
export default AssignmentView