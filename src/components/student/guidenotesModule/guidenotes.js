import React, { Component } from 'react';


class guidenotes extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*-Filter*/}
                <div className="row no-gutters">
                    <div className="col-md-12 col-xs-12 col-sm-12">
                        {/*Tutorial List Display*/}
                        <div className="card">
                            <div className="card-header">
                                <span className="card-text h3 font-weight-bold">List of Uploaded Guide Notes </span>
                            </div>
                            <div className="card-body">
                                {/*-filter*/}
                                <div>
                                    <b>Select Subject:</b>
                                    <select className="form-control-sm">
                                        <option> Physics</option>
                                        <option>Chemistry</option>
                                        <option> Biology</option>
                                    </select> &nbsp;
            <b>Select Chapter:</b>
                                    <select className="form-control-sm">
                                        <option> Chapter-1</option>
                                    </select> &nbsp;
          </div>
                                {/*/filter*/}
                                <div>
                                    <table className=" mt-3 table-bordered table">
                                        <thead>
                                            <tr className="bg-light">
                                                <th className="border-0">Name of the Title</th>
                                                <th className="border-0">Published by</th>
                                                <th className="border-0">Published on</th>
                                                <th className="border-0">Published Till</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td className="border-0"><a href="view.html">Title of the Guide Notes</a></td>
                                                <td className="border-0">Author Name</td>
                                                <td className="border-0">23-02-2019</td>
                                                <td className="border-0">28-02-2019</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {/*/Tutorial List Display*/}
                        </div>
                    </div>
                </div>
                {/*Filter*/}
            </div>

        );


    }
}
export default guidenotes