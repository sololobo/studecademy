import React, { Component } from 'react';
import './studentBokksView.css';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import Bookview from './studentBooksList'


class studentsBooksView extends Component {
    constructor(props) {
        super(props);

        this.listBooks = this.listBooks.bind(this);

        this.state = {

            booksview: [],
            isLoaded: false
        }
    }
    componentDidMount() {
        let url = global.API + "/studapi/public/api/viewanunit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unitID': this.props.unid })
        })
            .then(res => res.json())
            .then(json => {
                console.log(json)
                this.setState({
                    isLoaded: true,
                    booksview: json,

                })
            });



    }
    listBooks() {

        ReactDOM.render(<Bookview />, document.getElementById('contain1'))

    }
    render() {

        return (
            <div className="fadeIn animated">

                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">{this.props.bookdata.bookName}</span>
                        <a onClick={this.listBooks} className="float-right mt-2"><i className="icon-action-undo"></i> Back to Books List</a>
                    </div>
                    <div className="card-body pdf-view">
                        <embed 
                        src={`https://docs.google.com/gview?url=${global.img + this.props.bookdata.bookURL}&embedded=true`}
                        type="application/pdf" type="" width="100%" height="600px" />
                    </div>

                </div>

            </div>
        );


    }
}
export default studentsBooksView