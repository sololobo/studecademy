import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import BooksView from './studentBooksView';
import Cookies from 'js-cookie';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';

class studentBooksList extends Component {
    constructor(props) {
        super(props);


        this.state = {
            bookList: [],
            searchString : "",
        }
        this.bookSearchList = []
    }

    showBookview(book) {

        ReactDOM.render(<BooksView bookdata={book} />, document.getElementById('contain1'))

    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'Student_i_d': Cookies.get('studentid') })

        };


        let url = global.API + "/studapi/public/api/viewallsembooks";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            });






    }


    render() {
        var { bookList } = this.state
        if(this.state.searchString.replace(/\s/g, '') != ''){
           this.bookSearchList = SearchForObjectsWithName(bookList, this.state.searchString)
      }else{
          this.bookSearchList = bookList
      }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">
                            Books
                </span>
                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                    </div>
                    <div className="card-body">

                        <hr />
                        <div className="row">
                            {this.bookSearchList.map(book => (
                                <div className="col-md-3">
                                    <div className="card">
                                        <div className="card-body book-body">
                                            <img src={global.img + book.bookCoverPhoto} alt="book" className="book-preview" />
                                            <div className="h5 font-weight-bold py-2 px-3">{book.bookName}

                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <a onClick={() => this.showBookview(book)} className="float-right btn btn-primary text-white">Read Book</a>
                                            <a className="float-left badge badge-pill bg-light mt-1">- By {book.bookPublication}</a>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>


                </div>

            </div>


        );


    }
}
export default studentBooksList
