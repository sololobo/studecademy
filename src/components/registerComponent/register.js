import React, { Component } from 'react';
import logo from '../includes/media/tm-logo.png';
class Register extends Component {
    render() {
        return (
            <div class="container-fluid ">
                <div class="row h-100 align-items-center">
                    <div class=" col-md-3"></div>
                    <div class="col-md-6 px-5 py-5 bg-white registration">
                        <img src={logo} alt="" class="registration-logo" />
                        <h2 class="font-weight-bold">REGISTER</h2>
                        <hr />
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Name*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Contact Number*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Email ID*:</label>
                            <input type="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Date of Birth*:</label>
                            <input type="date" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Address*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Address 2:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="mb-0 font-weight-bold">City*:</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="mb-0 font-weight-bold">State*:</label>
                                    <select name="state" class=" states form-control" id="state">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="mb-0 font-weight-bold">Pin code*:</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" class="mb-0 font-weight-bold">Country*:</label>
                                    <select name="country" class=" states form-control" id="country">
                                        <option value="">Select Country</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Current Institute:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Guardian's Name*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Guardian's Email*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="" class="mb-0 font-weight-bold">Guardian's Contact Number*:</label>
                            <input type="text" class="form-control" />
                        </div>
                        <input type="checkbox" /> I Agree to the <a href="#"> Terms &amp; Conditions</a> <br />
                        <button class="btn btn-primary text-white form-control"> Create Account</button>
                        <a href="/" class="text-center mt-3 float-right"><i class="icon-action-undo"></i> Back to home page</a>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                
            </div>

            

        );
    }
}
export default Register