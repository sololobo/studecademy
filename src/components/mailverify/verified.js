import React, { Component } from 'react';
import logo from '../includes/media/tm-logo.png';
import swal from 'sweetalert';
import Cookies from 'js-cookie';


import   '../includes/global1';
class verified extends Component{

    render(){

        return (
            
            <div className="container-fluid">
        <div className="row h-100 align-items-center">
            <div className="col-md-4"></div>
            <div className="col-md-4 px-5 py-5 bg-white login">
                <img src={logo} alt="" className="login-logo" />
                <div className="mt-4">
                    <div className="alert alert-success" role="alert" >
                        <i className="fa fa-check-circle"></i>&nbsp; Email has been verified.<a href="/login"> Click here </a> to continue
                    </div>
                  
                </div>
               
                <hr />
                
            </div>
             <div className="col-md-4"></div>
         </div>
    

   
   </div>


        );


    }
}
export default verified