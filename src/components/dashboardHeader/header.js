import React, { Component } from 'react';

import Cookies from 'js-cookie';
class Header extends Component {
    constructor(props) {
        super(props);
        this.showLogout = this.showLogout.bind(this);

        this.state = {
            data: [],
            name: "",
            isLoaded: false,
            profilePic: ""
        }
    }
    showLogout() {

        //ReactDOM.render(<Logout />, document.getElementById('contain1'))

        localStorage.clear();
        Cookies.remove('username', { path: '/' });
        Cookies.remove('usertype', { path: '/' });
        Cookies.remove('orgid', { path: '/' });
        window.location.href = '/';


    }

    componentDidMount() {
        if ((Cookies.get('username')) == undefined) {
            window.location = "/login";
        }
        else {
           
         let url =  "http://3.7.45.100" + "/studapi/public/api/getname";
        
       console.warn(Cookies.get('username'));
        
        fetch(url,{
            method:'POST',
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            
            body:JSON.stringify({'emailid':Cookies.get('username')})
        }).then((result)=>{
            result.json()
            .then((resp)=>{
                
                
                //console.warn("resp",resp[0]['OrganisationName'])

                var d = new Date();
                d.setTime(d.getTime() + (1*24*60*60*1000));
                var expires = "expires="+ d.toUTCString();


                document.cookie = "orgid" + "=" + resp[0]['OrganisationID'] + ";" + expires + ";path=/";
                document.cookie = "orgname" + "=" + resp[0]['OrganisationName'] + ";" + expires + ";path=/";
                
                if(Cookies.get('usertype')==11){
                
               
                
                
                this.setState({
                    name:resp[0]['OrganisationName'],
                    profilePic:resp[0]['Logo']
                })
                
                 

                }

                if(Cookies.get('usertype')==12){
                    
                    document.cookie = "teacherid" + "=" + resp[0]['teacherID'] + ";" + expires + ";path=/";
                    

                    this.setState({
                        name:resp[0]['teacherName'],
                        profilePic:resp[0]['profilePic']
                    })
                    
                  }

                  if(Cookies.get('usertype')==13){
                   
                    document.cookie = "studentid" + "=" + resp[0]['studentID'] + ";" + expires + ";path=/";
                    
                    this.setState({
                        name:resp[0]['studentName'],
                        profilePic:resp[0]['profilePic']
                    })
                   
                    
                  }
                  
                  
                }
            )
        });

    
        
    } }
    
    render(){
        var { isLoaded, name, profilePic } = this.state;





        return (
            <header className="app-header navbar">
            <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">
                <span className="navbar-toggler-icon"></span>
            </button>
            <a className="navbar-brand" href="#"></a>
            <button className="navbar-toggler sidebar-toggler d-md-down-none" type="button">
                <span className="navbar-toggler-icon"></span>
            </button>
           
            <ul className="nav navbar-nav ml-auto">

            <li className="nav-item dropdown">
                    <a className="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                        aria-expanded="false">
                        <img  src={global.img+profilePic} className="img-avatar bg-primary" alt="" />
                     {/* <img  src={logo} className="img-avatar bg-primary" alt="" />*/}
                    </a>
                    <div className="dropdown-menu dropdown-menu-right"> 
                        
                        <a className="dropdown-item" onClick={this.showLogout}><i className="fa fa-lock"></i> Logout</a>
                    </div>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                        aria-expanded="false">
                        <h5>{name}</h5>
                    </a>
                    <div className="dropdown-menu dropdown-menu-right"> 
                        
                        <a className="dropdown-item" onClick={this.showLogout}><i className="fa fa-lock"></i> Logout</a>
                    </div>
                </li>
                <span className="mr-2"></span>
              
    
            </ul>
    
        </header>
    
            );
            
}
}
export default Header;