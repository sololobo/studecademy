import $ from 'jquery'

export const SearchForObjectsWithParams = (arrayOfVariable, sortParams) => {
   let ansArray = [];
   // console.log('The variables are', sortParams)
   arrayOfVariable.map((object) => {
      let toAdd = true;
      sortParams.forEach(function(param, i){
         // console.log(object)
            if(object[param[0]].toString() != param[1]){
               toAdd = false
            }

      })
      if(toAdd){
         ansArray.push(object)
      }
   })
   return(ansArray)
}
