import React, { Component } from 'react';
import './AdminBooksView.css';
import Cookies from 'js-cookie';
import Bookview from '../booksList/adminBooksList';
import ReactDOM from 'react-dom';

class AdminBooksView extends Component {
    constructor(props) {
        super(props);
        this.listBooks = this.listBooks.bind(this);


        this.state = {
            booksview: [],
            isLoaded: false
        }

    }
    componentDidMount() {




        let url = global.API + "/studapi/public/api/viewanunit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'bookID': this.props.bookid })
        })
            .then(res => res.json())
            .then(json => {
                console.log(json)
                this.setState({
                    isLoaded: true,
                    booksview: json,

                })
            });



    }

    listBooks() {

        ReactDOM.render(<Bookview />, document.getElementById('contain1'))

    }
    render() {
        var { isLoaded, booksview } = this.state;
        return (
            <div className="fadeIn animated">

                <div className="card">

                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">{this.props.bookdata.bookName}</span>
                        <a onClick={this.listBooks} className="float-right mt-2"><i className="icon-action-undo"></i> Back to Books List</a>
                    </div>
                    <div className="card-body pdf-view">
                        <embed src={global.img + this.props.bookdata.bookURL} type="application/pdf" width="100%" height="600px" />
                    </div>

                </div>

            </div>
        );


    }
}
export default AdminBooksView