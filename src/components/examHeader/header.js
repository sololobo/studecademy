import React, { Component } from 'react';

import Cookies from 'js-cookie';
class Header extends Component {
    constructor(props) {
        super(props);
        this.showLogout = this.showLogout.bind(this);

        this.state = {
            data: [],
            name: "",
            isLoaded: false,
            profilePic: ""
        }
    }
    showLogout() {

        //ReactDOM.render(<Logout />, document.getElementById('contain1'))

        localStorage.clear();
        Cookies.remove('username', { path: '/' });
        Cookies.remove('usertype', { path: '/' });
        Cookies.remove('orgid', { path: '/' });
        window.location.href = '/';


    }

    componentDidMount() {
        if ((Cookies.get('username')) == undefined) {
            window.location = "/login";
        }
        else {

            let url = "http://3.7.45.100" + "/studapi/public/api/getname";

            console.warn(Cookies.get('username'));

            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },

                body: JSON.stringify({ 'emailid': Cookies.get('username') })
            }).then((result) => {
                result.json()
                    .then((resp) => {


                        console.warn("resp", resp[0]['OrganisationName'])

                        var d = new Date();
                        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
                        var expires = "expires=" + d.toUTCString();


                        document.cookie = "orgid" + "=" + resp[0]['OrganisationID'] + ";" + expires + ";path=/";
                        document.cookie = "orgname" + "=" + resp[0]['OrganisationName'] + ";" + expires + ";path=/";

                        if (Cookies.get('usertype') == 11) {




                            this.setState({
                                name: resp[0]['OrganisationName'],
                                profilePic: resp[0]['Logo']
                            })



                        }

                        if (Cookies.get('usertype') == 12) {

                            document.cookie = "teacherid" + "=" + resp[0]['teacherID'] + ";" + expires + ";path=/";


                            this.setState({
                                name: resp[0]['teacherName']
                            })

                        }

                        if (Cookies.get('usertype') == 13) {

                            document.cookie = "studentid" + "=" + resp[0]['studentID'] + ";" + expires + ";path=/";

                            this.setState({
                                name: resp[0]['studentName']
                            })


                        }


                    }
                    )
            });



        }
    }

    render() {
        var { isLoaded, name, profilePic } = this.state;
        return (
               <div>
                    <div className="pace  pace-inactive">
                        <div className="pace-progress" data-progress-text="100%" data-progress={99} style={{ transform: 'translate3d(100%, 0px, 0px)' }}>
                            <div className="pace-progress-inner" />
                        </div>
                        <div className="pace-activity" />
                    </div>
                    <div className="navbar">
                        <h2 className="navbar-topic"> TCS NQT Test </h2>{/*------Exam Test Name---*/}
                        <div className="float-right">
                            <img src="assets/img/uploaded/user1.jpg" className="nav-avatar float-right" />
                            <p className="float-right nav-avatar-name">Somnath Sahu</p>
                        </div>
                    </div>
                </div>

        );

    }
}
export default Header;