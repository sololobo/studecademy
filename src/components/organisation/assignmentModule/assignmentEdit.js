import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';
import AssignmentList from './adminAssignmentList'

class assignmentEdit extends Component {
   constructor(props){
      super(props)

      //****************              bindings               ******************//
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.showSection = this.showSection.bind(this);
         this.showSubject = this.showSubject.bind(this);
         this.handleSemesterChange = this.handleSemesterChange.bind(this);
         this.onEditAssignmentFormSubmit = this.onEditAssignmentFormSubmit.bind(this)
      //**********************************************************************//

      //*********** dropdown default values ********************//
      this.showSession(this.props.data.unitID)
      this.showDepartment(this.props.data.sessionID)
      this.showSemester(this.props.data.departmentID)
      this.showSection(this.props.data.semesterID)
      this.showSubject(this.props.data.semesterID)
      //*****************************************************//

      //*******************      state variables       ***********************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            semesterID : this.props.data.semesterID,
            sectionID : this.props.data.sectionID,
            subjectID : this.props.data.subjectID,
            publishedOn : this.props.data.publishedOn,
            publishedTill : this.props.data.publishedTill,
            lastSubmissionDate : this.props.data.lastSubmissionDate,
            assignmentName : this.props.data.assignmentName,
            chapterNames : this.props.data.chapterNames,
            maximumMarks : this.props.data.maximumMarks,
            status : this.props.data.status,
            assignmentID : this.props.data.assignmentID,
            organisation_i_d : this.props.organisation,

            unitList : this.props.unitList,
            sessionList : [],
            classList : [],
            semesterList : [],
            sectionList : [],
            subjectList : []
         }
      //****************************************************************//



   }

//***********     handling change in props    **********************************//
   static getDerivedStateFromProps(nextProps, prevState){
      if(nextProps.data!==prevState.data){
        return { someState: nextProps.data};
     }
     else return null;
  }

   componentDidUpdate(prevProps, prevState) {
     if(prevProps.data!==this.props.data){

        //*********** dropdown default values ********************//
         this.showSession(this.state.unitID)
         this.showDepartment(this.state.sessionID)
         this.showSemester(this.state.departmentID)
         this.showSection(this.state.semesterID)
         this.showSubject(this.state.semesterID)
        //*****************************************************//

       this.setState({
         unitID : this.props.data.unitID,
         sessionID : this.props.data.sessionID,
         departmentID : this.props.data.departmentID,
         semesterID : this.props.data.semesterID,
         sectionID : this.props.data.sectionID,
         subjectID : this.props.data.subjectID,
         publishedOn : this.props.data.publishedOn,
         publishedTill : this.props.data.publishedTill,
         lastSubmissionDate : this.props.data.lastSubmissionDate,
         assignmentName : this.props.data.assignmentName,
         chapterNames : this.props.data.chapterNames,
         maximumMarks : this.props.data.maximumMarks,
         status : this.props.data.status,
         assignmentID : this.props.data.assignmentID,
         organisation_i_d : this.props.organisation,

         unitList : this.props.unitList,
         sessionList : [],
         classList : [],
         semesterList : [],
         sectionList : [],
         subjectList : []
       })
     }
   }
//*****************************************************************************//


//************************ Dropdown async Calls ***************************//

   showSession(val) {
      this.setState({
         sessionList : [],
         classList : [],
         semesterList : [],
         sectionList : [],
         subjectList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });
   }

   showDepartment(val) {
      this.setState({
         classList : [],
         semesterList : [],
         sectionList : [],
         subjectList : [],

         sessionID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'session_i_d': val })
      };
      let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
      fetch(classURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   classList: json
               })
           });
   }

   showSemester(val) {
      this.setState({
         semesterList : [],
         sectionList : [],
         subjectList : [],
         departmentID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'department_i_d': val })

      };
      let semesterURL = global.API + "/studapi/public/api/getallsemidname";
      fetch(semesterURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   semesterList: json
               })
           });
   }

   handleSemesterChange(val){
      this.setState({
         sectionList : [],
         subjectList : []
      })
      this.showSection(val)
      this.showSubject(val)
      this.setState({semesterID : val})
   }

   showSection(val) {
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'semester_i_d': val })

      };
      let semesterURL = global.API + "/studapi/public/api/getallsectionidname";
      fetch(semesterURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sectionList: json
               })
           });
   }

   showSubject(val) {
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'semester_i_d': val })
      };
      let subUrl = global.API + "/studapi/public/api/getallsubjectidname";
      fetch(subUrl, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   subjectList: json
               })
           });
   }

   //************************************************************************************//




   //****************    form submission       *****************************//
      onEditAssignmentFormSubmit() {
         let url = global.API + "/studapi/public/api/updateassignment";
         let assUrl = this.props.data.assignmentURL
         if(document.getElementById("editAssignmentFile").files[0] != null)
            assUrl = document.getElementById("editAssignmentFile").files[0]
         let solUrl = this.props.data.solutionURL
         if(document.getElementById("editAssignmentsolFile").files[0] != null)
            solUrl = document.getElementById("editAssignmentsolFile").files[0]
         let data = {
            organisation_i_d : this.state.organisation_i_d,
            assignment_i_d : this.state.assignmentID,
            section_i_d : this.state.sectionID,
            subject_i_d : this.state.subjectID,
            teacher_i_d : null,
            chapter_names : this.state.chapterNames,
            assignment_name : this.state.assignmentName,
            assignment_u_r_l : assUrl,
            solution_url : solUrl,
            maximum_marks : this.state.maximumMarks,
            published_on : this.state.publishedOn,
            published_till : this.state.publishedTill,
            last_submission_date : this.state.lastSubmissionDate,
            status : this.state.status,
         }
         const fd = new FormData();
         $.each(data, function (key, value) {
              fd.append(key, value);
         })
         fetch(url, {
              method: 'POST',
              body: fd
         }).then((result) => {
              result.json().then((resp) => {
                  if (resp == 1) {
                      swal({
                          title: "Done!",
                          text: "Assignment Updated Sucessfully! Refresh to see the results!",
                          icon: "success",
                          button: "OK",
                      });
                      $('#editAssignment').modal('hide');

                  }
                  else {
                      swal({
                          title: "Error!",
                          text: "Assignment not  Updated",
                          icon: "warning",
                          button: "OK",
                      });
                  }
              })
         })
      }
   //****************************************************************************//


render() {

        return (

                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid ">
                                    <h4><span className="font-weight-bold text-center">Edit Homework/Assignment</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit :</label>
                                                <select id="editAssignmentUnitName" value = {this.state.unitID} onChange = {(e)=>{this.showSession(e.target.value)}} className="form-control" >
                                                    <option value="" selected>Select<span className="text-danger"><strong>*</strong></span> </option>
                                                       {this.state.unitList.map(unit => (
                                                          <option value={unit.unitID}>{unit.unitName}</option>
                                                       ))}
                                                </select>


                                            </div>
                                            <div className="form-group">
                                               <label for="" className="mb-0 font-weight-bold">Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                                               <select id="editAssignmentSemester" value = {this.state.semesterID} onChange = {(e)=>{this.handleSemesterChange(e.target.value)}} className="form-control" >
                                                   <option value="" selected>Select</option>
                                                      {this.state.semesterList.map(semester => (
                                                         <option value={semester.semesterID}>{semester.semesterName}</option>
                                                      ))}
                                               </select>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                                <select id="editAssignmentSession" value = {this.state.sessionID} onChange = {(e)=>{this.showDepartment(e.target.value)}} className="form-control" >
                                                    <option value="" selected>Select</option>
                                                       {this.state.sessionList.map(session => (
                                                          <option value={session.sessionID}>{session.session}</option>
                                                       ))}
                                                </select>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Section  <span className="text-danger"><strong>*</strong></span> :</label>
                                                <select name="editAssignmentSection" value = {this.state.sectionID} onChange = {(e)=>{this.setState({sectionID : e.target.value})}}  className="form-control">
                                                    <option value="" selected>Select</option>
                                                       {this.state.sectionList.map(section => (
                                                          <option value={section.sectionID}>{section.sectionName}</option>
                                                       ))}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">ClassName/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                                <select id="editAssignmentClassName" value = {this.state.departmentID} onChange = {(e)=>{this.showSemester(e.target.value)}} className="form-control">
                                                   <option value="" selected>Select</option>
                                                      {this.state.classList.map(classes => (
                                                          <option value={classes.departmentID}>{classes.departmentName}</option>
                                                      ))}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject  <span className="text-danger"><strong>*</strong></span> </label>
                                                <select id="editAssignmentSubject" value = {this.state.subjectID} onChange = {(e)=>{this.setState({subjectID : e.target.value})}} className="form-control">
                                                    <option value="" selected>Select</option>
                                                       {this.state.subjectList.map(subject => (
                                                          <option value={subject.subjectID}>{subject.subjectName}</option>
                                                      ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label className="font-weight-bold"> Chapter :</label>

                                                <input type="text" id="editAssignmentChapterName" value = {this.state.chapterNames} onChange = {(e)=>{this.setState({chapterNames : e.target.value})}} className="form-control" />

                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Edit Published on  <span className="text-danger"><strong>*</strong></span> </label>
                                            <input type="datetime-local" id="editAssignmentPublishedDate" value = {this.state.publishedOn} onChange = {(e)=>{this.setState({publishedOn : e.target.value})}} className="form-control" />
                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Edit Published Till  :</label>
                                            <input type="datetime-local" id="editAssignmentPublishedTill" value = {Date(this.state.publishedTill)} onChange = {(e)=>{this.setState({publishedTill : e.target.value})}} className="form-control" />
                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Edit Last Submission Date  <span className="text-danger"><strong>*</strong></span> </label>
                                            <input type="datetime-local" id="editAssignmentActiveTill"  value = {this.state.lastSubmissionDate}onChange = {(e)=>{this.setState({lastSubmissionDate : e.target.value})}} className="form-control" />
                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Maximum Marks  </label>
                                            <input type="number" id="editAssignmentMaximumMarks" value = {this.state.maximumMarks} onChange = {(e)=>{this.setState({maximumMarks : e.target.value})}} className="form-control" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-3">

                                            <input type="text" disabled value = {this.state.publishedOn} className="form-control" />
                                        </div>
                                        <div className="col-md-3">

                                            <input type="text" disabled  value = {this.state.publishedTill}  className="form-control" />
                                        </div>
                                        <div className="col-md-3">

                                            <input type="text" disabled value = {this.state.lastSubmissionDate} className="form-control" />
                                        </div>
                                    </div>
                                    <div className="form-group row mt-3">
                                        <div className="col-md-12">
                                            <label htmlFor className="mb-0 form-group  font-weight-bold"> Name of the  Assignment  <span className="text-danger"><strong>*</strong></span> </label> &nbsp;
                                                <input type="text" id="editAssignmentName" value = {this.state.assignmentName} onChange = {(e)=>{this.setState({assignmentName : e.target.value})}} className="form-control " />

                                        </div>
                                        </div>
                                        <div className="form-group row mt-3">
                                           <div className="col-md-6">
                                               <label htmlFor className="mb-0 form-group font-weight-bold">Upload Homework (Only .docx, .pdf allowed) <span className="text-danger"><strong>*</strong></span>:</label>
                                               <input type="file" id="editAssignmentFile" name="assignment_u_r_l"  className="form-control " />

                                           </div>
                                           <div className="col-md-6">
                                               <label htmlFor className="mb-0 form-group font-weight-bold">Upload Solution (Only .docx, .pdf allowed) :</label>
                                               <input type="file" id="editAssignmentsolFile" name="solution_url"  className="form-control " />

                                           </div>
                                        </div>

                                    <hr />
                                    <div className="float-right ">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button" onClick = {this.onEditAssignmentFormSubmit} className="btn btn-primary text-white">Upload Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>


        );


    }
}
export default assignmentEdit
