import React, { Component } from 'react';
import swal from 'sweetalert';

class adminAssignmentCheck extends Component {


    updateAssignment(){

        
        let url = global.API + "/studapi/public/api/updateassignsubmission";
        

        let marks = document.getElementById('marks').value;
        
        const fd = new FormData();
        fd.append('submission_i_d', this.props.data.assignmentview.submissionID);
        fd.append('marks_obtained', marks);
        console.log(fd);
        
       
        
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Assignment Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Assignment not updated Sucessfully",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })


    }

    render() {

        return (

            <div className="fadeIn animated">
                <div className="row">
                    <div className="col-md-8 col-xl-8 col-sm-12 ">
                        <div className="card">
                            <div className="card-header bg-white">
                                <div className="row">
                                    <div className="col-6">
                                        <h5><b>Name:</b> {this.props.data.assignmentview.studentName}</h5>
                                    </div>
                                    <div className="col-6">
                                        <h5><b>Roll No.: </b> {this.props.data.assignmentview.rollNo}</h5>
                                    </div>
                                </div>
                                <hr />
                                <div className="row">
                                    <div className="col-6">
                                        <h5><b>className/Department:</b>  {this.props.data.fromAssignmentList.departmentName}</h5>
                                    </div>
                                    <div className="col-6">
                                        <h5><b>Subject: </b> {this.props.data.fromAssignmentList.subjectName}</h5>
                                    </div>
                                </div>
                                <hr />
                                <h5><b>Name of the Topic:</b>   {this.props.data.fromAssignmentList.chapterNames}</h5>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="btn-group float-right">
                                            <button className="btn-group btn btn-light" data-toggle="tooltip"
                                                title="For right Answer"><i className="fa fa-check text-success"></i></button>
                                            <button className="btn-group btn btn-light" data-toggle="tooltip"
                                                title="For wrong Answer"><span className="text-danger">X</span></button>
                                            <button className="btn-group btn btn-light" data-toggle="tooltip"
                                                title="Draw a point to indicate error area"><i
                                                    className="fa fa-pencil text-warning"></i></button>
                                            <button className="btn-group btn btn-light" data-toggle="tooltip"
                                                title="Rubb your checking"><i className="fa fa-eraser"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <embed
                                src={`https://docs.google.com/gview?url=${global.img + this.props.data.assignmentview.submissionURL}&embedded=true`}
                                    //src={global.img + this.props.data.assignmentview.submissionURL}
                                    type="application/pdf" type="" width="100%" height="480px" />
                                   
                            </div>
                            <div className="card-footer">
                            <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">

                                    <label for="" className="mb-0 font-weight-bold">Maximum Marks</label>
                                    <input type="number" defaultValue={this.props.data.fromAssignmentList.maximumMarks} disabled name="addr1"  className="form-control" />

                                   
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Marks Obtained:</label>

                                    <input type="number" id="marks" defaultValue={this.props.data.assignmentview.marksObtained}  className="form-control" />

                             
                                </div>
                            </div>
                        </div>
                       
                              <div className="form-group">
                                    <label for="" className="font-weight-bold"> Write review</label>
                                    <textarea className="form-control" name="" id="" rows="3"></textarea>
                                </div>
                                <button type="submit" onClick={()=>this.updateAssignment()} className="btn btn-primary float-right">Finish Checking and
                            Submit</button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-xl-4 col-sm-12">


                        <div className="card">
                            <div className="card-header">
                                Question Paper <a href="#" className="float-right btn btn-light" data-toggle="tooltip"
                                    title="Zoom to Show Question Paper"><i className="fa fa-search-plus"></i></a>
                            </div>
                            <div className="card-body">
                                <div className="q-view">
                                    <embed
                                    src={`https://docs.google.com/gview?url=${global.img + this.props.data.fromAssignmentList.assignmentURL}&embedded=true`}
                                      //  src={global.img + this.props.data.fromAssignmentList.assignmentURL}
                                        type="application/pdf" type="" width="100%" height="480px" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        );


    }
}
export default adminAssignmentCheck