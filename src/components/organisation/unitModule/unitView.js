import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import Unitlist from './unitList';
import Edite from './unitEdit';
import swal from 'sweetalert';

class viewUnit extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.unid);
    this.listUnit = this.listUnit.bind(this);
    this.editeUnit = this.editeUnit.bind(this);
    this.checkdt = this.checkdt.bind(this);
    this.state = {
      unitview: [],
      isLoaded: false
    }

  }
  checkdt(val){
    console.log(val);
    if(val=="" || val==null)
    return "NA";
    else
    return val;
  }
  componentDidMount() {




    let url = global.API + "/studapi/public/api/viewanunit";
    fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },

      body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unitID': this.props.unid })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json)
        this.setState({
          isLoaded: true,
          unitview: json,

        })
      });



  }


  listUnit() {

    ReactDOM.render(<Unitlist />, document.getElementById('contain1'))

  }
  editeUnit(unit1) {
    let name = "Edit unit";
    ReactDOM.render(<Edite undata={unit1} webname={name} />, document.getElementById('contain1'))

  }
  deleteUnit(id1) {
    console.log(id1);
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover  Unit!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          let url = global.API + "/studapi/public/api/deleteunit";
          fetch(url, {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unID': id1 })
          })
            .then((resp1) => resp1.json()
              .then((resp) => {
                if (resp[0]['result'] == 1) {
                  swal("Unit deleted!", {
                    icon: "success",
                  });
                  this.listUnit();
                }

              }));
        }

      });
  }
  render() {
    var { unitview } = this.state;

    return (
      <div className="fadeIn animated">
        <div className="card">
          <div className="card-header">
            <span className="font-weight-bold text-primary mb-0 h3">Unit</span>
            <a onClick={this.listUnit} className="float-right mt-2"><i className="icon-action-undo"></i> Back to list Units</a>
          </div>
          {unitview.map(unit => (
            <div className="card-body">


              <h2>
                <img id="blah" src={global.img + unit.unitLogo} alt="your image" className="organization-logo" />
                <span className="font-weight-bold">{unit.unitName}</span>

              </h2>
              <hr />
              <div className="row">
                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-people"></i>
                      </div>
                      <div className="h4 mb-0">8500</div>
                      <small className="text-muted text-uppercase font-weight-bold">Active Users</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>


                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-pie-chart"></i>
                      </div>
                      <div className="h4 mb-0">2800</div>
                      <small className="text-muted text-uppercase font-weight-bold">Registered Students</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar" role="progressbar" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>


                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-speedometer"></i>
                      </div>
                      <div className="h4 mb-0">54GB</div>
                      <small className="text-muted text-uppercase font-weight-bold">Resource USes</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-danger" role="progressbar" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>


                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-speech"></i>
                      </div>
                      <div className="h4 mb-0">Done</div>
                      <small className="text-muted text-uppercase font-weight-bold">Payment Status</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-info" role="progressbar" style={{ width: "25%" }} aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>


              </div>
              <div className="border pt-2 pb-2 pl-3 pr-3">
                <span className=" font-weight-bold h3 border-title">Unit Details:</span>

                <div className="row p-4" >
                  <div className="col-md-6">
                    <p><b>Name of the Unit:</b> &nbsp; <input type="text" value={unit.unitName} className="form-control bg-white " /></p>
                    <p><b>UNIT ID:</b> &nbsp; <input type="text" value={unit.unitID} className="form-control bg-white " /> </p>
                    <p><b>Name of the organisation:</b> &nbsp;<input type="text" value={unit.unitName} className="form-control bg-white " /></p>
                    <p><b>Organization type:</b> &nbsp;<input type="text" value={unit.organisationType} className="form-control bg-white " /></p>
                    <p><b>Address:</b> &nbsp;<input type="text" value={unit.Address} className="form-control bg-white " /></p>
                    <p><b>City:</b> &nbsp;<input type="text" value={unit.City} className="form-control bg-white " /></p>
                    <p><b>State:</b> &nbsp;<input type="text" value={unit.State} className="form-control bg-white " /></p>
                    <p><b>PIN Code:</b> &nbsp;<input type="text" value={unit.PinCode} className="form-control bg-white " /></p>
                    <p><b>Country:</b> &nbsp;<input type="text" value={unit.country} className="form-control bg-white " /></p>
                    <p><b>Telephone Number(Unit):</b> &nbsp;<input type="text" value={unit.telephoneNo} className="form-control bg-white " /></p>
                    <p><b>Website of organization:</b> &nbsp;<input type="text" value={unit.website} className="form-control bg-white " /></p>
                    <p><b>Organization Email Address:</b> &nbsp;<input type="text" value={unit.orgEmailID} className="form-control bg-white " /></p>
                  </div>
                  <div className="col-md-6">
                    <p><b>Subscription Starts on:</b> &nbsp;<input type="text" value={unit.subscriptionStartDate} className="form-control bg-white " /></p>
                    <p><b>Subscription Ends on:</b> &nbsp;<input type="text" value={this.checkdt(unit.subscriptionEndDate)} className="form-control bg-white " /></p>
                    <p><b>Name of Contact Person:</b> &nbsp;<input type="text" value={unit.NameOfContactPerson} className="form-control bg-white " /></p>
                    <p><b>Email Address:</b> &nbsp;<input type="text" value={unit.contactPersonEmailID} className="form-control bg-white " /></p>
                    <p><b>Contact Number(Contact person):</b> &nbsp;<input type="text" value={unit.contactNo} className="form-control bg-white " /></p>
                    <p><b>Designation:</b> &nbsp;<input type="text" value={unit.designation} className="form-control bg-white " /></p>
                    <p><b>gender:</b> &nbsp;<input type="text" value={unit.contactPersonGender} className="form-control bg-white " /></p>
                  </div>
                </div>
              </div>
              <hr />
              <div className="border pt-2 pr-4 pl-4 pb-3">
                <table className="mt-3 table table-bordered table-responsive-md">
                  <thead className="bg-light">
                    <tr>
                      <th rowspan="2" className="border">Name of Modules</th>
                      <th colspan="3" className="text-center border">Action</th>
                    </tr>
                    <tr className="border">
                      <th className="border">Read</th>
                      <th className="border">Write</th>
                      <th className="border">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Unit</td>
                      <td><input type="checkbox" checked disabled /></td>
                      <td><input type="checkbox" checked disabled /></td>
                      <td><input type="checkbox" checked disabled /></td>
                    </tr>
                    <tr>
                      <td>Exam Modules</td>
                      <td><input type="checkbox" disabled /></td>
                      <td><input type="checkbox" checked disabled /></td>
                      <td><input type="checkbox" checked disabled /></td>
                    </tr>

                  </tbody>
                </table>
                <hr />
                <div className="float-right mt-4 mb-2">
                  <a onClick={() => this.editeUnit(unit)} className="btn text-dark btn-warning"><i className="icon-pencil"></i>Edit Unit</a>
                  <a onClick={() => this.deleteUnit(unit.unitID)} className="btn-danger text-white btn"  >< i className="icon-trash"></i>Delete Unit </a>
                </div>
              </div>
            </div>
          ))}
        </div>

      </div>
    );


  }
}
export default viewUnit