import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import Edite from './unitEdit'
import Create from './unitAdd'
import Unitview from './unitView'
import './unit.css';
import swal from 'sweetalert';
import $ from 'jquery'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
class unitList extends Component {
    constructor(props) {
        super(props);
        this.editeUnit = this.editeUnit.bind(this);
        this.createUnit = this.createUnit.bind(this);
        this.showUnit = this.showUnit.bind(this);
        this.deleteUnit = this.deleteUnit.bind(this)
        this.checkdt = this.checkdt.bind(this)

        this.state = {
            unitList: [],
            isLoaded: false,
            searchString : ""
        }
        this.unitSearchList = []

    }
    checkdt(val){

        if(val=="" || val==null || val==undefined)
        return "NA";
        else
        return val;
      }
    deleteUnit(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Unit!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deleteunit";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unID': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Unit deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    showUnit(id2) {

        ReactDOM.render(<Unitview unid={id2} />, document.getElementById('contain1'))
    }
    editeUnit(unit1) {
        let name = "Edit unit";
        ReactDOM.render(<Edite undata={unit1} webname={name} />, document.getElementById('contain1'))

    }
    createUnit() {

        ReactDOM.render(<Create />, document.getElementById('contain1'))

    }

    componentDidMount() {




        let url = global.API + "/studapi/public/api/viewallorgaunit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })
        })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    unitList: json
                })
            }).then(() => {
                let count = Object.keys(this.state.unitList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });



    }
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Unit !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deleteunit";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unID': value })
                            })
                        })
                        swal("All Selected Unit Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Students cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }
// refresh Page
refreshPage(){
   this.componentDidMount();
}

    render() {

        var { isLoaded, unitList } = this.state;
        if(this.state.searchString.replace(/\s/g, '') != ''){
           this.unitSearchList = SearchForObjectsWithName(unitList, this.state.searchString)
       }else{
          this.unitSearchList = unitList
       }
        if (!isLoaded)
            return (
                <div className="fadeIn animated">
                    <div className="card">
                        <div className="card-header">
                            <h3 className="font-weight-bold">Unit
            <a onClick={this.createUnit}><span className=" h5  mt-2 font-weight-bold float-right">+ Create New Unit</span></a>
                            </h3>
                        </div>
                        <div className="card-body">
                            <table className="table table-stripped table-bordered table-responsive">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                        <th className="border-0">Name of UNIT</th>
                                        <th className="border-0">Unit Code</th>
                                        <th className="border-0">Name of Institute</th>
                                        <th className="border-0">Name of Representative</th>
                                        <th className="border-0">Contact Number(Contact person)</th>
                                        <th className="border-0">Website Address</th>
                                        <th className="border-0">Organization Logo</th>
                                        <th className="border-0">User Capacity</th>
                                        <th className="border-0">Subscription Starts from</th>
                                        <th className="border-0">Ending On</th>
                                        <th className="border-0">Current Status</th>
                                        <th className="border-0">Subscribed Package</th>
                                        <th className="border-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <h2 >Loading....</h2>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>





            );
        else {

            return (
                <div className="fadeIn animated">
                    <div className="card">
                        <div className="card-header">
                        <div className="row">
                        <div className="col-md-4">
                        <h3 className="font-weight-bold">Unit:  </h3>
                        </div>
                        <div className="col-md-4">

                        </div>
                        <div className="col-md-4">
                        <a onClick={this.createUnit}><span className=" h5  mt-2 font-weight-bold float-right">+ Create New Unit</span></a>

                        </div>
                        </div>



                        </div>
                        <div className="card-body">
                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Selected All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                            <table className="table table-stripped table-bordered table-responsive">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                        <th className="border-0">Name of UNIT</th>
                                        <th className="border-0">Unit Code</th>
                                        <th className="border-0">Name of Representative</th>
                                        <th className="border-0">Contact Number(Contact Person)</th>
                                        <th className="border-0">Website Address</th>
                                        <th className="border-0">Organization Logo</th>
                                        <th className="border-0">User Capacity</th>
                                        <th className="border-0">Subscription Starts from</th>
                                        <th className="border-0">Ending On</th>
                                        <th className="border-0">Current Status</th>
                                        <th className="border-0">Subscribed Package</th>
                                        <th className="border-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.unitSearchList.map(unit => (

                                        <tr>
                                             <td className="border-0"><input type="checkbox" value={unit.unitID} className="checkBoxForDeletion" /></td>
                                            <td className="border-0">{unit.unitName}</td>
                                            <td className="border-0">{unit.unitID}</td>
                                            <td className="border-0">{unit.NameOfContactPerson}</td>
                                            <td className="border-0">{unit.contactNo}</td>
                                            <td className="border-0">{unit.website}</td>
                                            <td className="border-0"><img src={global.img + unit.unitLogo} alt="Unit Logo" className="unit-logo" /></td>
                                            <td className="border-0">{unit.maxNumOfStudents}</td>
                                            <td className="border-0">{unit.subscriptionStartDate}</td>
                                            <td className="border-0" id="enddate">

                                            {this.checkdt(unit.subscriptionEndDate)} <span className="badge badge-pill bg-warning" id="asindate"></span>
                                            </td>
                                            <td className="border-0"><span className="badge badge-pill bg-light">{unit.currentStatus}</span></td>
                                            <td className="border-0">
                                                <a href="#" data-target="#showPackage" data-toggle="modal"><i className="icon-eye"></i></a>
                                            </td>
                                            <td className="border-0">
                                                <span><a onClick={() => this.showUnit(unit.unitID)} className="text-primary"><i
                                                    className="icon-eye"></i></a></span>    <br />
                                                <span><a onClick={() => this.editeUnit(unit)} className="text-warning"><i
                                                    className="icon-pencil"></i></a></span> <br />
                                                <span><a onClick={() => this.deleteUnit(unit.unitID)} className="text-danger"  >< i className="icon-trash"></i>
                                                </a></span>
                                            </td>


                                        </tr>
                                    ))}
                                </tbody>
                            </table>

                        </div>
                    </div>

                    {/*-modal for package*/}
                    <div className="modal fade" id="showPackage" tabIndex={-1} role="dialog" aria-labelledby="modelTitleshowPackage" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="container-fluid text-center">
                                        <h4 className="font-weight-bold">Package Name</h4>
                                        <hr />
                                        <li>All Package Selected</li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*-/modal for package ->*/}


                </div>
            );
        }


    }
}
export default unitList
