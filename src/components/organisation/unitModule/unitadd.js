import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Unit from './unitList';
import Cookies from 'js-cookie';
import $ from 'jquery';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

import swal from 'sweetalert';

class unitAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            unit_i_d: "",
            organisation_i_d: Cookies.get('orgid'),
            unit_name: "",
            organisation_type: "",
            organisation_name: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            pin_code: "",
            country: "",
            telephone_no: 0,
            telephone_no1: 0,
            telephone_no2: 0,
            website: "",
            org_logo: "",
            org_email_i_d: "",
            subscription_start_date: "",
            subscription_end_date: "",
            name_of_contact_person: "",
            contact_person_email_i_d: "",
            contact_no: "",
            gender:"",
            altercontact_no:"",
            contact_no1: "",
            designation: "",
            max_num_of_students: "",
            max_file_size: 1024,
            unit_creator_i_d: "",
            unit_created_on: new Date().toISOString().slice(0, 10),
            unit_editor_i_d: "",
            unit_edited_on: "",
            ii: 0
        }

        this.showUnit = this.showUnit.bind(this);
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
    showUnit() {

        ReactDOM.render(<Unit />, document.getElementById('contain1'))

    }



    submit() {
        var pin = document.getElementById("pin").value;
        var website = document.getElementById("website").value;
        var tel = document.getElementById("tel").value;
        var email = document.getElementById("email").value;
        var contactPerEmail = document.getElementById("contactPerEmail").value;
        var contactPerNo = document.getElementById("contactPerNo").value;
        var maxStudentsNo = document.getElementById("maxStudentsNo").value;
        var i = 0;
        var pinCheck = /^[0-9]{6}$/;
        var websiteCheck = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,80}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
        var telCheck = /^[0-9]{10}$/;
        var emailCheck = /^[A-Za-z0-9_.]{3,60}@[A-Za-z0-9]{3,60}[.]{1}[A-Za-z.]{2,6}$/;
        var maxStudentsNoCheck = /^[0-9]{1,8}$/;

        if (i < 15) {
            if (document.getElementById("unitName").value == "") {
                document.getElementById("unitNameError").innerHTML = "*Please fill this field";
                document.getElementById("unitName").focus();
                document.getElementById("unitName").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unitName").value != "") {
                document.getElementById("unitNameError").innerHTML = "";
                i = i + 1;
                document.getElementById("unitName").style.borderColor = "";
            }

            if (document.getElementById("address1").value == "") {
                document.getElementById("addressError").innerHTML = "*Please fill this field";
                document.getElementById("address1").focus();
                document.getElementById("address1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("address1").value != "") {
                document.getElementById("addressError").innerHTML = "";
                i = i + 1;
                document.getElementById("address1").style.borderColor = "";
            }
            if (document.getElementById("pin").value == "") {
                document.getElementById("pinError").innerHTML = "*Please fill this field";
                document.getElementById("pin").focus();
                document.getElementById("pin").style.borderColor = "#FF0000";
            }

            if (document.getElementById("pin").value != "") {
                if (!pinCheck.test(pin)) {
                    document.getElementById("pinError").innerHTML = "*Invalid PIN";
                    document.getElementById("pin").focus();
                    document.getElementById("pin").style.borderColor = "#FF0000";
                }

                if (pinCheck.test(pin)) {
                    document.getElementById("pinError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("pin").style.borderColor = "";
                }
            }
          {/*  if (document.getElementById("website").value == "") {
                document.getElementById("websiteError").innerHTML = "*Please fill this field";
                document.getElementById("website").focus();
                document.getElementById("website").style.borderColor = "#FF0000";
            }

            if (document.getElementById("website").value != "") {
                if (!websiteCheck.test(website)) {
                    document.getElementById("websiteError").innerHTML = "*Invalid Website";
                    document.getElementById("website").focus();
                    document.getElementById("website").style.borderColor = "#FF0000";
                }

                if (websiteCheck.test(website)) {
                    document.getElementById("websiteError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("website").style.borderColor = "";
                }
            }*/}
            if (document.getElementById("subscriptionStart").value == "") {
                document.getElementById("subscriptionStartError").innerHTML = "*Please fill this field";
                document.getElementById("subscriptionStart").focus();
                document.getElementById("subscriptionStart").style.borderColor = "#FF0000";
            }

            if (document.getElementById("subscriptionStart").value != "") {
                document.getElementById("subscriptionStartError").innerHTML = "";
                i = i + 1;
                document.getElementById("subscriptionStart").style.borderColor = "";
            }

            if (document.getElementById("orgType").value == "") {
                document.getElementById("orgTypeError").innerHTML = "*Please fill this field";
                document.getElementById("orgType").focus();
                document.getElementById("orgType").style.borderColor = "#FF0000";
            }

            if (document.getElementById("orgType").value != "") {
                document.getElementById("orgTypeError").innerHTML = "";
                i = i + 1;
                document.getElementById("orgType").style.borderColor = "";
            }

            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }

            if (document.getElementById("state").value == "") {
                document.getElementById("stateError").innerHTML = "*Please fill this field";
                document.getElementById("state").focus();
                document.getElementById("state").style.borderColor = "#FF0000";
            }

            if (document.getElementById("state").value != "") {
                document.getElementById("stateError").innerHTML = "";
                i = i + 1;
                document.getElementById("state").style.borderColor = "";
            }
            if (document.getElementById("tel").value == "") {
                document.getElementById("telError").innerHTML = "*Please fill this field";
                document.getElementById("tel").focus();
                document.getElementById("tel").style.borderColor = "#FF0000";
            }

            if (document.getElementById("tel").value != "") {
                if (!telCheck.test(tel)) {
                    document.getElementById("telError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("tel").focus();
                    document.getElementById("tel").style.borderColor = "#FF0000";
                }

                if (telCheck.test(tel)) {
                    document.getElementById("telError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("tel").style.borderColor = "";
                }
            }

            if (document.getElementById("country").value == "") {
                document.getElementById("countryError").innerHTML = "*Please fill this field";
                document.getElementById("country").focus();
                document.getElementById("country").style.borderColor = "#FF0000";
            }

            if (document.getElementById("country").value != "") {
                document.getElementById("countryError").innerHTML = "";
                i = i + 1;
                document.getElementById("country").style.borderColor = "";
            }
            if (document.getElementById("email").value == "") {
                document.getElementById("emailError").innerHTML = "*Please fill this field";
                document.getElementById("email").focus();
                document.getElementById("email").style.borderColor = "#FF0000";
            }

            if (document.getElementById("email").value != "") {
                if (!emailCheck.test(email)) {
                    document.getElementById("emailError").innerHTML = "*Invalid Email Id";
                    document.getElementById("email").focus();
                    document.getElementById("email").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(email)) {
                    document.getElementById("emailError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("email").style.borderColor = "";
                }
            }
            if (document.getElementById("contactPerson").value == "") {
                document.getElementById("contactPersonError").innerHTML = "*Please fill this field";
                document.getElementById("contactPerson").focus();
                document.getElementById("contactPerson").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contactPerson").value != "") {
                document.getElementById("contactPersonError").innerHTML = "";
                i = i + 1;
                document.getElementById("contactPerson").style.borderColor = "";
            }

            if (document.getElementById("gender").value == "") {
                document.getElementById("genderError").innerHTML = "*Please fill this field";
                document.getElementById("gender").focus();
                document.getElementById("gender").style.borderColor = "#FF0000";
            }

            if (document.getElementById("gender").value != "") {
                document.getElementById("genderError").innerHTML = "";
                i = i + 1;
                document.getElementById("gender").style.borderColor = "";
            }
            if (document.getElementById("contactPerEmail").value == "") {
                document.getElementById("contactPerEmailError").innerHTML = "*Please fill this field";
                document.getElementById("contactPerEmail").focus();
                document.getElementById("contactPerEmail").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contactPerEmail").value != "") {


                if (!emailCheck.test(contactPerEmail)) {
                    document.getElementById("contactPerEmailError").innerHTML = "*Invalid Email Id";
                    document.getElementById("contactPerEmail").focus();
                    document.getElementById("contactPerEmail").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(contactPerEmail)) {
                    document.getElementById("contactPerEmailError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("contactPerEmail").style.borderColor = "";
                }
            }
            if (document.getElementById("contactPerNo").value == "") {
                document.getElementById("contactPerNoError").innerHTML = "*Please fill this field";
                document.getElementById("contactPerNo").focus();
                document.getElementById("contactPerNo").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contactPerNo").value != "") {
                if (!telCheck.test(contactPerNo)) {
                    document.getElementById("contactPerNoError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("contactPerNo").focus();
                    document.getElementById("contactPerNo").style.borderColor = "#FF0000";
                }

                if (telCheck.test(contactPerNo)) {
                    document.getElementById("contactPerNoError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("contactPerNo").style.borderColor = "";
                }
            }
            if (document.getElementById("maxStudentsNo").value == "") {
                document.getElementById("maxStudentsNoError").innerHTML = "*Please fill this field";
                document.getElementById("maxStudentsNo").focus();
                document.getElementById("maxStudentsNo").style.borderColor = "#FF0000";
            }

            if (document.getElementById("maxStudentsNo").value != "") {
                if (!maxStudentsNoCheck.test(maxStudentsNo)) {
                    document.getElementById("maxStudentsNoError").innerHTML = "*Invalid Student Number";
                    document.getElementById("maxStudentsNo").focus();
                    document.getElementById("maxStudentsNo").style.borderColor = "#FF0000";
                }

                if (maxStudentsNoCheck.test(maxStudentsNo)) {
                    document.getElementById("maxStudentsNoError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("maxStudentsNo").style.borderColor = "";
                }
            }
            if (i == 15) {
                console.log(i);
                this.state.ii = 15;
            }
        }
        if (this.state.ii == 15) {


            if(this.state.altercontact_no!="") {
        
                this.state.contact_no=this.state.contact_no1 +","+ this.state.altercontact_no
                 }
                 else{
                    this.state.contact_no=this.state.contact_no1
                 }
                 if(this.state.telephone_no2!=0) {
        
                    this.state.telephone_no=this.state.telephone_no1 +","+ this.state.telephone_no2
                     }
                     else{
                        this.state.telephone_no=this.state.telephone_no1
                     }


                
            console.log(this.state);
            let url = global.API + "/studapi/public/api/addunit";
            let data = this.state;
            const fd = new FormData();
            //formData.append('file', this.state.org_logo);
            $.each(data, function (key, value) {
                fd.append(key, value);
            })
            console.log(fd);
            fetch(url, {
                method: 'POST',

                body: fd
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp[0]['regResult'] == 1) {
                        swal({
                            title: "Done!",
                            text: "Unit Added Sucessfully",
                            icon: "success",
                            button: "OK",

                        });
                        this.showUnit();
                    }

                    else if (resp[0]['regResult'] == 2){
                        swal({
                            title: "Error!",
                            text: " unit is already present ",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Unit Not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                })
            })
            console.log(data);


        }
    }

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">Add Unit</span>
                        <a onClick={this.showUnit} className="float-right mt-2">+ List of Units</a>
                    </div>
                    <div className="card-body">
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 border-title">Unit Details:</span>
                            <hr />

                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Organisation:</label>
                                        <input type="text" value={Cookies.get('orgname')} disabled className="form-control  bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Organisation Type <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="organisation_type" id="orgType" value={this.state.organisation_type} onChange={(data) => { this.setState({ organisation_type: data.target.value }) }} className="form-control  bg-white">
                                            <option value="">Select</option>
                                            <option value="Training Institute">Training Institute</option>
                                            <option value="College">College</option>
                                            <option value="School">School</option>
                                            <option value="Company">Company</option>
                                            <option value="Plant">Plant</option>
                                            <option value="Head Office">Head Office</option>
                                            <option value="Regional Office">Regional Office</option>
                                            <option value="University">University</option>
                                        </select>
                                        <span id="orgTypeError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Organisation Email Address <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="email" name="org_email_i_d" value={this.state.org_email_i_d} onChange={(data) => { this.setState({ org_email_i_d: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="emailError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="address1" name="address1" value={this.state.address1} onChange={(data) => { this.setState({ address1: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="addressError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group ">
                                        <label for="" className="mb-0 font-weight-bold">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                        <CountryDropdown id="country" name="country" value={this.state.country} onChange={(data) => { this.selectCountry(data) }} className="form-control  bg-white" />
                                        <span id="countryError" class="text-danger font-weight-bold"></span>
                                    </div>


                                </div>

                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="unitName" name="unit_name" value={this.state.unit_name} onChange={(data) => { this.setState({ unit_name: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="unitNameError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Website of the Organisation :</label>
                                        <input type="text" id="website" name="website" value={this.state.website} onChange={(data) => { this.setState({ website: data.target.value }) }} className="form-control  bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="tel" name="telephone_no" onChange={(data) => { this.setState({ telephone_no1: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="telError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address 2:</label>
                                        <input type="text" id="address2" name="address2" value={this.state.address2} onChange={(data) => { this.setState({ address2: data.target.value }) }} className="form-control  bg-white" />
                                    </div>


                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State <span className="text-danger"><strong>*</strong></span>:</label>
                                        <RegionDropdown country={this.state.country} id="state" name="state" value={this.state.state} onChange={(data) => { this.selectRegion(data) }} className="form-control  bg-white" />
                                        <span id="stateError" class="text-danger font-weight-bold"></span>
                                    </div>

                                    {/*
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Subsciption Ends on <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="date" id="subscriptionEnd" name="subscription_end_date" value={this.state.subscription_end_date} onChange={(data) => {this.setState({subscription_end_date:data.target.value})}} className="form-control  bg-white" />
                                        <span id="subscriptionEndError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    */}
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Upload Logo:</label> <br />
                                        <input type="file" id="logo" name="org_logo" onChange={(data) => { this.setState({ org_logo: data.target.files[0] }) }} className="form-control  bg-white" />
                                        <span id="logoError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Subsciption Starts From <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="date" id="subscriptionStart" name="subscription_start_date" value={this.state.subscription_start_date} onChange={(data) => { this.setState({ subscription_start_date: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="subscriptionStartError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Telephone Number :</label>
                                        <input type="text" id="tel1" name="telephone_no" onChange={(data) => { this.setState({ telephone_no2: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="telError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="number" id="pin" name="pin_code" value={this.state.pin_code} onChange={(data) => { this.setState({ pin_code: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="pinError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="city" name="city" value={this.state.city} onChange={(data) => { this.setState({ city: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="cityError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    
                                    {/*<div className="form-group" style={{visibility:"hidden"}}>
                                        <label for="" className="mb-0 font-weight-bold">Current Status <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="current_status" id="status" value={this.state.current_status} onChange={(data) => {this.setState({current_status:data.target.value})}} className="form-control  bg-white">
                                            <option value="">Select</option>
                                            <option value="Pending" selected >Pending</option>
                                            <option value="Active">Active</option>
                                            <option value="Deactive">Deactivated</option>
                                        </select>
                                        <span id="statusError" class="text-danger font-weight-bold"></span>
                                </div>*/}
                                
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Contact Person <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="contactPerson" name="name_of_contact_person" value={this.state.name_of_contact_person} onChange={(data) => { this.setState({ name_of_contact_person: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="contactPersonError" class="text-danger font-weight-bold"></span>
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email Address <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="contactPerEmail" name="contact_person_email_i_d" value={this.state.contact_person_email_i_d} onChange={(data) => { this.setState({ contact_person_email_i_d: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="contactPerEmailError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>

                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Gender <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="current_status" id="gender" value={this.state.gender} onChange={(data) => { this.setState({ gender: data.target.value }) }} className="form-control  bg-white">
                                            <option value="">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <span id="genderError" class="text-danger font-weight-bold"></span>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="number" id="contactPerNo" name="contact_no" value={this.state.contact_no1} onChange={(data) => { this.setState({ contact_no1: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="contactPerNoError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Designation :</label>
                                        <input type="text" id="desig" name="designation" value={this.state.designation} onChange={(data) => { this.setState({ designation: data.target.value }) }} className="form-control  bg-white" />
                                        
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Alternativ Contact Number:</label>
                                        <input type="number" id="altercontactPerNo" name="altercontact_no" value={this.state.altercontact_no} onChange={(data) => { this.setState({ altercontact_no: data.target.value }) }} className="form-control  bg-white" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group" >
                                        <label for="" className="mb-0 font-weight-bold">Maximum Number of Students <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="number" id="maxStudentsNo" name="max_num_of_students" value={this.state.max_num_of_students} onChange={(data) => { this.setState({ max_num_of_students: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="maxStudentsNoError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                               {/* <div className="col-md-6">
                                    <div className="form-group" style={{visibility:"hidden"}} >
                                        <label for="" className="mb-0 font-weight-bold">Maximum number of uploading File Size(MB): </label>
                                        <input type="number" name="max_file_size" value={} onChange={(data) => { this.setState({ max_file_size: data.target.value }) }} className="form-control  bg-white" />

                                    </div>
                                </div>*/}
                            </div>
                            <button type="submit" onClick={() => { this.submit() }} className="btn btn-success"><i className="icon-check"></i> Save &amp; Proceed</button>
                        </div>
                    </div>
                </div>

            </div>
        );


    }
}
export default unitAdd
