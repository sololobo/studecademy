import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Unit from './unitList';
import $ from 'jquery';
import Cookies from 'js-cookie';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import logo from '../../../media/features/books.png';
import swal from 'sweetalert';
class unitEdit extends Component {
    constructor(props) {
        super(props);
        this.changePass = this.changePass.bind(this);
        this.selectCountry=this.selectCountry.bind(this)
        this.selectRegion=this.selectRegion.bind(this)
        console.log(CountryRegionData);
        this.state = {
            organisation: [],
            country:"",
            state:"",
            unitList: [],
            isLoaded: false

        }

        this.showUnit = this.showUnit.bind(this);
    }
    showUnit() {

        ReactDOM.render(<Unit />, document.getElementById('contain1'))

    }

    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
    changePass() {
        //let email = document.getElementById('email1').innerHTML;
        let email = Cookies.get('username')
        let pwd = document.getElementById('newpass').value;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }

                            }));
                }

            });

    }
    componentDidMount() {
        setTimeout(function () { //Start the timer
            console.log("dgxf") //After 1 second, set render to true


            let url = global.API + "/studapi/public/api/viewanunit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'unitID': this.props.undata.unitID })
        })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    unitList: json
                })
            });
        

        }.bind(this), 1500)
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
       
      console.log(  document.getElementById("orgeditstate").value)
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
    submit(unit) {
        this.state = {
            unit_i_d: this.props.undata.unitID,
            organisation_i_d: Cookies.get('orgid'),
            unit_name: document.getElementById("uname").value,
            organisation_type: document.getElementsByName("organisation_type")[0].value,
            gender: document.getElementsByName("gender")[0].value,
            addr1: document.getElementsByName("address1")[0].value,
            addr2: document.getElementsByName("address2")[0].value,
            city: document.getElementsByName("city")[0].value,
            state: document.getElementsByName("state")[0].value,
            pin_code: document.getElementsByName("pin_code")[0].value,
            country: document.getElementsByName("pin_code")[0].value,
           // telephone_no: document.getElementsByName("telephone_no")[0].value + ","+document.getElementById("telno2").value,
            website: document.getElementsByName("website")[0].value,
            org_email_i_d: document.getElementsByName("org_email_i_d")[0].value,
            subscription_start_date: document.getElementsByName("subscription_start_date")[0].value,
            name_of_contact_person: document.getElementsByName("name_of_contact_person")[0].value,
            contact_person_email_i_d: document.getElementsByName("contact_person_email_i_d")[0].value,
          //  contact_no: document.getElementById("contact_no").value + "," + document.getElementById("alter_contact_no").value,
            designation: document.getElementsByName("designation")[0].value,
            current_status: document.getElementsByName("current_status")[0].value,
            max_num_of_students: document.getElementsByName("max_num_of_students")[0].value,
            max_file_size: document.getElementsByName("max_file_size")[0].value
        }
        console.log(this.state); 
        let url = global.API + "/studapi/public/api/editunit";
        let data = this.state;
        const fd = new FormData();
        if(document.getElementById("telno2").value!="") {
        
       let telephone_no= document.getElementsByName("telephone_no")[0].value + ","+document.getElementById("telno2").value
       fd.append('telephone_no', telephone_no);
        }
        else{
            let telephone_no= document.getElementsByName("telephone_no")[0].value
            fd.append('telephone_no', telephone_no);
        }
        if(document.getElementById("alter_contact_no").value!="") {
        
            let contact_no= document.getElementById("contact_no").value + "," + document.getElementById("alter_contact_no").value
            fd.append('contact_no', contact_no);
             }
             else{
                 let contact_no= document.getElementById("contact_no").value
                 fd.append('contact_no', contact_no);
             }
        if (document.getElementById('logot').files.length == 0)
        
        fd.append('logo', unit.unitLogo);
    else 
        fd.append('logo', document.getElementById('logot').files[0]);
      
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        }) 
       


        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Unit Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.showUnit()

                }
                else {
                    swal({
                        title: "Error",
                        text: "Unit Not Updated",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
        console.log(data);
        
    }
    render() {

        var { isLoaded, unitList } = this.state;
        console.log(unitList);
      
     
        return (
            <div className="fadeIn animated">
            <div className="card">
                <div className="card-header">
                    <span className="font-weight-bold text-primary mb-0 h3">Edit Unit</span>
                    <a onClick={this.showUnit}  type="button" className="float-right mt-2">+ List of Units</a>
                </div>
                <div className="card-body">
                    {unitList.map(unit => (
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 border-title">Unit Details:</span>
                            <hr />

                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Organisation:</label>
                                        <input type="text" id="unitName" name="unit_name" value={Cookies.get('orgname')} disabled onChange={(data) => { this.setState({ unit_name: data.target.value }) }} disabled className="form-control  bg-white" />
                                        <span id="unitNameError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Type of Organisation  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="organisation_type" onChange={(data) => { this.setState({ organisation_type: data.target.value }) }} className="form-control bg-white" >
                                            <option value={unit.organisationType} >{unit.organisationType}</option>
                                            <option value="Training Institute">Training Institute</option>
                                            <option value="College">College</option>
                                            <option value="School">School</option>
                                            <option value="Company">Company</option>
                                            <option value="Plant">Plant</option>
                                            <option value="Head Office">Head Office</option>
                                            <option value="Regional Office">Regional Office</option>
                                            <option value="University">University</option>
                                        </select>
                                        <span id="orgTypeError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Organization Email Address  <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" name="org_email_i_d" defaultValue={unit.orgEmailID} onChange={(data) => { this.setState({ org_email_i_d: data.target.value }) }} className="form-control bg-white" />
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="address1" defaultValue={unit.Address1} onChange={(data) => { this.setState({ address: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group mt-4">
                                        <label for="" className="mb-0 font-weight-bold">Country  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="country" defaultValue={unit.country} onChange={(data) => { this.setState({ country: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Current Status :</label>
                                        <input type="text" name="current_status" id="status" disabled defaultValue={unit.currentStatus} onChange={(data) => { this.setState({ current_status: data.target.value }) }} className="form-control  bg-white"/>
                                   
                                    </div>


                                </div>

                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="unit_name" id="uname" defaultValue={unit.unitName} onChange={(data) => { this.setState({ unit_name: data.target.value }) }} className="form-control bg-white" />
                                        <span id="unitNameError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Website of the Organisation :</label>
                                        <input type="text" name="website" defaultValue={unit.website} onChange={(data) => { this.setState({ website: data.target.value }) }} className="form-control bg-white" />

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number :</label>
                                        <input type="text" id="tel" name="telephone_no" defaultValue={unit.telephoneNo.split(',')[0]} onChange={(data) => { this.setState({ telephone_no: data.target.value }) }} className="form-control  bg-white" />
                                        <span id="telError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address 2:</label>
                                        <input type="text" name="address2" defaultValue={unit.Address2} onChange={(data) => { this.setState({ address: data.target.value }) }} className="form-control bg-white" />
                                    </div>


                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="state" defaultValue={unit.State} onChange={(data) => { this.setState({ state: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                            
                                </div>
                                <div className="col-md-4">
                                    <div className="row">
                                        <div className="col-md-4">
                                            <img id="blah" src={global.img + unit.unitLogo} alt="your image" className="organization-logo" />
                                        </div>
                                        <div className="col-md-8">
                                        <input type="file" id="logot" name= "" className="form-control bg-white"  />
                                        </div>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Subsciption Starts From   <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="date" name="subscription_start_date" defaultValue={unit.subscriptionStartDate} onChange={(data) => { this.setState({ subscription_start_date: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Telephone Number:</label>
                                        <input type="text" name="telephone_no" id='telno2' defaultValue={unit.telephoneNo.split(',')[1]} onChange={(data) => { this.setState({ telephone_no1: data.target.value }) }} className="form-control bg-white" />
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="pin_code" defaultValue={unit.PinCode} onChange={(data) => { this.setState({ pin_code: data.target.value }) }} className="form-control bg-white" />
                                        <span id="pinError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="city" defaultValue={unit.City} onChange={(data) => { this.setState({ city: data.target.value }) }} className="form-control bg-white" />
                                    </div>



                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Contact Person  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="name_of_contact_person" defaultValue={unit.NameOfContactPerson} onChange={(data) => { this.setState({ name_of_contact_person: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email Address :</label>
                                        <input type="text" name="contact_person_email_i_d" defaultValue={unit.contactPersonEmailID} onChange={(data) => { this.setState({ contact_person_email_i_d: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                </div>

                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Gender  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="gender" id="gender" value={this.state.gender} onChange={(data) => { this.setState({ gender: data.target.value }) }} className="form-control  bg-white">
                                            <option value={unit.contactPersonGender} selected>{unit.contactPersonGender}</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>

                                    <div className="form-group">

                                        <label for="" className="mb-0 font-weight-bold">Contact Number  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" id="contact_no" defaultValue={unit.contactNo.split(',')[0]} onChange={(data) => { this.setState({ contact_no: data.target.value }) }} className="form-control bg-white" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Designation  :</label>
                                        <input type="text" name="designation" defaultValue={unit.designation} onChange={(data) => { this.setState({ designation: data.target.value }) }} className="form-control bg-white" />

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Alternativ Contact Number:</label>
                                        <input type="text" name="alter_contact_no" id="alter_contact_no" defaultValue={unit.contactNo.split(',')[1]} onChange={(data) => { this.setState({ alter_contact_no: data.target.value }) }} className="form-control bg-white" />

                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Maximum Number of Students  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="number" name="max_num_of_students" defaultValue={unit.maxNumOfStudents} onChange={(data) => { this.setState({ max_num_of_students: data.target.value }) }} className="form-control bg-white" />

                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Maximum number of uploading File Size(MB): </label>
                                        <input type="text" disabled name="max_file_size" defaultValue={unit.maxFileSize} onChange={(data) => { this.setState({ max_file_size: data.target.value }) }} className="form-control bg-white" />

                                    </div>
                                </div>
                            </div>
                            
                            <button type="submit" onClick={() => { this.submit(unit) }} className="btn btn-success"><i className="icon-check"></i> Save &amp; Proceed</button>
                        </div>
                    ))}
                </div>
            </div>

        </div>
    );
    }
}
export default unitEdit
