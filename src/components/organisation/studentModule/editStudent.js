import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import update from 'react-addons-update';
import Bulkentry from './addStudentMaster'
import Students from './studentList'
import './editStudent.css'

class EditStudents  extends Component {

    constructor(props) {
        super(props);
        this.bulkEntry = this.bulkEntry.bind(this);


        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);


        this.showSemester = this.showSemester.bind(this);

        this.showSection = this.showSection.bind(this);

        this.resetFields = this.resetFields.bind(this);
        this.state = {
            unitList: [],
            sessionList: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            studentview: this.props.studentdata

        }
        // console.warn("student view is", this.state.studentview)
        this.showSession(this.state.studentview.unitID)
        this.showDepartment(this.state.studentview.sessionID)
        this.showSemester(this.state.studentview.departmentID)
        this.showSection(this.state.studentview.semesterID)
    }

    bulkEntry() {

        ReactDOM.render(<Bulkentry />, document.getElementById('contain1'));
    }
    //react's update is a real *** about spaces.. Be careful. Not a single space after comma.
    resetFields(){
      console.warn(this.state.studentview)
      this.setState(update(this.state, {
          studentview:{
             address1: {$set : ''},
             address2: {$set : ''},
             city: {$set : ''},
             contactNo: {$set : ''},
             country: {$set : ''},
             dateOfAdmission: {$set : ''},
             dateOfBirth: {$set : ''},
             departmentID: {$set : ''},
             departmentName: {$set : ''},
             fatherContactNo: {$set : ''},
             fatherEmailID: {$set : ''},
             fatherName: {$set : ''},
             gender: {$set : ''},
             guardianContactNo: {$set : ''},
             guardianEmail: {$set : ''},
             guardianName: {$set : ''},
             motherContactNo: {$set : ''},
             motherEmailID: {$set : ''},
             motherName: {$set : ''},
             pinCode: {$set : ''},
             profilePic: {$set : ''},
             rollNo: {$set : ''},
             sectionID: {$set : ''},
             sectionName: {$set : ''},
             semesterID: {$set : ''},
             semesterName:{$set : ''},
             session: {$set : ''},
             sessionID: {$set : ''},
             state: {$set : ''},
             status: {$set : ''},
             studentName: {$set : ''}
          },
          sessionList : {$set : []},
          departmentList : {$set : []},
          semesterList : {$set : []},
          sectionList : {$set : []}
      }))

      this.toggleToEdit();
   }


    showSession(val) {
      this.setState(update(this.state, {
         studentview:{
            unitID: {$set :  val},
            sessionID:{$set : ''},
            departmentID:{$set : ''},
            semesterID:{$set : ''},
            sectionID:{$set : ''}
         },
         sessionList : {$set : []},
         departmentList : {$set : []},
         semesterList : {$set : []},
         sectionList : {$set : []}
      }))
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessionList: json
                })
            });
    }



    showDepartment(val) {
      this.setState(update(this.state, {
          studentview:{
             sessionID: {$set :  val},
             departmentID:{$set : ''},
             semesterID:{$set : ''},
             sectionID:{$set : ''}
          },
          departmentList : {$set : []},
          semesterList : {$set : []},
          sectionList : {$set : []}
      }))


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemester(val) {
      this.setState(update(this.state, {
          studentview:{
             departmentID: {$set :  val},
             semesterID:{$set : ''},
             sectionID:{$set : ''}
          },
          semesterList : {$set : []},
          sectionList : {$set : []}
      }))
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSection(val) {
      this.setState(update(this.state, {
          studentview:{
             semesterID: {$set :  val},
             sectionID:{$set : ''}
          }
      }))


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }



    submit() {
        let data = this.state.studentview
        console.log(this.state.studentview)
        const fd = new FormData();
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        fd.append('student_editor_i_d', Cookies.get('orgid'))
        let url = global.API + "/studapi/public/api/updatestudent";
        fetch(url, {
            method: 'POST',
            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                try {
                    if (resp[0].result == 1) {
                        swal("Student Updated!", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        ReactDOM.render(<Students /> , document.getElementById('contain1'));
                    }
                    else {
                        swal("There's something wrong!! :(", {
                            icon: "error",
                        });
                    }
                }
                catch{
                    swal("There's something wrong!! :(", {
                        icon: "error",
                    });
                }
            })
        })

    }


    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };



        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });

    }


//This function makes currentProfile pic editable
toggleToEdit(){
   $('#currProfilePhotoDiv').css({'display':'none'})
   $('#editedProfilePic').css({'display':'block'})
}

    render() {
        var { unitList, sessionList, classList, semesterList, sectionList, studentview } = this.state;
        return (


            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">
                            Edit Student's Entry
                </span>
                    </div>
                        <div className="card-body">
                            <div className="border pt-2 pb-2 pl-3 pr-3">
                                <span className=" font-weight-bold h3 text-success border-title">Academic Details:</span>

                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Unit  <span className="text-danger"><strong>*</strong></span> </label>
                                            <select name="" id="unit1" value = {studentview.unitID} className="form-control bg-white" onChange={(e)=>{this.showSession(e.target.value)}}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <select name="" id="semester1" value = {studentview.semesterID} className="form-control bg-white" onChange={(e)=>this.showSection(e.target.value)}>
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">

                                            <label for="" className="mb-0 font-weight-bold">Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <select name="" id="session1" value={studentview.sessionID} className="form-control bg-white" onChange={(e)=>this.showDepartment(e.target.value)}>
                                                <option value="" selected>Select</option>
                                                {sessionList.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>


                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Section  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <select name="sections_i_d" value={studentview.sectionID} onChange={(data) => { this.setState(update(this.state, {studentview : {sectionID :{$set :  data.target.value}}})) }} className="form-control bg-white">
                                                <option value="" selected>Select</option>
                                                {sectionList.map(section => (
                                                    <option value={section.sectionID}>{section.sectionName}</option>

                                                ))}

                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-4">

                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">className/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <select name="" id="department1" value = {studentview.departmentID} onChange={(e) => this.showSemester(e.target.value)} className="form-control bg-white">
                                                <option value="" selected>Select</option>
                                                
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Date of Admission  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="date" name="date_of_admission" value={studentview.dateOfAdmission} onChange={(data) => {  this.setState(update(this.state, {studentview : {dateOfAdmission :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-3"></div>
                            <div className="border pt-2 pb-2 pl-3 pr-3">
                                <span className=" font-weight-bold h3 text-success border-title">Candidate Details:</span>
                                <div className="row">
                                    <div className="col-md-4">

                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Name  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="student_name" value = {studentview.studentName} onChange={(data) => {  this.setState(update(this.state, {studentview : {studentName :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Contact Number  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="contact_no" value={studentview.contactNo} onChange={(data) => {  this.setState(update(this.state, {studentview : {contactNo :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group">
                                           <label for="" className="mb-0 font-weight-bold">Gender  <span className="text-danger"><strong>*</strong></span> :</label>
                                           <select name="gender" id="gender" value={studentview.gender} onChange={(data) => {  this.setState(update(this.state, {studentview : {gender :{$set :  data.target.value}}}))  }} className="form-control    bg-white">
                                               <option value="">Select</option>
                                               <option value="Male">Male</option>
                                               <option value="Female">Female</option>
                                           </select>
                                       </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Roll No  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="number" name="roll_no" value={studentview.rollNo} onChange={(data) => {  this.setState(update(this.state, {studentview : {rollNo :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Email ID  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="email_i_d" value={studentview.emailID} onChange={(data) => {  this.setState(update(this.state, {studentview : {emailID :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Date Of Birth  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="date" name="date_of_birth" value={studentview.dateOfBirth} onChange={(data) => {  this.setState(update(this.state, {studentview : {dateOfBirth :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>

                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group mb-1">
                                            <label for="" className="mb-0 font-weight-bold">Upload Profile Picture: <button className="font-weight-bold primary"  onClick = {()=>this.toggleToEdit()} id="editProfilePhoto">Edit</button></label> <br />
                                            <div id="currProfilePhotoDiv" className = "">
                                               <img id="currProfilePhoto" src = {global.img + studentview.profilePic} className = "" />
                                            </div>
                                            <div id = "editedProfilePic">
                                               <div className="form-group files">
                                                   <input type="file" name="profile_pic"  id="profilepic" onChange={(data) => {  this.setState(update(this.state, {studentview : {profilePic :{$set :  data.target.value}}}))  }} className="form-control bg-white" multiple="" />
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="p-3"></div>
                            <div className="border pt-2 pb-2 pl-3 pr-3">
                                <span className=" font-weight-bold h3 text-success border-title">Personal Details:</span>
                                <div className="row">
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Father's name  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="father_name" value={studentview.fatherName} onChange={(data) => {  this.setState(update(this.state, {studentview : {fatherName :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Mother's Name  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="mother_name" value={studentview.motherName} onChange={(data) => {  this.setState(update(this.state, {studentview : {motherName :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Guardian's Name  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="guardian_name" value={studentview.guardianName} onChange={(data) => {  this.setState(update(this.state, {studentview : {guardianName :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Address  <span className="text-danger"><strong>*</strong></span> :</label>
                                            <input type="text" name="address1" value={studentview.address1} onChange={(data) => {  this.setState(update(this.state, {studentview : {address1 :{$set :  data.target.value}}})) }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">City<span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="city" value={studentview.city} onChange={(data) => {  this.setState(update(this.state, {studentview : {city :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Account Status<span className="text-danger"><strong>*</strong></span>:</label>
                                            <select name="status" value={studentview.status} onChange={(data) => {  this.setState(update(this.state, {studentview : {status :{$set :  data.target.value}}}))  }} id="" className="form-control bg-white">
                                                <option value="">Select...</option>
                                                <option value="Active">Active</option>
                                                <option value="Suspended">Suspended</option>
                                                <option value="Deactivated">Deactivated</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Father's Contact Number :</label>
                                            <input type="text" name="father_contact_no" value={studentview.fatherContactNo} onChange={(data) => {  this.setState(update(this.state, {studentview : {fatherContactNo :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Mother's Contact Number :</label>
                                            <input type="text" name="mother_contact_no" value={studentview.motherContactNo} onChange={(data) => {  this.setState(update(this.state, {studentview : {motherContactNo :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Guardian's Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="guardian_contact_no" value={studentview.guardianContactNo} onChange={(data) => { this.setState(update(this.state, {studentview : {guardianContactNo :{$set :  data.target.value}}}))  }} id="" className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Address2:</label>
                                            <input type="text" name="address2" value={studentview.address2} onChange={(data) => {  this.setState(update(this.state, {studentview : {address2 :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="country" value={studentview.country} onChange={(data) => {  this.setState(update(this.state, {studentview : {country :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                     
                                  
                                    </div>
                                    <div className="col-md-4">
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Father's Email ID:</label>
                                            <input type="text" name="father_email_i_d" value={studentview.fatherEmailID} onChange={(data) => {  this.setState(update(this.state, {studentview : {fatherEmailID :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Mother's Email ID:</label>
                                            <input type="text" name="mother_email_i_d" value={studentview.motherEmailID} onChange={(data) => {  this.setState(update(this.state, {studentview : {motherEmailID :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">Guardian's Email ID<span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="guardian_email" value={studentview.guardianEmail} onChange={(data) => {  this.setState(update(this.state, {studentview : {guardianEmail :{$set :  data.target.value}}}))  }} id="" className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">PIN code<span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="pin_code" value={studentview.pinCode} onChange={(data) => { this.setState(update(this.state, {studentview : {pinCode :{$set :  data.target.value}}}))  }} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group">
                                            <label for="" className="mb-0 font-weight-bold">State<span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="state" value={studentview.state} onChange={(data) => {  this.setState(update(this.state, {studentview : {state :{$set :  data.target.value}}}))  }} id="" className="form-control bg-white" />
                                        </div>
                               
                                    </div>
                                </div>
                            </div>

                        </div>
                    <div className="card-footer">
                        <div className="float-right">
                            <button onClick={() => { this.submit() }} className="btn btn-success"><i className="icon-check"></i> Save and Update</button>
                        </div>


                    </div>

                </div>



            </div>


        );


    }
}
export default EditStudents
