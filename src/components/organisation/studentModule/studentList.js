import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import EditStudents from './editStudent';
import Singlestudentview from './studentView'
import Manualadd from './addStudent'
import Bulkadd from './addStudentMaster';
import swal from 'sweetalert';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class studentList extends Component {
    constructor(props) {
        super(props);
        this.showSinglestudent = this.showSinglestudent.bind(this);
        this.addSinglestudent = this.addSinglestudent.bind(this);
        this.addbulkStudents = this.addbulkStudents.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.deleteStudents = this.deleteStudents.bind(this);
        this.changePass1 = this.changePass1.bind(this);
        this.updateParamsForSearch = this.updateParamsForSearch.bind(this);
        this.showSection = this.showSection.bind(this)
        this.deleteSelectedOptions = this.deleteSelectedOptions.bind(this);
        this.state = {
            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            studentList: [],
            searchString: '',
            searchParams: [],
        }
        this.studentSearchList = []
    }
    changePass1() {
        console.log("h")
        let email = document.getElementById("email2").value;
        let pwd = document.getElementById('newpass').value;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                    $('resetPassword').modal('hide');
                                    $('body').removeClass('modal-open');
                                    $('.modal-backdrop').remove();
                                }

                            }));
                }

            });

    }
    deleteStudents(id1) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Student!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletestud";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'studentID': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Student Deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                 
                                }

                            }));
                }

            });
    }
    showSinglestudent(id2) {

        ReactDOM.render(<Singlestudentview studentid={id2} />, document.getElementById('contain1'))
    }
    addSinglestudent(id2) {

        ReactDOM.render(<Manualadd />, document.getElementById('contain1'))
    }
 
    addbulkStudents() {

        ReactDOM.render(<Bulkadd />, document.getElementById('contain1'))

    }
    editStudent(student) {

        ReactDOM.render(<EditStudents studentdata={student} />, document.getElementById('contain1'))

    }
    showSession() {

        let val = document.getElementById("unit1").value;
        this.updateParamsForSearch('unitID', val)
        this.setState({ sessionList1: [] })
        this.setState({ classList: [] })
        this.setState({ semesterList: [] })
        this.setState({ sectionList: [] })
        document.getElementById("session1").value = ''
        document.getElementById("department1").value = ''
        document.getElementById("semester1").value = ''
        document.getElementById("section1").value = ''
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }

    showDepartment() {
        let val = document.getElementById("session1").value;
        this.setState({ classList: [] })
        this.setState({ semesterList: [] })
        this.setState({ sectionList: [] })
        document.getElementById("department1").value = ''
        document.getElementById("semester1").value = ''
        document.getElementById("section1").value = ''
        this.updateParamsForSearch('sessionID', val)


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    showSemester() {
        let val = document.getElementById("department1").value;
        this.setState({ semesterList: [] })
        this.setState({ sectionList: [] })
        document.getElementById("semester1").value = ''
        document.getElementById("section1").value = ''
        this.updateParamsForSearch('departmentID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSection() {

        let val = document.getElementById("semester1").value;
        this.setState({ sectionList: [] })
        document.getElementById("section1").value = ''
        this.updateParamsForSearch('semesterID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }





    componentDidMount() {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallorgastu";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    studentList: json
                })
            }).then(() => {
                let count = Object.keys(this.state.studentList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });


    }

    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Students!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletestud";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'studentID': value })
                            })
                        })
                        swal("All Selected Students Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Students cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

// refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { unitList, sessonList1, classList, semesterList, sectionList, studentList } = this.state;
        if (this.state.searchParams.length > 0) {
            this.studentSearchList = SearchForObjectsWithParams(studentList, this.state.searchParams)
        }
        else {
            this.studentSearchList = studentList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.studentSearchList = SearchForObjectsWithName(this.studentSearchList, this.state.searchString)
        }
        return ([
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold h3">
                            List of Enrolled Students
                </span>
                        <span className="float-right"><a onClick={this.addSinglestudent}><i className="icon-plus"></i> Student Entry </a></span>
                        <span className="float-right"><a onClick={this.addbulkStudents}><i className="icon-plus"></i> Upload Students Master Data</a>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </div>
                    <div className="card-body">


                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()}>Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="" className=" form-control" id="unit1" onChange={this.showSession}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartment}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">

                                        <div className="col-md-4 form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemester} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSection}>
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>

                                            <select name="" id="section1" className=" form-control" onChange={(e) => { this.updateParamsForSearch("sectionID", e.target.value) }}>
                                                <option value="" selected>Select</option>
                                                {sectionList.map(section => (
                                                    <option value={section.sectionID}>{section.sectionName}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>


                        <hr />

                        <div className="table-responsive">
                            <table className="datatable table table-bordered">
                                <thead>
                                    <tr className="bg-light table-head-fixed">
                                        <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                        <th className="border-0">Profile Picture</th>
                                        <th className="border-0">Name</th>
                                        <th className="border-0">Roll No.</th>
                                        <th className="border-0">Contact Number</th>
                                        <th className="border-0">Email ID</th>
                                        <th className="border-0">Name of Unit</th>
                                        <th className="border-0">Class/Department</th>
                                        <th className="border-0">Semester</th>
                                        <th className="border-0">Section</th>
                                        <th className="border-0">Date of Admission</th>
                                        <th className="border-0">Current Status</th>
                                        <th className="border-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.studentSearchList.map(students => (
                                        <tr>
                                            <td className="border-0"><input type="checkbox" value={students.studentID} className="checkBoxForDeletion" /></td>
                                            <td className="border-0"><img src={global.img + students.profilePic} alt="profile picture" className="students_profile_pic" /></td>
                                            <td className="border-0">{students.studentName}</td>
                                            <td className="border-0">{students.rollNo}</td>
                                            <td className="border-0">{students.contactNo}</td>
                                            <td className="border-0" id="email1">{students.emailID}</td>
                                            <td className="border-0">{students.unitName}</td>
                                            <td className="border-0">{students.departmentName}</td>
                                            <td className="border-0">{students.semesterName}</td>
                                            <td className="border-0">{students.sectionName}</td>
                                            <td className="border-0">{students.dateOfAdmission}</td>
                                            <td className="border-0">
                                                <span className="badge badge-pill bg-light">{students.status}</span>
                                            </td>
                                            <td className="border-0">
                                                <span><a href="#" className="text-success" onClick={() => document.getElementById('email2').value=students.emailID} data-toggle="modal" data-target="#resetPassword"><i className="icon-key"></i></a></span>
                                                <span><a onClick={() => this.editStudent(students)} className="text-warning"><i className="icon-pencil"></i></a></span>
                                                <span><a onClick={() => this.showSinglestudent(students.studentID)} className="text-warning"><i className="icon-eye"></i></a></span>
                                                <span><a onClick={() => this.deleteStudents(students.studentID)} className="text-danger" ><i className="icon-trash"></i></a></span>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                {/*-modal for reset Password*/}
                <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Reset Password</span></h4>
                                    <hr />
                                    <div className="form-group">
                                        <label htmlFor className="font-weight-bold mb-0">Your email:</label>
                                        <input type="text" id="email2" className="form-control-sm" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                                        <input type="password" id="newpass" className="form-control-sm" />
                                    </div>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="submit"  onClick={(data) => this.changePass1()} className="btn btn-warning">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for reset Password */}

            </div>
        ]);


    }
}
export default studentList
