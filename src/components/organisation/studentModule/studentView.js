import React, { Component } from 'react';
import Studenttlist from './studentList'
import EditStudents from './editStudent';
import swal from 'sweetalert';
import update from 'react-addons-update';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import './studentView.css';
class studentView extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.unid);
    this.listStudents = this.listStudents.bind(this);
    this.deleteStudents = this.deleteStudents.bind(this);
    this.editStudent = this.editStudent.bind(this);
    this.state = {
      studenttview: [],
     
      isLoaded: false
    }

  }
  componentDidMount() {




    let url = global.API + "/studapi/public/api/studentprofileview";
    fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },

      body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'student_id': this.props.studentid })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json)
        this.setState({
          isLoaded: true,
          studenttview: json,

        })
        console.log(json)
      });
     


  }



  listStudents() {

    ReactDOM.render(<Studenttlist />, document.getElementById('contain1'))

  }
  editStudent(student) {

    ReactDOM.render(<EditStudents studentdata={student} />, document.getElementById('contain1'))

}
deleteStudents(id1) {
  swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover  Student!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
      .then((willDelete) => {
          if (willDelete) {
              let url = global.API + "/studapi/public/api/deletestud";
              fetch(url, {
                  method: 'POST',
                  headers: {
                      "Content-Type": "application/json",
                      "Accept": "application/json"
                  },

                  body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'studentID': id1 })
              })
                  .then((resp1) => resp1.json()
                      .then((resp) => {
                          if (resp[0]['result'] == 1) {
                              swal("Student Deleted!", {
                                  icon: "success",
                              });
                              this.listStudents();
                              this.componentDidMount();
                           
                          }

                      }));
          }

      });
}
  render() {

    var { studenttview ,} = this.state;
    console.log(studenttview)
    return (
      <div className="fadeIn animated">
        {studenttview.map(students => (
          <div className="card">
            <div className="card-header">
              <span className="font-weight-bold text-primary mb-0 h3">Student's Information</span>
              <a onClick={this.listStudents} className="float-right mt-2"><i className="icon-action-undo" /> Back to list of Students</a>
            </div>

            <div className="card-body">
              <h2>
                <img id="blah" src={global.img + students.profilePic} alt="Student's Photo" className="students-view-photo" />
                <span className="font-weight-bold">{students.studentName}</span>
                <span className="float-right small"><a href="#" className="btn  btn-primary text-white mt-4" data-toggle="modal" data-target="#resetPassword"> Reset Password</a></span>
              </h2>
              <hr />
              {/*
              <div className="row">
                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-user-follow" />
                      </div>
                      <div className="h4 mb-0">75%</div>
                      <small className="text-muted text-uppercase font-weight-bold">Attendance</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-success" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-badge" />
                      </div>
                      <div className="h4 mb-0">200</div>
                      <small className="text-muted text-uppercase font-weight-bold">Exam Taken</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-warning" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-pie-chart" />
                      </div>
                      <div className="h4 mb-0">28%</div>
                      <small className="text-muted text-uppercase font-weight-bold">Overall Performance</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-md-3">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-speedometer" />
                      </div>
                      <div className="h4 mb-0">240</div>
                      <small className="text-muted text-uppercase font-weight-bold">View Guidesnotes</small>
                      <div className="progress progress-xs mt-3 mb-0">
                        <div className="progress-bar bg-danger" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              */}
              <div className="border pt-2 pb-2 pl-3 pr-3">
                <span className=" font-weight-bold h3 border-title">Details of Student:</span>
                <div className="row mt-4">
                  <div className="col-md-6">
                    <span><b>Students ID:</b>&nbsp;{students.studentID}</span><br />
                    <span><b>Name:</b>&nbsp;{students.studentName} </span><br />
                    <span><b>Contact Number:</b>&nbsp;{students.contactNo} </span><br />
                    <span><b>Contact Email:</b>&nbsp;{students.emailID} </span><br />
                    <span><b>Session:</b>&nbsp;{students.session} </span> <br />
                      <span><b>Class/Department:</b>&nbsp;{students.departmentName}</span><br />
                     <span><b>Semester:</b>&nbsp;{students.semesterName}</span> <br />
                    <span><b>Roll:</b>&nbsp;{students.rollNo} </span><br />
                    
                    <span><b>Name of the Institute:</b>&nbsp;{students.unitName} </span><br />
                    <span><b>Date of Admission:</b>&nbsp;{students.dateOfAdmission} </span><br />
                    <span><b>Address:</b>&nbsp;{students.address1} </span><br />
                    <span><b>Address:</b>&nbsp;{students.address2} </span><br />
                    <span><b>City:</b>&nbsp;{students.city} </span><br />
                    <span><b>Country:</b>&nbsp;{students.country} </span><br />
                    <span><b>PIN:</b>&nbsp;{students.pinCode} </span><br />
                  </div>
                  <div className="col-md-6">
                    <span><b>Father's Name:</b>&nbsp;{students.fatherName} </span><br />
                    <span><b>Father's Contact Number:</b>&nbsp;{students.fatherContactNo} </span><br />
                    <span><b>Mother's Mail ID:</b>&nbsp;{students.motherEmailID} </span><br />
                    <span><b>Mother's Name:</b>&nbsp;{students.motherName} </span><br />
                    <span><b>Mother's Contact Number:</b>&nbsp;{students.motherContactNo} </span><br />
                    <span><b>Guardian''s Mail ID:</b>&nbsp;{students.guardianEmail} </span><br />
                    <span><b>Guardian's Name:</b>&nbsp;{students.guardianName} </span><br />
                    <span><b>Guardian's Contact Number:</b>&nbsp;{students.guardianContactNo} </span><br />
                    <span><b>Guardian's Mail ID:</b>&nbsp;{students.guardianEmail} </span><br />
                  </div>
                </div>
              </div>

            </div>
            <div className="card-footer">
              <div className="float-right mt-4 mb-2">
                <a onClick={() => this.editStudent(students)} className=" btn text-dark btn-warning"><i className="icon-pencil"></i> Edit Student</a>
                <a onClick={() => this.deleteStudents(students.studentID)} className="btn-danger text-white btn"><i className="icon-trash" ></i> Delete Student</a>
              </div>
            </div>


          </div>
        ))}
     
      </div>


    );


  }
}
export default studentView