import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './addStudentMaster.css';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert'

import Manualentry from './addStudent'
import Liststudents from './studentList'

class addStudentMaster extends Component {

    constructor(props) {
        super(props);
        this.studentsList = this.studentsList.bind(this);
        this.manualEntry = this.manualEntry.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSection = this.showSection.bind(this);




        this.state = {

            sectionList: [],
            date_of_admission: "",
            student_created_on: new Date().toISOString().slice(0, 10),

            sections_i_d: 0,
            file1: null,
            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: [],
            ii: 0




        }

    }
    resetFields() {

        this.setState({
            file1: "",
            unitList: "",
            sessonList1: "",
            classList: "",
            semesterList: "",

        })


    }
    studentsList() {

        ReactDOM.render(<Liststudents />, document.getElementById('contain1'));
    }

    manualEntry() {

        ReactDOM.render(<Manualentry />, document.getElementById('contain1'));
    }

    showSession() {

        let val = document.getElementById("unit1").value;

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }



    showDepartment() {
        let val = document.getElementById("session1").value;



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemester() {

        let val = document.getElementById("department1").value;


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSection() {

        let val = document.getElementById("semester1").value;


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }




    submit() {
        var i = 0;
        if (i < 6) {
            if (document.getElementById("unit1").value == "") {
                document.getElementById("unit1Error").innerHTML = "*Please fill this field";
                document.getElementById("unit1").focus();
                document.getElementById("unit1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit1").value != "") {
                document.getElementById("unit1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit1").style.borderColor = "";
            }
            if (document.getElementById("semester1").value == "") {
                document.getElementById("semester1Error").innerHTML = "*Please fill this field";
                document.getElementById("semester1").focus();
                document.getElementById("semester1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester1").value != "") {
                document.getElementById("semester1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("semester1").style.borderColor = "";
            }

            if (document.getElementById("session1").value == "") {
                document.getElementById("session1Error").innerHTML = "*Please fill this field";
                document.getElementById("session1").focus();
                document.getElementById("session1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session1").value != "") {
                document.getElementById("session1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("session1").style.borderColor = "";
            }
            if (document.getElementById("sections_i_d").value == "") {
                document.getElementById("sections_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("sections_i_d").focus();
                document.getElementById("sections_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("sections_i_d").value != "") {
                document.getElementById("sections_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("sections_i_d").style.borderColor = "";
            }
            if (document.getElementById("department1").value == "") {
                document.getElementById("department1Error").innerHTML = "*Please fill this field";
                document.getElementById("department1").focus();
                document.getElementById("department1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department1").value != "") {
                document.getElementById("department1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department1").style.borderColor = "";
            }
            if (document.getElementById("file").value == "") {
                document.getElementById("fileError").innerHTML = "*Please chose a file";
                document.getElementById("file").focus();
                document.getElementById("file").style.borderColor = "#FF0000";
            }

            if (document.getElementById("file").value != "") {
                document.getElementById("fileError").innerHTML = "";
                i = i + 1;
                document.getElementById("file").style.borderColor = "";
            }
            if (i == 6) {
                console.log(i);
                this.state.ii = 6;
            }
        }
        if (this.state.ii == 6) {
            console.log(this.state);
            let url = global.API + "/studapi/public/api/addstudentbulk";
            let data = this.state;
            console.log(this.state)
            const fd = new FormData();
            //formData.append('file', this.state.org_logo);
            $.each(data, function (key, value) {
                fd.append(key, value);
            })
            console.log(fd);
            fetch(url, {
                method: 'POST',

                body: fd
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Student Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        this.studentsList();

                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Student Not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                        this.resetFields();
                    }
                })
            })
            console.log(data);
        }
    }

    componentDidMount() {
        console.log(new Date().toISOString().slice(0, 10))
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };



        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }



    render() {
        var { unitList, sessonList1, classList, semesterList, sectionList } = this.state;
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold h3">Upload Student's Master Data</span>
                        <span className="float-right"><a onClick={this.manualEntry} className="btn btn-primary text-white">Manual Entry</a></span>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                    <select name="" id="unit1" className="form-control" onChange={this.showSession}>
                                        <option value="" selected>Select</option>
                                        {unitList.map(unit => (
                                            <option value={unit.unitID}>{unit.unitName}</option>

                                        ))}
                                    </select>
                                    <span id="unit1Error" class="text-danger font-weight-bold"></span>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">

                                    <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span> :</label>
                                    <select name="" id="session1" className="form-control" onChange={this.showDepartment}>
                                        <option value="" selected>Select</option>
                                        {sessonList1.map(session => (
                                            <option value={session.sessionID}>{session.session}</option>

                                        ))}
                                    </select>
                                    <span id="session1Error" class="text-danger font-weight-bold"></span>

                                </div>
                            </div>
                        </div>
                        <div className="row">

                            <div className="col-md-4">

                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">className/Department <span className="text-danger"><strong>*</strong></span> :</label>
                                    <select name="" id="department1" onChange={this.showSemester} className="form-control">
                                        <option value="" selected>Select</option>
                                        {classList.map(classes => (
                                            <option value={classes.departmentID}>{classes.departmentName}</option>

                                        ))}
                                    </select>
                                    <span id="department1Error" class="text-danger font-weight-bold"></span>

                                </div>
                            </div>

                            <div className="col-md-4">

                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span> :</label>
                                    <select name="" id="semester1" className="form-control" onChange={this.showSection}>
                                        <option value="" selected>Select</option>
                                        {semesterList.map(semester => (
                                            <option value={semester.semesterID}>{semester.semesterName}</option>

                                        ))}
                                    </select>
                                    <span id="semester1Error" class="text-danger font-weight-bold"></span>

                                </div>
                            </div>
                            <div className="col-md-4">

                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Section <span className="text-danger"><strong>*</strong></span> :</label>
                                    <select name="sections_i_d" id="sections_i_d" value={this.state.sections_i_d} onChange={(data) => { this.setState({ sections_i_d: data.target.value }) }} className="form-control">
                                        <option value="" selected>Select</option>
                                        {sectionList.map(section => (
                                            <option value={section.sectionID}>{section.sectionName}</option>

                                        ))}

                                    </select>
                                    <span id="sections_i_dError" class="text-danger font-weight-bold"></span>

                                </div>
                            </div>
                        </div>
                        <hr />
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse alert alert-warning" id="helpArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Duplicate emails are not allowed.
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Provide Working Email ID.
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Date Format should follow: YYYY-MM-DD.
                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">

                                        <div className="col-md-6">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  User our provided sample file for uploading data.
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Please fill all the mandatory fields
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*-/filter*/}
                        <div className="form-group files mt-3">
                            <span className="text-right">
                                <a href="/students.csv" className="float-right"><i className="icon-cloud-download"></i> Our Sample format</a>
                            </span>


                            <label className="font-weight-bold">Upload Your File <span className="text-danger"><strong>*</strong></span> :  <span className="small">.XLS, .CSV File Supported only</span>  <span className="bg-warning p-1 alert " data-toggle="collapse" data-target="#helpArea" aria-expanded="false" aria-controls="helpArea"  ><i class="icon-question font-weight-bold text-bule"></i> help</span></label>

                            <input type="file" id="file" onChange={(data) => { this.setState({ file1: data.target.files[0] }) }} className="form-control" />
                            <span id="fileError" class="text-danger font-weight-bold"></span>
                        </div>
                        <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style={{ width: "75%" }}></div>
                        </div>
                        <hr />
                        {/* <div className="table-responsive">
                                <table className="datatable table table-bordered">
                                    <thead>
                                        <tr className="bg-light table-head-fixed">
                                            <th className="border-0"><input type="checkbox" onclick="toggle(this);"/></th>
                                            <th className="border-0">ID</th>
                                            <th className="border-0">Profile Picture</th>
                                            <th className="border-0">Name</th>
                                            <th className="border-0">Contact Number</th>
                                            <th className="border-0">Email ID</th>
                                            <th className="border-0">Name of Institution</th>
                                            <th className="border-0">className/Department</th>
                                            <th className="border-0">Semester</th>
                                            <th className="border-0">Father's Name</th>
                                            <th className="border-0">Father's Contact Number</th>
                                            <th className="border-0">Father's Mail ID</th>
                                            <th className="border-0">Mother's Name</th>
                                            <th className="border-0">Mother's Contact Number</th>
                                            <th className="border-0">Mother's Mail ID</th>
                                            <th className="border-0">Guardian''s Name</th>
                                            <th className="border-0">Guardian's Contact Number</th>
                                            <th className="border-0">Guardian's Mail ID</th>
                                            <th className="border-0">Address</th>
                                            <th className="border-0">City</th>
                                            <th className="border-0">State</th>
                                            <th className="border-0">PIN Code</th>
                                            <th className="border-0">Country</th>
                                            <th className="border-0">Date of Admission</th>
                                            <th className="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="border-0"><input type="checkbox" name="" id=""/></td>
                                            <td className="border-0">18705516014</td>
                                            <td className="border-0"><img src="../img/avatars/4.jpg" alt="profile picture" className="students_profile_pic"/></td>
                                            <td className="border-0">Sumanth Sah</td>
                                            <td className="border-0">9876543210</td>
                                            <td className="border-0">example@xyz.com</td>
                                            <td className="border-0">Techno International New Town</td>
                                            <td className="border-0">Mechanical Engineering</td>
                                            <td className="border-0">1st Semester</td>
                                            <td className="border-0">A shah</td>
                                            <td className="border-0">9876543210</td>
                                            <td className="border-0">Example@dmoain.com</td>
                                            <td className="border-0">S Sah</td>
                                            <td className="border-0">9876543210</td>
                                            <td className="border-0">Example@dmoain.com</td>
                                            <td className="border-0">A shah</td>
                                            <td className="border-0">9876543210</td>
                                            <td className="border-0">Example@dmoain.com</td>
                                            <td className="border-0">Kestopur</td>
                                            <td className="border-0">Kolkata</td>
                                            <td className="border-0">West Bengal</td>
                                            <td className="border-0">700102</td>
                                            <td className="border-0">India</td>
                                            <td className="border-0">26-11-2019</td>
                                            <td className="border-0">
                                                <span><a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash"></i></a></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
        */}
                        <button type="submit" onClick={() => { this.submit() }} className="btn btn-success mt-3 float-right"><i className="icon-check"></i> Save and Proceed</button>

                    </div>
                </div>

                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>




        );


    }
}
export default addStudentMaster