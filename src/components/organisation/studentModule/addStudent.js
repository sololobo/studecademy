import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert'

import Bulkentry from './addStudentMaster'
import Liststudents from './studentList'
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';


class addStudent extends Component {

    constructor(props) {
        super(props);
        this.bulkEntry = this.bulkEntry.bind(this);
        this.studentsList = this.studentsList.bind(this);
        this.showUnit = this.showUnit.bind(this);
        this.showSessions = this.showSessions.bind(this);
        this.showDeperments = this.showDeperments.bind(this);
        this.showSemesters = this.showSemesters.bind(this);
        this.resetFields = this.resetFields.bind(this);

        console.log("add students")
        this.state = {
            sectionList: [],

            roll_no: "",
            sections_i_d: 0,
            student_name: "",
            contact_no: "",
            email_i_d: "",
            gender: "",
            date_of_birth: "",
            profile_pic: "",
            date_of_admission: "",
            father_name: "",
            father_email_i_d: "",
            father_contact_no: "",
            mother_name: "",
            mother_email_i_d: "",
            mother_contact_no: "",
            guardian_name: "",
            guardian_email: "",
            guardian_contact_no: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            country: "",
            pin_code: "",
            status: "",
            ii: 0,
            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: []




        }
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }

      selectRegion (val) {
        this.setState({ state: val });
      }
    resetFields() {

        this.setState({
            organisation_i_d: Cookies.get('orgid'),

            roll_no: "",
            sections_i_d: 0,
            student_name: "",
            contact_no: "",
            email_i_d: "",
            gender: "",
            date_of_birth: "",
            profile_pic: "",
            date_of_admission: "",
            father_name: "",
            father_email_i_d: "",
            father_contact_no: "",
            mother_name: "",
            mother_email_i_d: "",
            mother_contact_no: "",
            guardian_name: "",
            guardian_email: "",
            guardian_contact_no: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            country: "",
            pin_code: "",
            status: "",


        })


    }
    bulkEntry() {

        ReactDOM.render(<Bulkentry />, document.getElementById('contain1'));
    }

    studentsList() {

        ReactDOM.render(<Liststudents />, document.getElementById('contain1'));
    }



    showUnit() {

        let val = document.getElementById("unit1").value;

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }



    showSessions() {
        let val = document.getElementById("session1").value;



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }


    showDeperments() {

        let val = document.getElementById("department1").value;


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSemesters() {

        let val = document.getElementById("semester1").value;


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }



    submit() {
        var pin_code = document.getElementById("pin_code").value;
        var email_i_d = document.getElementById("email_i_d").value;
        var guardian_email_id = document.getElementById("guardian_email_id").value;
        var contact_no = document.getElementById("contact_no").value;
        var guardian_contact_no = document.getElementById("guardian_contact_no").value;
        var i = 0;
        var pinCheck = /^[0-9]{6}$/;
        //var websiteCheck = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,80}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
        var telCheck = /^[0-9]{10}$/;
        var emailCheck = /^[A-Za-z0-9_.]{3,60}@[A-Za-z0-9]{3,60}[.]{1}[A-Za-z.]{2,6}$/;

        if (i < 24) {
            if (document.getElementById("unit1").value == "") {
                document.getElementById("unit1Error").innerHTML = "*Please fill this field";
                document.getElementById("unit1").focus();
                document.getElementById("unit1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit1").value != "") {
                document.getElementById("unit1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit1").style.borderColor = "";
            }
            if (document.getElementById("session1").value == "") {
                document.getElementById("session1Error").innerHTML = "*Please fill this field";
                document.getElementById("session1").focus();
                document.getElementById("session1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session1").value != "") {
                    document.getElementById("session1Error").innerHTML = "";
                    i = i + 1;
                    document.getElementById("session1").style.borderColor = "";

            }

            if (document.getElementById("department1").value == "") {
                document.getElementById("department1Error").innerHTML = "*Please fill this field";
                document.getElementById("department1").focus();
                document.getElementById("department1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department1").value != "") {
                document.getElementById("department1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department1").style.borderColor = "";
            }

            if (document.getElementById("semester1").value == "") {
                document.getElementById("semester1Error").innerHTML = "*Please fill this field";
                document.getElementById("semester1").focus();
                document.getElementById("semester1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester1").value != "") {
                document.getElementById("semester1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("semester1").style.borderColor = "";
            }


            if (document.getElementById("sections_i_d").value == "") {
                document.getElementById("sections_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("sections_i_d").focus();
                document.getElementById("sections_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("sections_i_d").value != "") {

                    document.getElementById("sections_i_dError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("sections_i_d").style.borderColor = "";
                }


            if (document.getElementById("date_of_admission").value == "") {
                document.getElementById("date_of_admissionError").innerHTML = "*Please fill this field";
                document.getElementById("date_of_admission").focus();
                document.getElementById("date_of_admission").style.borderColor = "#FF0000";
            }

            if (document.getElementById("date_of_admission").value != "") {
                document.getElementById("date_of_admissionError").innerHTML = "";
                i = i + 1;
                document.getElementById("date_of_admission").style.borderColor = "";
            }
            if (document.getElementById("student_name").value == "") {
                document.getElementById("student_nameError").innerHTML = "*Please fill this field";
                document.getElementById("student_name").focus();
                document.getElementById("student_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("student_name").value != "") {
                document.getElementById("student_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("student_name").style.borderColor = "";
            }

            if (document.getElementById("roll_no").value == "") {
                document.getElementById("roll_noError").innerHTML = "*Please fill this field";
                document.getElementById("roll_no").focus();
                document.getElementById("roll_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("roll_no").value != "") {
                document.getElementById("roll_noError").innerHTML = "";
                i = i + 1;
                document.getElementById("roll_no").style.borderColor = "";
            }
            if (document.getElementById("date_of_birth").value == "") {
                document.getElementById("date_of_birthError").innerHTML = "*Please fill this field";
                document.getElementById("date_of_birth").focus();
                document.getElementById("date_of_birth").style.borderColor = "#FF0000";
            }

            if (document.getElementById("date_of_birth").value != "") {
                document.getElementById("date_of_birthError").innerHTML = "";
                i = i + 1;
                document.getElementById("date_of_birth").style.borderColor = "";
            }

            if (document.getElementById("gender").value == "") {
                document.getElementById("genderError").innerHTML = "*Please fill this field";
                document.getElementById("gender").focus();
                document.getElementById("gender").style.borderColor = "#FF0000";
            }

            if (document.getElementById("gender").value != "") {
                document.getElementById("genderError").innerHTML = "";
                i = i + 1;
                document.getElementById("gender").style.borderColor = "";
            }
            if (document.getElementById("contact_no").value == "") {
                document.getElementById("contact_noError").innerHTML = "*Please fill this field";
                document.getElementById("contact_no").focus();
                document.getElementById("contact_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contact_no").value != "") {
                if (!telCheck.test(contact_no)) {
                    document.getElementById("contact_noError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("contact_no").focus();
                    document.getElementById("contact_no").style.borderColor = "#FF0000";
                }

                if (telCheck.test(contact_no)) {
                    document.getElementById("contact_noError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("contact_no").style.borderColor = "";
                }
            }
            if (document.getElementById("email_i_d").value == "") {
                document.getElementById("email_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("email_i_d").focus();
                document.getElementById("email_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("email_i_d").value != "") {
                if (!emailCheck.test(email_i_d)) {
                    document.getElementById("email_i_dError").innerHTML = "*Invalid Email Id";
                    document.getElementById("email_i_d").focus();
                    document.getElementById("email_i_d").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(email_i_d)) {
                    document.getElementById("email_i_dError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("email_i_d").style.borderColor = "";
                }
            }
            if (document.getElementById("mother_name").value == "") {
                document.getElementById("mother_nameError").innerHTML = "*Please fill this field";
                document.getElementById("mother_name").focus();
                document.getElementById("mother_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("mother_name").value != "") {
                document.getElementById("mother_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("mother_name").style.borderColor = "";
            }
             if (document.getElementById("father_name").value == "") {
                document.getElementById("father_nameError").innerHTML = "*Please fill this field";
                document.getElementById("father_name").focus();
                document.getElementById("father_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("father_name").value != "") {
                document.getElementById("father_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("father_name").style.borderColor = "";
            }
            if (document.getElementById("guardian_name").value == "") {
                document.getElementById("guardian_nameError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_name").focus();
                document.getElementById("guardian_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_name").value != "") {
                document.getElementById("guardian_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("guardian_name").style.borderColor = "";
            }

            if (document.getElementById("guardian_contact_no").value == "") {
                document.getElementById("guardian_contact_noError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_contact_no").focus();
                document.getElementById("guardian_contact_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_contact_no").value != "") {
                if (!telCheck.test(guardian_contact_no)) {
                    document.getElementById("guardian_contact_noError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("guardian_contact_no").focus();
                    document.getElementById("guardian_contact_no").style.borderColor = "#FF0000";
                }

                if (telCheck.test(guardian_contact_no)) {
                    document.getElementById("guardian_contact_noError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("guardian_contact_no").style.borderColor = "";
                }
            }

            if (document.getElementById("guardian_email_id").value == "") {
                document.getElementById("guardian_email_idError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_email_id").focus();
                document.getElementById("guardian_email_id").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_email_id").value != "") {
                if (!emailCheck.test(guardian_email_id)) {
                    document.getElementById("guardian_email_idError").innerHTML = "*Invalid Email Id";
                    document.getElementById("guardian_email_id").focus();
                    document.getElementById("guardian_email_id").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(guardian_email_id)) {
                    document.getElementById("guardian_email_idError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("guardian_email_id").style.borderColor = "";
                }
            }
            if (document.getElementById("address1").value == "") {
                document.getElementById("address1Error").innerHTML = "*Please fill this field";
                document.getElementById("address1").focus();
                document.getElementById("address1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("address1").value != "") {
                document.getElementById("address1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("address1").style.borderColor = "";
            }
            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }



            if (document.getElementById("country").value == "") {
                document.getElementById("countryError").innerHTML = "*Please fill this field";
                document.getElementById("country").focus();
                document.getElementById("country").style.borderColor = "#FF0000";
            }

            if (document.getElementById("country").value != "") {
                document.getElementById("countryError").innerHTML = "";
                i = i + 1;
                document.getElementById("country").style.borderColor = "";
            }

            if (document.getElementById("state").value == "") {
                document.getElementById("stateError").innerHTML = "*Please fill this field";
                document.getElementById("state").focus();
                document.getElementById("state").style.borderColor = "#FF0000";
            }

            if (document.getElementById("state").value != "") {
                document.getElementById("stateError").innerHTML = "";
                i = i + 1;
                document.getElementById("state").style.borderColor = "";
            }

            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }

            if (document.getElementById("pin_code").value == "") {
                document.getElementById("pin_codeError").innerHTML = "*Please fill this field";
                document.getElementById("pin_code").focus();
                document.getElementById("pin_code").style.borderColor = "#FF0000";
            }


            if (document.getElementById("pin_code").value != "") {
                if (!pinCheck.test(pin_code)) {
                    document.getElementById("pin_codeError").innerHTML = "*Invalid PIN";
                    document.getElementById("pin_code").focus();
                    document.getElementById("pin_code").style.borderColor = "#FF0000";
                }

                if (pinCheck.test(pin_code)) {
                    document.getElementById("pin_codeError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("pin_code").style.borderColor = "";
                }
            }


            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 24) {
                console.log(i);
                this.state.ii = 24;
            }
        }
        if (this.state.ii == 24) {

        let url = window.API + "/studapi/public/api/addstudent";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        fetch(url, {
            method: 'POST',
            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp[0]['result'])
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Student is added Sucessfully",
                        icon: "success",
                        button: "OK",

                     });

                    this.resetFields();
                }
             else if (resp[0]['result'] == 2) {
                    swal({
                        title: "Error!",
                        text: "Student is already added in the organisation",
                        icon: "warning",
                        button: "OK",

                    });
                    this.resetFields();
                }


                else {
                    swal({
                        title: "Error!",
                        text: "This email cannot be added as a student!",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })

    }}

    submitandprroceed() {
        var pin_code = document.getElementById("pin_code").value;
        var email_i_d = document.getElementById("email_i_d").value;
        var guardian_email_id = document.getElementById("guardian_email_id").value;
        var contact_no = document.getElementById("contact_no").value;
        var guardian_contact_no = document.getElementById("guardian_contact_no").value;
        var i = 0;
        var pinCheck = /^[0-9]{6}$/;
        //var websiteCheck = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,80}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
        var telCheck = /^[0-9]{10}$/;
        var emailCheck = /^[A-Za-z0-9_.]{2,60}@[A-Za-z0-9]{2,60}[.]{1}[A-Za-z.]{2,6}$/;

        if (i < 24) {
            if (document.getElementById("unit1").value == "") {
                document.getElementById("unit1Error").innerHTML = "*Please fill this field";
                document.getElementById("unit1").focus();
                document.getElementById("unit1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit1").value != "") {
                document.getElementById("unit1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit1").style.borderColor = "";
            }
            if (document.getElementById("session1").value == "") {
                document.getElementById("session1Error").innerHTML = "*Please fill this field";
                document.getElementById("session1").focus();
                document.getElementById("session1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session1").value != "") {
                    document.getElementById("session1Error").innerHTML = "";
                    i = i + 1;
                    document.getElementById("session1").style.borderColor = "";

            }



            if (document.getElementById("department1").value == "") {
                document.getElementById("department1Error").innerHTML = "*Please fill this field";
                document.getElementById("department1").focus();
                document.getElementById("department1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department1").value != "") {
                document.getElementById("department1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department1").style.borderColor = "";
            }

            if (document.getElementById("semester1").value == "") {
                document.getElementById("semester1Error").innerHTML = "*Please fill this field";
                document.getElementById("semester1").focus();
                document.getElementById("semester1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester1").value != "") {
                document.getElementById("semester1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("semester1").style.borderColor = "";
            }


            if (document.getElementById("sections_i_d").value == "") {
                document.getElementById("sections_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("sections_i_d").focus();
                document.getElementById("sections_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("sections_i_d").value != "") {

                    document.getElementById("sections_i_dError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("sections_i_d").style.borderColor = "";
                }


            if (document.getElementById("date_of_admission").value == "") {
                document.getElementById("date_of_admissionError").innerHTML = "*Please fill this field";
                document.getElementById("date_of_admission").focus();
                document.getElementById("date_of_admission").style.borderColor = "#FF0000";
            }

            if (document.getElementById("date_of_admission").value != "") {
                document.getElementById("date_of_admissionError").innerHTML = "";
                i = i + 1;
                document.getElementById("date_of_admission").style.borderColor = "";
            }
            if (document.getElementById("student_name").value == "") {
                document.getElementById("student_nameError").innerHTML = "*Please fill this field";
                document.getElementById("student_name").focus();
                document.getElementById("student_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("student_name").value != "") {
                document.getElementById("student_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("student_name").style.borderColor = "";
            }

            if (document.getElementById("roll_no").value == "") {
                document.getElementById("roll_noError").innerHTML = "*Please fill this field";
                document.getElementById("roll_no").focus();
                document.getElementById("roll_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("roll_no").value != "") {
                document.getElementById("roll_noError").innerHTML = "";
                i = i + 1;
                document.getElementById("roll_no").style.borderColor = "";
            }
            if (document.getElementById("date_of_birth").value == "") {
                document.getElementById("date_of_birthError").innerHTML = "*Please fill this field";
                document.getElementById("date_of_birth").focus();
                document.getElementById("date_of_birth").style.borderColor = "#FF0000";
            }

            if (document.getElementById("date_of_birth").value != "") {
                document.getElementById("date_of_birthError").innerHTML = "";
                i = i + 1;
                document.getElementById("date_of_birth").style.borderColor = "";
            }

            if (document.getElementById("gender").value == "") {
                document.getElementById("genderError").innerHTML = "*Please fill this field";
                document.getElementById("gender").focus();
                document.getElementById("gender").style.borderColor = "#FF0000";
            }

            if (document.getElementById("gender").value != "") {
                document.getElementById("genderError").innerHTML = "";
                i = i + 1;
                document.getElementById("gender").style.borderColor = "";
            }
            if (document.getElementById("contact_no").value == "") {
                document.getElementById("contact_noError").innerHTML = "*Please fill this field";
                document.getElementById("contact_no").focus();
                document.getElementById("contact_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contact_no").value != "") {
                if (!telCheck.test(contact_no)) {
                    document.getElementById("contact_noError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("contact_no").focus();
                    document.getElementById("contact_no").style.borderColor = "#FF0000";
                }

                if (telCheck.test(contact_no)) {
                    document.getElementById("contact_noError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("contact_no").style.borderColor = "";
                }
            }
            if (document.getElementById("email_i_d").value == "") {
                document.getElementById("email_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("email_i_d").focus();
                document.getElementById("email_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("email_i_d").value != "") {
                if (!emailCheck.test(email_i_d)) {
                    document.getElementById("email_i_dError").innerHTML = "*Invalid Email Id";
                    document.getElementById("email_i_d").focus();
                    document.getElementById("email_i_d").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(email_i_d)) {
                    document.getElementById("email_i_dError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("email_i_d").style.borderColor = "";
                }
            }
            if (document.getElementById("mother_name").value == "") {
                document.getElementById("mother_nameError").innerHTML = "*Please fill this field";
                document.getElementById("mother_name").focus();
                document.getElementById("mother_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("mother_name").value != "") {
                document.getElementById("mother_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("mother_name").style.borderColor = "";
            }
             if (document.getElementById("father_name").value == "") {
                document.getElementById("father_nameError").innerHTML = "*Please fill this field";
                document.getElementById("father_name").focus();
                document.getElementById("father_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("father_name").value != "") {
                document.getElementById("father_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("father_name").style.borderColor = "";
            }
            if (document.getElementById("guardian_name").value == "") {
                document.getElementById("guardian_nameError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_name").focus();
                document.getElementById("guardian_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_name").value != "") {
                document.getElementById("guardian_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("guardian_name").style.borderColor = "";
            }
            if (document.getElementById("guardian_contact_no").value == "") {
                document.getElementById("guardian_contact_noError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_contact_no").focus();
                document.getElementById("guardian_contact_no").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_contact_no").value != "") {
                if (!telCheck.test(guardian_contact_no)) {
                    document.getElementById("guardian_contact_noError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("guardian_contact_no").focus();
                    document.getElementById("guardian_contact_no").style.borderColor = "#FF0000";
                }

                if (telCheck.test(guardian_contact_no)) {
                    document.getElementById("guardian_contact_noError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("guardian_contact_no").style.borderColor = "";
                }
            }

            if (document.getElementById("guardian_email_id").value == "") {
                document.getElementById("guardian_email_idError").innerHTML = "*Please fill this field";
                document.getElementById("guardian_email_id").focus();
                document.getElementById("guardian_email_id").style.borderColor = "#FF0000";
            }

            if (document.getElementById("guardian_email_id").value != "") {
                if (!emailCheck.test(guardian_email_id)) {
                    document.getElementById("guardian_email_idError").innerHTML = "*Invalid Email Id";
                    document.getElementById("guardian_email_id").focus();
                    document.getElementById("guardian_email_id").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(guardian_email_id)) {
                    document.getElementById("guardian_email_idError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("guardian_email_id").style.borderColor = "";
                }
            }
            if (document.getElementById("address1").value == "") {
                document.getElementById("address1Error").innerHTML = "*Please fill this field";
                document.getElementById("address1").focus();
                document.getElementById("address1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("address1").value != "") {
                document.getElementById("address1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("address1").style.borderColor = "";
            }
            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }



            if (document.getElementById("country").value == "") {
                document.getElementById("countryError").innerHTML = "*Please fill this field";
                document.getElementById("country").focus();
                document.getElementById("country").style.borderColor = "#FF0000";
            }

            if (document.getElementById("country").value != "") {
                document.getElementById("countryError").innerHTML = "";
                i = i + 1;
                document.getElementById("country").style.borderColor = "";
            }

            if (document.getElementById("state").value == "") {
                document.getElementById("stateError").innerHTML = "*Please fill this field";
                document.getElementById("state").focus();
                document.getElementById("state").style.borderColor = "#FF0000";
            }

            if (document.getElementById("state").value != "") {
                document.getElementById("stateError").innerHTML = "";
                i = i + 1;
                document.getElementById("state").style.borderColor = "";
            }

            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }

            if (document.getElementById("pin_code").value == "") {
                document.getElementById("pin_codeError").innerHTML = "*Please fill this field";
                document.getElementById("pin_code").focus();
                document.getElementById("pin_code").style.borderColor = "#FF0000";
            }


            if (document.getElementById("pin_code").value != "") {
                if (!pinCheck.test(pin_code)) {
                    document.getElementById("pin_codeError").innerHTML = "*Invalid PIN";
                    document.getElementById("pin_code").focus();
                    document.getElementById("pin_code").style.borderColor = "#FF0000";
                }

                if (pinCheck.test(pin_code)) {
                    document.getElementById("pin_codeError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("pin_code").style.borderColor = "";
                }
            }


            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 24) {
                console.log(i);
                this.state.ii = 24;
            }
        }
        if (this.state.ii == 24) {

        console.log(this.state);
        let url = window.API + "/studapi/public/api/addstudent";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',
            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                if (resp[0]['result'] === 1) {
                    swal({
                        title: "Done!",
                        text: "Student is added Sucessfully",
                        icon: "success",
                        button: "OK",

                    });

                    this.studentsList();
                }
                else if (resp[0]['result'] == 2) {
                    swal({
                        title: "Error!",
                        text: "Student is already added in the organisation",
                        icon: "warning",
                        button: "OK",

                    });
                    this.resetFields();
                }

                else {
                    swal({
                        title: "Error!",
                        text: "This email cannot be added as a student!",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })

    }}


    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };



        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }


    render() {
        var { unitList, sessonList1, classList, semesterList, sectionList } = this.state;
        return (


            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Student's Entry</span>
                        <span className="float-right"><a onClick={this.bulkEntry} className="btn btn-primary text-white"><i className="icon-docs"></i> Add Multiple Entry</a></span>
                    </div>
                    <div className="card-body">
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 text-success border-title">Academic Details:</span>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Unit <span className="text-danger"><strong>*</strong></span> :</label>
                                       <select name="" id="unit1" className="form-control bg-white " onChange={this.showUnit}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                       </select>
                                       <span id="unit1Error" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                       <select name="" id="semester1" className="form-control bg-white " onChange={this.showSemesters}>
                                            <option value="" selected>Select</option>
                                            {semesterList.map(semester => (
                                                <option value={semester.semesterID}>{semester.semesterName}</option>

                                            ))}
                                       </select>
                                       <span id="semester1Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">

                                        <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span>:</label>
                                       <select name="" id="session1" className="form-control bg-white " onChange={this.showSessions}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                       </select>
                                       <span id="session1Error" class="text-danger font-weight-bold"></span>


                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Section <span className="text-danger"><strong>*</strong></span>:</label>
                                       <select id="sections_i_d" name="sections_i_d" value={this.state.sections_i_d} onChange={(data) => { this.setState({ sections_i_d: data.target.value }) }} className="form-control bg-white ">
                                            <option value="" selected>Select</option>
                                            {sectionList.map(section => (
                                                <option value={section.sectionID}>{section.sectionName}</option>

                                            ))}

                                       </select>
                                       <span id="sections_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">className/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                       <select name="" id="department1" onChange={this.showDeperments} className="form-control bg-white ">
                                            <option value="" selected>Select</option>
                                            {classList.map(classes => (
                                                <option value={classes.departmentID}>{classes.departmentName}</option>

                                            ))}
                                       </select>
                                       <span id="department1Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Date of Admission <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="date" id="date_of_admission" name="date_of_admission" value={this.state.date_of_admission} onChange={(data) => { this.setState({ date_of_admission: data.target.value }) }} className="form-control bg-white " />
                                         <span id="date_of_admissionError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-3"></div>
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 text-success border-title">Canditade Details:</span>
                            <div className="row">
                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="student_name" name="student_name" value={this.state.student_name} onChange={(data) => { this.setState({ student_name: data.target.value }) }} className="form-control bg-white " />
                                         <span id="student_nameError" class="text-danger font-weight-bold"></span>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="contact_no" name="contact_no" value={this.state.contact_no} onChange={(data) => { this.setState({ contact_no: data.target.value }) }} className="form-control bg-white " />
                                         <span id="contact_noError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Gender <span className="text-danger"><strong>*</strong></span>:</label>
                                         <select name="gender" id="gender" value={this.state.gender} onChange={(data) => { this.setState({ gender: data.target.value }) }} className="form-control    bg-white">
                                            <option value="">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                       </select>
                                       <span id="genderError" class="text-danger font-weight-bold"></span>

                                    </div>

                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Roll No <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="number" id="roll_no" name="roll_no" value={this.state.roll_no} onChange={(data) => { this.setState({ roll_no: data.target.value }) }} className="form-control bg-white " />
                                         <span id="roll_noError" class="text-danger font-weight-bold"></span>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email ID <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="email_i_d" name="email_i_d" value={this.state.email_i_d} onChange={(data) => { this.setState({ email_i_d: data.target.value }) }} className="form-control bg-white " />
                                         <span id="email_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Date of Birth <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="date" id="date_of_birth" name="date_of_birth" value={this.state.date_of_birth} onChange={(data) => { this.setState({ date_of_birth: data.target.value }) }} className="form-control bg-white " />
                                         <span id="date_of_birthError" class="text-danger font-weight-bold"></span>

                                    </div>

                                </div>
                                <div className="col-md-4">
                                    <div className="form-group mb-1">
                                        <label for="" className="mb-0 font-weight-bold">Upload Profile Picture:</label> <br />
                                        <div className="form-group files">
                                             <input type="file" id="profile_pic" name="profile_pic" id="profilepic" className="form-control bg-white " multiple="" />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p-3"></div>
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 text-success border-title">Personal Details:</span>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Father's name <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="father_name" name="father_name" value={this.state.father_name} onChange={(data) => { this.setState({ father_name: data.target.value }) }} className="form-control bg-white " />
                                         <span id="father_nameError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Mother's Name <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="mother_name" name="mother_name" value={this.state.mother_name} onChange={(data) => { this.setState({ mother_name: data.target.value }) }} className="form-control bg-white " />
                                         <span id="mother_nameError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Guardian's Name <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="guardian_name" name="guardian_name" value={this.state.guardian_name} onChange={(data) => { this.setState({ guardian_name: data.target.value }) }} className="form-control bg-white " />
                                         <span id="guardian_nameError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="address1" name="address1" value={this.state.address1} onChange={(data) => { this.setState({ address1: data.target.value }) }} className="form-control bg-white " />
                                         <span id="address1Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="city"  name="city" value={this.state.city} onChange={(data) => { this.setState({ city: data.target.value }) }} className="form-control bg-white " />
                                         <span id="cityError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Account Status <span className="text-danger"><strong>*</strong></span>:</label>
                                         <select id="status" name="status"  value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }}   className="form-control bg-white ">
                                            <option value="">Select...</option>
                                            <option value="Active">Active</option>
                                            <option value="Suspended">Suspended</option>
                                            <option value="Deactivated">Deactivated</option>
                                       </select>
                                       <span id="statusError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Father's Contact Number :</label>
                                         <input type="text" name="father_contact_no" id="father_contact_no" value={this.state.father_contact_no} onChange={(data) => { this.setState({ father_contact_no: data.target.value }) }} className="form-control bg-white " />
                                         <span id="father_contact_noError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Mother's Contact Number :</label>
                                         <input type="text" id="mother_contact_no" name="mother_contact_no" value={this.state.mother_contact_no} onChange={(data) => { this.setState({ mother_contact_no: data.target.value }) }} className="form-control bg-white " />
                                         <span id="mother_contact_noError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Guardian's Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="guardian_contact_no" name="guardian_contact_no" value={this.state.guardian_contact_no} onChange={(data) => { this.setState({ guardian_contact_no: data.target.value }) }}   className="form-control bg-white " />
                                         <span id="guardian_contact_noError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address 2:</label>
                                         <input type="text" id="address2" name="address2" value={this.state.address2} onChange={(data) => { this.setState({ address2: data.target.value }) }} className="form-control bg-white " />
                                         <span id="address2Error" class="text-danger font-weight-bold"></span>

                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                        <CountryDropdown id="country" name="country" value={this.state.country} onChange={(data) => {  this.selectCountry(data)}} className="form-control bg-white " />
                                         <span id="countryError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Father's Email ID:</label>
                                         <input type="text" id="father_email_i_d" name="father_email_i_d" value={this.state.father_email_i_d} onChange={(data) => { this.setState({ father_email_i_d: data.target.value }) }} className="form-control bg-white " />
                                         <span id="father_email_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Mother's Email ID:</label>
                                         <input type="text" id="mother_email_i_d" name="mother_email_i_d" value={this.state.mother_email_i_d} onChange={(data) => { this.setState({ mother_email_i_d: data.target.value }) }} className="form-control bg-white " />
                                         <span id="mother_email_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Guardian's Email ID <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="guardian_email_id" name="guardian_email" value={this.state.guardian_email} onChange={(data) => { this.setState({ guardian_email: data.target.value }) }} className="form-control bg-white " />
                                         <span id="guardian_email_idError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN code <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="pin_code" name="pin_code" value={this.state.pin_code} onChange={(data) => { this.setState({ pin_code: data.target.value }) }} className="form-control bg-white " />
                                         <span id="pin_codeError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State <span className="text-danger"><strong>*</strong></span>:</label>
                                        <RegionDropdown country={this.state.country} id="state" name="state" value={this.state.state} onChange={(data) => { this.selectRegion(data) }}  className="form-control bg-white " />
                                         <span id="stateError" class="text-danger font-weight-bold"></span>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="card-footer">
                        <div className="float-right">
                            <button onClick={() => { this.submit() }} className="btn btn-primary"><i className="icon-plus"></i> Add New Entry</button>
                            <button onClick={() => { this.submitandprroceed() }} className="btn btn-success"><i className="icon-check"></i> Save and Proceed</button>
                            <button onClick={() => { this.resetFields() }} className="btn btn-warning"><i className="icon-reset"></i> Reset</button>
                        </div>
                    </div>
                </div>


            </div>


        );


    }
}
export default addStudent
