import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import Sectionedit from './sectionEdit';
import $ from 'jquery';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class addSection extends Component {
    constructor(props) {
        super(props);

        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSemesterSection = this.showSemesterSection.bind(this);
        this.showASection = this.showASection.bind(this);
        this.deleteSection = this.deleteSection.bind(this);
        this.sectionEdit = this.sectionEdit.bind(this)


        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            sectionList: [],
            sectionList1: [],
            section_name: "",
            num_of_student_registered: "",
            status: "",
            sessonList1: [],
            classList: [],
            unitList: [],
            semesterList: [],
            semester_i_d: "",
            teacherList: [],
            editedSectionID: "",
            ii: 0,
            searchString: "",
            searchParams: ""

        }
        this.sectionSearchList = []
    }
    sectionEdit(section) {

        ReactDOM.render(<Sectionedit data={section} unitList = {this.state.unitList} teacherList = {this.state.teacherList} />, document.getElementById('editSectionContent'));
    }
    deleteSection(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Section!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletesec";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sectionID': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Section Deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }
    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }
    showSession(val) {

        this.updateParamsForSearch('unitID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }

    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {

        this.updateParamsForSearch('sessionID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }


    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }

    showSemester(val) {


        this.updateParamsForSearch('departmentID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSemesterSection() {

        let val = document.getElementById("semester1").value
        this.updateParamsForSearch('semesterID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList1: json
                })
            });

    }

    showASection() {
        let val = document.getElementById("section1").value
        this.updateParamsForSearch("sectionID", val)
    }

    submit() {
        var section_name = document.getElementById("section_name").value;
        var i = 0;
        var sintaxCheck = /^[A-Za-z0-9_ -]{2,60}$/;


        if (i < 7) {
            if (document.getElementById("unit2").value == "") {
                document.getElementById("unit2Error").innerHTML = "*Please fill this field";
                document.getElementById("unit2").focus();
                document.getElementById("unit2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit2").value != "") {
                document.getElementById("unit2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit2").style.borderColor = "";
            }
            if (document.getElementById("session2").value == "") {
                document.getElementById("session2Error").innerHTML = "*Please fill this field";
                document.getElementById("session2").focus();
                document.getElementById("session2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session2").value != "") {
                document.getElementById("session2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("session2").style.borderColor = "";

            }



            if (document.getElementById("department2").value == "") {
                document.getElementById("department2Error").innerHTML = "*Please fill this field";
                document.getElementById("department2").focus();
                document.getElementById("department2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department2").value != "") {
                document.getElementById("department2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department2").style.borderColor = "";
            }

            if (document.getElementById("semester_i_d").value == "") {
                document.getElementById("semester_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("semester_i_d").focus();
                document.getElementById("semester_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester_i_d").value != "") {
                document.getElementById("semester_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("semester_i_d").style.borderColor = "";
            }


            if (document.getElementById("section_name").value == "") {
                document.getElementById("section_nameError").innerHTML = "*Please fill this field";
                document.getElementById("section_name").focus();
                document.getElementById("section_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("section_name").value != "") {
                if (!sintaxCheck.test(section_name)) {
                    document.getElementById("section_nameError").innerHTML = "*Invalid Session Format";
                    document.getElementById("section_name").focus();
                    document.getElementById("section_name").style.borderColor = "#FF0000";
                }

                if (sintaxCheck.test(section_name)) {
                    document.getElementById("section_nameError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("section_name").style.borderColor = "";
                }
            }


            if (document.getElementById("class_teacher_i_d").value == "") {
                document.getElementById("class_teacher_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("class_teacher_i_d").focus();
                document.getElementById("class_teacher_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("class_teacher_i_d").value != "") {
                document.getElementById("class_teacher_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("class_teacher_i_d").style.borderColor = "";
            }
            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 7) {
                console.log(i);
                this.state.ii = 7;
            }
        }
        if (this.state.ii == 7) {
            console.log(this.state);
            let url = global.API + "/studapi/public/api/addsection";
            let data = this.state;
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Section  Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        $('#createSection').modal('hide')
                        this.componentDidMount();
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Section Not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                })
            })
        }
    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };



        let url = global.API + "/studapi/public/api/viewallorgasec";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            }).then(() => {
                let count = Object.keys(this.state.sectionList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });

        let teacherURL = global.API + "/studapi/public/api/getallteacheridname";
        fetch(teacherURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    teacherList: json
                })
            });



    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these sections!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesec";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({'sectionID': value })
                            })
                        })
                        swal("All Selected sections Deleted! Refresh to see the results :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sections cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }
    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }
// refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    
    render() {
        var { isLoaded, sectionList, sessonList1, classList, unitList, semesterList, teacherList, sectionList1 } = this.state;
        if (this.state.searchParams.length > 0) {
            this.sectionSearchList = SearchForObjectsWithParams(sectionList, this.state.searchParams)
        }
        else {
            this.sectionSearchList = sectionList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sectionSearchList = SearchForObjectsWithName(this.sectionSearchList, this.state.searchString)
        }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3 className="font-weight-bold">Section
      <a href="#" data-toggle="modal" data-target="#createSection"><span className=" h5  mt-2 font-weight-bold float-right">+ Create New Section</span></a>
                        </h3>
                    </div>
                    <div className="card-body">
                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()}>Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" onChange={this.showSemesterSection} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>
                                            <select name="" id="section1" onChange={this.showASection} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {sectionList1.map(section => (
                                                    <option value={section.sectionID}>{section.sectionName}</option>

                                                ))}
                                            </select>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>

                        <table className="table table-stripped table-bordered">
                            <thead>
                                <tr className="bg-light">
                                   <th className="border-0"></th>
                                    <th className="border-0">Unit</th>
                                    <th className="border-0">Session</th>
                                    <th className="border-0">Class/Department</th>
                                    <th className="border-0">semester</th>
                                    <th className="border-0">Name of Section</th>
                                    <th className="border-0">No of Student Registered</th>
                                    <th className="border-0">Class Teacher</th>
                                    <th className="border-0">Status</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.sectionSearchList.map(section => (

                                    <tr>

                                       <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value={section.sectionID} /></td>
                                        <td className="border-0">{section.unitName}</td>
                                        <td className="border-0">{section.session}</td>
                                        <td className="border-0">{section.departmentName}</td>
                                        <td className="border-0">{section.semesterName}</td>
                                        <td className="border-0">{section.sectionName}</td>
                                        <td className="border-0">{section.numOfStudentRegistered}</td>
                                        <td className="border-0">{section.teacherName}</td>
                                        <td className="border-0"><span className="badge badge-pill bg-light">{section.status}</span></td>
                                        <td className="border-0">
                                            <span><a href="#" data-toggle="modal" data-target="#editSection" onClick={(e) => this.sectionEdit(section)} className="text-warning"><i className="icon-pencil" /></a></span>
                                            <span><a onClick={() => this.deleteSection(section.sectionID)} className="text-danger" ><i className="icon-trash" />
                                            </a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                {/*Body Ends*/}


                {/*-modal for create new Section*/}
                <div className="modal fade" id="createSection" tabIndex={-1} role="dialog" aria-labelledby="modelTitlecreateSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Create New Section</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                        <span id="unit2Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                        <span id="session2Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Department <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                            <option value="" selected>Select</option>
                                            {classList.map(classes => (
                                                <option value={classes.departmentID}>{classes.departmentName}</option>

                                            ))}
                                        </select>
                                        <span id="department2Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="semester_i_d" name="semester_i_d" value={this.state.semester_i_d} onChange={(data) => { this.setState({ semester_i_d: data.target.value }) }} className="form-control">
                                            <option value="" selected>Select</option>
                                            {semesterList.map(semester => (
                                                <option value={semester.semesterID}>{semester.semesterName}</option>

                                            ))}

                                        </select>
                                        <span id="semester_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Section <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="section_name" name="section_name" value={this.state.section_name} onChange={(data) => { this.setState({ section_name: data.target.value }) }} className="form-control bg-white" placeholder="Section Name" />
                                        <span id="section_nameError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered :</label>
                                        <input type="number" id="num_of_student_registered" name="num_of_student_registered" value={this.state.num_of_student_registered} onChange={(data) => { this.setState({ num_of_student_registered: data.target.value }) }} className="form-control" />
                                        <span id="num_of_student_registeredError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold">Class Teacher <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="class_teacher_i_d" name="class_teacher_i_d" value={this.state.class_teacher_i_d} onChange={(data) => { this.setState({ class_teacher_i_d: data.target.value }) }} className="form-control">
                                            <option value="" selected>Select</option>
                                            {teacherList.map(teacher => (
                                                <option value={teacher.teacherID}>{teacher.teacherName}</option>

                                            ))}

                                        </select>
                                        <span id="class_teacher_i_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="status" name="status" value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }} className="form-control">
                                            <option value="" selected>Select..</option>
                                            <option value="Active" >Active</option>
                                            <option value="Deactive">Deactivate</option>
                                        </select>
                                        <span id="statusError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Create Section</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*--/modal for create new Section---*/}
                {/*-modal for edit new Section*/}
                <div className="modal fade" id="editSection" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div id="editSectionContent"></div>
                 { /*      <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Edit Section</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="" id="editSectionUnitName" className="form-control" onChange={(e) => this.showSession(e.target.value)}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSectionSessionName" className="form-control" onChange={(e) => this.showDepartment(e.target.value)}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSectionDepartment" onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                                            <option value="" selected>Select</option>
                                            {classList.map(classes => (
                                                <option value={classes.departmentID}>{classes.departmentName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSectionSemester" className="form-control">
                                            <option value="" selected>Select</option>
                                            {semesterList.map(semester => (
                                                <option value={semester.semesterID}>{semester.semesterName}</option>

                                            ))}

                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the  Section  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" id="editSectionName" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered:</label>
                                        <input type="number" id="editSectionStudents" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold">Class Teacher  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSectionTeacherName" className="form-control">
                                            <option value="" selected>Select</option>
                                            {teacherList.map(teacher => (
                                                <option value={teacher.teacherID}>{teacher.teacherName}</option>

                                            ))}

                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="unit_i_d" id="editSectionStatus" onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control">
                                            <option value="Active" selected>Active</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => this.onEditSectionFormSubmit()} className="btn btn-warning">Update Section</button>
                            </div>
                        </div>
                  */}  </div>
                </div>
                {/*--/modal for edit new section-*/}


            </div>
        );


    }
}
export default addSection
