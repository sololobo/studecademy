import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';

class sectionEdit extends Component {
   constructor(props){
      super(props)

      //***************              bindings               *********************//
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.onEditSectionFormSubmit = this.onEditSectionFormSubmit.bind(this)
      //**********************************************************************//

      //**************** dropdown default values ***************//
         this.showSession(this.props.data.unitID)
         this.showDepartment(this.props.data.sessionID)
         this.showSemester(this.props.data.departmentID)
      //*****************************************************//

      //***************      state variables       ***************************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            semesterID : this.props.data.semesterID,
            sectionID : this.props.data.sectionID,
            sectionName : this.props.data.sectionName,
            numOfStudentRegistered : this.props.data.numOfStudentRegistered,
            classTeacherID : this.props.data.classTeacherID,
            status : this.props.data.status,

            unitList : this.props.unitList,
            sessionList : [],
            classList : [],
            semesterList : [],
            teacherList : this.props.teacherList
         }
      //****************************************************************//

   }

   //***********     handling change in props    **********************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

           //**************** dropdown default values ***************//
              this.showSession(this.props.data.unitID)
              this.showDepartment(this.props.data.sessionID)
              this.showSemester(this.props.data.departmentID)
           //*****************************************************//

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             departmentID : this.props.data.departmentID,
             semesterID : this.props.data.semesterID,
             sectionID : this.props.data.sectionID,
             sectionName : this.props.data.sectionName,
             numOfStudentRegistered : this.props.data.numOfStudentRegistered,
             classTeacherID : this.props.data.classTeacherID,
             status : this.props.data.status,

             unitList : this.props.unitList,
             sessionList : [],
             classList : [],
             semesterList : [],
             teacherList : this.props.teacherList
          })
        }
      }
   //*****************************************************************************//





//************************ Dropdown async Calls *******************************//
   showSession(val) {
      this.setState({
         sessionList : [],
         classList : [],
         semesterList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });

   }

   showDepartment(val) {
      this.setState({
         classList : [],
         semesterList : [],
         sessionID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'session_i_d': val })

      };
      let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
      fetch(classURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   classList: json
               })
           });
   }

   showSemester(val) {
      this.setState({
         semesterList : [],
         departmentID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'department_i_d': val })

      };
      let semesterURL = global.API + "/studapi/public/api/getallsemidname";
      fetch(semesterURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   semesterList: json
               })
           });
   }
//************************************************************************************//

//*****************     edit section form     *******************************//
   onEditSectionFormSubmit() {
       let url = global.API + "/studapi/public/api/updatesection";
       let data = {
           section_i_d: this.state.sectionID,
           semester_i_d: this.state.semesterID,
           section_name: this.state.sectionName,
           section_code: null,
           num_of_student_registered: this.state.numOfStudentRegistered,
           class_teacher_i_d: this.state.classTeacherID,
           status: this.state.status
       }
       fetch(url, {
           method: 'POST',
           headers: {
               "Content-Type": "application/json",
               "Accept": "application/json"
           },
           body: JSON.stringify(data)
       }).then((result) => {
           result.json().then((resp) => {
               try {
                   if (resp[0].result == 1) {
                       swal("Section Updated! Refresh to see the result", {
                           icon: "success",
                       });
                       $('#editSection').modal("hide");
                   }
                   else {
                       swal("There's something wrong!! :(", {
                           icon: "error",
                       });
                   }
               }
               catch{
                   swal("There's something wrong!! :(", {
                       icon: "error",
                   });
               }
           })
       })
   }
//****************************************************************************//

render() {

        return (

            <div className="modal-content">
            <div className="modal-header">
                <h4><span className="font-weight-bold"> Edit Section</span></h4>
            </div>
            <div className="modal-body">
                <div className="container-fluid">
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select name="" id="editSectionUnitName" className="form-control" value = {this.state.unitID} onChange={(e) => this.showSession(e.target.value)}>
                            <option value="" selected>Select</option>
                               {this.state.unitList.map(unit => (
                                  <option value={unit.unitID}>{unit.unitName}</option>
                              ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSectionSessionName" className="form-control" value = {this.state.sessionID} onChange={(e) => this.showDepartment(e.target.value)}>
                            <option value="" selected>Select</option>
                               {this.state.sessionList.map(session => (
                                  <option value={session.sessionID}>{session.session}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Department  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSectionDepartment" value = {this.state.departmentID} onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                            <option value="" selected>Select</option>
                               {this.state.classList.map(classes => (
                                   <option value={classes.departmentID}>{classes.departmentName}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSectionSemester" value = {this.state.semesterID} onChange={(e) => this.setState({semesterID : e.target.value})} className="form-control">
                            <option value="" selected>Select</option>
                               {this.state.semesterList.map(semester => (
                                  <option value={semester.semesterID}>{semester.semesterName}</option>
                               ))}

                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the  Section  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="text" id="editSectionName" value = {this.state.sectionName} onChange={(e) => this.setState({sectionName : e.target.value})} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered:</label>
                        <input type="number" id="editSectionStudents" value = {this.state.numOfStudentRegistered} onChange={(e) => this.setState({numOfStudentRegistered : e.target.value})} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold">Class Teacher  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSectionTeacherName" value = {this.state.classTeacherID} onChange={(e) => this.setState({classTeacherID : e.target.value})} className="form-control">
                            <option value="" selected>Select</option>
                               {this.state.teacherList.map(teacher => (
                                  <option value={teacher.teacherID}>{teacher.teacherName}</option>
                               ))}

                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select name="unit_i_d" id="editSectionStatus" value = {this.state.status} onChange={(e) => this.setState({status : e.target.value})} className="form-control">
                           <option value ="" selected>Select</option>
                            <option value="Active" >Active</option>
                            <option value="Pending">Pending</option>
                            <option value="Deactive">Deactive</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" onClick={() => this.onEditSectionFormSubmit()} className="btn btn-warning">Update Section</button>
            </div>
        </div>


        );


    }
}
export default sectionEdit
