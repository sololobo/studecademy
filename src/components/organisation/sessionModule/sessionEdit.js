import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';

class sessionEdit extends Component {
   constructor(props){
      super(props)

      //***************              bindings               *********************//
         this.onEditSessionFormSubmit = this.onEditSessionFormSubmit.bind(this)
      //**********************************************************************//

      //***************      state variables       ***************************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            session : this.props.data.session,
            startDate : this.props.data.startDate,
            endDate : this.props.data.endDate,
            status : this.props.data.status,

            unitList : this.props.unitList,
         }
      //****************************************************************//
   }

   //***********     handling change in props    **********************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             session : this.props.data.session,
             startDate : this.props.data.startDate,
             endDate : this.props.data.endDate,
             status : this.props.data.status,

             unitList : this.props.unitList
          })
        }
      }
   //*****************************************************************************//


   //*******************     edit session form     *******************//
   onEditSessionFormSubmit() {
      let url = global.API + "/studapi/public/api/editsession";
      let data = {
           sessID: this.state.sessionID,
           unID: this.state.unitID,
           sess: this.state.session,
           startDate: this.state.startDate,
           endDate: this.state.endDate,
           status: this.state.status,
      }
      fetch(url, {
           method: 'POST',
           headers: {
               "Content-Type": "application/json",
               "Accept": "application/json"
           },
           body: JSON.stringify(data)
      }).then((result) => {
           result.json().then((resp) => {
               try {
                   if (resp[0].result == 1) {
                       swal("Session Updated! Refresh to see the results :)", {
                           icon: "success",
                       });
                       $('#editSession').modal("hide");
                   }
                   else {
                       swal("There's something wrong!! :(", {
                           icon: "error",
                       });
                   }
               }
               catch{
                   swal("There's something wrong!! :(", {
                       icon: "error",
                   });
               }
           })
      })
   }
   //***************************************************************************//

render() {

        return (

            <div className="modal-content">
            <div className="modal-header">
                <h4><span className="font-weight-bold"> Edit Session</span></h4>
            </div>
            <div className="modal-body">
                <div className="container-fluid">
                    <div className="form-group">

                        <label htmlFor className="mb-0 font-weight-bold">Select Unit  <span className="text-danger"><strong>*</strong></span> :</label>

                        <select id="unID" name="unID"
                           value = {this.state.unitID}
                           onChange = {(e) => this.setState({unitID : e.target.value})}
                            className="form-control">
                            <option value="">Select </option>
                               {this.state.unitList.map(unit => (
                                   <option value={unit.unitID}>{unit.unitName}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="text" id="sess" name="sess"
                            className="form-control"
                            onChange = {(e) => {this.setState({session : e.target.value})}}
                            value = {this.state.session}
                            placeholder="Session XXXX-XX" />

                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Starting from  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="date" id="startDate"
                            name="startDate"
                            onChange = {(e) => {this.setState({startDate : e.target.value})}}
                            value = {this.state.startDate}
                            className="form-control" />

                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Ending on  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="date" id="endDate" name="endDate"
                           onChange = {(e) => {this.setState({endDate : e.target.value})}}
                           value = {this.state.endDate}
                            className="form-control" />

                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="status" name="status"
                           onChange = {(e) => {this.setState({status : e.target.value})}}
                           value = {this.state.status}
                            className="form-control">
                            <option value="" >Select..</option>
                            <option value="Active" selected>Active</option>
                            <option value="Suspended">Suspended</option>
                        </select>

                    </div>
                </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" onClick={() => this.onEditSessionFormSubmit()} className="btn btn-warning">Update Session</button>
            </div>
        </div>


        );


    }
}
export default sessionEdit
