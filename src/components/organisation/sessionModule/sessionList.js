import React, { Component } from 'react';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';
import ReactDOM from 'react-dom';
import Sessionedit from './sessionEdit'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
class sessionList extends Component {
    constructor(props) {
        super(props);

        this.showSession = this.showSession.bind(this);
        this.showUnitSession = this.showUnitSession.bind(this);
        this.changePass = this.changePass.bind(this);
        this.deleteSession = this.deleteSession.bind(this);
        this.resetModal = this.resetModal.bind(this);
        this.sessionEdit = this.sessionEdit.bind(this);
        this.state = {
            sessonList: [],
            sessonList2: [],
            sessonList1: [],

            unID: "",
            sess: "",
            startDate: "",
            endDate: "",
            status: "",
            sessID: "",
            unitList: [],
            editedSessionID: "",
            ii: 0,
            touched: {
                unID: false,
                sess: false,
                startDate: false,
                endDate: false,
                status: false,
            },
            searchString: "",
            searchParams: []
        }
        this.sessionSearchList = []
    }

    sessionEdit(session) {

        ReactDOM.render(<Sessionedit data = {session} unitList = {this.state.unitList} />, document.getElementById('editSessionContent'));
    }


    changePass() {
        let email = document.getElementById('email1').innerHTML;
        let pwd = document.getElementById('newpass').value;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });

    }
    deleteSession(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletesession";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Session deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    showSession() {
        let val = document.getElementById("unit1").value
        this.updateParamsForSearch('unitID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };

        console.log(val);
        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }
    showUnitSession() {
        let val = document.getElementById("session1").value
        this.updateParamsForSearch('sessionID', val)
    }

    submit() {
        var sess = document.getElementById("sess").value;
        var i = 0;
        var sessCheck = /^[A-Za-z0-9 _-]{3,20}$/;


        if (i < 5) {
            if (document.getElementById("unID").value == "") {
                document.getElementById("unIDError").innerHTML = "*Please fill this field";
                document.getElementById("unID").focus();
                document.getElementById("unID").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unID").value != "") {
                document.getElementById("unIDError").innerHTML = "";
                i = i + 1;
                document.getElementById("unID").style.borderColor = "";
            }
            if (document.getElementById("sess").value == "") {
                document.getElementById("sessError").innerHTML = "*Please fill this field";
                document.getElementById("sess").focus();
                document.getElementById("sess").style.borderColor = "#FF0000";
            }

            if (document.getElementById("sess").value != "") {
                if (!sessCheck.test(sess)) {
                    document.getElementById("sessError").innerHTML = "*Invalid Session Format";
                    document.getElementById("sess").focus();
                    document.getElementById("sess").style.borderColor = "#FF0000";
                }

                if (sessCheck.test(sess)) {
                    document.getElementById("sessError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("sess").style.borderColor = "";
                }
            }



            if (document.getElementById("startDate").value == "") {
                document.getElementById("startDateError").innerHTML = "*Please fill this field";
                document.getElementById("startDate").focus();
                document.getElementById("startDate").style.borderColor = "#FF0000";
            }

            if (document.getElementById("startDate").value != "") {
                document.getElementById("startDateError").innerHTML = "";
                i = i + 1;
                document.getElementById("startDate").style.borderColor = "";
            }
            if (document.getElementById("endDate").value == "") {
                document.getElementById("endDateError").innerHTML = "*Please fill this field";
                document.getElementById("endDate").focus();
                document.getElementById("endDate").style.borderColor = "#FF0000";
            }

            if (document.getElementById("endDate").value != "") {
                document.getElementById("endDateError").innerHTML = "";
                i = i + 1;
                document.getElementById("endDate").style.borderColor = "";
            }
            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 5) {
                console.log(i);
                this.state.ii = 5;
            }
        }
        if (this.state.ii == 5) {

            let url = global.API + "/studapi/public/api/addsession";
            let data = this.state;
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Session Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        $('#createSession').modal('hide')
                        this.componentDidMount();
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Session Not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                    }

                })
            })
        }


    }
    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };



        let sessionURL = global.API + "/studapi/public/api/viewallorgasession";
        fetch(sessionURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList: json,
                })
            }).then(() => {
                let count = this.state.sessonList.length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }




    resetModal() {
        document.getElementById("unID").value = ""
        document.getElementById("sess").value = ""
        document.getElementById("startDate").value = ""
        document.getElementById("endDate").value = ""
        document.getElementById("status").value = ""
        this.setState({
            unID: "",
            sess: "",
            startDate: "",
            endDate: "",
            status: "",
            touched: {
                unID: false,
                sess: false,
                startDate: false,
                endDate: false,
                status: false,
            }
        })
    }


    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, sessonList, unitList, sessonList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }



        return (

            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3 className="font-weight-bold">Session
                    <a href="#" data-toggle="modal" data-target="#createSession" onClick={() => this.resetModal()}><span className=" h5  mt-2 font-weight-bold float-right">+ Create New Session</span></a>
                        </h3>
                    </div>
                    <div className="card-body">
                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()}>Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                        <div className="row">
                                            <div className="col-md-6 form-group">
                                                <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                                <select name="" id="unit1" className="form-control bg-white" onChange={this.showSession}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>

                                            </div>
                                            <div className="col-md-6 form-group">
                                                <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                                <select name="" id="session1" className="form-control bg-white" onChange={this.showUnitSession}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>
                        <table className="table table-stripped table-bordered table-responsive-lg">
                            <thead>
                                <tr className="bg-light">
                                    <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                    <th className="border-0">Name of UNIT</th>
                                    <th className="border-0">Session</th>
                                    <th className="border-0">Starting from</th>
                                    <th className="border-0">Ending On</th>
                                    <th className="border-0">Status</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.sessionSearchList.map(session => (
                                    <tr>
                                        <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value={session.sessionID} /></td>
                                        <td className="border-0">{session.unitName}</td>
                                        <td className="border-0">{session.session}</td>
                                        <td className="border-0">{session.startDate}</td>
                                        <td className="border-0">{session.endDate}</td>
                                        <td className="border-0"><span className="badge badge-pill bg-light">{session.status}</span></td>
                                        <td className="border-0">
                                            <span><a href="#" data-target="#editSession" onClick={(e) => this.sessionEdit(session)} data-toggle="modal" className="text-warning"><i className="icon-pencil"></i></a></span>
                                            <span><a onClick={() => this.deleteSession(session.sessionID)} className="text-danger" ><i className="icon-trash"></i></a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>
                </div>

                {/*-modal for create new session*/}
                <div className="modal fade" id="createSession" tabIndex={-1} role="dialog" aria-labelledby="modelTitlecreateSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Create New Session</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">

                                        <label htmlFor className="mb-0 font-weight-bold">Select Unit <span className="text-danger"><strong>*</strong></span> :</label>

                                        <select id="unID" name="unID"
                                            value={this.state.unID}
                                            onChange={(data) => { this.setState({ unID: data.target.value }) }}
                                            className="form-control">
                                            <option value="">--Select unit--</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}

                                        </select>
                                        <span id="unIDError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="sess" name="sess"
                                            value={this.state.sess}
                                            onChange={(data) => { this.setState({ sess: data.target.value }) }}
                                            className="form-control"
                                            placeholder="Session XXXX-XX" />
                                        <span id="sessError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Starting from <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="date" id="startDate"
                                            value={this.state.startDate}
                                            name="startDate"
                                            onChange={(data) => { this.setState({ startDate: data.target.value }) }}
                                            className="form-control" />
                                        <span id="startDateError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Ending on <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="date" id="endDate" name="endDate"
                                            value={this.state.endDate}
                                            onChange={(data) => { this.setState({ endDate: data.target.value }) }}
                                            className="form-control" />
                                        <span id="endDateError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="status" name="status"
                                            value={this.state.status}
                                            onChange={(data) => { this.setState({ status: data.target.value }) }}
                                            className="form-control">
                                            <option value="" >Select..</option>
                                            <option value="Active" selected>Active</option>
                                            <option value="Suspended">Suspended</option>
                                        </select>
                                        <span id="statusError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Create New Session</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*--/modal for create new session---*/}
                {/*-modal for edit new session*/}
                <div className="modal fade" id="editSession" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div id="editSessionContent"></div>
                   {/*      <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Edit Session</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">

                                        <label htmlFor className="mb-0 font-weight-bold">Select Unit  <span className="text-danger"><strong>*</strong></span> :</label>

                                        <select id="unID" name="unID"
                                            value={this.state.unID}
                                            onChange={(data) => { this.setState({ unID: data.target.value }) }}
                                            className="form-control">
                                            <option value="">--Select unit--</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}

                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" id="sess" name="sess"
                                            value={this.state.sess}
                                            onChange={(data) => { this.setState({ sess: data.target.value }) }}
                                            className="form-control"
                                            placeholder="Session XXXX-XX" />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Starting from  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="date" id="startDate"
                                            value={this.state.startDate}
                                            name="startDate"
                                            onChange={(data) => { this.setState({ startDate: data.target.value }) }}
                                            className="form-control" />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Ending on  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="date" id="endDate" name="endDate"
                                            value={this.state.endDate}
                                            onChange={(data) => { this.setState({ endDate: data.target.value }) }}
                                            className="form-control" />

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="status" name="status"
                                            value={this.state.status}
                                            onChange={(data) => { this.setState({ status: data.target.value }) }}
                                            className="form-control">
                                            <option value="" >Select..</option>
                                            <option value="Active" selected>Active</option>
                                            <option value="Suspended">Suspended</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => this.onEditSessionFormSubmit()} className="btn btn-warning">Update Session</button>
                            </div>
                        </div>

                        */}
                    </div>


                </div>
                {/*--/modal for edit new session---*/}



            </div>
        );


    }
}
export default sessionList
