import React, { Component } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-inline';
import { Redirect } from 'react-router-dom';


class CreateNotice extends Component {


    render() {

        return (
            <main className="inbox">
                <p className="text-center">New Message</p>
                <div className="row mb-3">
                    <label htmlFor="to" className="col-md-3col-form-label">To:</label>
                    <div className="col-md-9 ">
                        <fieldset className="form-group">
                            <select id="select2-2" className="form-control select2-multiple" multiple>
                                <option>Option 1</option>
                                <option selected>Option 2</option>
                                <option>Option 3</option>
                                <option>Option 4</option>
                                <option>Option 5</option>
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div className="row mb-3">
                    <label htmlFor="to" className="col-md-3  col-form-label">Subject:</label>
                    <div className="col-md-9">
                        <fieldset className="form-group">
                            <input type="text" className="form-control" />
                        </fieldset>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 ml-auto">
                        <div className="form-group mt-4">
                        <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>
                       
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-success">Send</button>
                            <button type="submit" className="btn btn-warning">Draft</button>
                            <button type="submit" className="btn btn-danger">Discard</button>
                        </div>
                    </div>
                </div>
            </main>


        );


    }
}
export default CreateNotice