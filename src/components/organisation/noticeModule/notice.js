import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';
import Listnotice from './list'
import Createnotice from './create'
import Viewnotice from './noticeView'
import Sentnotice from './sent'
import Draftnotice from './draft'
import Trashnotice from './trash'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class Noticeboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.sessionSearchList = []
        this.listnotice = this.listnotice.bind(this);
        this.viewnotice = this.viewnotice.bind(this);
        this.sentnotice = this.sentnotice.bind(this);
        this.draftnotice = this.draftnotice.bind(this);
        this.trashnotice = this.trashnotice.bind(this);
        this.createnotice = this.createnotice.bind(this);
    }
    listnotice() {

        ReactDOM.render(<Listnotice />, document.getElementById('contain2'))

    }
    viewnotice() {

        ReactDOM.render(<Viewnotice />, document.getElementById('contain2'))

    }
    sentnotice() {

        ReactDOM.render(<Sentnotice />, document.getElementById('contain2'))

    }
    draftnotice() {

        ReactDOM.render(<Draftnotice />, document.getElementById('contain2'))

    }
    trashnotice() {

        ReactDOM.render(<Trashnotice />, document.getElementById('contain2'))

    }
    createnotice() {

        ReactDOM.render(<Createnotice />, document.getElementById('contain2'))

    }

    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, sessonList, unitList, sessonList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }

        return (
            <div className="animated fadeIn">
                <div className="card">

                    <div className="card-header">
                        <h2 className="card-text text-uppercase"><b> Noticeboard </b></h2>
                    </div>

                    <div className="card-body">
                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Status:</b></label>
                                            <select name="" id="session1" className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Date:</b></label>

                                            <input type="date" name id className="form-control-sm" />
                                        </div>

                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>




                        <div className="email-app mb-4">
                            <nav>
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.listnotice} ><i className="icon-envelope-letter" />Notices<span className="badge badge-danger badge-pill">4</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.createnotice}><i className="icon-plus" />Create Notice</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.sentnotice}><i className="icon-logout" /> Sent Notices</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border " onClick={this.draftnotice}><i className="icon-layers" /> Draft Notices</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.trashnotice}><i className="icon-trash" /> Trash</a>
                                    </li>
                                </ul>
                            </nav>
                            <div id="contain2">
                                <Listnotice></Listnotice>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );


    }
}
export default Noticeboard