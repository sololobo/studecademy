import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';
//import AssignmentList from './adminAssignmentList'

class subjectEdit extends Component {
   constructor(props){
      super(props)

      //***************              bindings               *********************//
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.onEditSubjectFormSubmit = this.onEditSubjectFormSubmit.bind(this)
      //**********************************************************************//

      //**************** dropdown default values ***************//
         this.showSession(this.props.data.unitID)
         this.showDepartment(this.props.data.sessionID)
         this.showSemester(this.props.data.departmentID)
      //*****************************************************//


      //***************      state variables       ***************************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            semesterID : this.props.data.semesterID,
            subjectID : this.props.data.subjectID,
            subjectName : this.props.data.subjectName,
            status : this.props.data.status,

            unitList : this.props.unitList,
            sessionList : [],
            classList : [],
            semesterList : []
         }
      //****************************************************************//
   }


//***********     handling change in props    **********************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

           //**************** dropdown default values ***************//
              this.showSession(this.state.unitID)
              this.showDepartment(this.state.sessionID)
              this.showSemester(this.state.departmentID)
           //*****************************************************//

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             departmentID : this.props.data.departmentID,
             semesterID : this.props.data.semesterID,
             subjectID : this.props.data.subjectID,
             subjectName : this.props.data.subjectName,
             status : this.props.data.status,

             unitList : this.props.unitList,
             sessionList : [],
             classList : [],
             semesterList : []
          })
        }
      }
//*****************************************************************************//




   //************************ Dropdown async Calls *******************************//

   showSession(val) {
      this.setState({
         sessionList : [],
         classList : [],
         semesterList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });

   }

   showDepartment(val) {
      this.setState({
         classList : [],
         semesterList : [],
         sessionID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'session_i_d': val })

      };
      let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
      fetch(classURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   classList: json
               })
           });
   }

   showSemester(val) {
      this.setState({
         semesterList : [],
         departmentID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'department_i_d': val })

      };
      let semesterURL = global.API + "/studapi/public/api/getallsemidname";
      fetch(semesterURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   semesterList: json
               })
           });
   }


   //************************************************************************************//

   //*****************         edit submission form         *****************************//
   onEditSubjectFormSubmit() {
      let url = global.API + "/studapi/public/api/updatesubject";
      let data = {
           subject_i_d: this.state.subjectID,
           semester_i_d: this.state.semesterID,
           subject_name: this.state.subjectName,
           status: this.state.status,
      }
      fetch(url, {
           method: 'POST',
           headers: {
               "Content-Type": "application/json",
               "Accept": "application/json"
           },
           body: JSON.stringify(data)
      }).then((result) => {
           result.json().then((resp) => {
               try {
                   if (resp[0]['result'] === 1) {
                       swal("Subject Updated!", {
                           icon: "success",
                       });
                       $('#editSubject').modal("hide");
                       // this.componentDidMount();
                   }
                   else {
                       swal("There's something wrong!! Some of the fields are missing probably :(", {
                           icon: "error",
                       });
                   }
               }
               catch{
                   swal("One of the important fields is left blank!! :(", {
                       icon: "error",
                   });
               }
           })
      })
   }

   //********************************************************************************//


render() {

        return (

            <div className="modal-content">
            <div className="modal-header">
                <h4><span className="font-weight-bold"> Edit Subject</span></h4>
            </div>
            <div className="modal-body">
                <div className="container-fluid">
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select name="" id="editSubjectUnitName" className="form-control" value = {this.state.unitID} onChange={(e) => this.showSession(e.target.value)}>
                            <option value="" selected>Select</option>
                               {this.state.unitList.map(unit => (
                                  <option value={unit.unitID}>{unit.unitName}</option>
                              ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Session  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSubjectSessionName" className="form-control" value = {this.state.sessionID} onChange={(e) => this.showDepartment(e.target.value)}>
                            <option value="" selected>Select</option>
                               {this.state.sessionList.map(session => (
                                  <option value={session.sessionID}>{session.session}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Class/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSubjectDepartment" value = {this.state.departmentID} onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                            <option value="" selected>Select</option>
                               {this.state.classList.map(classes => (
                                   <option value={classes.departmentID}>{classes.departmentName}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold">Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editSubjectSemesterName" value = {this.state.semesterID} onChange={(e) => this.setState({semesterID : e.target.value})} className="form-control">
                            <option value="" selected>Select</option>
                               {this.state.semesterList.map(semester => (
                                  <option value={semester.semesterID}>{semester.semesterName}</option>
                               ))}
                        </select>
                    </div>
                    <table className="table table-border">
                        <thead>
                            <tr>
                                <th className="border-0">Name of the Subject  <span className="text-danger"><strong>*</strong></span> </th>
                                <th className="border-0">Status  <span className="text-danger"><strong>*</strong></span> </th>
                                <th className="border-0" />
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="newtable">
                                <td className="border-0">
                                    <input type="text" id="editSubjectName" value = {this.state.subjectName} onChange={(e) => this.setState({subjectName : e.target.value})} className="form-control bg-white" />
                                </td>
                                <td className="border-0">
                                    <select name id="editSubjectStatus" value = {this.state.status} onChange={(e) => this.setState({status : e.target.value})} className="form-control bg-white">
                                        <option value = "" selected> Select </option>
                                        <option value="Active">Active</option>
                                        <option value="Suspended">Suspended</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" onClick={() => this.onEditSubjectFormSubmit()} className="btn btn-warning">Update Subject</button>
            </div>
        </div>

        );


    }
}
export default subjectEdit
