import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';
import Subjectedit from './subjectEdit'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
class subjectList extends Component {
    constructor(props) {
        super(props);
        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSemesterSubject = this.showSemesterSubject.bind(this);
        this.showASubject = this.showASubject.bind(this);
        this.deleteSubject = this.deleteSubject.bind(this);
        this.subjectEdit = this.subjectEdit.bind(this);


        this.state = {

            subjectList: [],
            semester_i_d: 0,

            subject_name: "",
            status: "",
            sessonList1: [],
            classList: [],
            unitList: [],
            semesterList: [],
            subjectList1: [],
            ii: 0,
            editedSubjectID: 0,
            searchString: "",
            searchParams: "",
        }
        this.subjectSearchList = []
    }

    subjectEdit(subject) {

        ReactDOM.render(<Subjectedit data = {subject} unitList = {this.state.unitList}/>, document.getElementById('editSubjectContent'));
    }


    deleteSubject(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Unit!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletesub";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'subject_i_d': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Subject deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }

    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }

    showSession(val) {

        this.updateParamsForSearch('unitID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }

    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {

        this.updateParamsForSearch('sessionID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }

    showSemester(val) {

        this.updateParamsForSearch('departmentID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    showSemesterSubject() {

        let val = document.getElementById("semester1").value
        this.updateParamsForSearch('semesterID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList1: json
                })
            });

    }

    showASubject() {
        let val = document.getElementById("subject1").value
        this.updateParamsForSearch('subjectID', val)


    }


    submit() {
        var i = 0;

        if (i < 6) {
            if (document.getElementById("unit2").value == "") {
                document.getElementById("unit2Error").innerHTML = "*Please fill this field";
                document.getElementById("unit2").focus();
                document.getElementById("unit2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit2").value != "") {
                document.getElementById("unit2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit2").style.borderColor = "";
            }

            if (document.getElementById("session2").value == "") {
                document.getElementById("session2Error").innerHTML = "*Please fill this field";
                document.getElementById("session2").focus();
                document.getElementById("session2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session2").value != "") {
                document.getElementById("session2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("session2").style.borderColor = "";
            }

            if (document.getElementById("department2").value == "") {
                document.getElementById("department2Error").innerHTML = "*Please fill this field";
                document.getElementById("department2").focus();
                document.getElementById("department2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department2").value != "") {
                document.getElementById("department2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department2").style.borderColor = "";
            }

            if (document.getElementById("semester_i_d").value == "") {
                document.getElementById("semester_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("semester_i_d").focus();
                document.getElementById("semester_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester_i_d").value != "") {
                document.getElementById("semester_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("semester_i_d").style.borderColor = "";
            }

            if (document.getElementById("subject_name").value == "") {
                document.getElementById("subject_nameError").innerHTML = "*Please fill this field";
                document.getElementById("subject_name").focus();
                document.getElementById("subject_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("subject_name").value != "") {
                document.getElementById("subject_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("subject_name").style.borderColor = "";
            }


            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }



            if (i == 6) {
                console.log(i);
                this.state.ii = 6;
            }
        }
        if (this.state.ii == 6) {

            let url = global.API + "/studapi/public/api/addsubject";
            let data = this.state;
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Subject Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        $('#createSubject').modal('hide')
                        this.componentDidMount();
                    }

                    else {
                        swal({
                            title: "Error!",
                            text: "Subject not added",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                })
            })
        }
    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallorgasub";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            }).then(() => {
                let count = Object.keys(this.state.subjectList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });


        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }


    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }
    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Subject!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesub";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'subject_i_d': value })
                            })
                        })
                        swal("All Selected Subject Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Subject cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }
    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, subjectList, sessonList1, unitList, classList, semesterList, subjectList1 } = this.state;
        if (this.state.searchParams.length > 0) {
            this.subjectSearchList = SearchForObjectsWithParams(subjectList, this.state.searchParams)
        }
        else {
            this.subjectSearchList = subjectList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.subjectSearchList = SearchForObjectsWithName(this.subjectSearchList, this.state.searchString)
        }

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3 className="font-weight-bold">Subject
                        <a href="#" data-toggle="modal" data-target="#createSubject"><span className=" h5  mt-2 font-weight-bold float-right">+ Add  New Subject</span></a>
                        </h3>
                    </div>
                    <div className="card-body">

                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">

                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSemesterSubject}>
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="" id="subject1" onChange={this.showASubject} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {subjectList1.map(subject => (
                                                    <option value={subject.subjectID}>{subject.subjectName}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>
                        <table className="table table-stripped table-bordered table-responsive-lg">
                            <thead>
                                <tr className="bg-light">
                                    <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                    <th className="border-0">Unit</th>
                                    <th className="border-0">Session</th>
                                    <th className="border-0">Class/Department</th>
                                    <th className="border-0">Semester</th>
                                    <th className="border-0">Subject</th>
                                    <th className="border-0">Status</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.subjectSearchList.map(subject => (
                                    <tr>
                                        <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value={subject.subjectID} /></td>
                                        <td className="border-0">{subject.unitName}</td>
                                        <td className="border-0">{subject.session}</td>
                                        <td className="border-0">{subject.departmentName}</td>
                                        <td className="border-0">{subject.semesterName}</td>
                                        <td className="border-0">{subject.subjectName}</td>
                                        <td className="border-0"><span className="badge badge-pill bg-light">{subject.status}</span></td>
                                        <td className="border-0">
                                            <span><a href="#" data-target="#editSubject" onClick={(e) => this.subjectEdit(subject)} data-toggle="modal" className="text-warning"><i
                                                className="icon-pencil"></i></a></span>
                                            <span><a onClick={() => this.deleteSubject(subject.subjectID)} className="text-danger"><i className="icon-trash"></i>
                                            </a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>
                </div>

                {/*-modal*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}
                {/*<!- add semestersubject-->*/}
                {/*-modal for Add New Subject*/}
                <div className="modal fade" id="createSubject" tabIndex={-1} role="dialog" aria-labelledby="modelTitlecreateSession" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Add New Subject</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                        <span id="unit2Error" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Session <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                        <span id="session2Error" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Class/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                            <option value="" selected>Select</option>
                                            {classList.map(classes => (
                                                <option value={classes.departmentID}>{classes.departmentName}</option>

                                            ))}
                                        </select>
                                        <span id="department2Error" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="semester_i_d" name="semester_i_d" value={this.state.semester_i_d} onChange={(data) => { this.setState({ semester_i_d: data.target.value }) }} className="form-control">
                                            <option value="" selected>Select</option>
                                            {semesterList.map(semester => (
                                                <option value={semester.semesterID}>{semester.semesterName}</option>

                                            ))}
                                        </select>
                                        <span id="semester_i_dError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <table className="table table-border">
                                        <thead>
                                            <tr>
                                                <th className="border-0">Name of the Subject <span className="text-danger"><strong>*</strong></span>:</th>
                                                <th className="border-0">Status  <span className="text-danger"><strong>*</strong></span>:</th>
                                                <th className="border-0" />
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td className="border-0">
                                                    <input type="text" id="subject_name" name="subject_name" value={this.state.subject_name} onChange={(data) => { this.setState({ subject_name: data.target.value }) }} className="form-control" />
                                                    <span id="subject_nameError" class="text-danger font-weight-bold"></span>
                                                </td>
                                                <td className="border-0">
                                                    <select id="status" name="status" value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }} className="form-control">
                                                        <option value="">Select...</option>
                                                        <option value="Active">Active</option>
                                                        <option value="Suspended">Suspended</option>
                                                    </select>
                                                    <span id="statusError" class="text-danger font-weight-bold"></span>
                                                </td>
                                                <td className="border-0">
                                                    <span className=""><i className="icon-minus text-danger " /></span>&nbsp;&nbsp;&nbsp;
                                                    <span className=""><i className="icon-plus" type="button" onclick="rowFunction('newtable')" value="Add a row" /></span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*--/modal for create new Subject---*/}
                {/*-modal for edit Subject*/}
                <div className="modal fade" id="editSubject" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditSession" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                      <div id="editSubjectContent"></div>
                      {/*  <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Edit Subject</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="" id="editSubjectUnitName" className="form-control" onChange={(e) => this.showSession(e.target.value)}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSubjectSessionName" className="form-control" onChange={(e) => this.showDepartment(e.target.value)}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Class/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSubjectDepartment" onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                                            <option value="" selected>Select</option>
                                            {classList.map(classes => (
                                                <option value={classes.departmentID}>{classes.departmentName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold">Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editSubjectSemesterName" className="form-control">
                                            <option value="" selected>Select</option>
                                            {semesterList.map(semester => (
                                                <option value={semester.semesterID}>{semester.semesterName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <table className="table table-border">
                                        <thead>
                                            <tr>
                                                <th className="border-0">Name of the Subject  <span className="text-danger"><strong>*</strong></span> </th>
                                                <th className="border-0">Status  <span className="text-danger"><strong>*</strong></span> </th>
                                                <th className="border-0" />
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id="newtable">
                                                <td className="border-0">
                                                    <input type="text" id="editSubjectName" className="form-control bg-white" />
                                                </td>
                                                <td className="border-0">
                                                    <select name id="editSubjectStatus" className="form-control bg-white">
                                                        <option value="Active">Active</option>
                                                        <option value="Suspended">Suspended</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => this.onEditSubjectFormSubmit()} className="btn btn-warning">Update Subject</button>
                            </div>
                        </div>
                   */}
                    </div>
                </div>
                {/*--/modal for edit Subject--*/}
            </div>


        );


    }
}
export default subjectList
