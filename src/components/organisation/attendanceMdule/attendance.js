import React, { Component } from 'react';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';

class attendance extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.sessionSearchList = []
    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, sessonList, unitList, sessonList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }

        return (
            <div className="fadeIn animated">
                {/*-===Body Content===*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Attendance</span>
                    </div>
                    <div className="card-body">

                    <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">


                                        <div className="col-md-3 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSemesterSubject}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-3 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>
                                            <select name="" id="subject1" onChange={this.showASubject} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-3 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold">   <b>Subject: </b></label>
                                            <select name id className="form-control">
                                                <option value>--Choose--</option>
                                            </select>
                                        </div>
                                        <div className="col-md-3 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold">   <b>Month:</b></label>
                                            <select name id className="form-control">
                                                <option value>--Choose--</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>
                        {/*/filter*/}
                        <hr />
                        {/*Routine List*/}
                        <table className="table table-bordered table-responsive">
                            <thead className="text-center">
                                <tr>
                                    <th>Roll Number</th>
                                    <th>Name of the Student</th>
                                    <th>01</th>
                                    <th>02</th>
                                    <th>03</th>
                                    <th>04</th>
                                    <th>05</th>
                                    <th>06</th>
                                    <th>07</th>
                                    <th>08</th>
                                    <th>09</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <th>23</th>
                                    <th>24</th>
                                    <th>25</th>
                                    <th>26</th>
                                    <th>27</th>
                                    <th>28</th>
                                    <th>29</th>
                                    <th>30</th>
                                    <th>31</th>
                                </tr>
                            </thead>
                            <tbody className="text-center">
                                <tr>
                                      
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                            </tbody>
                        </table>
                        {/*/Routine List*/}
                    </div>
                    <div className="card-footer">
                        <a href="#" className="float-right btn btn-primary text-white"><i className="icon-action-redo" /> Export Report</a>
                    </div>
                </div>
                {/*-===/Body Content===*/}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for delete */}


            </div>

        );


    }
}
export default attendance