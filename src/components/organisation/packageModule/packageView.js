import React, { Component } from 'react';


class packageView extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">View Package</span>
                        <a href="package-create.html" className="float-right">+ Create New Package</a>
                    </div>
                    <div className="card-body">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name of Package</th>
                                    <th>Selected Modules</th>
                                    <th className="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Organization</td>
                                    <td>
                                        <span>Unit</span>, <span>Session</span>, <span>Examination</span>,
                                                <span>Group Chat</span>
                                    </td>
                                    <td className="text-center">
                                        <span className="btn btn-warning mt-2"><a href="package-edit.html"
                                            className="text-dark"><i className="icon-pencil"></i>
                                                        Add/Edit/Modify</a></span>
                                        <span className="btn btn-danger mt-2"><a href="#" className="text-white"
                                            data-toggle="modal" data-target="#deleteAlert"><i
                                                className="icon-trash"></i> Delete</a></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                {/*-modal*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}

            </div>
        );


    }
}
export default packageView