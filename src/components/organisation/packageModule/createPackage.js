import React, { Component } from 'react';


class createPackage extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3><span className="font-weight-bold text-primary mb-0"> Create New Package</span></h3>
                    </div>
                    <div className="card-body">
                        <b>Name of the Package* :</b> &nbsp;
                                    <input type="text" className="form-control-sm" />
                        <alert className="alert badge badge-pill mt-2 text-danger"><i className="fa fa-info-circle"></i> Package Name Already Exist</alert>
                        <hr />
                        <span className="h4 font-weight-bold">Please Select Modules:</span> <br />

                        <table className="mt-3 table table-responsive-md table-stripped table-bordered table-condensed">
                            <thead className="bg-light">
                                <tr>
                                    <th rowspan="2" className="border"><input type="checkbox" onclick="toggle(this);" /> Select All</th>
                                    <th rowspan="2" className="border">Name of Modules</th>
                                    <th colspan="3" className="text-center border">Action</th>
                                </tr>
                                <tr className="border">
                                    <th className="border">Read</th>
                                    <th className="border">Write</th>
                                    <th className="border">Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Unit</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Subject</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Session</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Group/Department/className</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>className Activity</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Attendance</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Examination</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Home work/Assignment</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Guide Notes</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Report Card</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Books</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Question Bank</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Routine</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Holiday List</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Performance of Teacher</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Performance of Students</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Payments/Fees Collection</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Forum</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Noticeboard</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" /></td>
                                    <td>Group Chat</td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                    <td><input type="checkbox" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="card-footer">
                        <button className="btn btn-primary bg-primary float-right"><i className="icon-check"></i> Save</button>
                        <button className="btn btn-light float-right bg-warning text-dark mr-2"><i className="icon-trash"></i> Discard</button>
                    </div>
                </div>
            </div>

        );


    }
}
export default createPackage