import React, { Component } from 'react';
import './adminBooksList.css';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import Bookedit from './bookEdit';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
import Bookview from './AdminBooksView'



class adminBooksList extends Component {
    constructor(props) {
        super(props);
        this.bookViews = this.bookViews.bind(this);

        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSectionSubjectAdd = this.showSectionSubjectAdd.bind(this);
        this.showSectionList = this.showSectionList.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        this.showSemesterSubject = this.showSemesterSubject.bind(this);
        this.showAllBooks = this.showAllBooks.bind(this);
        this.showBook = this.showBook.bind(this);
        this.deleteBook = this.deleteBook.bind(this);
        this.bookEdit = this.bookEdit.bind(this)


        this.state = {
            subject_i_d: 0,
            book_name: "",

            book_author_name: "",
            book_publication: "",
            bookList: [],



            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            subjectList1: [],
            editedBookID: 0,
            ii: 0,
            searchString: "",
            searchParams: []

        }
        this.bookSearchList = []
    }
    deleteBook(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  this Book!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletebook";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'book_i_d': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Book Deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    bookEdit(book) {
        ReactDOM.render(<Bookedit data = {book} unitList = {this.state.unitList} />, document.getElementById('editBookContent'));
    }
    bookViews(id2) {
        ReactDOM.render(<Bookview bookid={id2} />, document.getElementById('contain1'));
    }
    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }
    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }
    showSession(val) {

        this.updateParamsForSearch('unitID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }
    //Department
    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }
    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }
    showDepartment(val) {

        this.updateParamsForSearch('sessionID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }
    //Semester
    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }
    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }
    showSemester(val) {

        this.updateParamsForSearch('departmentID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }
    //Section
    showSectionList() {
        let val = document.getElementById("semester1").value;

        this.showSection(val);
    }
    showSectionSubjectAdd() {

        let val = document.getElementById("semester2").value;

        this.showSection(val);
        this.showSubject(val);

    }
    showSection(val) {

        this.updateParamsForSearch('semesterID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }
    //Subject
    showSubject(val) {

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            });
    }
    showSemesterSubject() {

        let val = document.getElementById("semester1").value
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList1: json
                })
            });

    }
    showAllBooks() {

        let val = document.getElementById("subject1").value
        this.updateParamsForSearch('subjectID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'subjectID': val })

        };


        let sessionURL = global.API + "/studapi/public/api/viewallbooks";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            });

    }
    submit() {
        var i = 0;
        if (i < 10) {
            if (document.getElementById("unit2").value == "") {
                document.getElementById("unit2Error").innerHTML = "*Please fill this field";
                document.getElementById("unit2").focus();
                document.getElementById("unit2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit2").value != "") {
                document.getElementById("unit2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit2").style.borderColor = "";
            }
            if (document.getElementById("session2").value == "") {
                document.getElementById("session2Error").innerHTML = "*Please fill this field";
                document.getElementById("session2").focus();
                document.getElementById("session2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session2").value != "") {
                document.getElementById("session2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("session2").style.borderColor = "";

            }



            if (document.getElementById("department2").value == "") {
                document.getElementById("department2Error").innerHTML = "*Please fill this field";
                document.getElementById("department2").focus();
                document.getElementById("department2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department2").value != "") {
                document.getElementById("department2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department2").style.borderColor = "";
            }

            if (document.getElementById("semester2").value == "") {
                document.getElementById("semester2Error").innerHTML = "*Please fill this field";
                document.getElementById("semester2").focus();
                document.getElementById("semester2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester2").value != "") {
                document.getElementById("semester2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("semester2").style.borderColor = "";
            }


            if (document.getElementById("subject_i_d").value == "") {
                document.getElementById("subject_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("subject_i_d").focus();
                document.getElementById("subject_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("subject_i_d").value != "") {

                document.getElementById("subject_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("subject_i_d").style.borderColor = "";
            }


            if (document.getElementById("book_publication").value == "") {
                document.getElementById("book_publicationError").innerHTML = "*Please fill this field";
                document.getElementById("book_publication").focus();
                document.getElementById("book_publication").style.borderColor = "#FF0000";
            }

            if (document.getElementById("book_publication").value != "") {
                document.getElementById("book_publicationError").innerHTML = "";
                i = i + 1;
                document.getElementById("book_publication").style.borderColor = "";
            }
            if (document.getElementById("book_author_name").value == "") {
                document.getElementById("book_author_nameError").innerHTML = "*Please fill this field";
                document.getElementById("book_author_name").focus();
                document.getElementById("book_author_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("book_author_name").value != "") {
                document.getElementById("book_author_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("book_author_name").style.borderColor = "";
            }
            if (document.getElementById("book_name").value == "") {
                document.getElementById("book_nameError").innerHTML = "*Please fill this field";
                document.getElementById("book_name").focus();
                document.getElementById("book_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("book_name").value != "") {
                document.getElementById("book_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("book_name").style.borderColor = "";
            }
            if (document.getElementById("bookcp").value == "") {
                document.getElementById("bookcpError").innerHTML = "*Please fill this field";
                document.getElementById("bookcp").focus();
                document.getElementById("bookcp").style.borderColor = "#FF0000";
            }

            if (document.getElementById("bookcp").value != "") {
                document.getElementById("bookcpError").innerHTML = "";
                i = i + 1;
                document.getElementById("bookcp").style.borderColor = "";
            }
            if (document.getElementById("bookurl").value == "") {
                document.getElementById("bookurlError").innerHTML = "*Please fill this field";
                document.getElementById("bookurl").focus();
                document.getElementById("bookurl").style.borderColor = "#FF0000";
            }

            if (document.getElementById("bookurl").value != "") {
                document.getElementById("bookurlError").innerHTML = "";
                i = i + 1;
                document.getElementById("bookurl").style.borderColor = "";
            }
            if (i == 10) {
                console.log(i);
                this.state.ii = 10;
            }
        }
        if (this.state.ii == 10) {
            console.log(this.state);
            let url = global.API + "/studapi/public/api/addbooks";
            let data = this.state;

            const fd = new FormData();
            $.each(data, function (key, value) {
                fd.append(key, value);
            })
            // console.log(document.getElementById("bookcp").files[0]);
            fd.append('book_cover_photo', document.getElementById("bookcp").files[0]);
            fd.append('book_u_r_', document.getElementById("bookurl").files[0]);
            console.log(fd);
            fetch(url, {
                method: 'POST',

                body: fd
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Book Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        this.componentDidMount();
                    }
                    else {
                        swal({
                            title: "Done!",
                            text: "Book not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                })
            })
        }
    }
    showBook(book) {

        ReactDOM.render(<Bookview bookdata={book} />, document.getElementById('contain1'))
    }
    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallorgabooks";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            })
            .then(() => {
                let count = Object.keys(this.state.bookList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });



        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }
    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Books!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletebook";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'book_i_d': value })
                            })
                        })
                        swal("All Selected Books Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Books cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }
    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }
    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {

        var { isLoaded, bookList, unitList, semesterList, sessonList1, sectionList, classList, subjectList, subjectList1 } = this.state;
        if (this.state.searchParams.length > 0) {
            this.bookSearchList = SearchForObjectsWithParams(bookList, this.state.searchParams)
        }
        else {
            this.bookSearchList = bookList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.bookSearchList = SearchForObjectsWithName(this.bookSearchList, this.state.searchString)
        }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold"> List of Books</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-target="#addBook" data-toggle="modal">+ Add New book</a></span>
                    </div>
                    <div className="card-body">
                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" onChange={this.showSemesterSection} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="" id="subject1" onChange={this.showAllBooks} className="form-control">
                                                <option value="" selected>Select</option>
                                                {subjectList1.map(subject => (
                                                    <option value={subject.subjectID}>{subject.subjectName}</option>

                                                ))}
                                            </select>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>

                        <div>
                            <div className="table-responsive">
                                <table className="table table-bordered ">
                                    <thead>
                                        <tr className="bg-light table-head-fixed">
                                            <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                            <th className="border-0">Preview</th>
                                            <th className="border-0">Name of the Book</th>
                                            <th className="border-0">Author of the Book</th>
                                            <th className="border-0">Publisher</th>
                                            <th className="border-0">Views</th>
                                            <th className="border-0">Online user</th>
                                            <th className="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.bookSearchList.map(book => (
                                            <tr>
                                                <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value={book.bookID} /></td>
                                                <td className="border-0">
                                                    <img src={global.img + book.bookCoverPhoto} alt="" className="book_preview" />
                                                </td>
                                                <td className="border-0">{book.bookName}</td>
                                                <td className="border-0">{book.bookAuthorName}</td>
                                                <td className="border-0">{book.bookPublication}</td>
                                                <td className="border-0">{book.bookViews}</td>
                                                <td className="border-0">{book.bookOnlineUser}</td>
                                                <td className=" border-0">
                                                    <span><a onClick={() => this.showBook(book)} className="text-primary font-weight-bold h4"><i className="icon-eye" title="preview Book" data-placement="top"></i> </a></span>

                                                    <span><a href="#" data-toggle="modal" onClick={() => this.bookEdit(book)} data-target="#editBook" className="text-warning font-weight-bold h4"><i className="icon-pencil" title="Edit Book" data-placement="top"></i> </a></span>

                                                    <span><a onClick={() => this.deleteBook(book.bookID)} className="text-danger font-weight-bold h4" ><i className="icon-trash" title="Delete Book" data-placement="top"></i> </a></span>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {/*-add subject book*/}
                {/*-modal for add new book*/}
                <div className="modal fade" id="addBook" tabIndex={-1} role="dialog" aria-labelledby="modelTitleaddBook" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold text-center">Add New Book</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>
                                                <span id="unit2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                       </div>
                                        <div className="col-md-6">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>

                                                <span id="session2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">
                                        <div className="col-md-4">
                                        <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">className/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                                <span id="department2Error" class="text-danger font-weight-bold"></span>
                                            </div>

                                        </div>
                                        <div className="col-md-4">
                                        <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="semester2" id="semester2" onChange={this.showSectionSubjectAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                                <span id="semester2Error" class="text-danger font-weight-bold"></span>

                                            </div>

                                        </div>
                                        <div className="col-md-4">

                                          <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="subject_i_d" name="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.setState({ subject_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                                <span id="subject_i_dError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>

                                   </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Publisers Name <span className="text-danger"><strong>*</strong></span>:</label>
                                                <input type="text" id="book_publication" name="book_publication" value={this.state.book_publication} onChange={(data) => { this.setState({ book_publication: data.target.value }) }} className="form-control" />
                                                <span id="book_publicationError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Author Name <span className="text-danger"><strong>*</strong></span>:</label>
                                                <input type="text" id="book_author_name" name="book_author_name" value={this.state.book_author_name} onChange={(data) => { this.setState({ book_author_name: data.target.value }) }} className="form-control" />
                                                <span id="book_author_nameError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">

                                        <div className="col-md-12">

                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Title of this Book <span className="text-danger"><strong>*</strong></span>:</label>
                                                <input type="text" id="book_name" name="book_name" value={this.state.book_name} onChange={(data) => { this.setState({ book_name: data.target.value }) }} className="form-control" />
                                                <span id="book_nameError" class="text-danger font-weight-bold"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload Cover Photo of the Book <span className="text-danger"><strong>*</strong></span>:</label>
                                                <input type="file" name="book_cover_photo" id="bookcp" className="form-control" />
                                                <span id="bookcpError" class="text-danger font-weight-bold"></span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload the book <span className="text-danger"><strong>*</strong></span>:</label>
                                                <input type="file" name="book_u_r_l" id="bookurl" className="form-control" />
                                                <span id="bookurlError" class="text-danger font-weight-bold"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => { this.submit() }} className="btn btn-success float-right ">Save and Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for add new book */}
                {/*-modal for editnew book*/}
                <div className="modal fade" id="editBook" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditBook" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div id="editBookContent"></div>
                     {/*
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold text-center">Edit New Book</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit :</label>
                                                <select name="" id="editBookUnitName" className="form-control" onChange={(e) => this.showSession(e.target.value)}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester:</label>
                                                <select id="editBookSemesterName" className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session:</label>
                                                <select id="editBookSessionName" className="form-control" onChange={(e) => this.showDepartment(e.target.value)}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>


                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">className/Department:</label>
                                                <select id="editBookClassName" onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject</label>
                                                <select id="editBookSubjectName" className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Publisers Name</label>
                                                <input type="text" id="editBookPublishersName" name="book_publication" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Author Name</label>
                                                <input type="text" id="editBookAuthorName" name="book_author_name" className="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">

                                        <div className="col-md-12">

                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Title of this Book</label>
                                                <input type="text" id="editBookTitleName" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload Cover Photo of the Book</label>
                                                <input type="file" id="editBookCoverPhoto" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload the book</label>
                                                <input type="file" id="editBookURL" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => this.onEditBookFormSubmit()} className="btn btn-success float-right ">Save and Publish</button>
                                </div>
                            </div>
                        </div>
                    */}
                    </div>
                </div>
                {/*-/modal for editnew book */}

            </div>


        );


    }
}
export default adminBooksList
