import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';
import Cookies from 'js-cookie';

class bookEdit extends Component {
   constructor(props){
      super(props)

      //****************              bindings               ******************//
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.showSubject = this.showSubject.bind(this);
         this.onEditBookFormSubmit = this.onEditBookFormSubmit.bind(this)
      //**********************************************************************//


      //*********** dropdown default values ********************//
         this.showSession(this.props.data.unitID)
         this.showDepartment(this.props.data.sessionID)
         this.showSemester(this.props.data.departmentID)
         this.showSubject(this.props.data.semesterID)
      //*****************************************************//

      //*******************      state variables       ***********************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            semesterID : this.props.data.semesterID,
            subjectID : this.props.data.subjectID,
            bookID : this.props.data.bookID,
            bookPublication : this.props.data.bookPublication,
            bookAuthorName : this.props.data.bookAuthorName,
            bookName : this.props.data.bookName,
            bookCoverPhoto : this.props.data.bookCoverPhoto,
            bookURL : this.props.bookURL,

            unitList : this.props.unitList,
            sessionList : [],
            classList : [],
            semesterList : [],
            subjectList : []
         }
      //****************************************************************//


   }

   //***********     handling change in props    **********************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

           //*********** dropdown default values ********************//
              this.showSession(this.props.data.unitID)
              this.showDepartment(this.props.data.sessionID)
              this.showSemester(this.props.data.departmentID)
              this.showSubject(this.props.data.semesterID)
           //*****************************************************//

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             departmentID : this.props.data.departmentID,
             semesterID : this.props.data.semesterID,
             subjectID : this.props.data.subjectID,
             bookID : this.props.data.bookID,
             bookPublication : this.props.data.bookPublication,
             bookAuthorName : this.props.data.bookAuthorName,
             bookName : this.props.data.bookName,
             bookCoverPhoto : this.props.data.bookCoverPhoto,
             bookURL : this.props.bookURL,

             unitList : this.props.unitList,
             sessionList : [],
             classList : [],
             semesterList : [],
             subjectList : []
          })
        }
      }
   //*****************************************************************************//




   //************************ Dropdown async Calls ***************************//
   showSession(val) {
      this.setState({
         sessionList : [],
         classList : [],
         semesterList : [],
         sectionList : [],
         subjectList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });
   }

   showDepartment(val) {
      this.setState({
         classList : [],
         semesterList : [],
         sectionList : [],
         subjectList : [],

         sessionID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'session_i_d': val })

      };
      let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
      fetch(classURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   classList: json
               })
           });
   }

   showSemester(val) {
      this.setState({
         semesterList : [],
         sectionList : [],
         subjectList : [],
         departmentID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'department_i_d': val })

      };
      let semesterURL = global.API + "/studapi/public/api/getallsemidname";
      fetch(semesterURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   semesterList: json
               })
           });
   }

   showSubject(val) {
      this.setState({subjectList : [], semesterID : val});
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'semester_i_d': val })
      };
      let subUrl = global.API + "/studapi/public/api/getallsubjectidname";
      fetch(subUrl, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   subjectList: json
               })
           });
   }

   //************************************************************************************//

//***************       book form submission      ******************************//
   onEditBookFormSubmit() {
      let url = global.API + "/studapi/public/api/updatebooks";
      let coverPhoto = this.state.bookCoverPhoto;
      if(document.getElementById('editBookCoverPhoto').files[0] != "")
         coverPhoto = document.getElementById('editBookCoverPhoto').files[0]
      let  book_url = this.state.bookURL;
      if( document.getElementById("editBookURL").files[0] != "")
         book_url =  document.getElementById("editBookURL").files[0]
      const fd = new FormData();
      fd.append('book_i_d', this.state.bookID);
      fd.append('subject_i_d', this.state.subjectID);
      fd.append('book_name', this.state.bookName);
      fd.append('book_cover_photo', coverPhoto);
      fd.append('book_author_name', this.state.bookAuthorName);
      fd.append('book_publication', this.state.bookPublication);
      fd.append('book_u_r_l', book_url);
      fd.append('book_editor_id', Cookies.get('orgid'));
      fetch(url, {
           method: 'POST',

           body: fd
      }).then((result) => {
           result.json().then((resp) => {
               console.log(resp);
               try {
                   if (resp[0].result == 1) {
                       swal("Book Updated!", {
                           icon: "success",
                       });
                       $('#editBook').modal("hide");
                       // this.componentDidMount();
                   }
                   else {
                       swal("There's something wrong!! :(", {
                           icon: "error",
                       });
                   }
               }
               catch (err) {
                   swal("One of the important fields is missing :(", {
                       icon: "error",
                   });
               }
           })
      })
   }
//******************************************************************************//
render() {

        return (
            <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold text-center">Edit New Book</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="" id="unit2" className="form-control" value = {this.state.unitID} onChange = {(e)=>{this.showSession(e.target.value)}}>
                                                    <option value="" selected>Select</option>
                                                       {this.state.unitList.map(unit => (
                                                          <option value={unit.unitID}>{unit.unitName}</option>
                                                       ))}
                                                </select>
                                                <span id="unit2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                       </div>
                                        <div className="col-md-6">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="session2" className="form-control" value = {this.state.sessionID} onChange = {(e)=>{this.showDepartment(e.target.value)}}>
                                                    <option value="" selected>Select</option>
                                                       {this.state.sessionList.map(session => (
                                                          <option value={session.sessionID}>{session.session}</option>
                                                       ))}
                                                </select>

                                                <span id="session2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">
                                        <div className="col-md-4">
                                        <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">className/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="department2"  className="form-control" value = {this.state.departmentID} onChange = {(e)=>{this.showSemester(e.target.value)}}>
                                                    <option value="" selected>Select</option>
                                                       {this.state.classList.map(classes => (
                                                          <option value={classes.departmentID}>{classes.departmentName}</option>
                                                      ))}
                                                </select>
                                                <span id="department2Error" class="text-danger font-weight-bold"></span>
                                            </div>

                                        </div>
                                        <div className="col-md-4">
                                        <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="semester2" id="semester2"  className="form-control" value = {this.state.semesterID} onChange = {(e)=>{this.showSubject(e.target.value)}}>
                                                    <option value="" selected>Select</option>
                                                       {this.state.semesterList.map(semester => (
                                                         <option value={semester.semesterID}>{semester.semesterName}</option>
                                                      ))}
                                                </select>
                                                <span id="semester2Error" class="text-danger font-weight-bold"></span>

                                            </div>

                                        </div>
                                        <div className="col-md-4">

                                          <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="subject_i_d" name="subject_i_d" value = {this.state.subjectID} onChange = {(e)=>{this.setState({subjectID : e.target.value})}}  className="form-control">
                                                    <option value="" selected>Select</option>
                                                       {this.state.subjectList.map(subject => (
                                                          <option value={subject.subjectID}>{subject.subjectName}</option>
                                                      ))}
                                                </select>
                                                <span id="subject_i_dError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>

                                   </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Publisers Name</label>
                                                <input type="text" id="editBookPublishersName" name="book_publication" value = {this.state.bookPublication} onChange = {(e)=>{this.setState({bookPublication : e.target.value})}} className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Author Name</label>
                                                <input type="text" id="editBookAuthorName" name="book_author_name" className="form-control" value = {this.state.bookAuthorName} onChange = {(e)=>{this.setState({bookAuthorName : e.target.value})}} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">

                                        <div className="col-md-12">

                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Title of this Book</label>
                                                <input type="text" id="editBookTitleName" className="form-control" value = {this.state.bookName} onChange = {(e)=>{this.setState({bookName : e.target.value})}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload Cover Photo of the Book</label>
                                                <input type="file" id="editBookCoverPhoto" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload the book</label>
                                                <input type="file" id="editBookURL" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => this.onEditBookFormSubmit()} className="btn btn-success float-right ">Save and Publish</button>
                                </div>
                            </div>
                        </div>


        );


    }
}
export default bookEdit
