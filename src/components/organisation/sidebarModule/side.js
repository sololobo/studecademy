import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Dashboard from '../dashboardModule/dashboard'
import Unit from '../unitModule/unitList'
import Session from '../sessionModule/sessionList'
import Classes from '../classModule/classList'
import Section from '../sectionModule/sectionList'
import Sem from '../semesterModule/semesterList'
import Subject from '../subjectModule/subjectList'
import Teachers from '../mentorModule/mentorList'
import Students from '../studentModule/studentList'
import Assignment from '../assignmentModule/adminAssignmentList'
import Books from '../bookModule/adminBooksList'
import Profile from '../profileModule/orgProfile'
import AdminRoutineList from '../routineModule/adminRoutineList.js'
import Feed from '../feedModule/feed'
import Attendance from '../attendanceMdule/attendance'
import Classactivity from '../classactivityModule/classActivity'
import Guidenote from '../guidenoteModule/guidenotes'
import Questionbank from '../questionbankModule/QuestionBank'
import Exam from '../examModule/test'
import Notice from '../noticeModule/notice'
import Room from '../roomModule/room'
import Holiday from '../holidayModule/holiday'
// import TestComponent from '../testComponent/testComponent.js'

class Side extends Component {
    constructor(props) {
        super(props);
        this.showDashboard = this.showDashboard.bind(this);
        this.showUnit = this.showUnit.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showClasses = this.showClasses.bind(this);
        this.showSemister = this.showSemister.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        this.showTeachers = this.showTeachers.bind(this);
        this.showStudents = this.showStudents.bind(this);
        this.showAss = this.showAss.bind(this);
        this.showBooks = this.showBooks.bind(this);
        this.showProfile = this.showProfile.bind(this);
        this.showRoutine = this.showRoutine.bind(this);
        this.feed = this.feed.bind(this);
        this.attendance = this.attendance.bind(this);
        this.classactivity = this.classactivity.bind(this);
        this.guidenote = this.guidenote.bind(this);
        this.questionbank = this.questionbank.bind(this);
        this.exam = this.exam.bind(this);
        this.notice = this.notice.bind(this);
        this.room = this.room.bind(this);
        this.holiday = this.holiday.bind(this);
        this.dropdowntoggler1 = this.dropdowntoggler1.bind(this);
        this.dropdowntoggler2 = this.dropdowntoggler2.bind(this);

    }
    showDashboard() {

        ReactDOM.render(<Dashboard />, document.getElementById('contain1'))

    }
    showUnit() {

        ReactDOM.render(<Unit />, document.getElementById('contain1'))

    }
    showSession() {

        ReactDOM.render(<Session />, document.getElementById('contain1'))

    }
    showClasses() {

        ReactDOM.render(<Classes />, document.getElementById('contain1'))

    }
    showSubject() {

        ReactDOM.render(<Subject />, document.getElementById('contain1'))

    }
    showTeachers() {

        ReactDOM.render(<Teachers />, document.getElementById('contain1'))

    }
    showStudents() {

        ReactDOM.render(<Students />, document.getElementById('contain1'))

    }
    showAss() {

        ReactDOM.render(<Assignment />, document.getElementById('contain1'))

    }
    showBooks() {

        ReactDOM.render(<Books />, document.getElementById('contain1'))

    }
    showSection() {

        ReactDOM.render(<Section />, document.getElementById('contain1'))

    }
    showSemister() {

        ReactDOM.render(<Sem />, document.getElementById('contain1'))

    }
    showProfile() {

        ReactDOM.render(<Profile />, document.getElementById('contain1'))

    }
    showRoutine() {
        ReactDOM.render(<AdminRoutineList />, document.getElementById('contain1'))
    }
    feed() {

        ReactDOM.render(<Feed />, document.getElementById('contain1'))

    }
    attendance() {

        ReactDOM.render(<Attendance />, document.getElementById('contain1'))

    }
    classactivity() {
        ReactDOM.render(<Classactivity />, document.getElementById('contain1'))
    }
    guidenote() {
        ReactDOM.render(<Guidenote />, document.getElementById('contain1'))
    }
    questionbank() {
        ReactDOM.render(<Questionbank />, document.getElementById('contain1'))
    }
    exam() {
        ReactDOM.render(<Exam />, document.getElementById('contain1'))
    }
    notice() {
        ReactDOM.render(<Notice />, document.getElementById('contain1'))
    }
    room() {
        ReactDOM.render(<Room />, document.getElementById('contain1'))
    }
    holiday() {
        ReactDOM.render(<Holiday />, document.getElementById('contain1'))
    }
    dropdowntoggler1() {
        if (document.getElementById("nav-dropdown-troggler-1").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-1").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-1").className += " open"
        }
    }
    dropdowntoggler2() {
        if (document.getElementById("nav-dropdown-troggler-2").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-2").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-2").className += " open"
        }
    }
    dropdowntoggler3() {
        if (document.getElementById("nav-dropdown-troggler-3").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-3").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-3").className += " open"
        }
    }
    dropdowntoggler4() {
        if (document.getElementById("nav-dropdown-troggler-4").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-4").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-4").className += " open"
        }
    }
    dropdowntoggler5() {
        if (document.getElementById("nav-dropdown-troggler-5").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-5").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-5").className += " open"
        }
    }
    dropdowntoggler6() {
        if (document.getElementById("nav-dropdown-troggler-6").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-6").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-6").className += " open"
        }
    }
    render() {


        return ([

            <div className="sidebar">
                <nav className="sidebar-nav">
                    <ul className="nav">
                        <li className="nav-item">
                            <a onClick={this.showProfile} className="nav-link"><i className="icon-user-follow" />My Organisation</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showDashboard} className="nav-link"><i className="icon-user-follow" />My Dashboard</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showTeachers} className="nav-link"><i className="icon-user-follow" /> Teachers</a>
                        </li>
                        <li class="nav-item nav-dropdown " id="nav-dropdown-troggler-1">
                            <a class="nav-link nav-dropdown-toggle " onClick={this.dropdowntoggler1}><i class="icon-paper-clip"></i>Unit </a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.showUnit} className="nav-link"><i className="icon-user-follow" /> Unit Detail</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showUnit} className="nav-link"><i className="icon-user-follow" />Teacher Mapping  </a>
                                </li>

                                <li className="nav-item">
                                    <a onClick={this.showSession} className="nav-link"><i className="icon-doc" /> Session</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showClasses} className="nav-link"><i className="icon-badge" /> Class/Department</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showSemister} className="nav-link"><i className="icon-badge" /> Semester/Term</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showSection} className="nav-link"><i className="icon-badge" /> Section</a>
                                </li>
                                <li className="nav-item">

                                    <a onClick={this.showSubject} className="nav-link"><i className="icon-doc" /> Subjects </a>
                                </li>
                                <li className="nav-item">

                                    <a onClick={this.holiday} className="nav-link"><i className="icon-book-open" /> Holidays</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.room} className="nav-link"><i className="icon-badge" /> Rooms </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showRoutine} className="nav-link"><i className="icon-notebook" /> Routine</a>
                                </li>
                            </ul>
                        </li>
                        {/*  <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-2">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler2}></a>
                            <ul class="nav-dropdown-items">

                                <li className="nav-item">

                                    <a onClick={this.showSubject} className="nav-link"></a>
                                </li>
                               
                            </ul>
        </li>*/}
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-3">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler3}><i class="icon-paper-clip"></i>Students</a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.showAss} className="nav-link"><i className="icon-notebook" />Admission  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showStudents} className="nav-link"><i className="icon-user-follow" />Student's Detail</a>
                                </li>

                                <li className="nav-item">

                                    <a onClick={this.showSubject} className="nav-link"><i className="icon-doc" /> Fees  </a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-4">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler4}><i class="icon-paper-clip"></i>Activities </a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.attendance} className="nav-link"><i className="icon-book-open" /> Attendance  </a>
                                </li>

                                <li className="nav-item">
                                    <a onClick={this.classactivity} className="nav-link"><i className="icon-book-open" />Activity Log   </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showAss} className="nav-link"><i className="icon-notebook" /> Assignment</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.guidenote} className="nav-link"><i className="icon-book-open" /> Guide Note  </a>
                                </li>

                                <li className="nav-item">
                                    <a onClick={this.notice} className="nav-link"><i className="icon-book-open" />Notice Board   </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.feed} className="nav-link"><i className="icon-book-open" /> Feedback  </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-5">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler5}><i class="icon-paper-clip"></i>Performance</a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.showRoutine} className="nav-link"><i className="icon-notebook" /> Marks Distribution  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.exam} className="nav-link"><i className="icon-book-open" />Exam  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.questionbank} className="nav-link"><i className="icon-book-open" /> Question Bank  </a>
                                </li>

                                <li className="nav-item">

                                    <a onClick={this.showSubject} className="nav-link"><i className="icon-doc" />Markes  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showAss} className="nav-link"><i className="icon-notebook" />Admit Card  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showStudents} className="nav-link"><i className="icon-user-follow" />Report Card  </a>
                                </li>


                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-6">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler6}><i class="icon-paper-clip"></i>Resources </a>
                            <ul class="nav-dropdown-items">

                                <li className="nav-item">
                                    <a onClick={this.showBooks} className="nav-link"><i className="icon-book-open" /> Books</a>
                                </li>


                            </ul>
                        </li>


                    </ul>
                </nav>
                <button className="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>



        ]

        );


    }
}
export default Side;
