import React, { Component } from 'react';


class EditExam extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h4><b>Add new test:</b></h4>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Name of the Test*:</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Class/Department/Batch*:</label>
                                    <select className="form-control">
                                        <option value>-- All Department --</option>
                                        <option value>Mechnical</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Semester/Section*:</label>
                                    <select className="form-control">
                                        <option value>-- All Semester --</option>
                                        <option value>Mechnical</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Subject*:</label>
                                    <select className="form-control">
                                        <option value>-- All Subject --</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Chapter*:</label>
                                    <select className="form-control">
                                        <option value>-- All Chapter --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Full Marks*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Pass Marks*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Duration*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Starting Date*:
            </label>
                                    <input type="date" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Ending Date*:
            </label>
                                    <input type="date" className="form-control" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Course Objective*:</label>
                                    <textarea name id cols={3} className="form-control" defaultValue={""} />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Addional Information*:</label>
                                    <textarea name id cols={3} className="form-control" defaultValue={""} />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            {/*<span class=" font-weight-bold h3 border-title">Upload Test:</span>
          <div class="form-group files mt-3">
            <label>Upload Your File </label>
            <input type="file" class="form-control" multiple="">
          </div>
          <hr>*/}
                            <div className="mb-2">
                                <select name id className="form-control-sm">
                                    <option value>--Select Option--</option>
                                    <option value>Delete</option>
                                </select>
                                <button type="button" className="btn btn-sm">Save</button>
                            </div>
                            <table className="table table-bordered text-center">
                                <thead>
                                    <tr>
                                        <th rowSpan={2}><input type="checkbox" name id onclick="toggle(this);" /></th>
                                        <th rowSpan={2}>Name of the question</th>
                                        <th colSpan={4}> Options</th>
                                        <th rowSpan={2}>Solution</th>
                                        <th rowSpan={2}>Right Answer</th>
                                        <th colSpan={2}>Marks</th>
                                        <th rowSpan={2}>Action</th>
                                    </tr>
                                    <tr>
                                        <th>Option A</th>
                                        <th>Option B</th>
                                        <th>Option C</th>
                                        <th>Option D</th>
                                        <th>Marks</th>
                                        <th>Negative Marks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" name id /></td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td className="m-0">
                                            <input type="text" className="form-control border-0" />
                                        </td>
                                        <td className="m-0">
                                            <input type="text" className="form-control border-0" />
                                        </td>
                                        <td className="m-0">
                                            <textarea name id rows={2} className="form-control border-0" defaultValue={""} />
                                        </td>
                                        <td>
                                            <a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" /></a>
                                            <a href="#" className="text-primary"><i className="icon-plus" /></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {/*-modal for delete alert*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}


            </div>


        );


    }
}
export default EditExam