
import React, { Component } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-inline';
import { Redirect } from 'react-router-dom';


class CreateExam extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h4><b>Add new test:</b></h4>
                    </div>
                    <div className="card-body">
                        <div className="row p-3">
                            <div className="form-group">
                                <b>Select Questions Type:</b>
                                <select name id className="form-control-sm">
                                    <option value>MCQ</option>
                                    <option value>Descriptive</option>
                                </select>
                            </div>
                            <div className="form-group ml-4">
                                <b>Maximum Number:</b>
                                <input type="number" className="form-control-sm" />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Name of the Test*:</label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Class/Department/Batch*:</label>
                                    <select className="form-control">
                                        <option value>-- All Department --</option>
                                        <option value>Mechnical</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Semester/Section*:</label>
                                    <select className="form-control">
                                        <option value>-- All Semester --</option>
                                        <option value>Mechnical</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Subject*:</label>
                                    <select className="form-control">
                                        <option value>-- All Subject --</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Chapter*:</label>
                                    <select className="form-control">
                                        <option value>-- All Chapter --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Full Marks*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Pass Marks*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-2">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Duration*:
            </label>
                                    <input type="text" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Starting Date*:
            </label>
                                    <input type="date" className="form-control" />
                                </div>
                            </div>
                            <div className="col-md-3">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">
                                        Ending Date*:
            </label>
                                    <input type="date" className="form-control" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Course Objective*:</label>
                                    <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>

                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label htmlFor className="mb-0 font-weight-bold">Addional Information*:</label>

                                    <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>


                                </div>
                            </div>
                        </div>
                        <hr />
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 border-title">Upload Test:</span>
                            <div className="form-group files mt-3">
                                <label>Upload Your File </label>
                                <input type="file" className="form-control" multiple />
                            </div>
                            <hr />
                            {/*-Display if user select MCQ*/}
                            <div className="row p-3">
                                <b>Question No.: &nbsp;</b>
                                <input type="number" className="form-control-sm mb-3" />
                                <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>
                        {/*-Options are given below*/}
                                <div className="col-md-6">
                                    <div className="form-group row mt-3">
                                        <b className="col-md-1">A.
                <input type="checkbox" /></b>

                                     <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>

                                    </div>
                                    <div className="form-group row mt-3">
                                        <b className="col-md-1">B.
                <input type="checkbox" /></b>

                                     <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>

                                    </div>
                                    <div className="form-group row mt-3">
                                        <b className="col-md-1">C.
                <input type="checkbox" /></b>

                                     <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>

                                    </div>
                                    <div className="form-group row mt-3">
                                        <b className="col-md-1">D.
                <input type="checkbox" /></b>

                                     <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>

                                    </div>
                                </div>
                                {/*Question information*/}
                                <div className="col-md-6 mt-3">
                                    <div className="form-group mr-2">
                                        <b>Marks :*</b>
                                        <input type="text" className="form-control-sm" />
                                        <b className=" ml-2">Negative Marks:</b>
                                        <input type="text" className="form-control-sm" />
                                    </div>
                                    <hr />
                                    <div className="form-group mr-2">
                                    <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>
                       </div>
                                </div>
                            </div>{/*/row*/}
                            <hr />
                            <div className="justify-content-center">
                                <button className="btn btn-primary text-center justify-content-center">+ Add Next Question</button>
                                <a href="#" data-toggle="modal" data-target="deleteAlert" className="text-danger float-md-right"><i className="icon-trash" /> Delete This Question</a>
                            </div>
                            <hr />
                            {/*/Display if user select MCQ*/}
                            {/*-Display if user select descriptive*/}
                            <div className="row p-3">
                                <b>Question No.: &nbsp;</b>
                                <input type="number" className="form-control-sm mb-3" />
                                <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>
                         {/*-Options are given below*/}
                                <div className="col-md-6 mt-3">
                                    <div className="form-group">
                                    <div className="message-area">
                                <CKEditor
                                    editor={ClassicEditor}
                                    data=""
                                    rows={4} cols={50}
                                    onInit={editor => {
                                        // You can store the "editor" and use when it is needed.
                                        console.log('Editor is ready to use!', editor);
                                    }}
                                    onChange={(event, editor) => {
                                        const data = editor.getData();
                                        console.log({ event, editor, data });
                                    }}
                                    onBlur={(event, editor) => {
                                        console.log('Blur.', editor);
                                    }}
                                    onFocus={(event, editor) => {
                                        console.log('Focus.', editor);
                                    }}
                                /></div>
                        </div>
                                </div>
                                {/*Question information*/}
                                <div className="col-md-6 mt-3">
                                    <div className="form-group mr-2">
                                        <b>Marks :*</b>
                                        <input type="text" className="form-control-sm" />
                                        <b className=" ml-2">Negative Marks:</b>
                                        <input type="text" className="form-control-sm" />
                                    </div>
                                </div>
                            </div>{/*/row*/}
                            <hr />
                            <div className="justify-content-center">
                                <button className="btn btn-primary text-center justify-content-center">+ Add Next Question</button>
                                <a href="#" data-toggle="modal" data-target="deleteAlert" className="text-danger float-md-right"><i className="icon-trash" /> Delete This Question</a>
                            </div>
                            <hr />
                            {/*/Display if user select descriptive*/}
                        </div>
                    </div>{/*/card-body*/}
                    <div className="card-footer text-md-right">
                        <button className="btn btn-success "><i className="icon-check" /> Save</button>
                        <button className="btn btn-secondary "><i className="icon-doc" /> Schedule</button>
                        <button className="btn btn-warning "><i className="icon-action-undo" /> Reset</button>
                    </div>
                </div>{/*/card*/}

                {/*-modal for delete alert*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}

            </div>


        );


    }
}
export default CreateExam
