
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Examedit from './editExam'
import Examadd from './createExam'
import Examreport from './report'


class Test extends Component {
    constructor(props) {
        super(props);
        this.addExam = this.addExam.bind(this);
        this.examReport = this.examReport.bind(this);
    }
    addExam() {

        ReactDOM.render(<Examadd />, document.getElementById('contain1'))

    }
    editExam() {

        ReactDOM.render(<Examedit />, document.getElementById('contain1'))

    }
    examReport() {

        ReactDOM.render(<Examreport />, document.getElementById('contain1'))

    }
    render() {

        return (

            <div className="fadeIn animated">
                <div className="card">


                    <div className="card-header">

                        <div className="row">
                            <div className="col-md-6 form-group">
                                <h2><b>List of Tests:</b></h2>
                            </div>
                            <div className="col-md-6 form-group">
                                <span className="float-right">
                                    <a onClick={this.addExam} className="btn btn-primary text-white"><i className="icon-plus" /> Create New Test</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">

                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">


                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSemesterSubject}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="" id="subject1" onChange={this.showASubject} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold">   <b>Status: </b></label>
                                            <select name id className="form-control">
                                                <option value>--Choose Status--</option>
                                                <option value>Published</option>
                                                <option value>Scheduled</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>
                        <div>
                            {/*create new exam*/}

                        </div>
                        <hr />
                        {/*test info*/}
                        <div className="table-responsive">
                            <table className="table table-bordered datatable border">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0"><input type="checkbox" onclick="toggle(this);" /></th>
                                        <th className="border-0">Name of the Exam</th>
                                        <th className="border-0">Taken By</th>
                                        <th className="border-0">Full Marks</th>
                                        <th className="border-0">Published on</th>
                                        <th className="border-0">Status</th>
                                        <th className="border-0">Test Taken</th>
                                        <th className="border-0">Analytics</th>
                                        <th className="border-0">Action</th>
                                      
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="border-0"><input type="checkbox" /></td>
                                        <td className="border-0"><a href="edit-exam.html">Exam name I</a></td>
                                        <td className="border-0">Teacher Name</td>
                                        <td className="border-0">45</td>
                                        <td className="border-0">25-07-2020</td>
                                        <td className="border-0"><span className="badge badge-pill badge-light">Active</span></td>
                                        <td className="border-0">25</td>
                                       
                                        <td className="border-0">
                                            <a onClick={this.examReport} className="btn btn-success text-white"><i className="icon-action-redo" /> View Report</a>
                                        </td>
                                        <td className="border-0">
                                            <span><a  onClick={() => this.editExam()} className="text-warning"><i
                                                className="icon-pencil"></i></a></span>
                                            <span><a onClick={() => this.deleteExam()} className="text-danger"><i className="icon-trash"></i>
                                            </a></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {/*test info--*/}
                    </div>{/*Card body*/}
                </div>
            </div>


        );


    }
}
export default Test