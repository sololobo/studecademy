import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';

class semesterEdit extends Component {
   constructor(props){
      super(props)

      //***************              bindings               *********************//
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.onEditSemesterFormSubmit = this.onEditSemesterFormSubmit.bind(this)
      //**********************************************************************//

      //**************** dropdown default values ***************//
         this.showSession(this.props.data.unitID)
         this.showDepartment(this.props.data.sessionID)
      //*****************************************************//

      //***************      state variables       ***************************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            semesterID : this.props.data.semesterID,
            semesterName : this.props.data.semesterName,
            numOfStudentsRegistered : this.props.data.numOfStudentsRegistered,
            status : this.props.data.status,

            unitList : this.props.unitList,
            sessionList : [],
            classList : [],
         }
      //****************************************************************//
   }

   //***********     handling change in props    **********************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

           //**************** dropdown default values ***************//
              this.showSession(this.props.data.unitID)
              this.showDepartment(this.props.data.sessionID)
           //*****************************************************//

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             departmentID : this.props.data.departmentID,
             semesterID : this.props.data.semesterID,
             semesterName : this.props.data.semesterName,
             numOfStudentsRegistered : this.props.data.numOfStudentsRegistered,
             status : this.props.data.status,

             unitList : this.props.unitList,
             sessionList : [],
             classList : []
          })
        }
      }
   //*****************************************************************************//

   //************************ Dropdown async Calls *******************************//

   showSession(val) {
      this.setState({
         sessionList : [],
         classList : [],
         semesterList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });

   }

   showDepartment(val) {
      this.setState({
         classList : [],
         semesterList : [],
         sessionID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'session_i_d': val })

      };
      let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
      fetch(classURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   classList: json
               })
           });
   }
   //************************************************************************************//

//*******************     edit session form     *******************//
   onEditSemesterFormSubmit() {
      let url = global.API + "/studapi/public/api/editsemester";
      let data = {
           semester_i_d: this.state.semesterID,
           department_i_d: this.state.departmentID,
           semester_name: this.state.semesterName,
           num_of_students_registered: this.state.numOfStudentsRegistered,
           status: this.state.status,
      }
      fetch(url, {
           method: 'POST',
           headers: {
               "Content-Type": "application/json",
               "Accept": "application/json"
           },
           body: JSON.stringify(data)
      }).then((result) => {
           result.json().then((resp) => {
               try {
                   if (resp[0].result == 1) {
                       swal("Semester Updated! Refresh to see the result", {
                           icon: "success",
                       });
                       $('#editSemester').modal("hide");
                   }
                   else {
                       swal("There's something wrong!! :(", {
                           icon: "error",
                       });
                   }
               }
               catch{
                   swal("There's something wrong!! :(", {
                       icon: "error",
                   });
               }
           })
      })
   }
//***************************************************************************//

render() {

        return (
            <div className="modal-content">
            <div className="modal-header">
                <h4><span className="font-weight-bold"> Edit Semester</span></h4>
            </div>
            <div className="modal-body">
                <div className="container-fluid">
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the unit  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select className="form-control" id="editSemesterUnitName" value = {this.state.unitID} onChange={(e) => this.showSession(e.target.value)}>
                            <option value="" selected>Unit</option>
                               {this.state.unitList.map(unit => (
                                  <option value={unit.unitID}>{unit.unitName}</option>
                              ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Session  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select className="form-control" id="editSemesterSessionName" value = {this.state.sessionID} onChange={(e) => this.showDepartment(e.target.value)}>
                            <option value="" selected>Session</option>
                               {this.state.sessionList.map(session => (
                                  <option value={session.sessionID}>{session.session}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Class/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select className="form-control" id="editSemesterClassName"  value = {this.state.departmentID} onChange={(e) => this.setState({departmentID : e.target.value})}>
                            <option value="" selected>Class/Department</option>
                               {this.state.classList.map(classes => (
                                   <option value={classes.departmentID}>{classes.departmentName}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the  Semester  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="text" id="editSemesterName" value = {this.state.semesterName} onChange = {(e)=>this.setState({semesterName : e.target.value})} className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered  :</label>
                        <input id="editSemesterStudents" type="text" className="form-control" value = {this.state.numOfStudentsRegistered} onChange = {(e)=>this.setState({numOfStudentsRegistered : e.target.value})} />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select className="form-control" id="editSemesterStatus" value = {this.state.status} onChange = {(e)=>this.setState({status : e.target.value})}>
                            <option value="Active" selected>Active</option>
                            <option value="Pending">Pending</option>
                            <option value="Deactive">Deactive</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button"  className="btn btn-warning" onClick = {(e) => this.onEditSemesterFormSubmit()}>Update Semester</button>
            </div>
        </div>

        );


    }
}
export default semesterEdit
