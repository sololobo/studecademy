import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Guidenoteview from './guidenotesPreview'

class createGuidenotes extends Component {
    constructor(props) {
        super(props);
        this.guidenoteview = this.guidenoteview.bind(this);
    }
    guidenoteview() {

        ReactDOM.render(<Guidenoteview />, document.getElementById('contain1'))

    }
    render() {
     
        return (
           <div className="fadeIn animated">
  {/*------------- Article Section---*/}
  <h4><b>Create Tutorial</b></h4>
  {/*--------Create Article--*/}
  <div className="card">
    <div className="card-body">
      {/*--- article section--*/}
      <div className="row">
        <div className="col-md-8 col-xs-12">
          <div className="form-group">
            <h5><b>Title</b></h5>
            <input type="text" name="title" className="form-control" />
          </div>
          <div className="form-group">
            <h5><b>Write Article</b></h5>
            <textarea className="form-control" rows={10} defaultValue={""} />
            <h3 className="font-weight-bold text-center">OR</h3>
            <div className="form-group mt-2">
              <label htmlFor className="font-weight-bold">Upload Files(.pdf,docx, .pptx allowed)</label>
              <input type="file" className="form-control file" />
            </div>
            <div className="form-control mt-2">
              <label htmlFor className="font-weight-bold">Name of the Test</label>
              <input type="text" className="form-control mb-2" />
              <button className="btn btn-primary">Assign Test</button>
              <button className="btn btn-dark">Create New Test</button>
            </div>
          </div>
          <div>
          </div>
        </div>
        <div className="col-md-4 col-xs-12">
          <div className="card-body">
            <button className="btn btn-success"> Preview </button>
            <button className="btn btn-primary"> Publish Now </button>
            <button className="btn btn-secondary"> Save Draft </button>
            <button className="btn btn-danger" data-toggle="modal" data-target="#deleteAlert"> Delete </button>
          </div>
          <div className="card-body">
            Select Session
            <select className="form-control mb-2">
              <option> 2019 </option>
              <option> 2018 </option>
              <option> 2016 </option>
            </select>
            Select Department
            <select className="form-control mb-2">
              <option> AEIE </option>
              <option> CSE </option>
              <option> EE </option>
            </select>
            Select Semester
            <select className="form-control mb-2">
              <option> 1st Semester </option>
              <option> 2nd Semester</option>
              <option> 3rd Semester</option>
            </select>
            Select Subject
            <select className="form-control mb-2">                              
              <option> Physics</option>
              <option> Basic Electronics</option>
            </select>
            Select Chapter
            <select className="form-control mb-2">                              
              <option> Chapter 1</option>
              <option> Basic Electronics</option>
            </select>
            Published Till
            <input type="date" className="form-control" />
          </div>
        </div>                    
      </div>
      {/*----/article section--*/}
    </div>
  </div>
  {/*--------/create article-*/}
  {/*----------/Article Section-----*/}
</div>

        );


    }
}
export default createGuidenotes