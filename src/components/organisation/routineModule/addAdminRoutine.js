import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert'
import './routine.css'

class AddAdminRoutine extends Component {

      constructor(props){
         super(props);
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.showSection = this.showSection.bind(this);
         this.makeTimeTableVisible = this.makeTimeTableVisible.bind(this);
         this.updateTimeTable = this.updateTimeTable.bind(this);
         this.handleSectionIDChange = this.handleSectionIDChange.bind(this);
         this.handleRoutineFieldsChange = this.handleRoutineFieldsChange.bind(this);
         this.updateRoutine = this.updateRoutine.bind(this);
         this.state = {
            unitList: [],
            sessionList: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            teacherList: [],

            routineID : '',
            numberOfPeriods: 0,
            nameOfRoutine:'',
            sectionID : '',
            timeList : [],
            routineList : {
               Monday : {},
               Tuesday :{},
               Wednesday : {},
               Thursday : {},
               Friday : {},
               Saturday : {},
               Sunday :{}
            },
         }
      }

      componentDidMount() {

          const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

          };


          let unitURL = "http://3.7.45.100/studapi/public/api/getallunitidname";
          fetch(unitURL, requestOptions)
             .then(res => res.json())
             .then(json => {
                  this.setState({
                     unitList: json
                  })
             });

             let teacherURL = "http://3.7.45.100/studapi/public/api/getallteacheridname";
             fetch(teacherURL, requestOptions)
                .then(res => res.json())
                .then(json => {
                     this.setState({
                        teacherList: json
                     })
                });
                // $('#routineTable').css({'display' : 'block'})
                // $('#createTimeTablePara').css({'display' : 'none'})
                // let array = this.state.routineList
                // const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
                // for(let i = 0; i < 7; i++){
                //    for(let j = 0; j < this.state.numberOfPeriods; j++){
                //       array[days[i]][j] =  [null, null, null]
                //       console.log('The array is', array[days[i]][j])
                //    }
                // }
                // // console.log(array)
                // this.setState({routineList : array})
      }

       showSession(){

           let val=document.getElementById("unit1").value;

           const requestOptions1 = {
               method: 'POST',
               headers: { 'Content-Type': 'application/json' },
               body:JSON.stringify({'unit_i_d':val})

           };


              let sessionURL="http://3.7.45.100/studapi/public/api/getallsessionidname";
              fetch(sessionURL,requestOptions1)
              .then(res => res.json())
              .then(json => {
                  this.setState({
                      sessionList: json
                  })
              });
          }



          showDepartment(){
           let val=document.getElementById("session1").value;



           const requestOptions1 = {
               method: 'POST',
               headers: { 'Content-Type': 'application/json' },
               body:JSON.stringify({'session_i_d':val})

           };


              let classURL="http://3.7.45.100/studapi/public/api/getalldepartmentidname";
              fetch(classURL,requestOptions1)
              .then(res => res.json())
              .then(json => {
                  this.setState({
                      classList: json
                  })
              });
          }


           showSemester(){

               let val=document.getElementById("department1").value;


            const requestOptions1 = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({'department_i_d':val})

            };


               let semesterURL="http://3.7.45.100/studapi/public/api/getallsemidname";
               fetch(semesterURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                   this.setState({
                       semesterList: json
                   })
               });
           }

           showSection(){

               let val=document.getElementById("semester1").value;


            const requestOptions1 = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({'semester_i_d':val})

            };


               let semesterURL="http://3.7.45.100/studapi/public/api/getallsectionidname";
               fetch(semesterURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                   this.setState({
                       sectionList: json
                   })
               });
           }


           showSubject(){

               let val=document.getElementById("semester1").value;


            const requestOptions1 = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({'semester_i_d':val})

            };


               let semesterURL="http://3.7.45.100/studapi/public/api/getallsubjectidname";
               fetch(semesterURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                   this.setState({
                       subjectList: json
                   })
               });
           }


   handleSectionIDChange(e){
      this.setState({sectionID : e.target.value})
      $('#createTimeTablePara').css({'display':'block'})
   }

   //function to make time table visible
   makeTimeTableVisible(){
      if($('#routineName').val() != '' && $('#numOfPeriods').val() != '' && $('#numOfPeriods').val()>=1 && $('#numOfPeriods').val()<=10){
         $("#tableForTimeTableDiv").css({'display':'block'})
         this.setState({numberOfPeriods: $('#numOfPeriods').val()})
         this.setState({nameOfRoutine: $('#routineName').val()})
         let array = this.state.routineList
         const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
         for(let i = 0; i < 7; i++){
            for(let j = 0; j < $('#numOfPeriods').val(); j++){
               array[days[i]][j] =  [null, null, null, null]
               // console.log('The array is', array[days[i]][j])
            }
         }
         // console.log(array)
         this.setState({routineList : array})
      }
   }
   //update Time Table
   updateTimeTable(){

      let data = []
      let val = this.state.numberOfPeriods
      for(let i = 1; i <= val; i++){
         let name = "Period "+i
         let startTime = "#StartTime"+i
         let endTime = "#EndTime" + i
         if($(startTime).val() !== '' && $(endTime).val() !== ''){
            let st = $(startTime).val(), et = $(endTime).val()
            data.push([name, st, et])
         }
      }
      console.log(data)
      const requestOptions1 = {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body:JSON.stringify({
             section_i_d: this.state.sectionID,
             org_i_d: Cookies.get('orgid'),
             routine_name: this.state.nameOfRoutine,
             number_of_periods: this.state.numberOfPeriods,
             routine_created_by: Cookies.get('orgid')
          })

      };


         let routineURL="http://3.7.45.100/studapi/public/api/addsectionroutine";
         fetch(routineURL,requestOptions1)
         .then(res => res.json())
         .then(json => {
            if(json[0].result == 0)
            {
               swal({
                   title: "Oops!",
                   text: "Routine of this section is already added!",
                   icon: "warning",
                   button: "OK",
               });
               $('#timetable').modal('hide')
               this.componentDidMount();

            }
            else
            {
               let allTime = []
               let rID = json[0].result;
               for(let i = 0; i < val; i++){
                  allTime.push([data[i][1], data[i][2]])
                  const requestOptions1 = {
                      method: 'POST',
                      headers: { 'Content-Type': 'application/json' },
                      body:JSON.stringify({
                        routine_i_d : rID,
                        period_name : data[i][0],
                        start_time : data[i][1],
                        end_time : data[i][2]
                      })
                  }
                  let timeURL="http://3.7.45.100/studapi/public/api/addroutinetime";
                  fetch(timeURL,requestOptions1)
                  .then(res => res.json())
                  .then(json => {

                  });

               };
               const requestOptions1 = {
                   method: 'POST',
                   headers: { 'Content-Type': 'application/json' },
                   body:JSON.stringify({
                     routine_i_d : rID,
                   })
               }
               this.setState({routineID : rID});
               let timeURL="http://3.7.45.100/studapi/public/api/getroutinetime";
               fetch(timeURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                  // console.log('Time is', json)
                  this.setState({timeList : json})
               });
               swal({
                   title: "Done!",
                   text: "Time Table added Sucessfully",
                   icon: "success",
                   button: "OK",
               });
               $('#timetable').modal('hide')
               $('#routineTable').css({'display' : 'block'})
               $('#createTimeTablePara').css({'display' : 'none'})
               // this.componentDidMount();
            }
         });
     }


    // this function takes care of the change of fields of routine
   handleRoutineFieldsChange(data){
      let day = $(data.target.parentElement.parentElement).attr('class');
      let child = $(data.target).attr('id');
      let period = child.match(/(\d+)/)[0];
      let routine = this.state.routineList
      let sID = null || routine[day][period][0]
      let tID = null || routine[day][period][1]
      let rID = null || routine[day][period][2]
      if(data.target.value === '') data.target.value = null
      if(child.includes('SPeriod')) sID = data.target.value
      if(child.includes('RPeriod')) rID = data.target.value
      if(child.includes('TPeriod')) tID = data.target.value

      var array = this.state.timeList;
      routine[day][period] = [sID, tID, rID, array[0].timeTableID]
      this.setState({routineList : routine})
   }


   //function to update the time routine for well and good
   updateRoutine(){
      let routine = this.state.routineList
      let numberOfPeriods = this.state.numberOfPeriods
      let days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
      for(let i = 0; i < 7; i++){
         let hasRoutine = false
         for(let j = 0; j < numberOfPeriods; j++){
            if(routine[days[i]][j][0] || routine[days[i]][j][1] || routine[days[i]][j][2]){
               hasRoutine = true;
               break;
            }
         }

         if(hasRoutine){
            for(let j = 0; j < numberOfPeriods; j++){
               const requestOptions1 = {
                   method: 'POST',
                   headers: { 'Content-Type': 'application/json' },
                   body:JSON.stringify({
                      routine_i_d : this.state.routineID,
                      subject_i_d : routine[days[i]][j][0],
                      teacher_i_d : routine[days[i]][j][1],
                      room_i_d : routine[days[i]][j][2],
                      time_table_i_d : routine[days[i]][j][3],
                      day_of_the_week : days[i]
                   })

               };


                  let periodURL="http://3.7.45.100/studapi/public/api/addroutineperiods";
                  fetch(periodURL,requestOptions1)
                  .then(res => res.json())
            }
         }
      }
      swal({
          title: "Done!",
          text: "Routine added Sucessfully",
          icon: "success",
          button: "OK",
      });
   }

    render() {
      var { unitList, sessionList, classList, semesterList, sectionList,  subjectList, teacherList, numberOfPeriods,timeList } = this.state;
      const assignTimeTable = []
      // inserting time table
      for(let i = 1; i <= numberOfPeriods; i++){
         let periodValue = "Period " + i
         let periodID = "Period" + i
         let startTimeValue = "StartTime" + i
         let endTimeValue = "EndTime" + i
         assignTimeTable.push(
            <tr>
              <td className="border-0"><input type="text" id = {periodID} className="form-control border-0" value = {periodValue} disabled /></td>
              <td className="border-0"><input type="time" id = {startTimeValue} className="form-control border-0" /></td>
              <td className="border-0"><input type="time" id = {endTimeValue} className="form-control border-0" /></td>
            </tr>
         )
      }
      const timeOfRoutine = []
      const subOfRoutine = []
      const teacherOfRoutine = []
      const roomOfRoutine = []
      for(let i = 0; i < numberOfPeriods; i++){
         let sName = "SPeriod" + (i);
         let tName = "TPeriod" + (i);
         let rName = "RPeriod" + (i);
         //inserting time list

         if(timeList.length > i){
            timeOfRoutine.push(
               <th> {timeList[i].startTime} - {timeList[i].endTime}</th>
            )
         }
         //inserting subjects of routine
         subOfRoutine.push(
            <th className="m-0 p-0">
               <select className="form-control border-0" id = {sName} onChange = {(e) => this.handleRoutineFieldsChange(e)}>
                  <option value ="">Select Subject</option>
                     {subjectList.map(subject => (
                        <option value={subject.subjectID}>{subject.subjectName}</option>

                     ))}
               </select>
            </th>
         )
         //inserting teachers of routine
         teacherOfRoutine.push(
            <th className="m-0 p-0">
               <select className="form-control border-0" id = {tName} onChange = {(e) => this.handleRoutineFieldsChange(e)}>
                  <option value ="">Select Teacher</option>
                     {teacherList.map(teacher => (
                        <option value={teacher.teacherID}>{teacher.teacherName}</option>

                     ))}
               </select>
            </th>
         )
         //inserting rooms of routine
         roomOfRoutine.push(
            <th className="m-0 p-0">
               <select className="form-control border-0" id = {rName} onChange = {(e) => this.handleRoutineFieldsChange(e)}>
                  <option value ="">Select Room</option>
               </select>
            </th>
         )
      }
      const tableOfRoutine = []
      //pushing the time list on to table of routine
      tableOfRoutine.push(
         <thead>
           <tr className="text-center text-muted">
             <th> Time </th>
               {timeOfRoutine}
           </tr>
         </thead>
      )
      const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
      //iterating for each day and have a column for that.
      for(let i = 0; i < 7; i++){
         tableOfRoutine.push(
            <tbody className="text-center">
              <tr className = {days[i]}>
                <th className="text-muted text-center text-uppercase" rowSpan={3}>{days[i]}</th>
                {subOfRoutine}
              </tr>
              <tr className = {days[i]}>
                {teacherOfRoutine}
              </tr>
              <tr className = {days[i]}>
                {roomOfRoutine}
              </tr>
            </tbody>
         )

      }



        return (
           <div className="fadeIn animated">
  {/*-===Routine*/}
  <div className="card">
    <div className="card-header">
      <b>Select Unit:</b>
      <select name id="unit1" onChange={this.showSession} className="ml-1 form-control-sm mr-2">
       <option value="" selected>Select</option>
      {unitList.map(unit => (
         <option value={unit.unitID}>{unit.unitName}</option>

      ))}
      </select> &nbsp;
      <b>Select Session:</b>
      <select name id="session1" className="ml-1 form-control-sm mr-2" onChange={this.showDepartment}>
       <option value="" selected>Select</option>
      {sessionList.map(session => (
         <option value={session.sessionID}>{session.session}</option>

      ))}
      </select> &nbsp;
      <b>Select Class/Department:</b>

      <select name id="department1" onChange={this.showSemester} className="ml-1 mr-2 form-control-sm">
      <option value="" selected>Select</option>
      {classList.map(classes => (
         <option value={classes.departmentID}>{classes.departmentName}</option>

      ))}
      </select>&nbsp;
      <b>Select Semester:</b>

      <select name id="semester1" className="ml-1 mr-2 form-control-sm" onChange={() =>{{this.showSection()}; {this.showSubject()}}}>
      <option value="" selected>Select</option>
      {semesterList.map(semester => (
         <option value={semester.semesterID}>{semester.semesterName}</option>

      ))}
      </select>&nbsp;
      <b>Select Section:</b>
      <select name id  onChange = {(e) => {this.handleSectionIDChange(e)}} className="ml-1 mr-2 form-control-sm">
       <option value="" selected>Select</option>
      {sectionList.map(section => (
         <option value={section.sectionID}>{section.sectionName}</option>
      ))}
      </select>
    </div>
    <div className="card-body">
      <div className="table-responsive">
        <p id="createTimeTablePara"className="text-center font-weight-bold"><a href="#" data-toggle="modal" data-target="#timetable">+ Create Time Table</a></p> {/*Display if there is no Routine Displayed*/}
        <div id = "routineTable">
        <table id="dtHorizontal" className="table table-striped table-bordered" cellSpacing={0}>
           {tableOfRoutine}
        </table>
        <button type="button" onClick = {this.updateRoutine} className="btn btn-primary">Save &amp; update</button>
        </div>
      </div>
    </div>
  </div>
  {/*--=======/routine====*/}

 {/*-modal*/}
<div className="modal fade" id="timetable" tabIndex={-1} role="dialog" aria-labelledby="modelTitletimetable" aria-hidden="true">
  <div className="modal-dialog modal-dialog-100" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title">Timetable</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div className="modal-body">
        <div className="container-fluid">
          {/*-Required Time table*/}
          <div className="b-4">
          <label htmlFor className="font-weight-bold">Name of Routine*:</label>
          <input type="text" id = "routineName" className="ml-2 form-control-sm" />
            <label htmlFor className="font-weight-bold ml-2"> &nbsp; No. of period*:</label>
            <input type="number" max = "10" min = "1" id="numOfPeriods" className=" mr-2form-control-sm" />
            <button className="btn btn-success btn-sm" onClick = {this.makeTimeTableVisible}><i className="icon-check" /> &nbsp; Submit</button>
          </div><br />
         <div id = "tableForTimeTableDiv">
          <table  className="table border table-responsive-lg">
            <thead>
              <tr className="bg-light">
                <th className="border-0">Name of the Period</th>
                <th className="border-0">Start time</th>
                <th className="border-0">End Time</th>
              </tr>
            </thead>
            <tbody>
               {assignTimeTable}
            </tbody>
          </table>
          </div>
        </div>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" onClick = {this.updateTimeTable} className="btn btn-primary">Save &amp; update</button>
      </div>
    </div>
  </div>
</div>
{/*/Modal*/}
</div>
        );


    }
}
export default AddAdminRoutine;
