import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import AddAdminRoutine from './addAdminRoutine'
import EditAdminRoutine from './editAdminRoutine'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class AdminRoutineList extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.sessionSearchList = []
        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSectionSubjectAdd = this.showSectionSubjectAdd.bind(this);
        this.showSectionList = this.showSectionList.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        this.showSemesterSubject = this.showSemesterSubject.bind(this);

        this.state = {

            unitList: [],
            sessionList: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            routineList: [],

        }
    }

    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }

    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }

    showSession(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = "http://3.7.45.100/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessionList: json
                })
            });
    }


    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = "http://3.7.45.100/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = "http://3.7.45.100/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }

    showSemester(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = "http://3.7.45.100/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    //Section

    showSectionList() {
        let val = document.getElementById("semester1").value;

        this.showSection(val);
    }

    showSectionSubjectAdd() {

        let val = document.getElementById("semester2").value;

        this.showSection(val);
        this.showSubject(val);

    }

    showSection(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = "http://3.7.45.100/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }

    //Subject



    showSubject(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = "http://3.7.45.100/studapi/public/api/getallsubjectidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            });
    }

    showSemesterSubject() {

        let val = document.getElementById("semester1").value
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = "http://3.7.45.100/studapi/public/api/getallsubjectidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList1: json
                })
            });

    }
    showSection() {

        let val = document.getElementById("semester1").value;


        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let semesterURL = "http://3.7.45.100/studapi/public/api/getallsectionidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }
    addRoutine() {
        ReactDOM.render(<AddAdminRoutine />, document.getElementById('contain1'));
    }
    editRoutine() {
        ReactDOM.render(<EditAdminRoutine />, document.getElementById('contain1'));
    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };

        let unitURL = "http://3.7.45.100/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });

        let routineURL = "http://3.7.45.100/studapi/public/api/viewroutinesadmin";
        fetch(routineURL, requestOptions)
            .then(res => res.json())
            .then(json => {

                this.setState({
                    routineList: json
                })
            });


    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {

        var { unitList,sessonList, sessionList, classList, semesterList, sectionList, subjectList, routineList } = this.state;
       

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold"> List of Routines</span>
                        <span className="float-right"><a onClick={() => this.addRoutine()} className="btn btn-primary text-white" >+ Add New Routine</a></span>
                    </div>
                    <div className="card-body">

                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">


                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSemesterSubject}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>
                                            <select name="" id="section" onChange={this.showASubject} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>


                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>




                        <div>
                            <div className="table-responsive">
                                <table className="table table-bordered ">
                                    <thead>
                                        <tr className="bg-light table-head-fixed">
                                        <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                   
                                            <th className="border-0">Unit</th>
                                            <th className="border-0 ">Session</th>
                                            <th className="border-0 ">Class</th>
                                            <th className="border-0 ">Semester</th>
                                            <th className="border-0 ">Section</th>
                                            <th className="border-0 ">Routine</th>
                                            <th className="border-0 ">Created On</th>
                                            <th className="border-0 ">Edited On</th>
                                            <th className="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value="" /></td>
                                       
                                            <td className="border-right-0 border-top-0 border-bottom-0">Unit1</td>
                                            <td className="border-0 ">Session</td>
                                            <td className="border-0">Class1</td>
                                            <td className="border-0">Semester1</td>
                                            <td className="border-0">Section1</td>
                                            <td className="border-0">Routine1</td>
                                            <td className="border-0">20-02-2020</td>
                                            <td className="border-0">25-02-2020</td>
                                            <td className="border-left-0 border-top-0 border-bottom-0">
                                                <span><a className="text-primary font-weight-bold h4"><i className="icon-eye" title="View Routine" data-placement="top"></i> </a></span>

                                                <span><a href="#" onClick={this.editRoutine} className="text-warning font-weight-bold h4"><i className="icon-pencil" title="Edit Routine" data-placement="top"></i> </a></span>

                                                <span><a className="text-danger font-weight-bold h4" ><i className="icon-trash" title="Delete Routine" data-placement="top"></i> </a></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        );


    }
}
export default AdminRoutineList;
