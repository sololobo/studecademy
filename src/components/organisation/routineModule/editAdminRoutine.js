import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert'

class EditAdminRoutine extends Component {

      constructor(props){
         super(props);
         this.showSession = this.showSession.bind(this);
         this.showDepartment = this.showDepartment.bind(this);
         this.showSemester = this.showSemester.bind(this);
         this.showSection = this.showSection.bind(this);

         this.state = {
            unitList: [],
            sessionList: [],
            classList: [],
            semesterList: [],
            sectionList: [],
         }
      }

      componentDidMount() {

          const requestOptions = {
             method: 'POST',
             headers: { 'Content-Type': 'application/json' },
             body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

          };


          let unitURL = "http://3.7.45.100/studapi/public/api/getallunitidname";
          fetch(unitURL, requestOptions)
             .then(res => res.json())
             .then(json => {
                  this.setState({
                     unitList: json
                  })
             });
      }

       showSession(){

           let val=document.getElementById("unit1").value;

           const requestOptions1 = {
               method: 'POST',
               headers: { 'Content-Type': 'application/json' },
               body:JSON.stringify({'unit_i_d':val})

           };


              let sessionURL="http://3.7.45.100/studapi/public/api/getallsessionidname";
              fetch(sessionURL,requestOptions1)
              .then(res => res.json())
              .then(json => {
                  this.setState({
                      sessionList: json
                  })
              });
          }



          showDepartment(){
           let val=document.getElementById("session1").value;



           const requestOptions1 = {
               method: 'POST',
               headers: { 'Content-Type': 'application/json' },
               body:JSON.stringify({'session_i_d':val})

           };


              let classURL="http://3.7.45.100/studapi/public/api/getalldepartmentidname";
              fetch(classURL,requestOptions1)
              .then(res => res.json())
              .then(json => {
                  this.setState({
                      classList: json
                  })
              });
          }


           showSemester(){

               let val=document.getElementById("department1").value;


            const requestOptions1 = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({'department_i_d':val})

            };


               let semesterURL="http://3.7.45.100/studapi/public/api/getallsemidname";
               fetch(semesterURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                   this.setState({
                       semesterList: json
                   })
               });
           }

           showSection(){

               let val=document.getElementById("semester1").value;


            const requestOptions1 = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body:JSON.stringify({'semester_i_d':val})

            };


               let semesterURL="http://3.7.45.100/studapi/public/api/getallsectionidname";
               fetch(semesterURL,requestOptions1)
               .then(res => res.json())
               .then(json => {
                   this.setState({
                       sectionList: json
                   })
               });
           }


    render() {

        return (
           <div className="fadeIn animated">
  {/*-===Routine*/}
  <div className="card">
    <div className="card-header">
      <b>Select Unit</b>
      <select name id className="form-control-sm">
        <option value>Unit 1</option>
        <option value>Unit 2</option>
      </select> &nbsp;
      <b>Select Session</b>
      <select name id className="form-control-sm">
        <option value>2018</option>
        <option value>2019</option>
      </select> &nbsp;
      <b>Select Class/Department </b>
      <select name id className="form-control-sm">
        <option value>Class I</option>
        <option value>Class II</option>
      </select>
      <b>Select Semester</b>
      <select name id className="form-control-sm">
        <option value>Semester 1</option>
        <option value>Semester II</option>
      </select>
      <b>Select Section</b>
      <select name id className="form-control-sm">
        <option value>Section 1</option>
        <option value>Section II</option>
      </select>
      <div className="float-right">
        <a href="#" className="text-primary" data-toggle="modal" data-target="#timetable"><i className="icon-clock" />&nbsp; timetable</a>
        <a href="#" className="btn btn-light text-warning"><i className="icon-pencil" /> Edit</a>
      </div>
    </div>
    <div className="card-body">
      <div className="table-responsive">
        <p className="text-center font-weight-bold"><a href="#" data-toggle="modal" data-target="#timetable">+ Create Time Table</a></p> {/*Display if there is no Routine Displayed*/}
        <table id="dtHorizontal" className="table table-striped table-bordered" cellSpacing={0}>
          <thead>
            <tr className="text-center text-muted">
              <th> Time </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
          </thead>
          <tbody className="text-center">
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Monday</th>
              <th>Physics</th>
              <th>Chemistry</th>
              <th>--</th>
              <th>Mathematics</th>
              <th> </th>
              <th>Electronics</th>
              <th>Bengali</th>
              <th>--</th>
              <th>English</th>
            </tr>
            <tr>
              <th>Mr.PH Sharma</th>
              <th>Mr.PH Sharma</th>
              <th>Mr.PH Sharma</th>
              <th>--</th>
              <th>--</th>
              <th>Mr.PH Sharma</th>
              <th>Mr.PH Sharma</th>
              <th>Mr-PH  Sharma</th>
              <th>-</th>
            </tr>
            <tr>
              <th>R3</th>
              <th>R2</th>
              <th>R3</th>
              <th>--</th>
              <th>--</th>
              <th>R4</th>
              <th>R4</th>
              <th>R5</th>
              <th>-</th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Tuesday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase"  rowSpan={3}>Wednesday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Thursday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Friday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Saturday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="text-muted text-center text-uppercase" rowSpan={3}>Sunday</th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select> </th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
            <tr>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
              <th className="m-0 p-0"><select className="form-control border-0"><option /></select></th>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {/*--=======/routine====*/}

 {/*-modal*/}
<div className="modal fade" id="timetable" tabIndex={-1} role="dialog" aria-labelledby="modelTitletimetable" aria-hidden="true">
  <div className="modal-dialog modal-dialog-100" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title">Timetable</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div className="modal-body">
        <div className="container-fluid">
          {/*-Required Time table*/}
          <div className="form-control">
            <label htmlFor className="font-weight-bold">No. of period:*</label>
            <input type="text" className="form-control-sm" />
            <button className="btn btn-success btn-sm"><i className="icon-check" /> &nbsp; Submit</button>
          </div>
          <table className="table border table-responsive-lg">
            <thead>
              <tr className="bg-light">
                <th className="border-0">Name of the Period</th>
                <th className="border-0">Start time</th>
                <th className="border-0">End Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
              <tr>
                <td className="border-0"><input type="text" className="form-control border-0" placeholder="Name of Period" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
                <td className="border-0"><input type="time" className="form-control border-0" /></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className="modal-footer">
        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" className="btn btn-primary">Save &amp; update</button>
      </div>
    </div>
  </div>
</div>
{/*/Modal*/}
</div>
        );


    }
}
export default EditAdminRoutine;
