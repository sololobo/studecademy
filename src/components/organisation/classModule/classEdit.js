import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import swal from 'sweetalert';

class assignmentEdit extends Component {
   constructor(props){
      super(props)

      //***************              bindings               *********************//
         this.showSession = this.showSession.bind(this);
         this.onEditDepartmentFormSubmit = this.onEditDepartmentFormSubmit.bind(this)
      //**********************************************************************//


            //**************** dropdown default values ***************//
               this.showSession(this.props.data.unitID)
            //*****************************************************//

      //***************      state variables       ***************************//
         this.state = {
            unitID : this.props.data.unitID,
            sessionID : this.props.data.sessionID,
            departmentID : this.props.data.departmentID,
            departmentName : this.props.data.departmentName,
            departmentHOD : this.props.data.departmentHOD,
            numOfStudentRegistered : this.props.data.numOfStudentRegistered,
            status : this.props.data.status,

            unitList : this.props.unitList,
            sessionList : [],
            teacherList : this.props.teacherList
         }
      //****************************************************************//

   }


   //***********     handling change in props    *****************************//
      static getDerivedStateFromProps(nextProps, prevState){
         if(nextProps.data!==prevState.data){
           return { someState: nextProps.data};
        }
        else return null;
     }

      componentDidUpdate(prevProps, prevState) {
        if(prevProps.data!==this.props.data){

           //**************** dropdown default values ***************//
             this.showSession(this.props.data.unitID)
           //*****************************************************//

          this.setState({
             unitID : this.props.data.unitID,
             sessionID : this.props.data.sessionID,
             departmentID : this.props.data.departmentID,
             departmentName : this.props.data.departmentName,
             departmentHOD : this.props.data.departmentHOD,
             numOfStudentRegistered : this.props.data.numOfStudentRegistered,
             status : this.props.data.status,

             unitList : this.props.unitList,
             sessionList : [],
             teacherList : this.props.teacherList
          })
        }
      }
   //*****************************************************************************//



//************************ Dropdown async Calls *******************************//
   showSession(val) {
      this.setState({
         sessionList : [],
         unitID : val
      })
      const requestOptions = {
           method: 'POST',
           headers: { 'Content-Type': 'application/json' },
           body: JSON.stringify({ 'unit_i_d': val })
      };
      let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
      fetch(sessionURL, requestOptions)
           .then(res => res.json())
           .then(json => {
               this.setState({
                   sessionList : json
               })
           });

   }
//******************************************************************************//


//******************      edit class form      *******************************//
onEditDepartmentFormSubmit() {
    let url = global.API + "/studapi/public/api/editdepartment";
    let data = {
        department_i_d: this.state.departmentID,
        session_i_d: this.state.sessionID,
        department_name: this.state.departmentName,
        department_h_o_d: this.state.departmentHOD,
        num_of_student_registered: this.state.numOfStudentRegistered,
        status: this.state.status,
    }
    fetch(url, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        body: JSON.stringify(data)
    }).then((result) => {
        result.json().then((resp) => {
            try {
                if (resp[0].result == 1) {
                    swal("Department Updated! Refresh to see the result! :)", {
                        icon: "success",
                    });
                    $('#classEdit').modal("hide");
                }
                else {
                    swal("There's something wrong!! :(", {
                        icon: "error",
                    });
                }
            }
            catch{
                swal("There's something wrong!! :(", {
                    icon: "error",
                });
            }
        })
    })
}
//***************************************************************************//

render() {

        return (


            <div className="modal-content">
            <div className="modal-header">
                <h4><span className="font-weight-bold"> Edit New Class/Department</span></h4>
            </div>
            <div className="modal-body">
                <div className="container-fluid">
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Select Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select name="" id="editClassUnitName" className="form-control" value = {this.state.unitID} onChange={(data) => this.showSession(data.target.value)}>
                           {this.state.unitList.map(unit => (
                              <option value={unit.unitID}>{unit.unitName}</option>
                          ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Select Session  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select id="editClassSessionName" value = {this.state.sessionID} onChange = {(e) => {this.setState({sessionID : e.target.value})}} className="form-control" >
                            <option value="" selected >Select</option>
                               {this.state.sessionList.map(session => (
                                  <option value={session.sessionID}>{session.session}</option>
                               ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Class/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="text" id="editClassName" value = {this.state.departmentName} onChange = {(e) => {this.setState({departmentName : e.target.value})}} name="department_name" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Class teacher/Head of the Department  <span className="text-danger"><strong>*</strong></span> :</label>
                           <select id="editClassTeacherName" value = {this.state.departmentHOD} onChange = {(e) => {this.setState({departmentHOD : e.target.value})}} className="form-control" >
                               <option value="" selected >Select</option>
                                  {this.state.teacherList.map(teacher => (
                                     <option value={teacher.teacherID}>{teacher.teacherName}</option>
                                  ))}
                           </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered  <span className="text-danger"><strong>*</strong></span> :</label>
                        <input type="number" id="editClassStudents" value = {this.state.numOfStudentRegistered} onChange = {(e) => {this.setState({numOfStudentRegistered : e.target.value})}} name="num_of_student_registered" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                        <select type="text" id="editClassStatus" name="status" value = {this.state.status} onChange = {(e) => {this.setState({status : e.target.value})}}  className="form-control">
                            <option value="">--Select --</option>
                            <option value="Active">Active</option>
                            <option value="Suspended">Suspended</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" onClick={() => this.onEditDepartmentFormSubmit()} className="btn btn-warning">Update Class/Department</button>
            </div>
        </div>

        );


    }
}
export default assignmentEdit
