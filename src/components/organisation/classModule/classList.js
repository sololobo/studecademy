import React, { Component } from 'react';
import Cookies from 'js-cookie';
import Classedit from './classEdit';
import ReactDOM from 'react-dom';
import swal from 'sweetalert';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
import $ from 'jquery';

class classList extends Component {
    constructor(props) {
        super(props);
        this.showSession = this.showSession.bind(this);
        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSessionDepartment = this.showSessionDepartment.bind(this);
        this.showADepartment = this.showADepartment.bind(this);
        this.deleteClass = this.deleteClass.bind(this);
        this.classEdit = this.classEdit.bind(this);
        this.resetf=this.resetf.bind(this)


        this.state = {

            classesList: [],
            teacherList: [],
            session_i_d: 0,
            sessonList1: [],
            department_name: "",
            department_h_o_d: "",
            num_of_student_registered: "",
            status: "",
            unitList: [],
            classesList1: [],
            editDepartmentID: 0,
            unit_i_d: "",
            ii: 0,
            searchString: "",
            searchParams: []
        }
        this.classSearchList = []
    }

    classEdit(classes) {
        ReactDOM.render(<Classedit data = {classes} unitList = {this.state.unitList} teacherList = {this.state.teacherList} />, document.getElementById('classEditContent'));
    }



    deleteClass(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Unit!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletedep";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'deptID': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Department deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    showSessionList() {
        let val = document.getElementById("unit1").value;
        if (val==""){
            this.setState({
                classesList1: []
            })
        }
        this.showSession(val);


    }
    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }
    showSession(val) {

        this.updateParamsForSearch('unitID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }
    showSessionDepartment() {
        let val = document.getElementById("session1").value
        if (val==""){
            this.setState({
                classesList1: []
            })
        }
        this.updateParamsForSearch('sessionID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classesList1: json
                })
            });
    }
    showADepartment() {
        let val = document.getElementById("departmentID").value
        this.updateParamsForSearch('departmentID', val)

    }
    resetf(){
        document.getElementById("unit2").selectedIndex = 0
        document.getElementById("sessionid").selectedIndex = 0
        document.getElementById("department_name").selectedIndex = 0
        document.getElementById("status").selectedIndex = 0
        document.getElementById("department_h_o_d").selectedIndex = 0
        document.getElementById("num_of_student_registered").value = ""

    }
    submit() {
        var department_name = document.getElementById("department_name").value;
        var i = 0;
        var sintaxCheck = /^[A-Za-z0-9_ -]{3,60}$/;


        if (i < 6) {
            if (document.getElementById("unit2").value == "") {
                document.getElementById("unit2Error").innerHTML = "*Please fill this field";
                document.getElementById("unit2").focus();
                document.getElementById("unit2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit2").value != "") {
                document.getElementById("unit2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit2").style.borderColor = "";
            }
            if (document.getElementById("sessionid").value == "") {
                document.getElementById("sessionidError").innerHTML = "*Please fill this field";
                document.getElementById("sessionid").focus();
                document.getElementById("sessionid").style.borderColor = "#FF0000";
            }

            if (document.getElementById("sessionid").value != "") {
                document.getElementById("sessionidError").innerHTML = "";
                i = i + 1;
                document.getElementById("sessionid").style.borderColor = "";
            }
            if (document.getElementById("department_name").value == "") {
                document.getElementById("department_nameError").innerHTML = "*Please fill this field";
                document.getElementById("department_name").focus();
                document.getElementById("department_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department_name").value != "") {
                if (!sintaxCheck.test(department_name)) {
                    document.getElementById("department_nameError").innerHTML = "*Invalid Class Format";
                    document.getElementById("department_name").focus();
                    document.getElementById("department_name").style.borderColor = "#FF0000";
                }

                if (sintaxCheck.test(department_name)) {
                    document.getElementById("department_nameError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("department_name").style.borderColor = "";
                }
            }

            if (document.getElementById("num_of_student_registered").value == "") {
                document.getElementById("num_of_student_registeredError").innerHTML = "*Please fill this field";
                document.getElementById("num_of_student_registered").focus();
                document.getElementById("num_of_student_registered").style.borderColor = "#FF0000";
            }

            if (document.getElementById("num_of_student_registered").value != "") {
                document.getElementById("num_of_student_registeredError").innerHTML = "";
                i = i + 1;
                document.getElementById("num_of_student_registered").style.borderColor = "";
            }
            if (document.getElementById("department_h_o_d").value == "") {
                document.getElementById("department_h_o_dError").innerHTML = "*Please fill this field";
                document.getElementById("department_h_o_d").focus();
                document.getElementById("department_h_o_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department_h_o_d").value != "") {
                document.getElementById("department_h_o_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("department_h_o_d").style.borderColor = "";
            }
            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 6) {
                console.log(i);
                this.state.ii = 6;
            }
        }
        if (this.state.ii == 6) {

            let url = global.API + "/studapi/public/api/adddepartment";
            let data = this.state;
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp == 1) {
                        swal({
                            title: "Done!",
                            text: "Department Added Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        $('#createClass').modal('hide')
                        this.componentDidMount();
                        this.resetf()
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "Department not Added ",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                })
            })
        }
    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let classesURL = global.API + "/studapi/public/api/viewallorgadepartment";
        fetch(classesURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classesList: json
                })
            }).then(() => {
                let count = Object.keys(this.state.classesList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });

        let teacherURL = global.API + "/studapi/public/api/getallteacheridname";
        fetch(teacherURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    teacherList: json
                })
            });

    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }
    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Classes!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletedep";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({'deptID': value })
                            })
                        })
                        swal("All Selected classes Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some classes cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }
    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

// refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, classesList, unitList, sessionList, sessonList1, classesList1, teacherList } = this.state;

        if (this.state.searchParams.length > 0) {
            this.classSearchList = SearchForObjectsWithParams(classesList, this.state.searchParams)
        }
        else {
            this.classSearchList = classesList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.classSearchList = SearchForObjectsWithName(this.classSearchList, this.state.searchString)
        }


        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3 className="font-weight-bold">Class/Department
                        <a href="#" data-toggle="modal" data-target="#createClass"><span className=" h5  mt-2 font-weight-bold float-right">+ Create New Class/Department</span></a>
                        </h3>
                    </div>

                    <div className="card-body">
                    <div className="card">
                        <div className="card-body pt-0 pb-0">

                            <button className="btn btn-warning float-left"onClick={() => this.selectAllOptions()} >Select All</button>

                            <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                            <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                            <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                            <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                        </div>
                    </div>
                    {/*-modal filter*/}
                    <div className="  ">
                        <div className="collapse" id="commentArea">
                            <div className="media-body">
                                <div className="row">
                                    <div className="col-md-4 form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>

                                        <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>

                                    </div>
                                    <div className="col-md-4 form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                        <select name="" id="session1" className=" form-control" onChange={this.showSessionDepartment}>
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="col-md-4 form-group">
                                        <b>Class/Department:</b>
                                        <select name="" id="departmentID" className=" form-control" onChange={this.showADepartment}>
                                            <option value="" selected>Select</option>
                                            {classesList1.map(classes1 => (
                                                <option value={classes1.departmentID}>{classes1.departmentName}</option>

                                            ))}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/*-/filter*/}


                    </div>

                        <table className="mt-2 table table-stripped table-bordered table-responsive-lg">
                            <thead>

                                <tr className="bg-light">
                                    <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} className = "" name="" id="" /></th>
                                    <th className="border-0">Unit</th>
                                    <th className="border-0">Session</th>
                                    <th className="border-0">Name of Class/Department</th>
                                    <th className="border-0">Class Teacher/Head of the Department</th>
                                    <th className="border-0">No of Student Registered</th>
                                    <th className="border-0">Status</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.classSearchList.map(classes => (
                                    <tr>
                                        <td className="border-0"><input type="checkbox" value = {classes.departmentID} className = "checkBoxForDeletion" name="" id="" /></td>
                                        <td className="border-0">{classes.unitName}</td>
                                        <td className="border-0">{classes.session}</td>
                                        <td className="border-0">{classes.departmentName}</td>
                                        <td className="border-0">{classes.teacherName}</td>
                                        <td className="border-0">{classes.numOfStudentRegistered}</td>
                                        <td className="border-0"><span className="badge badge-pill bg-light">{classes.status}</span></td>
                                        <td className="border-0">
                                            <span><a href="#" data-target="#classEdit" onClick={(e) => this.classEdit(classes)} data-toggle="modal" className="text-warning"><i
                                                className="icon-pencil"></i></a></span>
                                            <span><a onClick={() => this.deleteClass(classes.departmentID)} className="text-danger"><i className="icon-trash"></i>
                                            </a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>
                </div>



                {/*<!- add unit session-->*/}
                {/*-modal for create new class/depaartment*/}
                <div className="modal fade" id="createClass" tabIndex={-1} role="dialog" aria-labelledby="modelTitlecreateSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Create New Class/Department</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Select Unit <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                        <span id="unit2Error" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Select Session <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="sessionid" name="session_i_d" value={this.state.session_i_d} onChange={(data) => { this.setState({ session_i_d: data.target.value }) }} className="form-control" >
                                            <option value="" selected>Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                        <span id="sessionidError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Class/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" id="department_name" name="department_name" value={this.state.department_name} onChange={(data) => { this.setState({ department_name: data.target.value }) }} className="form-control bg-white" placeholder="Class/Department Name" />
                                        <span id="department_nameError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Class teacher/Head of the Department <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select id="department_h_o_d" name="department_h_o_d" value={this.state.department_h_o_d} onChange={(data) => { this.setState({ department_h_o_d: data.target.value }) }} className="form-control" >
                                            <option value="" selected>Select</option>
                                            {teacherList.map(teacher => (
                                                <option value={teacher.teacherID}>{teacher.teacherName}</option>

                                            ))}
                                        </select>
                                        <span id="department_h_o_dError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="number" id="num_of_student_registered" name="num_of_student_registered" value={this.state.num_of_student_registered} onChange={(data) => { this.setState({ num_of_student_registered: data.target.value }) }} className="form-control" />
                                        <span id="num_of_student_registeredError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status <span className="text-danger"><strong>*</strong></span>:</label>
                                        <select name="status" id="status" value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }} className="form-control">
                                            <option value="">--Select --</option>
                                            <option value="Active">Active</option>
                                            <option value="Suspended">Suspended</option>
                                        </select>
                                        <span id="statusError" class="text-danger font-weight-bold"></span>

                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" onClick={() => this.resetf()} data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => this.submit()} className="btn btn-warning">Add Class/Department</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*--/modal for create new Class/Department---*/}
                {/*-modal for edit new Class/Department*/}
                <div className="modal fade" id="classEdit" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditSession" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div id="classEditContent"></div>
                     {/*   <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold"> Edit New Class/Department</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Select Unit  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="" id="editClassUnitName" className="form-control" onChange={(data) => this.showSession(data.target.value)}>
                                            <option value="" selected>Select</option>
                                            {unitList.map(unit => (
                                                <option value={unit.unitID}>{unit.unitName}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Select Session  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select id="editClassSessionName" className="form-control" >
                                            <option value="" selected >Select</option>
                                            {sessonList1.map(session => (
                                                <option value={session.sessionID}>{session.session}</option>

                                            ))}
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Name of the Class/Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" id="editClassName" name="department_name" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Class teacher/Head of the Department  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" id="editClassTeacherName" name="department_h_o_d" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> No of Students Registered  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="number" id="editClassStudents" name="num_of_student_registered" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor className="mb-0 font-weight-bold"> Status  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select type="text" id="editClassStatus" name="status" className="form-control">
                                            <option value="">--Select --</option>
                                            <option value="Active">Active</option>
                                            <option value="Suspended">Suspended</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => this.onEditDepartmentFormSubmit()} className="btn btn-warning">Update Class/Department</button>
                            </div>
                        </div>
                   */}
                    </div>
                </div>
                {/*--/modal for edit new  Class/Department--*/}



            </div>
        );


    }
}
export default classList
