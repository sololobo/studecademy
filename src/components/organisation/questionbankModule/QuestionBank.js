import React, { Component } from 'react';


class QuestionBank extends Component {

    render() {

        return (
            <div className="fadeIn animated">


<div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" >
                                                <option value="" selected>Select</option>
                                              
                                            </select>
                                        </div>
                                        <div className="col-md-6 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" >
                                                <option value="" selected>Select</option>
                                           
                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">

                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1"  className=" form-control">
                                                <option value="" selected>Select</option>
                                          
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" >
                                                <option value="" selected>Select</option>
                                        
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="" id="subject1"  className=" form-control">
                                                <option value="" selected>Select</option>
                                           
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>
                       


                {/*-===Body Content Start====*/}
                <div className="row">
                    <div className="col-md-9 col-xl-9 col-sm-12">
                        <div className="card">
                            {/*--question bank*/}
                            <div className="card-header">
                                <span className="h3 font-weight-bold"> Create your own Question bank</span>
                                <span className="float-right">
                                    Select Subject
            <select name id className="form-control-sm">
                                        <option value>Verbal</option>
                                        <option value>Quantitive</option>
                                    </select>
                                </span>
                            </div>
                            <div className="card-body">

                                <h4><strong>Verbal </strong></h4>
                                <hr />
                                <div id="accordion" role="tablist">
                                    {/*tablist started*/}
                                    {/*chpter 1 started*/}
                                    <div className="card">
                                        <div className="card-header" id="chap-1" role="tab">
                                            Time &amp; Distance
                <div className="float-right">
                                                <a data-toggle="collapse" href="#chap1" aria-expanded="true"><i className="icon-arrow-down-circle" /></a> <a href="#" data-target="#deleteAlert" data-toggle="modal"><i className="fa fa-trash-o text-danger table-bordered ml-2 p-1" /></a>
                                            </div>
                                        </div>
                                        {/*-inside chapter*/}
                                        <div id="chap1" className="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div className="card-body">
                                                {/*-tablist question*/}
                                                <ul className="nav nav-tabs" role="tablist">
                                                    <li className="nav-item">
                                                        <a className="nav-link active" data-toggle="tab" href="#mark1" role="tab" aria-controls="publish"> Mark 1 <span className="badge badge-pill badge-primary">4</span></a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a className="nav-link" data-toggle="tab" href="#mark2" role="tab" aria-controls="draft">
                                                            Marks 2 <span className="badge badge-pill badge-primary">3</span></a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a className="nav-link" data-toggle="tab" href="#mark3" role="tab" aria-controls="trash">
                                                            Marks 3 <span className="badge badge-pill badge-primary">2</span></a>
                                                    </li>
                                                </ul>
                                                {/*-/tablist question*/}
                                                {/*- tablist question area*/}
                                                <div className="tab-content">
                                                    {/*-marks 1 area*/}
                                                    <div className="tab-pane active" id="mark1" role="tabpanel">
                                                        <div className="button-group mb-4">
                                                            <span className="border border-primary btn btn-light"><input type="checkbox" className="p-1" />
                          Select All</span>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1view" data-toggle="modal"><i className="icon-eye" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1edit" data-toggle="modal"><i className="icon-pencil" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#deleteAlert" data-toggle="modal"><i className="icon-trash" /></button>
                                                        </div>
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 1 area*/}
                                                    {/*-marks 2 area*/}
                                                    <div className="tab-pane" id="mark2" role="tabpanel">
                                                        <div className="button-group mb-4">
                                                            <span className="border border-primary btn btn-light"><input type="checkbox" className="p-1" />
                          Select All</span>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1view" data-toggle="modal"><i className="icon-eye" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1edit" data-toggle="modal"><i className="icon-pencil" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#deleteAlert" data-toggle="modal"><i className="icon-trash" /></button>
                                                        </div>
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 2 area*/}
                                                    {/*-marks 3 area*/}
                                                    <div className="tab-pane" id="mark3" role="tabpanel">
                                                        <div className="button-group mb-4">
                                                            <span className="border border-primary btn btn-light"><input type="checkbox" className="p-1" />
                          Select All</span>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1view" data-toggle="modal"><i className="icon-eye" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#Modalchap1q1edit" data-toggle="modal"><i className="icon-pencil" /></button>
                                                            <button type="button" className="btn btn-light" data-target="#deleteAlert" data-toggle="modal"><i className="icon-trash" /></button>
                                                        </div>
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 3 area*/}
                                                </div>
                                                {/*-/tablist question area*/}
                                            </div>
                                        </div>
                                        {/*-/inside Chapter*/}
                                    </div>
                                    {/*-/chapter 1 started*/}
                                    {/*chpter 2 started*/}
                                    <div className="card">
                                        <div className="card-header" id="chap-1" role="tab">
                                            Comprehension
                <div className="float-right">
                                                <a data-toggle="collapse" href="#chap2" aria-expanded="true"><i className="icon-arrow-down-circle" /></a> <a href="#" data-target="#deleteAlert" data-toggle="modal"><i className="fa fa-trash-o text-danger table-bordered ml-2 p-1" /></a>
                                            </div>
                                        </div>
                                        {/*-inside chapter*/}
                                        <div id="chap2" className="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div className="card-body">
                                                {/*-tablist question*/}
                                                <ul className="nav nav-tabs" role="tablist">
                                                    <li className="nav-item">
                                                        <a className="nav-link active" data-toggle="tab" href="#mark1chap2" role="tab" aria-controls="publish"> Mark 1 <span className="badge badge-pill badge-primary">4</span></a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a className="nav-link" data-toggle="tab" href="#mark2chap2" role="tab" aria-controls="draft">
                                                            Marks 2 <span className="badge badge-pill badge-primary">3</span></a>
                                                    </li>
                                                    <li className="nav-item">
                                                        <a className="nav-link" data-toggle="tab" href="#mark3chap2" role="tab" aria-controls="trash">
                                                            Marks 3 <span className="badge badge-pill badge-primary">2</span></a>
                                                    </li>
                                                </ul>
                                                {/*-/tablist question*/}
                                                {/*- tablist question area*/}
                                                <div className="tab-content">
                                                    {/*-marks 1 area*/}
                                                    <div className="tab-pane active" id="mark1chap2" role="tabpanel">
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 1 area*/}
                                                    {/*-marks 2 area*/}
                                                    <div className="tab-pane" id="mark2chap2" role="tabpanel">
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 2 area*/}
                                                    {/*-marks 3 area*/}
                                                    <div className="tab-pane" id="mark3chap2" role="tabpanel">
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <input type="checkbox" className="form-control-sm" /> Lorem ipsum dolor sit, amet consectetur
                      adipisicing elit. Odit facilis sunt quis incidunt? Error, fugiat rerum. Commodi ab tenetur
                      labore itaque consectetur doloribus fugiat mollitia, voluptatibus eum qui dolore
                      reiciendis.<br />
                                                        <hr />
                                                        <h2 className="text-center strong"><button className="btn-secondary btn" data-toggle="modal" data-target="#Modalquestion">+ Add Question</button></h2>
                                                    </div>
                                                    {/*-/marks 3 area*/}
                                                </div>
                                                {/*-/tablist question area*/}
                                            </div>
                                        </div>
                                        {/*-/inside Chapter*/}
                                    </div>
                                    {/*-/chapter 2 started*/}
                                </div>
                                {/*-/tablist end*/}
                                <button className="btn-secondary btn  h2">+ Add Chapter</button>
                            </div>
                            <div className="card-footer text-right">
                                <button className="btn btn-success"><i className="icon-check" /> Save Now</button>
                            </div>
                        </div>
                        {/*-/question bank*/}
                    </div>
                    <div className="col-md-3 col-xl-3 col-sm-12">
                        <div className="card">
                            {/*-Question status generation*/}
                            <div className="card-header text-center">
                                Question Status
        </div>
                            <div className="card-body">
                                <b>Selected Chapter:</b> Time &amp; Distance, trigonometry<br />
                                <b>Total Question Selected:</b> 20<br />
                                <hr />
                                <b className="text-primary">1 Mark:</b> 10<br />
                                <b className="text-primary">2 Marks: </b> 5<br />
                                <b className="text-primary">3 Marks: </b> 5<br />
                                <hr />
                                <b>Total Marks: </b> 35
        </div>
                            <div className="card-footer">
                                <div className="float-left"><button className="btn btn-success" data-toggle="modal" data-target="#Modalpublish">Publish for Exam</button></div>
                                <div className="float-right"><button className="btn btn-warning"><i className="icon-action-undo" />
              Reset</button>
                                </div>
                            </div>
                        </div>
                        {/*-/Question status generation*/}
                        <button className="btn btn-primary form-control"><i className="icon-cloud-download"> </i> Download Sample Question
        Format</button>
                    </div>
                </div>
                {/*-===/Ends Body Content===*/}



                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for delete */}
                {/* Modal question edit*/}
                <div className="modal fade" id="Modalchap1q1edit" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100 modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title text-center font-weight-bold" id="Modalchap1q1editTitle"> Edit Question</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row p-3">
                                    <textarea name id rows={2} className="form-control" placeholder="Enter your question" defaultValue={""} />
                                    {/*-Options are given below*/}
                                    <div className="col-md-6">
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">A.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">B.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">C.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">D.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                    </div>
                                    {/*Question information*/}
                                    <div className="col-md-6 mt-3">
                                        <div className="form-group mr-2">
                                            <b>Marks :*</b>
                                            <input type="text" className="form-control-sm" /> <br />
                                            <b className=" ml-2">Negative Marks:</b>
                                            <input type="text" className="form-control-sm" />
                                        </div>
                                        <hr />
                                        <div className="form-group mr-2">
                                            <textarea col={3} className="form-control" placeholder="Solution of the Question" defaultValue={""} />
                                        </div>
                                    </div>
                                </div>
                                {/*/row*/}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary">Update Now</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-----/modal question edit--*/}
                {/* Modal question view*/}
                <div className="modal fade" id="Modalchap1q1view" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100 modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title text-center font-weight-bold" id="Modalchap1q1viewTitle">View Question</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="row p-3">
                                    <textarea name id rows={2} className="form-control" placeholder="Enter your question" defaultValue={""} />
                                    {/*-Options are given below*/}
                                    <div className="col-md-6">
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">A.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">B.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">C.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                        <div className="form-group row mt-3">
                                            <b className="col-md-1">D.
                  <input type="checkbox" /></b>
                                            <textarea name id rows={2} placeholder="Answer" className="form-control col-md-10" defaultValue={""} />
                                        </div>
                                    </div>
                                    {/*Question information*/}
                                    <div className="col-md-6 mt-3">
                                        <div className="form-group mr-2">
                                            <b>Marks :*</b>
                                            <input type="text" className="form-control-sm" /> <br />
                                            <b className=" ml-2">Negative Marks:</b>
                                            <input type="text" className="form-control-sm" />
                                        </div>
                                        <hr />
                                        <div className="form-group mr-2">
                                            <textarea col={3} className="form-control" placeholder="Solution of the Question" defaultValue={""} />
                                        </div>
                                    </div>
                                </div>
                                {/*/row*/}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" data-dismiss="modal" aria-label="Close">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-----/modal question edit--*/}
                {/* Modal publish*/}
                <div className="modal fade" id="Modalpublish" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100 modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title text-center font-weight-bold" id="ModalpublishTitle"> Test Information</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="row">
                                        <div className="col-md-8 col-xs-12">
                                            <label className="mb-0">Name of the test<span className="text-danger">*</span></label>
                                            <input className="form-control" type="text" placeholder="Name of the Test" />
                                        </div>
                                        <div className="col-md-4 col-xs-12">
                                            <label className="mb-0">Department<span className="text-danger">*</span></label>
                                            <select className="form-control">
                                                <option> Department</option>
                                                <option> CSE </option>
                                                <option> ME</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 col-xs-12">
                                            <label className="mb-0">Semester<span className="text-danger">*</span></label>
                                            <select className="form-control">
                                                <option>Semester</option>
                                                <option> 1st Semester </option>
                                                <option> 2nd Semester</option>
                                                <option> 3rd Semester</option>
                                            </select>
                                        </div>
                                        <div className="col-md-5 col-xs-12">
                                            <label className="mb-0">Subject<span className="text-danger">*</span></label>
                                            <select className="form-control">
                                                <option> Select Subject</option>
                                                <option> Analystics Instrumentation </option>
                                                <option> Basic Electronics &amp; Instrumentation</option>
                                            </select>
                                        </div>
                                        <div className="col-md-3 col-xs-12">
                                            <label className="mb-0">Category<span className="text-danger">*</span></label>
                                            <select className="form-control">
                                                <option> Category</option>
                                                <option> Mock Test </option>
                                                <option> MCQ Test</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-4 col-xs-12">
                                            <label className="mb-0">Full Marks<span className="text-danger">*</span></label>
                                            <input className="form-control" type="Number" placeholder="Full Marks" />
                                        </div>
                                        <div className="col-md-4 col-xs-12">
                                            <label className="mb-0">Pass Marks<span className="text-danger">*</span></label>
                                            <input className="form-control" type="Number" name placeholder="Pass Marks" />
                                        </div>
                                        <div className="col-md-4 col-xs-12">
                                            <label className="mb-0">Duration<span className="text-danger">*</span></label>
                                            <input className="form-control" type="Number" placeholder="Duration in Min" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <label className="mb-0">Starting Date<span className="text-danger">*</span></label>
                                            <input className="form-control" type="date" name data-toggle="datepicker" placeholder="Starting Date" />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <label className="mb-0">Last Date of submission<span className="text-danger">*</span></label>
                                            <input className="form-control" type="date" name id="datepicker" placeholder="Last Date of submission" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 col-xs-12">
                                            <label className="mb-0">Course Objective<span className="text-danger">*</span></label>
                                            <textarea className="form-control" type="text" defaultValue={"                      "} />
                                        </div>
                                        <div className="col-md-6 col-xs-12">
                                            <label className="mb-0">Additional Information</label>
                                            <textarea className="form-control" type="text" defaultValue={"                      "} />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary">Publish</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-----/modal publish--*/}
                {/* Modal question upload*/}
                <div className="modal fade" id="Modalquestion" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title font-weight-bold" id="ModalquestionTitle"> Add Question</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {/* Drop Zone */}
                                <div className="form-group files mt-3">
                                    <span className="text-right"><a href="#" className="float-right"><i className="icon-cloud-download" /> Our Sample
                format</a></span>
                                    <label className="font-weight-bold">Upload Your File: <span className="small">.XLS, .CSV File Supported only</span>
                                    </label>
                                    <input type="file" className="form-control" multiple />
                                </div>
                                {/* Progress Bar */}
                                <div className="progress">
                                    <div className="progress-bar" role="progressbar" aria-valuenow={60} aria-valuemin={0} aria-valuemax={100} style={{ width: '60%' }}>
                                        <span className="sr-only">60% Complete</span>
                                    </div>
                                </div>
                                {/* Upload Finished */}
                                <div className="js-upload-finished">
                                    <h5 className="font-weight-bold mt-2">Processed files</h5>
                                    <div className="list-group">
                                        <a href="#" className="list-group-item list-group-item-success"><span className="badge alert-success pull-left">20
                  Files has been uploaded </span></a>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary">Upload Now</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-----/modal question upload--*/}


            </div>

        );


    }
}
export default QuestionBank