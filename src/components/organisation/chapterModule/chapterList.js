import React, { Component } from 'react';
import swal from 'sweetalert';
 
class chapterList extends Component {
    constructor(props) {
        super(props);


        this.state = {
            chapterList: [],
            subject_i_d: 0,
            chapter_name: "",
            status: ""






        }
    }

    submit() {
        console.log(this.state);
        let url = global.API + "/studapi/public/api/addchapter";
        let data = this.state;
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Chapterr Added Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                }
                else if (resp == 2) {
                    swal({
                        title: "Error!",
                        text: "Assignment Already present",
                        icon: "warning",
                        button: "OK",
                    });
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Semester Not  Added",
                        icon: "warning",
                        button: "OK",
                    });
                }

            })
        })
    }

    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },

        };


        let url = global.API + "/studapi/public/api/viewallchap";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    chapterList: json
                })
            });



    }

    render() {

        var { isLoaded, chapterList } = this.state;
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h3 className="font-weight-bold">Chapter
            <a href="#" data-toggle="modal" data-target="#createSubject"><span className=" h5  mt-2 font-weight-bold float-right">+ Add  New Chapter</span></a>
                        </h3>
                    </div>
                    <div className="card-body">
                        <div className="">
                            <div className="form-group">
                                <b>Unit:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">Unit</option>
                                </select>
                                <b>Session:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">2019</option>
                                    <option value="" disabled>2018</option>
                                </select>
                                <b>Class/Department:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">className 4</option>
                                    <option value="" >className 5</option>
                                </select>
                                <b>Semester:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">1st semester</option>
                                    <option value="" >className 5</option>
                                </select>
                                <b>Subject:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">Subject 1</option>
                                    <option value="" >Subject 2</option>
                                </select>

                                <b>Chapter:</b>
                                <select name="" id="" className="form-control-sm">
                                    <option value="">Chap 1</option>
                                    <option value="" >chap 2</option>
                                </select>
                            </div>
                        </div>
                        <table className="table table-stripped table-bordered table-responsive-lg">
                            <thead>
                                <tr className="bg-light">
                                    <th className="border-0"><input type="checkbox" onclick="toggle(this);" /></th>
                                    <th className="border-0">Unit</th>
                                    <th className="border-0">Session</th>
                                    <th className="border-0">Class/Department</th>
                                    <th className="border-0">Semester</th>
                                    <th className="border-0">Subject</th>
                                    <th className="border-0">Chapter</th>
                                    <th className="border-0">Status</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {chapterList.map(chapter => (
                                    <tr>
                                        <td className="border-0"><input type="checkbox" name="" id="" /></td>
                                        <td className="border-0">Unit 1</td>
                                        <td className="border-0">Session</td>
                                        <td className="border-0">ME</td>
                                        <td className="border-0">Semester 1</td>
                                        <td className="border-0">Subject 1</td>
                                        <td className="border-0">{chapter.chapterName}</td>
                                        <td className="border-0"><span className="badge badge-pill bg-light">{chapter.status}</span></td>
                                        <td className="border-0">
                                            <span><a href="#" data-target="#editSubject" data-toggle="modal" className="text-warning"><i
                                                className="icon-pencil"></i></a></span>
                                            <span><a href="#" className="text-danger" data-toggle="modal"
                                                data-target="#deleteAlert"><i className="icon-trash"></i>
                                            </a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>

                    </div>
                </div>

                <div>
                    {/*-modal*/}
                    <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="container-fluid text-center">
                                        <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                        <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button" className="btn btn-warning">Yes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*-/modal*/}
                    {/*-add semestersubject-->*/}
                    {/*-modal for Add New chapter*/}
                    <div className="modal fade" id="createSubject" tabIndex={-1} role="dialog" aria-labelledby="modelTitlecreateSession" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-100" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4><span className="font-weight-bold"> Add New Chapter</span></h4>
                                </div>
                                <div className="modal-body">
                                    <div className="container-fluid">
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Unit:</label>
                                            <select className="form-control">
                                                <option value="">UNit 1</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Session:</label>
                                            <select className="form-control">
                                                <option value="">2019</option>
                                                <option value disabled>2018</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Class/Department:</label>
                                            <select className="form-control">
                                                <option value="">Class 4</option>
                                                <option value="">Class 5</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Semester:</label>
                                            <select className="form-control">
                                                <option value="">Class 4</option>
                                                <option value="">Class 5</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Subject:</label>
                                            <select name="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.setState({ subject_i_d: data.target.value }) }} className="form-control">
                                                <option value="">Select...</option>
                                                <option value="2">Maths</option>
                                            </select>
                                        </div>
                                        <table className="table table-border">
                                            <thead>
                                                <tr>
                                                    <th className="border-0">Name of the Chapter</th>
                                                    <th className="border-0">Status</th>
                                                    <th className="border-0" />
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="border-0">
                                                        <input name="chapter_name" value={this.state.chapter_name} onChange={(data) => { this.setState({ chapter_name: data.target.value }) }} type="text" className="form-control" />
                                                    </td>
                                                    <td className="border-0">
                                                        <select name="status" value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }} className="form-control">
                                                            <option value="">Select</option>
                                                            <option value="Active">Active</option>
                                                            <option value="Suspended">Suspended</option>
                                                        </select>
                                                    </td>
                                                    <td className="border-0">
                                                        <span><i className="icon-minus text-danger" /></span>
                                                        <span><i className="icon-plus" /></span>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*--/modal for create new Subject---*/}
                    {/*-modal for edit Subject*/}
                    <div className="modal fade" id="editSubject" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditSession" aria-hidden="true">
                        <div className="modal-dialog modal-dialog-100" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4><span className="font-weight-bold"> Edit Chapter</span></h4>
                                </div>
                                <div className="modal-body">
                                    <div className="container-fluid">
                                        <div className="form-group">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold"> Unit:</label>
                                                <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                    <option value="">UNit 1</option>
                                                </select>
                                            </div>
                                            <label htmlFor className="mb-0 font-weight-bold"> Session:</label>
                                            <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                <option value="">2019</option>
                                                <option value disabled>2018</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Class/Department:</label>
                                            <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                <option value="">Class 4</option>
                                                <option value="">Class 5</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold">Semester:</label>
                                            <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                <option value="">Class 4</option>
                                                <option value="">Class 5</option>
                                            </select>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> Subject:</label>
                                            <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                <option value="">Subject 1</option>
                                                <option value="">Class 5</option>
                                            </select>
                                        </div>
                                        <table className="table table-border">
                                            <thead>
                                                <tr>
                                                    <th className="border-0">Name of the Chapter</th>
                                                    <th className="border-0">Status</th>
                                                    <th className="border-0" />
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="border-0">
                                                        <input type="text" className="form-control bg-white" />
                                                    </td>
                                                    <td className="border-0">
                                                        <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                            <option value="">Active</option>
                                                            <option value="">Suspended</option>
                                                        </select>
                                                    </td>
                                                    <td className="border-0">
                                                        <span><i className="icon-plus" /></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="border-0">
                                                        <input type="text" className="form-control bg-white" />
                                                    </td>
                                                    <td className="border-0">
                                                        <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control bg-white">
                                                            <option value="">Active</option>
                                                            <option value="">Suspended</option>
                                                        </select>
                                                    </td>
                                                    <td className="border-0">
                                                        <span><i className="icon-minus text-danger" /></span>
                                                        <span><i className="icon-plus" /></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="button" className="btn btn-warning">Update Subject</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*--/modal for edit Subject--*/}
                </div>

            </div>
        );


    }
}
export default chapterList