import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';
import Teacherentry from './addMentor'
import Bulkentry from './addMentorMaster'
import Manualedit from './editMentor';
import MentorView from './mentorView'
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';

class mentorList extends Component {
  constructor(props) {
    super(props);
    this.teacherEntry = this.teacherEntry.bind(this);
    this.bulkEntry = this.bulkEntry.bind(this);
    this.mentorView = this.mentorView.bind(this);
    this.editManual = this.editManual.bind(this);
    this.deleteMentor = this.deleteMentor.bind(this)
    this.changePass = this.changePass.bind(this);
    this.deleteSelectedOptions = this.deleteSelectedOptions.bind(this)
    this.state = {
      mentorList: [],
      searchString: '',
    }
    this.mentorSearchList = []
  }
  changePass() {

    let email = document.getElementById('email1').value;
    console.log(email);
    let pwd = document.getElementById('newpass').value;
    const fd = new FormData();
    fd.append('email', email);
    fd.append('pwd', pwd);
    swal({
      title: "Are you sure?",
      text: "Sure to change?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          let url = global.API + "/studapi/public/api/changeuserpassword";
          fetch(url, {
            method: 'POST',

            body: fd
          })
            .then((resp1) => resp1.json()
              .then((resp) => {
                if (resp[0]['result'] == 1) {
                  swal("password changed", {
                    icon: "success",
                  });
                  this.componentDidMount();
                }

              }));
        }

      });

  }
  deleteMentor(id1) {
    console.log(id1);
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Teacher!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          let url = global.API + "/studapi/public/api/deleteteacher";
          fetch(url, {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
              "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'teacherID': id1 })
          })
            .then((resp1) => resp1.json()
              .then((resp) => {
                if (resp[0]['result'] == 1) {
                  swal("Teacher deleted!", {
                    icon: "success",
                  });
                  this.componentDidMount();
                }

              }));
        }

      });
  }
  teacherEntry() {

    ReactDOM.render(<Teacherentry />, document.getElementById('contain1'));
  }
  bulkEntry() {

    ReactDOM.render(<Bulkentry />, document.getElementById('contain1'));
  }
  editManual(men) {

    ReactDOM.render(<Manualedit mentordata={men} />, document.getElementById('contain1'))
  }


  mentorView(id2) {

    ReactDOM.render(<MentorView teacherid={id2} />, document.getElementById('contain1'))
  }


  componentDidMount() {

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ 'organisation_i_d': Cookies.get('orgid') })

    };


    let url = global.API + "/studapi/public/api/viewallteachers";
    fetch(url, requestOptions)
      .then(res => res.json())
      .then(json => {
        this.setState({
          mentorList: json
        })
      }).then(() => {
        let count = Object.keys(this.state.mentorList).length;
        if (count == 0) {
          swal({
            title: "Oops!",
            text: "Nothing to show!! ",
            icon: "info",
            button: "OK",
          });
        }
      });



  }
  //this function deletes the selected options.
  deleteSelectedOptions(){
     var toDelete = []
     let obj = $('.checkBoxForDeletion:checked')
     let len = obj.length
     Object.keys(obj).map(function(key, index){
        if(index < len){
           toDelete.push(obj[key].value)
        }
     })
     swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover these Teachers!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
     })
      .then((willDelete) => {
         if (willDelete) {
           try{
             toDelete.forEach(   function(value){
                let url = global.API + "/studapi/public/api/deleteteacher";
                fetch(url, {
                  method: 'POST',
                  headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                  },

                  body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'teacherID': value })
                })
             });
             this.componentDidMount();
             $(".checkBoxForDeletion").prop('checked', false)
            swal("All Selected Teachers Deleted! Refresh to see the results :D", {
                 icon: "success",
                });
         }catch(err){
            swal('Some Teachers Cannot be deleted! :(', {
             icon: "warning",
            });
         }
      }
      });
 }

 //this function toggles the state of all the options
   selectAllOptions(){
      if(!$('.checkBoxForDeletion:not(:checked)').length){
         $(".checkBoxForDeletion").prop('checked', false)
      }else{
            $(".checkBoxForDeletion").prop('checked', true)
      }

   }
// refresh Page
refreshPage(){
  this.componentDidMount();
}

  render() {
    var {  mentorList } = this.state;
    if(this.state.searchString.replace(/\s/g, '') != ''){
       this.mentorSearchList = SearchForObjectsWithName(mentorList, this.state.searchString)
   }else{
      this.mentorSearchList = mentorList
   }
    return (
      <div className="fadeIn animated">
        <div className="card">
          <div className="card-header">
            <span className="font-weight-bold h3">List of Teachers</span>
            <span className="float-right"><a onClick={this.teacherEntry}><i className="icon-plus"></i> Create Teachers </a></span>
            <span className="float-right"><a onClick={this.bulkEntry}><i className="icon-plus"></i> Upload Teachers Master Data</a>&nbsp;&nbsp;&nbsp;&nbsp;</span>
          </div>
          <div className = "">
         </div>
          <div className="card-body">

          <div className="card">
            <div className="card-body pt-0 pb-0">

          <button className = "btn btn-warning float-left" onClick={()=>this.selectAllOptions()} >Select All</button>

          <button className = "btn btn-primary float-left" onClick = {()=> this.deleteSelectedOptions()}>Delete Selected Options</button>
          <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

          <input type = "text" className = "form-control-sm  bg-white float-right mt-2" placeholder = "Type to Search" onChange = {(e) => this.setState({searchString : e.target.value})} ></input>
         </div>
          </div>

            <div className="table-responsive">
              <table className="datatable table table-bordered">
                <thead>
                  <tr className="bg-light table-head-fixed">
                    <th className="border-0"><input type="checkbox" onClick={()=>this.selectAllOptions()} /></th>
                    <th className="border-0">Profile Picture</th>
                    <th className="border-0">Name</th>
                    <th className="border-0">Contact Number</th>
                    <th className="border-0">Email ID</th>
                    <th className="border-0">Designation</th>
                    <th className="border-0">Specialization</th>
                    <th className="border-0">Qualification</th>
                    <th className="border-0">Current Status</th>
                    <th className="border-0">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {this.mentorSearchList.map(men => (
                    <tr>
                      <td className="border-0"><input type="checkbox" value={men.teacherID} className="checkBoxForDeletion"  /></td>
                      <td className="border-0"><img src={global.img + men.profilePic} alt="profile picture" className="students_profile_pic" /></td>
                      <td className="border-0">{men.teacherName}</td>
                      <td className="border-0" >{men.contactNo}</td>
                      <td className="border-0" >{men.emailID}</td>
                      <td className="border-0">{men.designation}</td>
                      <td className="border-0">{men.specialization}</td>
                      <td className="border-0">{men.qualification}</td>
                      <td className="border-0">
                        <span className="badge badge-pill bg-light">{men.status}</span>
                      </td>
                      <td className="border-0">
                        <span><button className="text-success btn p-0 m-0 btn-white" data-toggle="modal" data-target="#resetPassword" value={men.emailID} onClick={() =>document.getElementById('email1').value=men.emailID}><i className="icon-key"></i></button></span>
                        <span><a onClick={() => this.editManual(men)} className="text-warning"><i className="icon-pencil"></i></a></span>
                        <span><a onClick={() => this.mentorView(men.teacherID)} className="text-warning"><i className="icon-eye"></i></a></span>
                        <span><a onClick={() => this.deleteMentor(men.teacherID)} className="text-danger"><i className="icon-trash"></i></a></span>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>

          </div>
        </div>


        {/*-modal for reset Password*/}
        <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="container-fluid text-center">
                  <h4><span className="font-weight-bold">Reset Password</span></h4>
                  <hr />
                  <div className="form-group">
                  <label htmlFor className="font-weight-bold mb-0">Changing Password For:</label>
                    <input type="text" disabled className="form-control-sm" id="email1"  />
                    <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                    <input type="password" className="form-control-sm" id="newpass" />
                  </div>
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                  <button type="button" onClick={() => this.changePass()} className="btn btn-warning">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*-/modal for reset Password */}

      </div>
    );


  }
}
export default mentorList
