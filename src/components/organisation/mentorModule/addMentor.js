import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

import $ from 'jquery';
import swal from 'sweetalert';
import Bulkentry from './addMentorMaster'
import Mentorlist from './mentorList'
import './addMentor.css'

class addMentor extends Component {
    constructor(props) {
        super(props);
        this.bulkEntry = this.bulkEntry.bind(this);
        this.mentorList = this.mentorList.bind(this);
        this.resetFields = this.resetFields.bind(this);

        this.state = {
            organisation_i_d: Cookies.get('orgid'),

            teacher_name: "",
            contact_no: "",
            email_i_d: "",
            gender: "",
            date_of_birth: "",
            designation: "",
            specialization: "",
            qualification: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            country: "",
            pin_code: "",

            status: "",
            ii: 0

        }
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }

      selectRegion (val) {
        this.setState({ state: val });
      }

    resetFields() {

        this.setState({
            organisation_i_d: Cookies.get('orgid'),

            teacher_name: "",
            contact_no: "",
            email_i_d: "",
            gender: "",
            date_of_birth: "",
            designation: "",
            specialization: "",
            qualification: "",
            address1: "",
            address2: "",
            city: "",
            state: "",
            country: "",
            pin_code: "",
            status: ""

        })


    }
    bulkEntry() {

        ReactDOM.render(<Bulkentry />, document.getElementById('contain1'));
    }
    mentorList() {

        ReactDOM.render(<Mentorlist />, document.getElementById('contain1'));
    }

    submit() {
        var pin = document.getElementById("pin").value;
        var email = document.getElementById("email").value;
        var phNo = document.getElementById("phNo").value;
        var i = 0;
        var pinCheck = /^[0-9]{6}$/;
        //var websiteCheck = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,80}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
        var telCheck = /^[0-9]{10}$/;
        var emailCheck = /^[A-Za-z0-9_.]{3,60}@[A-Za-z0-9]{3,60}[.]{1}[A-Za-z.]{2,6}$/;

        if (i < 14) {
            if (document.getElementById("teacherName").value == "") {
                document.getElementById("teacherNameError").innerHTML = "*Please fill this field";
                document.getElementById("teacherName").focus();
                document.getElementById("teacherName").style.borderColor = "#FF0000";
            }

            if (document.getElementById("teacherName").value != "") {
                document.getElementById("teacherNameError").innerHTML = "";
                i = i + 1;
                document.getElementById("teacherName").style.borderColor = "";
            }
          {/* if (document.getElementById("desig").value == "") {
                document.getElementById("desigError").innerHTML = "*Please fill this field";
                document.getElementById("desig").focus();
                document.getElementById("desig").style.borderColor = "#FF0000";
            }

            if (document.getElementById("desig").value != "") {
                document.getElementById("desigError").innerHTML = "";
                i = i + 1;
                document.getElementById("desig").style.borderColor = "";
            }*/}
            if (document.getElementById("qualification").value == "") {
                document.getElementById("qualificationError").innerHTML = "*Please fill this field";
                document.getElementById("qualification").focus();
                document.getElementById("qualification").style.borderColor = "#FF0000";
            }

            if (document.getElementById("qualification").value != "") {
                document.getElementById("qualificationError").innerHTML = "";
                i = i + 1;
                document.getElementById("qualification").style.borderColor = "";
            }
            if (document.getElementById("specialization").value == "") {
                document.getElementById("specializationError").innerHTML = "*Please fill this field";
                document.getElementById("specialization").focus();
                document.getElementById("specialization").style.borderColor = "#FF0000";
            }

            if (document.getElementById("specialization").value != "") {
                document.getElementById("specializationError").innerHTML = "";
                i = i + 1;
                document.getElementById("specialization").style.borderColor = "";
            }

            if (document.getElementById("gender").value == "") {
                document.getElementById("genderError").innerHTML = "*Please fill this field";
                document.getElementById("gender").focus();
                document.getElementById("gender").style.borderColor = "#FF0000";
            }

            if (document.getElementById("gender").value != "") {
                document.getElementById("genderError").innerHTML = "";
                i = i + 1;
                document.getElementById("gender").style.borderColor = "";
            }
            if (document.getElementById("date_of_birth").value == "") {
                document.getElementById("dateOfBirthError").innerHTML = "*Please fill this field";
                document.getElementById("date_of_birth").focus();
                document.getElementById("date_of_birth").style.borderColor = "#FF0000";
            }

            if (document.getElementById("date_of_birth").value != "") {
                document.getElementById("dateOfBirthError").innerHTML = "";
                i = i + 1;
                document.getElementById("date_of_birth").style.borderColor = "";
            }


            if (document.getElementById("address1").value == "") {
                document.getElementById("addressError").innerHTML = "*Please fill this field";
                document.getElementById("address1").focus();
                document.getElementById("address1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("address1").value != "") {
                document.getElementById("addressError").innerHTML = "";
                i = i + 1;
                document.getElementById("address1").style.borderColor = "";
            }
            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }



            if (document.getElementById("country").value == "") {
                document.getElementById("countryError").innerHTML = "*Please fill this field";
                document.getElementById("country").focus();
                document.getElementById("country").style.borderColor = "#FF0000";
            }

            if (document.getElementById("country").value != "") {
                document.getElementById("countryError").innerHTML = "";
                i = i + 1;
                document.getElementById("country").style.borderColor = "";
            }

            if (document.getElementById("state").value == "") {
                document.getElementById("stateError").innerHTML = "*Please fill this field";
                document.getElementById("state").focus();
                document.getElementById("state").style.borderColor = "#FF0000";
            }

            if (document.getElementById("state").value != "") {
                document.getElementById("stateError").innerHTML = "";
                i = i + 1;
                document.getElementById("state").style.borderColor = "";
            }

            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }

            if (document.getElementById("pin").value == "") {
                document.getElementById("pinError").innerHTML = "*Please fill this field";
                document.getElementById("pin").focus();
                document.getElementById("pin").style.borderColor = "#FF0000";
            }


            if (document.getElementById("pin").value != "") {
                if (!pinCheck.test(pin)) {
                    document.getElementById("pinError").innerHTML = "*Invalid PIN";
                    document.getElementById("pin").focus();
                    document.getElementById("pin").style.borderColor = "#FF0000";
                }

                if (pinCheck.test(pin)) {
                    document.getElementById("pinError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("pin").style.borderColor = "";
                }
            }

            if (document.getElementById("email").value == "") {
                document.getElementById("emailError").innerHTML = "*Please fill this field";
                document.getElementById("email").focus();
                document.getElementById("email").style.borderColor = "#FF0000";
            }

            if (document.getElementById("email").value != "") {
                if (!emailCheck.test(email)) {
                    document.getElementById("emailError").innerHTML = "*Invalid Email Id";
                    document.getElementById("email").focus();
                    document.getElementById("email").style.borderColor = "#FF0000";
                }

                if (emailCheck.test(email)) {
                    document.getElementById("emailError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("email").style.borderColor = "";
                }
            }
            if (document.getElementById("phNo").value == "") {
                document.getElementById("phNoError").innerHTML = "*Please fill this field";
                document.getElementById("phNo").focus();
                document.getElementById("phNo").style.borderColor = "#FF0000";
            }

            if (document.getElementById("phNo").value != "") {
                if (!telCheck.test(phNo)) {
                    document.getElementById("phNoError").innerHTML = "*Invalid Telephone Number";
                    document.getElementById("phNo").focus();
                    document.getElementById("phNo").style.borderColor = "#FF0000";
                }

                if (telCheck.test(phNo)) {
                    document.getElementById("phNoError").innerHTML = "";
                    i = i + 1;
                    document.getElementById("phNo").style.borderColor = "";
                }
            }
            if (document.getElementById("status").value == "") {
                document.getElementById("statusError").innerHTML = "*Please fill this field";
                document.getElementById("status").focus();
                document.getElementById("status").style.borderColor = "#FF0000";
            }

            if (document.getElementById("status").value != "") {
                document.getElementById("statusError").innerHTML = "";
                i = i + 1;
                document.getElementById("status").style.borderColor = "";
            }

            if (i == 14) {
                console.log(i);
                this.state.ii = 14;
            }
        }
        if (this.state.ii == 14) {

        console.log(this.state);
        let url = global.API + "/studapi/public/api/addteacher";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Mentor Added Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.resetFields();
                }
                else if (resp == 2) {
                    swal({
                        title: "Error!",
                        text: "Mentor is Already added in the organisation",
                        icon: "warning",
                        button: "OK",
                    });
                }
                else {
                    swal({
                        title: "Error!",
                        text: "This emailID cannot be added as a Mentor",
                        icon: "warning",
                        button: "OK",
                    });
                }

            })
        })
    }}
    submitandprroceed() { var pin = document.getElementById("pin").value;
    var email = document.getElementById("email").value;
    var phNo = document.getElementById("phNo").value;
    var i = 0;
    var pinCheck = /^[0-9]{6}$/;
    //var websiteCheck = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,80}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
    var telCheck = /^[0-9]{10}$/;
    var emailCheck = /^[A-Za-z0-9_.]{3,60}@[A-Za-z0-9]{3,60}[.]{1}[A-Za-z.]{2,6}$/;

    if (i < 14) {
        if (document.getElementById("teacherName").value == "") {
            document.getElementById("teacherNameError").innerHTML = "*Please fill this field";
            document.getElementById("teacherName").focus();
            document.getElementById("teacherName").style.borderColor = "#FF0000";
        }

        if (document.getElementById("teacherName").value != "") {
            document.getElementById("teacherNameError").innerHTML = "";
            i = i + 1;
            document.getElementById("teacherName").style.borderColor = "";
        }
      {/* if (document.getElementById("desig").value == "") {
            document.getElementById("desigError").innerHTML = "*Please fill this field";
            document.getElementById("desig").focus();
            document.getElementById("desig").style.borderColor = "#FF0000";
        }

        if (document.getElementById("desig").value != "") {
            document.getElementById("desigError").innerHTML = "";
            i = i + 1;
            document.getElementById("desig").style.borderColor = "";
        }*/}
        if (document.getElementById("qualification").value == "") {
            document.getElementById("qualificationError").innerHTML = "*Please fill this field";
            document.getElementById("qualification").focus();
            document.getElementById("qualification").style.borderColor = "#FF0000";
        }

        if (document.getElementById("qualification").value != "") {
            document.getElementById("qualificationError").innerHTML = "";
            i = i + 1;
            document.getElementById("qualification").style.borderColor = "";
        }
        if (document.getElementById("specialization").value == "") {
            document.getElementById("specializationError").innerHTML = "*Please fill this field";
            document.getElementById("specialization").focus();
            document.getElementById("specialization").style.borderColor = "#FF0000";
        }

        if (document.getElementById("specialization").value != "") {
            document.getElementById("specializationError").innerHTML = "";
            i = i + 1;
            document.getElementById("specialization").style.borderColor = "";
        }

        if (document.getElementById("gender").value == "") {
            document.getElementById("genderError").innerHTML = "*Please fill this field";
            document.getElementById("gender").focus();
            document.getElementById("gender").style.borderColor = "#FF0000";
        }

        if (document.getElementById("gender").value != "") {
            document.getElementById("genderError").innerHTML = "";
            i = i + 1;
            document.getElementById("gender").style.borderColor = "";
        }
        if (document.getElementById("date_of_birth").value == "") {
            document.getElementById("dateOfBirthError").innerHTML = "*Please fill this field";
            document.getElementById("date_of_birth").focus();
            document.getElementById("date_of_birth").style.borderColor = "#FF0000";
        }

        if (document.getElementById("date_of_birth").value != "") {
            document.getElementById("dateOfBirthError").innerHTML = "";
            i = i + 1;
            document.getElementById("date_of_birth").style.borderColor = "";
        }


        if (document.getElementById("address1").value == "") {
            document.getElementById("addressError").innerHTML = "*Please fill this field";
            document.getElementById("address1").focus();
            document.getElementById("address1").style.borderColor = "#FF0000";
        }

        if (document.getElementById("address1").value != "") {
            document.getElementById("addressError").innerHTML = "";
            i = i + 1;
            document.getElementById("address1").style.borderColor = "";
        }
        if (document.getElementById("city").value == "") {
            document.getElementById("cityError").innerHTML = "*Please fill this field";
            document.getElementById("city").focus();
            document.getElementById("city").style.borderColor = "#FF0000";
        }

        if (document.getElementById("city").value != "") {
            document.getElementById("cityError").innerHTML = "";
            i = i + 1;
            document.getElementById("city").style.borderColor = "";
        }



        if (document.getElementById("country").value == "") {
            document.getElementById("countryError").innerHTML = "*Please fill this field";
            document.getElementById("country").focus();
            document.getElementById("country").style.borderColor = "#FF0000";
        }

        if (document.getElementById("country").value != "") {
            document.getElementById("countryError").innerHTML = "";
            i = i + 1;
            document.getElementById("country").style.borderColor = "";
        }

        if (document.getElementById("state").value == "") {
            document.getElementById("stateError").innerHTML = "*Please fill this field";
            document.getElementById("state").focus();
            document.getElementById("state").style.borderColor = "#FF0000";
        }

        if (document.getElementById("state").value != "") {
            document.getElementById("stateError").innerHTML = "";
            i = i + 1;
            document.getElementById("state").style.borderColor = "";
        }

        if (document.getElementById("city").value == "") {
            document.getElementById("cityError").innerHTML = "*Please fill this field";
            document.getElementById("city").focus();
            document.getElementById("city").style.borderColor = "#FF0000";
        }

        if (document.getElementById("city").value != "") {
            document.getElementById("cityError").innerHTML = "";
            i = i + 1;
            document.getElementById("city").style.borderColor = "";
        }

        if (document.getElementById("pin").value == "") {
            document.getElementById("pinError").innerHTML = "*Please fill this field";
            document.getElementById("pin").focus();
            document.getElementById("pin").style.borderColor = "#FF0000";
        }


        if (document.getElementById("pin").value != "") {
            if (!pinCheck.test(pin)) {
                document.getElementById("pinError").innerHTML = "*Invalid PIN";
                document.getElementById("pin").focus();
                document.getElementById("pin").style.borderColor = "#FF0000";
            }

            if (pinCheck.test(pin)) {
                document.getElementById("pinError").innerHTML = "";
                i = i + 1;
                document.getElementById("pin").style.borderColor = "";
            }
        }

        if (document.getElementById("email").value == "") {
            document.getElementById("emailError").innerHTML = "*Please fill this field";
            document.getElementById("email").focus();
            document.getElementById("email").style.borderColor = "#FF0000";
        }

        if (document.getElementById("email").value != "") {
            if (!emailCheck.test(email)) {
                document.getElementById("emailError").innerHTML = "*Invalid Email Id";
                document.getElementById("email").focus();
                document.getElementById("email").style.borderColor = "#FF0000";
            }

            if (emailCheck.test(email)) {
                document.getElementById("emailError").innerHTML = "";
                i = i + 1;
                document.getElementById("email").style.borderColor = "";
            }
        }
        if (document.getElementById("phNo").value == "") {
            document.getElementById("phNoError").innerHTML = "*Please fill this field";
            document.getElementById("phNo").focus();
            document.getElementById("phNo").style.borderColor = "#FF0000";
        }

        if (document.getElementById("phNo").value != "") {
            if (!telCheck.test(phNo)) {
                document.getElementById("phNoError").innerHTML = "*Invalid Telephone Number";
                document.getElementById("phNo").focus();
                document.getElementById("phNo").style.borderColor = "#FF0000";
            }

            if (telCheck.test(phNo)) {
                document.getElementById("phNoError").innerHTML = "";
                i = i + 1;
                document.getElementById("phNo").style.borderColor = "";
            }
        }
        if (document.getElementById("status").value == "") {
            document.getElementById("statusError").innerHTML = "*Please fill this field";
            document.getElementById("status").focus();
            document.getElementById("status").style.borderColor = "#FF0000";
        }

        if (document.getElementById("status").value != "") {
            document.getElementById("statusError").innerHTML = "";
            i = i + 1;
            document.getElementById("status").style.borderColor = "";
        }

        if (i == 14) {
            console.log(i);
            this.state.ii = 14;
        }
    }
    if (this.state.ii == 14) {

        console.log(this.state);
        let url = global.API + "/studapi/public/api/addteacher";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Mentor Added Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.mentorList();
                }
                else if (resp == 2) {
                    swal({
                        title: "Error!",
                        text: "Mentor is Already added in the organisation",
                        icon: "warning",
                        button: "OK",
                    });
                }
                else {
                    swal({
                        title: "Error!",
                        text: "This emailID cannot be added as a Mentor",
                        icon: "warning",
                        button: "OK",
                    });
                }

            })
        })
    }}

    previewImage() {
        $('#previewImage').css({ 'display': 'block' })
        // $('#profilepic').css({'display':'none'})
        $('#profilePicBox').css({ 'display': 'none' })
        var src = document.getElementById("profilepic");
        var target = document.getElementById("previewImage");
        var fr = new FileReader();
        // when image is loaded, set the src of the image where you want to display it
        fr.onload = function (e) { target.style.backgroundImage = 'url("' + this.result + '")' };
        // fill fr with image data
        fr.readAsDataURL(src.files[0]);
    }

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">
                            Teacher Entry
                            </span>
                        <span className="float-right"><a onClick={this.bulkEntry} className="btn btn-primary text-white"><i className="icon-docs"></i> Add Multiple Entry</a></span>
                    </div>
                    <div className="card-body">
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 text-success border-title">Information:</span>
                            <div className="row mt-3">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="teacherName" name="teacher_name" value={this.state.teacher_name} onChange={(data) => { this.setState({ teacher_name: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="teacherNameError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Designation:</label>
                                         <input type="text" id="desig" name="designation" value={this.state.designation} onChange={(data) => { this.setState({ designation: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="desigError" class="text-danger font-weight-bold"></span>
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Qualification <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="qualification" name="qualification" value={this.state.qualification} onChange={(data) => { this.setState({ qualification: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="qualificationError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Gender <span className="text-danger"><strong>*</strong></span>:</label>
                                         <select name="gender" id="gender" value={this.state.gender} onChange={(data) => { this.setState({ gender: data.target.value }) }} className="form-control  bg-white">
                                            <option value="">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <span id="genderError" class="text-danger font-weight-bold"></span>

                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Specialization <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="specialization" name="specialization" value={this.state.specialization} onChange={(data) => { this.setState({ specialization: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="specializationError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Date Of Birth <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="date" id="date_of_birth" name="date_of_birth" value={this.state.date_of_birth} onChange={(data) => { this.setState({ date_of_birth: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="dateOfBirthError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                                <div className="col-md-4" >
                                    <div className="form-group files" id="profilePicBox">
                                        <label for="profilepic" className="mb-0 font-weight-bold">Upload Profile Picture:</label> <br />
                                         <input type="file" id="profilepic" onChange={this.previewImage} className="form-control  bg-white" />
                                    </div>
                                    <div id="previewImage" className="files">

                                    </div>

                                </div>

                            </div>
                            <hr />
                            <div className="row mt-3">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="address1" name="address1" value={this.state.address1} onChange={(data) => { this.setState({ address1: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="addressError" class="text-danger font-weight-bold"></span>
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address 2:</label>
                                         <input type="text" id="address2" name="address2" value={this.state.address2} onChange={(data) => { this.setState({ address2: data.target.value }) }} className="form-control  bg-white" />

                                   </div>
                                </div>
                            </div>

                            <div className="row">

                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="city" name="city" value={this.state.city} onChange={(data) => { this.setState({ city: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="cityError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN Code <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="pin" name="pin_code" onChange={(data) => { this.setState({ pin_code: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="pinError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group mt-3">
                                        <label for="" className="mb-0 font-weight-bold">Account Status <span className="text-danger"><strong>*</strong></span>:</label>
                                       <select id="status" name="status" value={this.state.status} onChange={(data) => { this.setState({ status: data.target.value }) }} className="form-control  bg-white">
                                            <option value="">--Select--</option>
                                            <option value="Active">Active</option>
                                            <option value="Deactive">Deactivated</option>
                                       </select>

                                        <span id="statusError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                        <CountryDropdown type="text" id="country" name="country" value={this.state.country} onChange={(data) => {  this.selectCountry(data)}} className="form-control  bg-white" />

                                        <span id="countryError" class="text-danger font-weight-bold"></span>
                                    </div>


                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email ID <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="email" name="email_i_d" value={this.state.email_i_d} onChange={(data) => { this.setState({ email_i_d: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="emailError" class="text-danger font-weight-bold"></span>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State <span className="text-danger"><strong>*</strong></span>:</label>
                                        <RegionDropdown country={this.state.country} id="state" name="state" value={this.state.state} onChange={(data) => { this.selectRegion(data) }} className="form-control  bg-white" />

                                        <span id="stateError" class="text-danger font-weight-bold"></span>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                                         <input type="text" id="phNo" name="contact_no" onChange={(data) => { this.setState({ contact_no: data.target.value }) }} className="form-control  bg-white" />

                                        <span id="phNoError" class="text-danger font-weight-bold"></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="card-footer">
                        <div className="float-right">
                            <button onClick={() => { this.submit() }} className="btn btn-primary"><i className="icon-plus"></i> Add New Entry</button>
                            <button onClick={() => { this.submitandprroceed() }} className="btn btn-success"><i className="icon-check"></i> Save and Proceed</button>
                            <button onClick={() => { this.resetFields() }} className="btn btn-warning"><i className="icon-reset"></i> Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        );


    }
}
export default addMentor
