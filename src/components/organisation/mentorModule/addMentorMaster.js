import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './addMentorMaster.css';
import swal from 'sweetalert';
import Cookies from 'js-cookie';
import $ from 'jquery';
import Men from './mentorList';

import Manualentry from './addMentor'
class addMentorMaster extends Component {
    constructor(props) {
        super(props);
        this.entryManual = this.entryManual.bind(this);
        this.showMentor = this.showMentor.bind(this);



        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            cd: new Date().toISOString().slice(0, 10),
            file1: "",
            ii: 0

        }
    }
    showMentor() {

        ReactDOM.render(<Men />, document.getElementById('contain1'))
    }
    entryManual() {

        ReactDOM.render(<Manualentry />, document.getElementById('contain1'))
    }
    submit() { 
         var i = 0;
        if (i < 1) {
            if (document.getElementById("file").value == "") {
                document.getElementById("fileError").innerHTML = "*Please chose a file";
                document.getElementById("file").focus();
                document.getElementById("file").style.borderColor = "#FF0000";
            }

            if (document.getElementById("file").value != "") {
                document.getElementById("fileError").innerHTML = "";
                i = i + 1;
                document.getElementById("file").style.borderColor = "";
            }
            if (i == 1) {
                console.log(i);
                this.state.ii = 1;
            }
        }
        if (this.state.ii == 1) {
        console.log(this.state);
        let url = global.API + "/studapi/public/api/addteacherbulk";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Upload complete!",
                        text: "Job is done!",
                        icon: "success",
                    });
                    this.showMentor();
                }
                else
                    swal({
                        title: "Oops!",
                        text: "Data not uploaded!",
                        icon: "warning",
                    });
            })
        })
        console.log(data);
    }}



    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold h3">Upload Teacher's Master Data</span>
                        <span className="float-right"><a onClick={this.entryManual} className="btn btn-primary text-white">Manual Entry</a></span>
                    </div>
                    <div className="card-body">
                          {/*-modal filter*/}
                          <div className="  ">
                            <div className="collapse alert alert-warning" id="helpArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Duplicate emails are not allowed.
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Provide Working Email ID.
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Date Format should follow: YYYY-MM-DD.
                                            </div>
                                        </div>

                                    </div>

                                    <div className="row">

                                        <div className="col-md-6">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  User our provided sample file for uploading data.
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="alert alert-success" role="alert">
                                                <i className="fa fa-check-circle"></i>  Please fill all the mandatory fields
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*-/filter*/}
                        <div className="form-group files mt-3">
                            <span className="text-right"><a href="/teacher.csv" className="float-right"><i className="icon-cloud-download"></i> Our Sample format</a></span>
                            <label className="font-weight-bold">Upload Your File <span className="text-danger"><strong>*</strong></span> : <span className="small">.XLS, .CSV File Supported only</span>   <span className="bg-warning p-1 alert " data-toggle="collapse" data-target="#helpArea" aria-expanded="false" aria-controls="helpArea"  ><i class="icon-question font-weight-bold text-bule"></i> help</span>  </label>
                            <input type="file" id="file" name="org_logo" onChange={(data) => { this.setState({ file1: data.target.files[0] }) }} className="form-control" />
                            <span id="fileError" class="text-danger font-weight-bold"></span>
                       </div>
                        <div className="progress">
                            <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style={{ width: "75%" }}></div>
                        </div>
                        <hr />

                        {/*    <div className="table-responsive">
                                <table className="datatable table table-bordered">
                                    <thead>
                                        <tr className="bg-light table-head-fixed">
                                            <th className="border-0"><input type="checkbox" onclick="toggle(this);"/></th>
                                            <th className="border-0">ID</th>
                                            <th className="border-0">Profile Picture</th>
                                            <th className="border-0">Name</th>
                                            <th className="border-0">Contact Number</th>
                                            <th className="border-0">Email ID</th>
                                            <th className="border-0">Designation</th>
                                            <th className="border-0">Specialization</th>
                                            <th className="border-0">Qualification</th>
                                            <th className="border-0">Address</th>
                                            <th className="border-0">City</th>
                                            <th className="border-0">State</th>
                                            <th className="border-0">PIN Code</th>
                                            <th className="border-0">Country</th>
                                            <th className="border-0">Current Status</th>
                                            <th className="border-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="border-0"><input type="checkbox" name="" id=""/></td>
                                            <td className="border-0"><a href="view.html">18705516014</a></td>
                                            <td className="border-0"><img src="../img/avatars/4.jpg" alt="profile picture" className="students_profile_pic"/></td>
                                            <td className="border-0">Sumanth Sah</td>
                                            <td className="border-0">9876543210</td>
                                            <td className="border-0">example@xyz.com</td>
                                            <td className="border-0">Assistance Professor</td>
                                            <td className="border-0">Physics</td>
                                            <td className="border-0">M.Tech</td>
                                            <td className="border-0">Kestopur</td>
                                            <td className="border-0">Kolkata</td>
                                            <td className="border-0">West Bengal</td>
                                            <td className="border-0">700102</td>
                                            <td className="border-0">India</td>
                                            <td className="border-0">
                                                <span className="badge badge-pill bg-light">Active</span>
                                            </td>
                                            <td className="border-0">
                                                <span><a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash"></i></a></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                         */}
                        <button type="submit" onClick={() => { this.submit() }} className="btn btn-success mt-3 float-right"><i className="icon-check"></i> Save and Proceed</button>

                    </div>
                </div>

                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>



        );



    }
}
export default addMentorMaster