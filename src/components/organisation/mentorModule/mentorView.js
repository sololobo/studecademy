import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './mentorView.css';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import Mentorlist from './mentorList'


class mentorView extends Component {

  constructor(props) {
    super(props);
    console.log(this.props.teacherid);
    this.mentorList = this.mentorList.bind(this);

    this.changePass = this.changePass.bind(this);

    this.state = {
      mentorview: [],
      isLoaded: false
    }

  }
  changePass() {
    let email = this.state.mentorview[0].emailID;
    let pwd = document.getElementById('newpass').value;
    const fd = new FormData();
    fd.append('email', email);
    fd.append('pwd', pwd);
    swal({
      title: "Are you sure?",
      text: "Sure to change?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
      .then((willDelete) => {
        if (willDelete) {
          let url = global.API + "/studapi/public/api/changeuserpassword";
          fetch(url, {
            method: 'POST',

            body: fd
          })
            .then((resp1) => resp1.json()
              .then((resp) => {
                if (resp[0]['result'] == 1) {
                  swal("password changed", {
                    icon: "success",
                  });
                  this.componentDidMount();
                }

              }));
        }

      });

  }

  componentDidMount() {




    let url = global.API + "/studapi/public/api/viewateacher";
    fetch(url, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      },

      body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'teacher_i_d': this.props.teacherid })
    })
      .then(res => res.json())
      .then(json => {
        console.log(json)
        this.setState({
          isLoaded: true,
          mentorview: json,

        })
      });



  }
  mentorList() {

    ReactDOM.render(<Mentorlist />, document.getElementById('contain1'))

  }
  render() {
    var { isLoaded, mentorview } = this.state;
    console.log(mentorview)
    return (
      <div className="fadeIn animated">
        <div className="card">
          <div className="card-header">
            <span className="font-weight-bold text-primary mb-0 h3">Teacher's Information</span>
            <a onClick={this.mentorList} className="float-right mt-2"><i className="icon-action-undo"></i> Back to list of
                    Teachers</a>
          </div>
          {mentorview.map(mentor => (
            <div className="card-body">
              <h2>
                <img id="blah" src={global.img + mentor.profilePic} alt="teacher's Photo" className="teachers-view-photo" />
                <span className="font-weight-bold">{mentor.teacherName}</span>

                <span className="float-right small"><a href="#" className="btn  btn-primary text-white mt-4"
                  data-toggle="modal" data-target="#resetPassword"> Reset Password</a></span>
              </h2>
              <hr />
              {/*
              <div className="row">
                <div className="col-sm-6 col-md-4">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-badge"></i>
                      </div>
                      <div className="h4 mb-0">200</div>
                      <small className="text-muted text-uppercase font-weight-bold">Exam Upload</small>
                        <div className="progress progress-xs mt-3 mb-0">
                                    <div className="progress-bar bg-warning" role="progressbar" style="width: 25%"
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-6 col-md-4">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-pie-chart"></i>
                      </div>
                      <div className="h4 mb-0">28</div>
                      <small className="text-muted text-uppercase font-weight-bold">Upload Assignment</small>
                       <div className="progress progress-xs mt-3 mb-0">
                                    <div className="progress-bar" role="progressbar" style="width: 25%"
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-6 col-md-4">
                  <div className="card alert-blue">
                    <div className="card-body">
                      <div className="h1 text-muted text-right mb-4">
                        <i className="icon-speedometer"></i>
                      </div>
                      <div className="h4 mb-0">240</div>
                      <small className="text-muted text-uppercase font-weight-bold">Upload Guidesnotes</small>
                       <div className="progress progress-xs mt-3 mb-0">
                                    <div className="progress-bar bg-danger" role="progressbar" style="width: 25%"
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
                    </div>
                  </div>
                </div>

              </div>
              */}
              <div className="border pt-2 pb-2 pl-3 pr-3">
                <span className=" font-weight-bold h3 border-title">Details:</span>


                <div className="card-body">

                  <div className="row mt-3">
                    <div className="col-md-6">
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> ID:</label>
                        <input type="text" name="" id="" value={mentor.teacherID} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> Name:</label>
                        <input type="text" name="" id="" value={mentor.teacherName} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Contact Number:</label>
                        <input type="text" name="" id="" value={mentor.contactNo} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Email:</label>
                        <input type="text" name="" id="email1" value={mentor.emailID} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Designation:</label>
                        <input type="text" name="" id="" value={mentor.designation} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Specialization:</label>
                        <input type="text" name="" id="" value={mentor.specialization} disabled
                          className="form-control bg-white col-md-7" />
                      </div>


                    </div>
                    <div className="col-md-6">
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address:</label>
                        <input type="text" name="" id="" value={mentor.address1} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address 2:</label>
                        <input type="text" name="" id="" value={mentor.address2} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">City:</label>
                        <input type="text" name="" id="" value={mentor.city} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">State:</label>
                        <input type="text" name="" id="" value={mentor.state} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Country:</label>
                        <input type="text" name="" id="" value={mentor.country} disabled
                          className="form-control bg-white col-md-7" />
                      </div>
                      <div className="form-group  row">
                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">PIN Code:</label>
                        <input type="text" name="" id="" value={mentor.pinCode} disabled
                          className="form-control bg-white col-md-7" />
                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>



        {/*-modal for reset Password*/}
        <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="container-fluid text-center">
                  <h4><span className="font-weight-bold">Reset Password</span></h4>
                  <hr />
                  <div className="form-group">
                    <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                    <input type="password" className="form-control-sm" id="newpass" />
                  </div>
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                  <button type="button" onClick={() => this.changePass()} className="btn btn-warning">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*-/modal for reset Password */}

      </div>
    );


  }
}
export default mentorView
