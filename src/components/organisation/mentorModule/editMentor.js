import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import Bulkentry from './addMentorMaster'
import Mentorlist from './mentorList'
class editMentor extends Component {
    constructor(props) {
        super(props);
        this.bulkEntry = this.bulkEntry.bind(this);
       // this.changepic.this.changepic.bind(this)
        this.state = {

            mentorview: this.props.mentordata

        }
    }
    bulkEntry() {

        ReactDOM.render(<Bulkentry />, document.getElementById('contain1'));
    }


    submit() {
        let url = window.API + "/studapi/public/api/updateteacher";
        let data = this.state.mentorview;
        console.log(data)
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data[0], function (key, value) {

            fd.append(key, value);
        })
        fd.append('profile_pic', document.getElementById("profilepic").files[0]);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Teacher Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    ReactDOM.render(<Mentorlist />, document.getElementById('contain1'))
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Teacher Not Updated",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
    }
    

    componentDidMount() {



        let url = global.API + "/studapi/public/api/viewateacher";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'teacher_i_d': this.state.mentorview.teacherID })
        })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    mentorview: json,

                })
                console.log(json[0]['profilePic']);
            });



    }

    handleChange(columnName, columnValue) {
        let items = this.state.mentorview;
        items[0][columnName] = columnValue.toString();
        this.setState({ mentorview: items })
    }
    changepic(data){
        this.handleChange('profilePic',document.getElementById("profilepic").files[0] )
    }


    render() {
        var { mentorview } = this.state;

        return (

            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">
                            Teacher Entry
                  </span>
                        <span className="float-right"><a onClick={this.bulkEntry} className="btn btn-primary text-white"><i className="icon-docs"></i> Add Multiple Entry</a></span>
                    </div>
                    <div className="card-body">
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 text-success border-title">Information:</span>
                            <div className="row mt-3">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name  <span className="text-danger"><strong>*</strong></span>:</label>
                                        <input type="text" name="teacher_name" defaultValue={mentorview.teacherName} onChange={(data) => { this.handleChange('teacherName', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Designation:</label>
                                        <input type="text" name="designation" defaultValue={mentorview.designation} onChange={(data) => { this.handleChange('designation', data.target.value) }} className="form-control bg-white" />
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Qualification  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="qualification" defaultValue={mentorview.qualification} onChange={(data) => { this.handleChange('Qualification', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Gender  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="gender" id="gender" value={mentorview.gender} onChange={(data) => { this.handleChange('gender', data.target.value) }} className="form-control    bg-white">
                                            <option value="">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Specialization  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="specialization" defaultValue={mentorview.specialization} onChange={(data) => { this.handleChange('specialization', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Date Of Birth  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="date" name="date_of_birth" defaultValue={mentorview.dateOfBirth} onChange={(data) => { this.handleChange('dateOfBirth', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                <div className="form-group mb-1">
                                        <label for="" className="mb-0 font-weight-bold">Upload Profile Picture:</label> <br />
                                        <img src={global.img + mentorview.profilePic} alt="teacher's Image" className="students-photo" />
                                        <input type="file" id="profilepic" onChange={(data) => { this.changepic(data)}} className="form-control bg-white"/>
                                    </div>

                                </div>

                            </div>
                            <hr />
                            <div className="row mt-3">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="address1" defaultValue={mentorview.address1} onChange={(data) => { this.handleChange('address1', data.target.value) }} className="form-control bg-white" />
                                    </div>

                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address 2:</label>
                                        <input type="text" name="address2" defaultValue={mentorview.address2} onChange={(data) => { this.handleChange('address2', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                </div>
                            </div>

                            <div className="row">

                                <div className="col-md-4">

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="city" defaultValue={mentorview.city} onChange={(data) => { this.handleChange('city', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN Code  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="pin_code" defaultValue={mentorview.pinCode} onChange={(data) => { this.handleChange('pinCode', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group mt-3">
                                        <label for="" className="mb-0 font-weight-bold">Account Status  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <select name="status" defaultValue={mentorview.status} onChange={(data) => { this.handleChange('status', data.target.value) }} className="form-control bg-white">
                                            <option value="">--select--</option>
                                            <option value="Active">Active</option>
                                            <option value="Deactive">Deactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Country  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="country" defaultValue={mentorview.country} onChange={(data) => { this.handleChange('country', data.target.value) }} className="form-control bg-white" />
                                    </div>


                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email ID  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="email_i_d" defaultValue={mentorview.emailID} onChange={(data) => { this.handleChange('emailID', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                   
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="state" defaultValue={mentorview.state} onChange={(data) => { this.handleChange('state', data.target.value) }} className="form-control bg-white" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number  <span className="text-danger"><strong>*</strong></span> :</label>
                                        <input type="text" name="contact_no" defaultValue={mentorview.contactNo} onChange={(data) => { this.handleChange('contactNo', data.target.value) }} className="form-control bg-white" />
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="card-footer">
                        <div className="float-right">
                            <button onClick={() => { this.submit() }} className="btn btn-success"><i className="icon-check"></i> Update and Proceed</button>
                        </div>
                    </div>
                </div>
            </div>

        );


    }
}
export default editMentor
