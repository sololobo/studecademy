import React, { Component } from 'react';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';


class classActivity extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.sessionSearchList = []
    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, sessonList, unitList, sessonList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }

        return (
            <div className="fadeIn animated">
                {/*-main content*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Class Activity</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-toggle="modal" data-target="#addActivity">+ Add new activity</a></span>
                    </div>
                    <div className="card-body">

                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>
                                    <div className="row">


                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" className=" form-control" onChange={this.showSemesterSubject}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="" id="subject1" onChange={this.showASubject} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"><b>Select date:</b></label>



                                            <input type="date" name id className="form-control" />
                                        </div>
                                    </div>


                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>




                        <table className="table mt-2 table-bordered">
                            <thead>
                                <tr className="bg-light">
                                <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                   
                                    <th className="border-0">Name of the Activity</th>
                                    <th className="border-0">Subject</th>
                                    <th className="border-0">View</th>
                                    <th className="border-0">Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr> <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value="" /></td>
                                       
                                    <td className="border-0"><a href="#" data-target="#viewActivity" data-toggle="modal">Activity title</a></td>
                                    <td className="border-0">Physics</td>
                                    <td className="border-0">235</td>
                                    <td className="border-0">
                                        <span className="h4"><a href="#" className="text-danger font-weight-bold" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" /></a></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {/*/Main Content*/}



                {/*-modal for view activity*/}
                <div className="modal fade" id="viewActivity" tabIndex={-1} role="dialog" aria-labelledby="modelTitleviewActivity" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold">View Activity</span></h4>
                                    <hr />
                                    <fieldset>
                                        <label htmlFor className="mb-0 font-weight-bold">Published on:</label>
                                        <p>Date</p>
                                        <label htmlFor className="mb-0 font-weight-bold">Title of the Activity</label>
                                        <p>Title of activity</p>
                                        <label htmlFor className="font-weight-bold mb-0">Share Today's Class activity</label>
                                        <p>Today's Activity</p>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for view activity */}
                {/*-modal for add activity*/}
                <div className="modal fade" id="addActivity" tabIndex={-1} role="dialog" aria-labelledby="modelTitleaddActivity" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold">Add Activity</span></h4>
                                    <hr />
                                    <fieldset>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Session</label>
                                        <select name id className="form-control">
                                            <option value>Session</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Class/Department</label>
                                        <select name id className="form-control">
                                            <option value>Class/Department</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Section</label>
                                        <select name id className="form-control">
                                            <option value>Section</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Subject</label>
                                        <select name id className="form-control">
                                            <option value>Subject</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Date</label>
                                        <input type="date" className="form-control" />
                                        <label htmlFor className="mb-0 font-weight-bold">Title of the Activity</label>
                                        <input type="text" name id className="form-control" />
                                        <label htmlFor className="font-weight-bold mb-0">Share Today's Class activity</label>
                                        <textarea name id rows={3} className="form-control" defaultValue={""} />
                                    </fieldset>
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning  float-right">Add new activity</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for add activity */}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for delete */}


            </div>

        );


    }
}
export default classActivity