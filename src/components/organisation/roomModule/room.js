import React, { Component } from 'react';
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';

import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class room extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.sessionSearchList = []
    }
    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }

    //this function deletes the selected options.
    deleteSelectedOptions() {
        var toDelete = []
        let obj = $('.checkBoxForDeletion:checked')
        let len = obj.length
        Object.keys(obj).map(function (key, index) {
            if (index < len) {
                toDelete.push(obj[key].value)
            }
        })
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover these Session!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    try {
                        toDelete.forEach(function (value) {
                            let url = global.API + "/studapi/public/api/deletesession";
                            fetch(url, {
                                method: 'POST',
                                headers: {
                                    "Content-Type": "application/json",
                                    "Accept": "application/json"
                                },

                                body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'sess': value })
                            })
                        })
                        swal("All Selected Session Deleted :D", {
                            icon: "success",
                        });
                        this.componentDidMount();
                        $(".checkBoxForDeletion").prop('checked', false)
                    } catch (err) {
                        swal('Some Sessions cannot be deleted! :(', {
                            icon: "warning",
                        });
                    }
                }
            });
    }

    //this function toggles the state of all the options
    selectAllOptions() {
        if (!$('.checkBoxForDeletion:not(:checked)').length) {
            $(".checkBoxForDeletion").prop('checked', false)
        } else {
            $(".checkBoxForDeletion").prop('checked', true)
        }

    }

    // refresh Page
refreshPage(){
    this.componentDidMount();
 }
 
    render() {
        var { isLoaded, sessonList, unitList, sessonList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.sessionSearchList = SearchForObjectsWithParams(sessonList, this.state.searchParams)
        }
        else {
            this.sessionSearchList = sessonList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.sessionSearchList = SearchForObjectsWithName(this.sessionSearchList, this.state.searchString)
        }

        return (
            <div className="fadeIn animated">
                {/*-===Main Body===*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Room Allocation</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-target="#addRoom" data-toggle="modal"> + Add New Room</a></span>
                    </div>
                    <div className="card-body">

                        <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick={() => this.selectAllOptions()} >Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-purple float-left" onClick={() => this.refreshPage()}>Refresh</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>

                                            </select>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>



                        <table className="table mt-2 table-bordered">
                            <thead className="bg-light">
                                <tr>
                                    <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                    <th className="border-0">Unit</th>
                                    <th className="border-0">Room No</th>
                                    <th className="border-0">Allocation For</th>
                                    <th className="border-0">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className="border-0"><input type="checkbox" className="checkBoxForDeletion" value="" /></td>

                                    <td className="border-0">Unit 1</td>
                                    <td className="border-0">R403</td>
                                    <td className="border-0">Classroom</td>
                                    <td className="border-0">
                                        <span className="h4"><a href="#" className="text-warning font-weight-bold" data-toggle="modal" data-target="#editRoom"><i className="icon-pencil" /></a></span>
                                        <span className=" h4"><a href="#" className="text-danger font-weight-bold" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" /></a></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {/*===Main Body===*/}
                {/*========Room======*/}
                <div className="modal fade" id="addRoom" tabIndex={-1} role="dialog" aria-labelledby="modelTitleaddRoom" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold">Add New Room</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container ">


                                    <label htmlFor className="mb-0 font-weight-bold"> Select Unit</label>
                                    <select name id className="form-control">
                                        <option value>Unit 1</option>
                                        <option value>Unit 2</option>
                                    </select>
                                    <div className="unit-room">
                                        <table className="table table-border">
                                            <thead>
                                                <tr>
                                                    <th className="border-0">Room Number <span className="text-danger"><strong>*</strong></span>:</th>
                                                    <th className="border-0">Allocation for  <span className="text-danger"><strong>*</strong></span>:</th>
                                                    <th className="border-0" />
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="border-0">
                                                        <input type="text" className="form-control" />
                                                    </td>
                                                    <td className="border-0">
                                                        <input type="text" className="form-control" />
                                                    </td>
                                                    <td className="border-0">
                                                        <span className=""><i className="icon-minus text-danger " /></span>&nbsp;&nbsp;&nbsp;
                                                    <span className=""><i className="icon-plus" /></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>


                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for allocation of room */}
                {/*-modal for allocation of edit room*/}
                <div className="modal fade" id="editRoom" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditRoom" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h4><span className="font-weight-bold">Add New Room</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="container ">



                                    <div className="unit-room">
                                        <table className="table table-border">
                                            <thead>
                                                <tr>
                                                    <th className="border-0">Room Number <span className="text-danger"><strong>*</strong></span>:</th>
                                                    <th className="border-0">Allocation for  <span className="text-danger"><strong>*</strong></span>:</th>
                                                    <th className="border-0" />
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>

                                                    <td className="border-0">
                                                        <input type="text" className="form-control" />
                                                    </td>
                                                    <td className="border-0">
                                                        <input type="text" className="form-control" />
                                                    </td>
                                                    <td className="border-0">
                                                        <span className=""><i className="icon-minus text-danger " /></span>&nbsp;&nbsp;&nbsp;
                                                    <span className=""><i className="icon-plus" /></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>


                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" onClick={() => { this.submit() }} className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for allocation of edit  */}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        );


    }
}
export default room