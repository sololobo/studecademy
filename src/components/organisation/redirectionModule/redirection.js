import React, { Component } from 'react';
import Side from '../sidebarModule/side';
import Cookies from 'js-cookie';

import IdleTimer from 'react-idle-timer'
import Orgprofile from '../profileModule/orgProfile';
import swal from 'sweetalert'
class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.showProfile = this.showProfile.bind(this);
        this.onIdle = this.onIdle.bind(this);
    }
    onIdle(e) {
        console.log('user is idle', e)
        console.log('last active', this.idleTimer.getLastActiveTime())
        swal("You are idle for more than 15 minutes, Kindly login again")
.then((value) => {
    localStorage.clear();
    Cookies.remove('username', { path: '/' });
    Cookies.remove('usertype', { path: '/' });
    Cookies.remove('orgid', { path: '/' });
    window.location.href = '/';
    window.location = "/login";
});
      }
    showProfile() {

        document.getElementById("contain1").innerHTML = "true";

    }
    render() {

        return ([

            <div className="app-body" > <>
                <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={1000 * 60 * 15} />
        
                <Side></Side>
                <main class="main">


                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">Home</li>
                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>

                    <div class="container-fluid" id="contain1">
                        <Orgprofile></Orgprofile>

                    </div>

                </main>
    </>
            </div>
            
        ]);
        

    }
   
}
export default Dashboard;