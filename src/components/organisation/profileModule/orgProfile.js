import React, { Component } from 'react';
import './orgProfile.css';
import $ from 'jquery';
import Cookies from 'js-cookie';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import logo from '../../../media/features/books.png';
import swal from 'sweetalert';
class orgProfile extends Component {
    constructor(props) {
        super(props);
        this.changePass = this.changePass.bind(this);
        this.selectCountry=this.selectCountry.bind(this)
        this.selectRegion=this.selectRegion.bind(this)
        console.log(CountryRegionData);
        this.state = {
            organisation: [],
            country:"",
            state:""

        }

    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
    changePass() {
        //let email = document.getElementById('email1').innerHTML;
        let email = Cookies.get('username')
        let pwd = document.getElementById('newpass').value;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }

                            }));
                }

            });

    }
    componentDidMount() {
        setTimeout(function () { //Start the timer
            console.log("dgxf") //After 1 second, set render to true


            let url = global.API + "/studapi/public/api/viewaorgaprofile";
            console.log(url)
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },

                body: JSON.stringify({ 'orgID': Cookies.get('orgid') })
            })
                .then(res => res.json())
                .then(json => {
                    this.setState({
                        isLoaded: true,
                        organisation: json
                    
                    })
                    this.setState({country:json[0]["Country"]})
                    console.log(this.state.country)
                    this.setState({state:json[0]['State']})
                });


        }.bind(this), 1500)
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
       
      console.log(  document.getElementById("orgeditstate").value)
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
    submit(orga) {
        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            organisation_name: document.getElementById('orgeditname').value,
            tel_no: document.getElementById('orgedittelno').value,
            mob_no: document.getElementById('orgeditmobno').value,
            webs: document.getElementById('orgeditweb').value,
            // logo: document.getElementById('logo').value,
            // cover_photo: document.getElementById('coverPic').files[0],
            add1: document.getElementById('orgeditaddress1').value,
            add2: document.getElementById('orgeditaddress2').value,
            city: document.getElementById('orgeditcity').value,
            country: this.state.country,
            state:this.state.state,
            pin_code: document.getElementById('orgeditpin').value,
            contact_person_name: document.getElementById('orgeditcontactperson').value,
            contact_email: document.getElementById('orgeditcontactemail').value,
            contact_number: document.getElementById('orgeditcontactmobe').value,
            //  desig:document.getElementById('editBookSessionName').value
        }
        console.log(this.state);

        let url = global.API + "/studapi/public/api/editorgprofile";
        let data = this.state;
        // if (this.state.logo == null)
        //     this.state.logo = this.props.undata.logo;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        if (document.getElementById('logo').files.length == 0)
            //console.log(orga.Logo+"hi");
            fd.append('logo', orga.Logo);
        else
            fd.append('logo', document.getElementById('logo').files[0]);

        if (document.getElementById('cover_photo').files.length != 0)
            fd.append('cover_photo', document.getElementById('cover_photo').files[0]);


        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Profile Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.componentDidMount()

                }
                else {
                    swal({
                        title: "Error",
                        text: "Profile Not Updated",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
        console.log(data);
        
    }
    render() {

        var { isLoaded, organisation } = this.state;
        console.log(organisation);
      
     
        return (


            <div className="fadeIn animated">

                <span className="h3 font-weight-bold mb-3">Profile of Organisation:</span>
                {organisation.map(orga => (
                 
                    <div className="card mt-2">
                        <div className="card-header card-header1 h-100 ">
                           {orga.coverPhoto ? <img src={global.img + orga.coverPhoto} alt="" className="cover-photo-my-profile" id="cover-photo" /> : <img src={logo} alt="" className="cover-photo-my-profile" id="cover-photo" />}
                            {/*  <img src={logo} alt="profile picture" className="profile-photo-my-profile" id="profile-photo" />*/}
                            <img src={global.img + orga.Logo} alt="profile picture" className="profile-photo-my-profile" id="profile-photo" />
                        </div>
                        <div className="card-body">
                            <div className="pt-2 pb-2 pl-5 pr-5 border mb-5 mt-5">
                                <span className="border-title h3 font-weight-bold">Organisation Details:</span>

                                <div className="row mt-3">

                                    <div className="col-md-6">


                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2"> Name of The Organisation <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditname" defaultValue={orga.OrganisationName} className="form-control bg-white" disabled />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2"> Telephone Number:</label>
                                            <input type="text" name="" id="orgedittelno" defaultValue={orga.TelephoneNo} className="form-control bg-white" />
                                        </div>


                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Name of the Contact Person <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditcontactperson" defaultValue={orga.NameOfContactPerson} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Contact Email <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditcontactemail" defaultValue={orga.ContactEmail} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Address <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditaddress1" defaultValue={orga.Address1} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                            <CountryDropdown  defaultOptionLabel={this.state.country} name="" id="orgeditcuntry" value={this.state.country}  onChange={(data) => {  this.selectCountry(data)}}  className="form-control bg-white" />
                                                
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">City <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditcity" defaultValue={orga.City} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Subscription Active on:</label>
                                            <input type="text" name="" id="orgeditactiveted" placeholder="25-08-2019" disabled className="form-control bg-white" />
                                        </div>


                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Upload Cover Photo:</label>
                                            <input type="file" name="" id="cover_photo" className="form-control bg-white" onchange="readURL(this);" />
                                        </div>

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Organisation Website Address:</label>
                                            <input type="website" name="" id="orgeditweb" defaultValue={orga.Website} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2"> Mobile Number <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditmobno" defaultValue={orga.MobileNo} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Organisation Email Address <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="email" name="" id="orgeditmail" defaultValue={orga.emailID} disabled className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Contact person's Mobile Number <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditcontactmobe" defaultValue={orga.ContactNumber} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Address 2:</label>
                                            <input type="text" name="" id="orgeditaddress2" defaultValue={orga.Address2} className="form-control bg-white" />
                                        </div>

                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">State <span className="text-danger"><strong>*</strong></span>:</label>
                                            <RegionDropdown country={this.state.country} defaultOptionLabel={this.state.state}   name="" id="orgeditstate" value={this.state.state} onChange={(data) => { this.selectRegion(data) }}  className="form-control bg-white" />
                                        </div>

                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">PIN Code <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="text" name="" id="orgeditpin" defaultValue={orga.PinCode} className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Subscription Active Till:</label>
                                            <input type="text" name="" id="orgeditdismis" placeholder="25-09-2021" disabled className="form-control bg-white" />
                                        </div>
                                        <div className="form-group  ">
                                            <label for="" className="mb-2  font-weight-bold mt-2">Upload  Organisation Logo:</label>
                                            <input type="file" name="" id="logo" className="form-control bg-white" onchange="readURL(this);" />
                                        </div>


                                    </div>

                                </div>


                            </div>
                        </div>
                        <div className="card-footer">
                            <button type="submit" data-toggle="modal" data-target="#resetPassword" className="btn btn-success float-left text-white"><i className="icon-key"></i> Edit Password</button>

                            <button type="submit" onClick={() => { this.submit(orga) }} className="btn btn-primary float-right text-white">Update and Save</button>

                        </div>
                    </div>
                ))}
                {/*-modal for reset Password*/}
                <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Reset Password</span></h4>
                                    <hr />
                                    <div className="form-group">
                                        <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                                        <input type="password" className="form-control-sm" id="newpass" />
                                    </div>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" id="e" onClick={() => this.changePass()} className="btn btn-warning">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for reset Password */}

            </div>


        );




    }
}
export default orgProfile
