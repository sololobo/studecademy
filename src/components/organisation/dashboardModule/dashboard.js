import React, { Component } from 'react';
import $ from 'jquery';
import './dashboard.css';
import  './todo.css';

class Dashboard extends Component {

  render() {

    return (
      <div className="animated fadeIn">
        {/*--=====Dasboard header Graphs=======*/}
        {/*--Anlaytics*/}
        <div className="card-group mb-4">
          <div className="card">
            <div className="card-body">
              <div className="h1 text-muted text-right mb-4">
                <i className="icon-people" />
              </div>
              <div className="text-value">56 / ∞</div>
              <small className="text-muted text-uppercase font-weight-bold">Unit</small>
              <div className="progress progress-xs mt-3 mb-0">
                <div className="progress-bar bg-info" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
              </div>
            </div>
          </div>
          <div className="card ml-4">
            <div className="card-body">
              <div className="h1 text-muted text-right mb-4">
                <i className="icon-user-follow" />
              </div>
              <div className="text-value">385 / 500</div>
              <small className="text-muted text-uppercase font-weight-bold">Students</small>
              <div className="progress progress-xs mt-3 mb-0">
                <div className="progress-bar bg-success" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
              </div>
            </div>
          </div>
          <div className="card ml-4">
            <div className="card-body">
              <div className="h1 text-muted text-right mb-4">
                <i className="icon-basket-loaded" />
              </div>
              <div className="text-value">1238 / ∞</div>
              <small className="text-muted text-uppercase font-weight-bold">Mentors</small>
              <div className="progress progress-xs mt-3 mb-0">
                <div className="progress-bar bg-warning" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
              </div>
            </div>
          </div>
          <div className="card ml-4">
            <div className="card-body">
              <div className="h1 text-muted text-right mb-4">
                <i className="icon-speedometer" />
              </div>
              <div className="text-value">56GB / 1TB</div>
              <small className="text-muted text-uppercase font-weight-bold">Disk Usage</small>
              <div className="progress progress-xs mt-3 mb-0">
                <div className="progress-bar bg-danger" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
              </div>
            </div>
          </div>
        </div>
        {/*--/Analytics*/}
        <div className="row mt-4">
          <div className="col-6 col-lg-3">
            <div className="card ">
              <div className="card-body p-3 d-flex align-items-center">
                <i className="icon-docs bg-danger p-3 font-2xl mr-3" />
                <div>
                  <div className="text-value-sm text-danger">300</div>
                  <div className="text-muted text-uppercase font-weight-bold small">Total Test
              Uploaded</div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-6 col-lg-3">
            <div className="card">
              <div className="card-body p-3 d-flex align-items-center">
                <i className="icon-user bg-warning p-3 font-2xl mr-3" />
                <div>
                  <div className="text-value-sm text-warning">300</div>
                  <div className="text-muted text-uppercase font-weight-bold small">Current Students
              Online</div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-6 col-lg-3">
            <div className="card">
              <div className="card-body p-3 d-flex align-items-center">
                <i className="icon-bell bg-success text-white p-3 font-2xl mr-3" />
                <div>
                  <div className="text-value-sm text-success">150</div>
                  <div className="text-muted text-uppercase font-weight-bold small">Test Published
            </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-6 col-lg-3">
            <div className="card">
              <div className="card-body p-3 d-flex align-items-center">
                <i className="icon-docs text-white bg-primary p-3 font-2xl mr-3" />
                <div>
                  <div className="text-value-sm text-danger">200</div>
                  <div className="text-muted text-uppercase font-weight-bold small">Assignment
              Uploaded</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*--/row-*/}
        {/*- event section-*/}
        <div className="row">
          <div className="col-md-6 col-xs-12">
            {/*--notice--*/}
            <div className="card">
              <div className="card-header text-muted">
                <b>Notifications</b>
                <a href="#" className="float-right">+ Create Notice</a>
              </div>
              <div className="card-body scrollbar events-scroll">
                <div className="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>New Physics Assignement</strong> has updated. <a href="#"> View Now</a>
                  <button type="button btn btn-primary" className="close" data-dismiss="alert" aria-label="Close">
                    <i className="icon-close" />
                  </button>
                </div>
                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                  <strong>Holy guacamole!</strong> You should check in on some of those fields
            below.
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <i className="icon-close" />
                  </button>
                </div>
                <div className="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Holy guacamole!</strong> You should check in on some of those fields
            below.
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <i className="icon-close" />
                  </button>
                </div>
              </div>
            </div>
            {/*--/to do list--*/}
          </div>
          {/*--/col-*/}
          <div className="col-md-6 col-xs-12">
            {/*--to do list--*/}
            <div className="card">
              <div className="card-header text-muted">
                <b> To Do Lists</b>
              </div>
              <div className="card-body scrollbar events-scroll">
                <div className="add-items d-flex"> <input type="text" className="form-control todo-list-input" style={{ width: '75%' }} placeholder="What do you need to do today?" /> <button className="add btn btn-primary font-weight-bold todo-list-add-btn" style={{ width: '25%', height: '20%', borderRadius: 'none !important' }}>Add</button>
                </div>
                <div className="list-wrapper">
                  <ul className="d-flex flex-column-reverse todo-list">
                    <li>
                      <div className="form-check "> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> For what reason would it
                    be advisable. <i className="input-helper" /></label> <i className=" remove mdi mdi-close-circle-outline" /></div>
                    </li>
                    <li className="completed">
                      <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> For what reason
                    would it be advisable for me to think. <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                    </li>
                    <li>
                      <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> it be advisable for me to
                    think about business content? <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                    </li>
                    <li>
                      <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print Statements all <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                    </li>
                    <li className="completed">
                      <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> Call Rampbo <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                    </li>
                    <li>
                      <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print bills <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            {/*--/to do list--*/}
          </div>
          {/*--/col-*/}
        </div>
        {/*-/event section-*/}
      </div>

    );


  }
}
export default Dashboard