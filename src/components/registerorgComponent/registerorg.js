import React, { Component } from 'react';
 
import logo from '../includes/media/tm-logo.png';
import $ from 'jquery';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import swal from 'sweetalert';
class registerorg extends Component {
    constructor() {
        super();
        this.state = {

            orgName: "",
            telNo: "",
            mobNo: "",
            email: "",
            web: "",
            complogo: null,
            addr1: "",
            addr2: "",
            city: "",
            state: "",
            pin: "",
            country: "",
            contactPerName: "",
            conEmail: "",
            conNo: "",
            desig: "",
            ii:0
        }
    }
    selectCountry (val) {
        console.log(val);
        this.setState({ country: val });
      }
     
      selectRegion (val) {
        this.setState({ state: val });
      }
      submit() {
    
       var pin = document.getElementById("pin").value;
        var mobNo = document.getElementById("mobNo").value;
        var orgEmail = document.getElementById("orgEmail").value;
        var contactPerEmail = document.getElementById("contactPerEmail").value;
        var contactPerNo = document.getElementById("contactPerNo").value;
        var i = 0;
        var pinCheck = /^[0-9]{6}$/;
        var telCheck = /^[0-9]{10}$/;
        var emailCheck = /^[A-Za-z0-9_.]{3,20}@[A-Za-z0-9]{3,20}[.]{1}[A-Za-z0-9.]{2,6}$/;
      

        if (i < 11) {
            if (document.getElementById("orgName").value == "") {
                document.getElementById("orgNameError").innerHTML = "*Please fill this field";
                document.getElementById("orgName").focus();
                document.getElementById("orgName").style.borderColor = "#FF0000";
            }

            if (document.getElementById("orgName").value != "") {
                document.getElementById("orgNameError").innerHTML = "";
                i = i + 1;
                document.getElementById("orgName").style.borderColor = "";
            }
            if (!telCheck.test(mobNo)) {
                document.getElementById("mobNoError").innerHTML = "*Invalid Contact Number";
                document.getElementById("mobNo").focus();
                document.getElementById("mobNo").style.borderColor = "#FF0000";
            }

            if (telCheck.test(mobNo)) {
                document.getElementById("mobNoError").innerHTML = "";
                i = i + 1;
                document.getElementById("mobNo").style.borderColor = "";
            }
            if (!emailCheck.test(orgEmail)) {
                document.getElementById("orgEmailError").innerHTML = "*Invalid Email Id";
                document.getElementById("orgEmail").focus();
                document.getElementById("orgEmail").style.borderColor = "#FF0000";
            }

            if (emailCheck.test(orgEmail)) {
                document.getElementById("orgEmailError").innerHTML = "";
                i = i + 1;
                document.getElementById("orgEmail").style.borderColor = "";
            }
         
            if (!pinCheck.test(pin)) {
                document.getElementById("pinError").innerHTML = "*Invalid PIN";
                document.getElementById("pin").focus();
                document.getElementById("pin").style.borderColor = "#FF0000";
            }

            if (pinCheck.test(pin)) {
                document.getElementById("pinError").innerHTML = "";
                i = i + 1;
                document.getElementById("pin").style.borderColor = "";
            }
            if (!emailCheck.test(contactPerEmail)) {
                document.getElementById("contactPerEmailError").innerHTML = "*Invalid Email Id";
                document.getElementById("contactPerEmail").focus();
                document.getElementById("contactPerEmail").style.borderColor = "#FF0000";
            }

            if (emailCheck.test(contactPerEmail)) {
                document.getElementById("contactPerEmailError").innerHTML = "";
                i = i + 1;
                document.getElementById("contactPerEmail").style.borderColor = "";
            }

            if (!telCheck.test(contactPerNo)) {
                document.getElementById("contactPerNoError").innerHTML = "*Invalid contact Number";
                document.getElementById("contactPerNo").focus();
                document.getElementById("contactPerNo").style.borderColor = "#FF0000";
            }

            if (telCheck.test(contactPerNo)) {
                document.getElementById("contactPerNoError").innerHTML = "";
                i = i + 1;
                document.getElementById("contactPerNo").style.borderColor = "";
            }
            if (document.getElementById("contactPerson").value == "") {
                document.getElementById("contactPersonError").innerHTML = "*Please fill this field";
                document.getElementById("contactPerson").focus();
                document.getElementById("contactPerson").style.borderColor = "#FF0000";
            }

            if (document.getElementById("contactPerson").value != "") {
                document.getElementById("contactPersonError").innerHTML = "";
                i = i + 1;
                document.getElementById("contactPerson").style.borderColor = "";
            }
            if (document.getElementById("mycountry").value == "") {
                document.getElementById("countryError").innerHTML = "*Please fill this field";
                document.getElementById("mycountry").focus();
                document.getElementById("mycountry").style.borderColor = "#FF0000";
            }

            if (document.getElementById("mycountry").value != "") {
                document.getElementById("countryError").innerHTML = "";
                i = i + 1;
                document.getElementById("mycountry").style.borderColor = "";
            }
            if (document.getElementById("state").value == "") {
                document.getElementById("stateError").innerHTML = "*Please fill this field";
                document.getElementById("state").focus();
                document.getElementById("state").style.borderColor = "#FF0000";
            }

            if (document.getElementById("state").value != "") {
                document.getElementById("stateError").innerHTML = "";
                i = i + 1;
                document.getElementById("state").style.borderColor = "";
            }
            if (document.getElementById("city").value == "") {
                document.getElementById("cityError").innerHTML = "*Please fill this field";
                document.getElementById("city").focus();
                document.getElementById("city").style.borderColor = "#FF0000";
            }

            if (document.getElementById("city").value != "") {
                document.getElementById("cityError").innerHTML = "";
                i = i + 1;
                document.getElementById("city").style.borderColor = "";
            }
            if (document.getElementById("address1").value == "") {
                document.getElementById("addressError").innerHTML = "*Please fill this field";
                document.getElementById("address1").focus();
                document.getElementById("address1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("address1").value != "") {
                document.getElementById("addressError").innerHTML = "";
                i = i + 1;
                document.getElementById("address1").style.borderColor = "";
            }
            if(i==11){
                console.log(i);
                this.state.ii=11;
                }
            }
    if(this.state.ii==11){
            console.log(this.state);
        let url = window.API + "/studapi/public/api/registerorg";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('complogo', document.getElementById("company_logo").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
          if (resp[0]['regResult'] == 1){
                    swal({
                    title: "Done!",
                    text: "Information was saved successfully, Please access your given email id for activation link",
                    icon: "success",
                    button: "OK",

                });
            }
            else if (resp[0]['regResult'] == 2) {
                swal({
                    title: "Error!",
                    text: "Email is already registerd! ",
                    icon: "warning",
                    button: "OK",
                });
            }
            else  {
                swal({
                    title: "Error!",
                    text: "Not registor ,please ity again! ",
                    icon: "warning",
                    button: "OK",
                });
            }
        })
    })
    console.log(data);


}}



    render() {
        return (
            <div className="container-fluid ">
                <div className="row h-100 align-items-center">
                    <div className=" col-md-3"></div>
                    <div className="col-md-6 px-5 py-5 bg-white registration">
                        <img src={logo} alt="" className="registration-logo" />
                        <h2 className="font-weight-bold">REGISTER as an organization</h2>
                        <hr />
                        <div className="form-group">
                            <label for="" className="mb-0 font-weight-bold">Name of Orgnization  <span className="text-danger"><strong>*</strong></span>:</label>
                             <input type="text" id="orgName" name="orgName" value={this.state.orgName} onChange={(data) => { this.setState({ orgName: data.target.value }) }} className="form-control" />
                             <span id="orgNameError" class="text-danger font-weight-bold"></span>
                                   
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                               
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Mobile Number <span className="text-danger"><strong>*</strong></span></label>
                                     <input type="number" id="mobNo" name="mobNo" value={this.state.mobNo} onChange={(data) => { this.setState({ mobNo: data.target.value }) }} className="form-control" />
                                     <span id="mobNoError" class="text-danger font-weight-bold"></span>
                          
                                </div>
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Telephone number :</label>
                                     <input type="number" name="telNo" value={this.state.telNo} onChange={(data) => { this.setState({ telNo: data.target.value }) }} className="form-control" />
                                  
                                </div>
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Organization Email ID <span className="text-danger"><strong>*</strong></span>:</label>
                                     <input type="email" id="orgEmail" name="email" value={this.state.email} onChange={(data) => { this.setState({ email: data.target.value }) }} className="form-control" />
                                     <span id="orgEmailError" class="text-danger font-weight-bold"></span>
                          
                                </div>
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Website of the Organization:</label>
                                     <input type="url" id="website" name="web" value={this.state.web} onChange={(data) => { this.setState({ web: data.target.value }) }} className="form-control" />
                                     <span id="websiteError" class="text-danger font-weight-bold"></span>
                                   
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group mb-1">
                                    <label for="" className="mb-0 font-weight-bold">Upload Profile Picture:</label> <br />
                                    <div className="form-group files">
                                         <input type="file" id="company_logo" className="form-control" multiple="" />
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">

                                    <label for="" className="mb-0 font-weight-bold">Address1 <span className="text-danger"><strong>*</strong></span>:</label>
                                    <input type="text" id="address1" name="addr1" value={this.state.addr1} onChange={(data) => { this.setState({ addr1: data.target.value }) }} className="form-control" />
                                    <span id="addressError" class="text-danger font-weight-bold"></span>
                                     
                                   
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Address 2:</label>

                                    <input type="text" name="addr2" value={this.state.addr2} onChange={(data) => { this.setState({ addr2: data.target.value }) }} className="form-control" />

                             
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">City <span className="text-danger"><strong>*</strong></span>:</label>
                                     <input type="text" id="city"name="city" value={this.state.city} onChange={(data) => { this.setState({ city: data.target.value }) }} className="form-control" />
                                     <span id="cityError" class="text-danger font-weight-bold"></span>
                                  
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">PIN Code <span className="text-danger"><strong>*</strong></span>:</label>
                                     <input type="number" id="pin" name="pin" value={this.state.pin} onChange={(data) => { this.setState({ pin: data.target.value }) }} className="form-control" />
                                     <span id="pinError" class="text-danger font-weight-bold"></span>
                                   
                                </div>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <label for="" className="mb-0 font-weight-bold">Country <span className="text-danger"><strong>*</strong></span>:</label>
                                    <CountryDropdown id="mycountry" value={this.state.country} onChange={(data) => {  this.selectCountry(data)}} className="form-control  bg-white" />
                                    <span id="countryError" class="text-danger font-weight-bold"></span>
                                     
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="form-group">
                                <label for="" className="mb-0 font-weight-bold">State <span className="text-danger"><strong>*</strong></span>:</label>
                                <RegionDropdown country={this.state.country} id="state"  value={this.state.state} onChange={(data) => { this.selectRegion(data) }} className="form-control  bg-white" />
                                <span id="stateError" class="text-danger font-weight-bold"></span>
                                        
                                </div>
                            </div>
                        </div>
                        

                        <hr />
                        <div className="form-group">
                            <label for="" className="mb-0 font-weight-bold">Name of Contact Person <span className="text-danger"><strong>*</strong></span>:</label>
                             <input type="text" id="contactPerson" name="contactPerName" value={this.state.contactPerName} onChange={(data) => { this.setState({ contactPerName: data.target.value }) }} className="form-control" />
                             <span id="contactPersonError" class="text-danger font-weight-bold"></span>
                                                     
                        </div>
                        <div className="form-group">
                            <label for="" className="mb-0 font-weight-bold">Contact Email <span className="text-danger"><strong>*</strong></span>:</label>
                             <input type="email" id="contactPerEmail" name="conEmail" value={this.state.conEmail} onChange={(data) => { this.setState({ conEmail: data.target.value }) }} className="form-control" />
                             <span id="contactPerEmailError" class="text-danger font-weight-bold"></span>
                                         
                        </div>
                        <div className="form-group">
                            <label for="" className="mb-0 font-weight-bold">Contact Number <span className="text-danger"><strong>*</strong></span>:</label>
                             <input type="number" id="contactPerNo" name="conNo" value={this.state.conNo} onChange={(data) => { this.setState({ conNo: data.target.value }) }} className="form-control" />
                             <span id="contactPerNoError" class="text-danger font-weight-bold"></span>
                                             
                        </div>
                        <div className="form-group">
                            <label for="" className="mb-0 font-weight-bold">Designation :</label>
                             <input type="text" id="desig" name="desig" value={this.state.desig} onChange={(data) => { this.setState({ desig: data.target.value }) }} className="form-control" />
                             <span id="desigError" class="text-danger font-weight-bold"></span>
                                         
                        </div>
                        {/* <input type="checkbox" /> I Agree to the <a href="#"> Terms &amp; Conditions</a>*/}
                         <br />
                        <button  type="submit" onClick={() => { this.submit() }} className="btn btn-primary text-white form-control mb-2"> Create Account</button>

                        <a href="/" className="text-center mt-3 float-right"><i className="icon-action-undo"></i> Back to home page</a>
                    </div>
                    <div className="col-md-3"></div>
                </div>
            </div>
            



        );
    }
}
export default registerorg