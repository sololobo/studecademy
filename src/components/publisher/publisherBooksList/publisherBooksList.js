import React, { Component } from 'react';
import './publisherBooksList.css';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import Bookview from '../publisherBooksView/publisherBooksView'



class publisherBooksList extends Component {
    constructor(props) {
        super(props);
        this.bookViews = this.bookViews.bind(this);

        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        this.showSectionSubjectAdd = this.showSectionSubjectAdd.bind(this);
        this.showSectionList = this.showSectionList.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        this.showSemesterSubject = this.showSemesterSubject.bind(this);
        this.showAllBooks = this.showAllBooks.bind(this);

        this.showBook = this.showBook.bind(this);
        this.deleteBook = this.deleteBook.bind(this)


        this.state = {
            subject_i_d: 0,
            book_name: "",

            book_author_name: "",
            book_publication: "",
            bookList: [],



            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            subjectList1: [],
            editedBookID: 0

        }
    }
    deleteBook(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  this Book!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deletebook";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'book_i_d': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Book Deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    bookViews(id2) {

        ReactDOM.render(<Bookview bookid={id2} />, document.getElementById('contain1'));
    }
    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }

    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }

    showSession(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }


    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }

    showSemester(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    //Section

    showSectionList() {
        let val = document.getElementById("semester1").value;

        this.showSection(val);
    }

    showSectionSubjectAdd() {

        let val = document.getElementById("semester2").value;

        this.showSection(val);
        this.showSubject(val);

    }

    showSection(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }

    //Subject



    showSubject(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            });
    }

    showSemesterSubject() {

        let val = document.getElementById("semester1").value
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList1: json
                })
            });

    }
    showAllBooks() {

        let val = document.getElementById("subject1").value
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'subjectID': val })

        };


        let sessionURL = global.API + "/studapi/public/api/viewallbooks";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            });

    }



    submit() {
        console.log(this.state);
        let url = global.API + "/studapi/public/api/addbooks";
        let data = this.state;

        const fd = new FormData();
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        // console.log(document.getElementById("bookcp").files[0]);
        fd.append('book_cover_photo', document.getElementById("bookcp").files[0]);
        fd.append('book_u_r_', document.getElementById("bookurl").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Book Added Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.componentDidMount();
                }
                else {
                    swal({
                        title: "Done!",
                        text: "Book not Added ",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
    }

    showBook(book) {

        ReactDOM.render(<Bookview bookdata={book} />, document.getElementById('contain1'))
    }



    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallorgabooks";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            })
            .then(() => {
                let count = Object.keys(this.state.bookList).length;
                if (count == 0) {
                    swal({
                        title: "Oops!",
                        text: "Nothing to show!! ",
                        icon: "info",
                        button: "OK",
                    });
                }
            });



        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });



    }

    editBookModalValues(obj) {
        this.setState({ editedBookID: obj.book.bookID });
        document.getElementById("editBookUnitName").value = obj.book.unitID;
        this.showSession(obj.book.unitID);
        this.showDepartment(obj.book.sessionID);
        this.showSemester(obj.book.departmentID);
        this.showSection(obj.book.semesterID);
        this.showSubject(obj.book.semesterID);
        document.getElementById('editBookSessionName').value = obj.book.sessionID;
        document.getElementById('editBookClassName').value = obj.book.departmentID;
        document.getElementById('editBookSemesterName').value = obj.book.semesterID;
        document.getElementById('editBookSubjectName').value = obj.book.subjectID;
        document.getElementById('editBookTitleName').value = obj.book.bookName;
        document.getElementById('editBookAuthorName').value = obj.book.bookAuthorName;
        document.getElementById('editBookPublishersName').value = obj.book.bookPublication;
        // document.getElementById('editBookCoverPhoto').value = obj.book.bookCoverPhoto;
        // document.getElementById('editBookURL').value = obj.book.bookURL;
    }
    onEditBookFormSubmit() {
        let url = global.API + "/studapi/public/api/updatebooks";
        let subID;
        try {
            subID = document.getElementById("editBookSubjectName").value;
        }
        catch{
            subID = "";
        }
        const fd = new FormData();
        fd.append('book_i_d', this.state.editedBookID);
        fd.append('subject_i_d', subID);
        fd.append('book_name', document.getElementById('editBookTitleName').value);
        fd.append('book_cover_photo', document.getElementById('editBookCoverPhoto').files[0]);
        fd.append('book_author_name', document.getElementById('editBookAuthorName').value);
        fd.append('book_publication', document.getElementById('editBookPublishersName').value);
        fd.append('book_u_r_l', document.getElementById("editBookURL").files[0]);
        fd.append('book_editor_id', Cookies.get('orgid'));
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.log(resp);
                try {
                    if (resp[0].result == 1) {
                        swal("Book Updated!", {
                            icon: "success",
                        });
                        $('#editBook').modal("hide");
                        this.componentDidMount();
                    }
                    else {
                        swal("There's something wrong!! :(", {
                            icon: "error",
                        });
                    }
                }
                catch (err) {
                    swal("One of the important fields is missing :(", {
                        icon: "error",
                    });
                }
            })
        })
    }
    render() {

        var { isLoaded, bookList, unitList, semesterList, sessonList1, sectionList, classList, subjectList, subjectList1 } = this.state;
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold"> List of Books</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-target="#addBook" data-toggle="modal">+ Add New book</a></span>
                    </div>
                    <div className="card-body">
                        <div>

                            <div className="">
                                <div className="form-group">
                                    <b>Unit:</b>
                                    <select name="" id="unit1" className="ml-1 mr-2 form-control-sm" onChange={this.showSessionList}>
                                        <option value="" selected>Select</option>
                                        {unitList.map(unit => (
                                            <option value={unit.unitID}>{unit.unitName}</option>

                                        ))}
                                    </select>
                                    <b>Session:</b>
                                    <select name="" id="session1" className="ml-1 mr-2 form-control-sm" onChange={this.showDepartmentList}>
                                        <option value="" selected>Select</option>
                                        {sessonList1.map(session => (
                                            <option value={session.sessionID}>{session.session}</option>

                                        ))}
                                    </select>
                                    <b>Class/Department:</b>
                                    <select name="" id="department1" onChange={this.showSemesterList} className="ml-1 mr-2 form-control-sm">
                                        <option value="" selected>Select</option>
                                        {classList.map(classes => (
                                            <option value={classes.departmentID}>{classes.departmentName}</option>

                                        ))}
                                    </select>
                                    <b>Semester:</b>
                                    <select name="" id="semester1" className="ml-1 mr-2 form-control-sm" onChange={this.showSemesterSubject}>
                                        <option value="" selected>Select</option>
                                        {semesterList.map(semester => (
                                            <option value={semester.semesterID}>{semester.semesterName}</option>

                                        ))}
                                    </select>
                                    <b>Subject:</b>
                                    <select name="" id="subject1" onChange={this.showAllBooks} className="ml-1 mr-2 form-control-sm">
                                        <option value="" selected>Select</option>
                                        {subjectList1.map(subject => (
                                            <option value={subject.subjectID}>{subject.subjectName}</option>

                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className="table-responsive">
                                <table className="table table-bordered ">
                                    <thead>
                                        <tr className="bg-light table-head-fixed">
                                            <th className="border-right-0">Preview</th>
                                            <th className="border-right-0 border-left-0">Name of the Book</th>
                                            <th className="border-right-0 border-left-0">Author of the Book</th>
                                            <th className="border-right-0 border-left-0">Publisher</th>
                                            <th className="border-right-0 border-left-0">Views</th>
                                            <th className="border-right-0 border-left-0">Online user</th>
                                            <th className="border-left-0">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {bookList.map(book => (
                                            <tr>
                                                <td className="border-right-0 border-top-0">
                                                    <img src={global.img + book.bookCoverPhoto} alt="" className="book_preview" />
                                                </td>
                                                <td className="border-0">{book.bookName}</td>
                                                <td className="border-0">{book.bookAuthorName}</td>
                                                <td className="border-0">{book.bookPublication}</td>
                                                <td className="border-0">{book.bookViews}</td>
                                                <td className="border-0">{book.bookOnlineUser}</td>
                                                <td className=" border-left-0 border-top-0 border-bottom-0">
                                                    <span><a onClick={() => this.showBook(book)} className="text-primary font-weight-bold h4"><i className="icon-eye" title="preview Book" data-placement="top"></i> </a></span>

                                                    <span><a href="#" data-toggle="modal" onClick={() => this.editBookModalValues({ book })} data-target="#editBook" className="text-warning font-weight-bold h4"><i className="icon-pencil" title="Edit Book" data-placement="top"></i> </a></span>

                                                    <span><a onClick={() => this.deleteBook(book.bookID)} className="text-danger font-weight-bold h4" ><i className="icon-trash" title="Delete Book" data-placement="top"></i> </a></span>
                                                </td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {/*-add subject book*/}
                {/*-modal for add new book*/}
                <div className="modal fade" id="addBook" tabIndex={-1} role="dialog" aria-labelledby="modelTitleaddBook" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold text-center">Add New Book</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit :</label>
                                                <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester:</label>
                                                <select name="semester2" id="semester2" onChange={this.showSectionSubjectAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session:</label>
                                                <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>


                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">className/Department:</label>
                                                <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject</label>
                                                <select name="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.setState({ subject_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Publisers Name</label>
                                                <input type="text" name="book_publication" value={this.state.book_publication} onChange={(data) => { this.setState({ book_publication: data.target.value }) }} className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Author Name</label>
                                                <input type="text" name="book_author_name" value={this.state.book_author_name} onChange={(data) => { this.setState({ book_author_name: data.target.value }) }} className="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">

                                        <div className="col-md-12">

                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Title of this Book</label>
                                                <input type="text" name="book_name" value={this.state.book_name} onChange={(data) => { this.setState({ book_name: data.target.value }) }} className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload Cover Photo of the Book</label>
                                                <input type="file" name="book_cover_photo" id="bookcp" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload the book</label>
                                                <input type="file" name="book_u_r_" id="bookurl" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => { this.submit() }} className="btn btn-success float-right ">Save and Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for add new book */}
                {/*-modal for editnew book*/}
                <div className="modal fade" id="editBook" tabIndex={-1} role="dialog" aria-labelledby="modelTitleeditBook" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold text-center">Edit New Book</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit :</label>
                                                <select name="" id="editBookUnitName" className="form-control" onChange={(e) => this.showSession(e.target.value)}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester:</label>
                                                <select id="editBookSemesterName" className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session:</label>
                                                <select id="editBookSessionName" className="form-control" onChange={(e) => this.showDepartment(e.target.value)}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>


                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">className/Department:</label>
                                                <select id="editBookClassName" onChange={(e) => this.showSemester(e.target.value)} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject</label>
                                                <select id="editBookSubjectName" className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Publisers Name</label>
                                                <input type="text" id="editBookPublishersName" name="book_publication" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Author Name</label>
                                                <input type="text" id="editBookAuthorName" name="book_author_name" className="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">

                                        <div className="col-md-12">

                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Title of this Book</label>
                                                <input type="text" id="editBookTitleName" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload Cover Photo of the Book</label>
                                                <input type="file" id="editBookCoverPhoto" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label htmlFor className="mb-0 font-weight-bold">Upload the book</label>
                                                <input type="file" id="editBookURL" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" onClick={() => this.onEditBookFormSubmit()} className="btn btn-success float-right ">Save and Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for editnew book */}

            </div>


        );


    }
}
export default publisherBooksList
