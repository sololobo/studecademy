import React, { Component } from 'react'
import teacher from '../../media/teacher_PNG84.png';
import arnab from '../../media/about/arnab.jpg';
import dhruba from '../../media/about/dhruba.jpg';
import somnath from '../../media/about/somnath.jpg';
import ranjan from '../../media/about/ranjan-majumder.jpg';
import pdf from '../../media/features/pdf.png';
import books from '../../media/features/books.png';
import liveclass from '../../media/features/liveclass.png';
import examportal from '../../media/features/013-trophy-1.png';
import noticeboard from '../../media/features/024-classroom-1.png';
import myforum from '../../media/features/noticeboard.png';
import aws from '../../media/technology/aws.png';
import abootstrap from '../../media/technology/bootstrap.png';
import alaravel from '../../media/technology/Laravel-logo.png';
import sql from '../../media/technology/sql.png';
import myreact from '../../media/technology/react.png';
import "../../media/App.css";
import "../../media/main.css";
class indexcomponent extends Component {
  render() {

    return (
      <div className="App">
        <div>
          {/*Navbar*/}
          <section>
            <nav className="navbar navbar-expand-lg bg-dark navbar-dark ">
              <a href="#" className="navbar-brand font-weight-bold">STUDECADEMY<sup>TM</sup></a>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
              </button>
              <div className="collapse navbar-collapse" id="navbarText">
                <ul className="navbar-nav ml-auto text-white">
                  <li className="nav-item">
                    <a href="#home" className="nav-link text-white">Home</a>
                  </li>
                  <li className="nav-item">
                    <a href="#features" className="nav-link text-white">Features</a>
                  </li>
                  <li className="nav-item">
                    <a href="#about" className="nav-link text-white">About us</a>
                  </li>
                  <li className="nav-item">
                    <a href="#contact" className="nav-link text-white">Contact us</a>
                  </li>
                  <li className="nav-item">
                    <a href="/login" className="nav-link text-white">Login</a>
                  </li>
                  <li className="nav-item">
                    <a href="/register" className="nav-link text-white">Registration</a>
                  </li>
                  <li className="nav-item">
                    <a href="/registerorg" className="nav-link text-white btn btn-primary">Registration as an organization</a>
                  </li>
                </ul>
              </div>
            </nav>
          </section>
          {/*Navbar ends*/}
          {/*hero content*/}
          <section id="home">
            <div className="jumbotron jumbotron-fluid">
              <div className="container">
                <div className="row mt-5">
                  <div className="col-md-7 mt-5">
                    {/* <h1 class="font-weight-bold text-center"><span class="header-welcome">Welcome to </span>  Studecademy<sup>TM</sup></h1>
                        <hr \>*/}
                    <h2 className="text-center text-uppercase header-title">A way to Unlock your online teaching
                    services</h2>
                    <h1 className="text-center text-primary font-weight-bold text-warning mb-0"><a href="#" data-target="#calltoaction" data-toggle="modal" className="text-white btn btn-lg bg-dark">Get a 7-Day Free Trial Now</a></h1>
                  </div>
                  <div className="col-md-5 d-block">
                    <img src={teacher} alt="Studecademy teacher" className="header-image" />
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/*hero content ends*/}
          {/*-features*/}
          <section id="features">
            <div className="jumbotron jumbotron-fluid bg-white mb-0">
              <div className="container mt-2 text-center">
                {/*<img src="media/features/008-podium.png" alt="" class="home-features">*/}
                <h2 className="font-weight-bold text-center text-uppercase mb-4">Our Features</h2>
                <hr />
                <div className="row mt-4 pt-4">
                  <div className="col-md-4 text-center features">
                    <img src={pdf} alt className="home-features" />
                    <h2 className="text-wrap">Homework / Assignment</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> Unlimited Homework/Assignments <br />
                      <i className="fa fa-check-circle h6 text-success" /> Scheduled Homework/Assignments <br />
                      <i className="fa fa-check-circle h6 text-success" /> Notification for submission <br />
                    </p>
                  </div>
                  <div className="col-md-4 text-center features">
                    <img src={books} alt className="home-features" />
                    <h2 className="text-wrap">Books</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> List of Books in PDF Format <br />
                      <i className="fa fa-check-circle h6 text-success" /> Unlimited Access of Books <br />
                      <i className="fa fa-check-circle h6 text-success" /> Anytime Accessible <br />
                    </p>
                  </div>
                  <div className="col-md-4 text-center features">
                    <img src={liveclass} alt className="home-features" />
                    <h2 className="text-wrap">Live Class</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> Set Meeting for your Students <br />
                      <i className="fa fa-check-circle h6 text-success" /> Whiteboard Accesiblity <br />
                      <i className="fa fa-check-circle h6 text-success" /> Screen Sharing <br />
                    </p>
                  </div>
                </div>
                <div className="row mt-4 ">
                  <div className="col-md-4 text-center features">
                    <img src={examportal} alt className="home-features" />
                    <h2 className="text-wrap">Exam Portal</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> Unlimited Mock Tests <br />
                      <i className="fa fa-check-circle h6 text-success" /> Scheduled Mock Tests / Examinations <br />
                      <i className="fa fa-check-circle h6 text-success" /> Analytics of Individuals <br />
                    </p>
                  </div>
                  <div className="col-md-4 text-center features">
                    <img src={noticeboard} alt className="home-features" />
                    <h2 className="text-wrap">Noticeboard</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> Interact with Students/Teachers <br />
                      <i className="fa fa-check-circle h6 text-success" /> Keep Tracking of Notice <br />
                      <i className="fa fa-check-circle h6 text-success" /> Send Notices between organization members
                    <br />
                    </p>
                  </div>
                  <div className="col-md-4 text-center features">
                    <img src={myforum} alt className="home-features" />
                    <h2 className="text-wrap">Forum</h2>
                    <hr />
                    <p>
                      <i className="fa fa-check-circle h6 text-success" /> Forum for Students' interaction<br />
                      <i className="fa fa-check-circle h6 text-success" /> Share their innovative things <br />
                      <i className="fa fa-check-circle h6 text-success" /> Appreciate your students <br />
                    </p>
                  </div>
                </div>
                <h6 className="font-weight-bold text-center mt-4">++ More</h6>
              </div>
            </div>
          </section>
          {/*Features ends*/}
          {/*about us*/}
          <section id="about">
            <div className="jumbotron jumbotron-fluid bg-dark text-white mb-0">
              <div className="container ">
                {/* <h2 class="text-center font-weight-bold">ABOUT US</h2>
            <hr \>
            <p class="text-center">
                Studecademy<sup>TM</sup> provides a Unique and Better solution to digitize your Organization/Training Institute or Small Organization. We Provide a complete solution to improve your academics Learning System for your organization.

                Studecademy<sup>TM</sup> gives an AI Based Solution to provide a Real-time result and Management system for your organization. Studecademy allows to build a strong relations between teachers and students. An Education can be visualize through our Portal.
            </p>*/}
                {/*-team*/}
                <h2 className="text-center font-weight-bold mt-4">MEET OUR TEAM</h2>
                <hr />
                <div className="row">
                  <div className="col-md-3">
                    <div className="card">
                      <div className="card-body text-dark text-center">
                        <img src={somnath} alt="team-member" className="team-photo" />
                        <p className="mb-0">
                          Somnath Sahu <br />
                          <span className="small">Co-Founder</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="card">
                      <div className="card-body text-dark text-center">
                        <img src={dhruba} alt="team-member" className="team-photo" />
                        <p className="mb-0">
                          Dhurbajyoti Sinharay <br />
                          <span className="small">Co-Founder</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="card">
                      <div className="card-body text-dark text-center">
                        <img src={arnab} alt="team-member" className="team-photo" />
                        <p className="mb-0">
                          Arnab Bhaumik <br />
                          <span className="small">Co-Founder</span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-3">
                    <div className="card">
                      <div className="card-body text-dark text-center">
                        <img src={ranjan} alt="team-member" className="team-photo" />
                        <p className="mb-0">
                          Ranjan Majumder <br />
                          <span className="small">Managing Partner</span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/*about us*/}
          {/*Technologies*/}
          <section id="technology">
            <div className="jumbotron jumbotron-fluid mb-0 bg-white">
              <div className="container">
                <h2 className="text-center font-weight-bold text-uppercase">Technology we use</h2>
                <div className="text-center mt-4">
                  <img src={myreact} alt className="technology-img" />
                  <img src={abootstrap} alt className="technology-img" />
                  <img src={alaravel} alt className="technology-img" />
                  <img src={sql} alt className="technology-img" />
                  <img src={aws} alt className="technology-img" />
                </div>
              </div>
            </div>
          </section>
          {/*Technologies ends*/}
          {/*-call to action*/}
          <section id="action">
            <div className="jumbotron jumbotron-fluid bg-warning text-dark mb-0">
              <div className="container">
                <h1 className="text-center text-primary font-weight-bold text-warning mb-0"><a href="#" data-target="#calltoaction" data-toggle="modal" className="text-white btn btn-lg bg-dark">Get a
                  7-Day Free Trial Now</a></h1>
              </div>
            </div></section>
          {/*/Call to action*/}
          {/*--contact*/}
          <section id="contact">
            <div className="jumbotron jumbotron-fluid bg-white text-dark mb-0">
              <div className="container">
                <div className="row">
                  <div className="col-md-6">
                    <h2 className="font-weight-bold">Contact us</h2>
                    <hr />
                    <p className="pt-4">
                      <b>Mail us:</b> <br />
                      <i className="fa fa-envelope" /> <a href="mailto:connect@eprojectbox.com" className="text-dark">connect@eprojectbox.com</a> <br />
                      <span className="mt-5"> <br /></span>
                      <b>Call us:</b> <br />
                      <i className="fa fa-phone" /> +91-8967867187 <br />
                      <i className="fa fa-phone" /> +91-7890225175 <br />
                      <i className="fa fa-phone" /> +91-9830527395 <br />
                    </p>
                  </div>
                  <div className="col-md-6 pt-4">
                    <form action="contact.php" method="post">
                      <div className="row pt-4">
                        <div className="col-md-6 pt-4">
                          <label htmlFor className="font-weight-bold mb-0">Name:</label>
                          <input type="text" required name="name" id="name" className="form-control" />
                        </div>
                        <div className="col-md-6 pt-4">
                          <label htmlFor className="font-weight-bold mb-0">Email:</label>
                          <input type="email" required id="email" name="email" className="form-control" />
                        </div>
                      </div>
                      <label htmlFor className="font-weight-bold mb-0 mt-2">Write your Message:</label>
                      <textarea name="message" id="message" rows={3} className="form-control" required defaultValue={""} />
                      <button type="submit" className="btn form-control bg-dark text-white mt-2">Submit</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/*--/contact*/}
          {/*footer*/}
          <section id="footer">
            <div className="jumbotron jumbotron-fluid bg-dark text-white mb-0">
              <div className="container text-center">
                <div className="row">
                  <div className="col-md-4 text-left">
                    <h5 className="font-weight-bold">About Us</h5>
                    <p>
                      Studecademy<sup>TM</sup> gives an AI Based Solution to provide a Real-time result and
                    Management system for your organization. Studecademy allows to build a strong relations
                    between teachers and students. An Education can be visualize through our Portal.
                  </p>
                  </div>
                  <div className="col-md-4 text-left">
                    <h5 className="font-weight-bold">Important Link</h5>
                    <p>
                      <a href="#" className="text-warning">Terms &amp; Condition</a> <br />
                      <a href="#" className="text-warning">Privacy Policy</a> <br />
                      <a href="#" className="text-warning">Refund Policy</a> <br />
                    </p>
                  </div>
                  <div className="col-md-4 text-left">
                    <h5 className="font-weight-bold">Follow us</h5>
                    <p>
                      <a href="#" className="text-white"><i className="fa fa-facebook-square h3 mr-3" /></a>
                      <a href="#" className="text-white"><i className="fa fa-youtube-square h3 mr-3" /></a>
                      <a href="#" className="text-white"><i className="fa fa-twitter-square h3 mr-3" /></a>
                    </p>
                  </div>
                </div>
                <hr />
                <span className="small">© 2020 All Rights Reserved Studecademy<sup>TM</sup> | Powered By <a href="https://eprojectbox.com" target="_blank" className="text-white">Eprojectbox Lab LLP</a></span>
              </div>
            </div>
          </section>
          {/*/footer*/}
          {/*-script*/}
          {/* Modal */}
          <div className="modal fade" id="calltoaction" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title font-weight-bold">Request for a Demo</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">
                  <div className="container-fluid">
                    <form action="demo.php" method="POST">
                      <label htmlFor className="mb-0 small font-weight-bold">Name:</label>
                      <input type="text" name="name" id="name" className="form-control" />
                      <label htmlFor className="mb-0 small font-weight-bold">Contact Number:</label>
                      <input type="number" name="contact" id="contact" className="form-control" />
                      <label htmlFor className="mb-0 small font-weight-bold">Contact Email:</label>
                      <input type="email" name="email" id="email" required className="form-control" />
                      <label htmlFor className="mb-0 small font-weight-bold">Name of the Organization:</label>
                      <input type="text" name="org_name" id="org_name" required className="form-control" />
                      <label htmlFor className="mb-0 small font-weight-bold">Required Demo for:</label>
                      <select className="form-control" name="required_demo" size={1} required>
                        <option value="school">School</option>
                        <option value="college">College</option>
                        <option value="university">University</option>
                        <option value="coaching_center">Coaching Centre</option>
                        <option value="orgnanization">Organization</option>
                        <option value="other">Other</option>
                      </select>
                      <label htmlFor className="mb-0 small font-weight-bold">Address:</label>
                      <input type="text" name="address" id="address" required className="form-control" />
                      <div className="row mt-2">
                        <div className="col-md-6">
                          <label htmlFor className="mb-0 small font-weight-bold">City:</label>
                          <input type="text" name="city" id="city" required className="form-control" />
                        </div>
                        <div className="col-md-6">
                          <label htmlFor className="mb-0 small font-weight-bold">District:</label>
                          <input type="text" name="district" id="district" required className="form-control" />
                        </div>
                        <div className="col-md-6">
                          <label htmlFor className="mb-0 small font-weight-bold">Country:</label>
                          <input type="text" name="country" id="country" required className="form-control" />
                        </div>
                        <div className="col-md-6">
                          <label htmlFor className="mb-0 small font-weight-bold">PIN code:</label>
                          <input type="number" name="pin" id="pin" required className="form-control" />
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary text-left" data-dismiss="modal">Close</button>
                  <button type="submit" className="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>

    );


  }
}
export default indexcomponent