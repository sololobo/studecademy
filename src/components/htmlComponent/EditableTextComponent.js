import React, { Component } from 'react'

class EditableText extends Component {
   constructor(props) {
      super(props);
      this.state = {
         name: props.name || '',
         id: props.editID || '',
         type: props.type || 'text',
         value: props.value || '',
         className: props.className,
         edit: false
      }
   }

   render() {
      return (
         this.state.edit === true &&
         <input
            name={this.state.name}
            id={this.state.editID}
            type={this.state.type}
            value={this.state.value}
            className={this.state.className}
            autoFocus
            onFocus={e => {
               const value = e.target.value
               e.target.value = ''
               e.target.value = value
            }}
            onChange={e => {
               this.setState({ value: e.target.value })
            }}
            onBlur={e => {
               this.setState({ edit: false })
            }}

            onKeyUp={e => {
               if (e.key === 'Escape' || e.key === 'Enter') {
                  this.setState({ edit: false })
               }
            }}
         />
         ||
         <span onClick={e => {
            this.setState({ edit: this.state.edit !== true })
         }}>         <input
               name={this.state.name}
               id={this.state.editID}
               type={this.state.type}
               value={this.state.value}
               className={this.state.className}
            />
         </span>
      )
   }
}

export default EditableText;
