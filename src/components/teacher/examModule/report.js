import React, { Component } from 'react';


class Report extends Component {

    render() {

        return (<div className="fadeIn animated">
            <div className="card">
                <div className="card-header">
                    <span className="h3 font-weight-bold">Name of the test:</span>
                    <span className="float-right"><a href="#" className="btn btn-success text-white"><i className="icon-action-redo" /> Export Report</a></span>
                </div>
                <div className="card-body">
                    <table className=" datatable table table-bordered">
                        <thead>
                            <tr>
                                <th>Roll No</th>
                                <th>Name</th>
                                <th>Total Marks</th>
                                <th>Marks obtained</th>
                                <th>Percentage</th>
                                <th>Average Speed</th>
                                <th>Accuracy</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td>Sofkf jajdf</td>
                                <td>50</td>
                                <td>23</td>
                                <td>23%</td>
                                <td>34 Sec</td>
                                <td>34%</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        );


    }
}
export default Report