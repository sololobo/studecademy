
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Examadd from './CreateExam'
import Examreport from './report'


class Test extends Component {
    constructor(props) {
        super(props);
        this.addExam = this.addExam.bind(this);
        this.examReport = this.examReport.bind(this);
    }
    addExam() {

        ReactDOM.render(<Examadd />, document.getElementById('contain1'))

    }
    examReport() {

        ReactDOM.render(<Examreport />, document.getElementById('contain1'))

    }
    render() {

        return (

            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <h2><b>List of Tests:</b></h2>
                    </div>
                    <div className="card-body">
                        <div>
                            <b>Unit:</b>
                            <select name id="unit1" className="ml-1 mr-2 form-control-sm">
                                <option value>Select</option>
                                <option value={9}>UpdateTested</option>
                            </select>
                            <b>Session:</b>
                            <select name id="session1" className="ml-1 mr-2 form-control-sm">
                                <option value>Select</option>
                            </select>
                            <b>Class/Department:</b>
                            <select id="department1" className="ml-1 mr-2 form-control-sm">
                                <option value>Select</option>
                            </select>
                            <b>Semester:</b>
                            <select id="semester1" className="ml-1 mr-2 form-control-sm">
                                <option value>Select</option>
                            </select>
                            <b>Subject:</b>
                            <select id="semester1" className="ml-1 mr-2 form-control-sm">
                                <option value>Select</option>
                            </select>
                        </div>
                        {/*-/filter*/}
                        <hr />
                        <div>
                            {/*-status section for filtering the exams like publishes, scheduled*/}
                            <b>Status: </b>
                            <select name id className="form-control-sm mr-2">
                                <option value>--Choose Status--</option>
                                <option value>Published</option>
                                <option value>Scheduled</option>
                            </select>
                            {/*-Action Will be work when multiple select has been selected*/}
                            <b>Action:</b>
                            <select name id className="form-control-sm">
                                <option value>--Choose Option--</option>
                                <option value>edit</option>
                                <option value>Delete</option>
                            </select>
                            <button className="btn btn-sm bg-light">Proceed</button>
                            {/*create new exam*/}
                            <span className="float-right">
                                <a onClick={this.addExam} className="btn btn-primary text-white"><i className="icon-plus" /> Create New Test</a>
                            </span>
                        </div>
                        <hr />
                        {/*test info*/}
                        <div className="table-responsive">
                            <table className="table table-bordered datatable border">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0"><input type="checkbox" onclick="toggle(this);" /></th>
                                        <th className="border-0">Name of the Exam</th>
                                        <th className="border-0">Taken By</th>
                                        <th className="border-0">Full Marks</th>
                                        <th className="border-0">Published on</th>
                                        <th className="border-0">Status</th>
                                        <th className="border-0">Test Taken</th>
                                        <th className="border-0">Analytics</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="border-0"><input type="checkbox" /></td>
                                        <td className="border-0"><a href="edit-exam.html">Exam name I</a></td>
                                        <td className="border-0">Teacher Name</td>
                                        <td className="border-0">45</td>
                                        <td className="border-0">25-07-2020</td>
                                        <td className="border-0"><span className="badge badge-pill badge-light">Active</span></td>
                                        <td className="border-0">25</td>
                                        <td className="border-0">
                                            <a onClick={this.examReport} className="btn btn-success text-white"><i className="icon-action-redo" /> View Report</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {/*test info--*/}
                    </div>{/*Card body*/}
                </div>
            </div>


        );


    }
}
export default Test