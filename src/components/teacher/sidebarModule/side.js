import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Dashboard from '../dashboardModule/dashboard'
import Profile from '../profileModule/mentorProfile'
import Assignment from '../assignmentModule/mentorassignmentList'
import Books from '../bookModule/mentorBooksList'
import RoutineList from '../routineModule/routine'
import Feed from '../feedModule/feed'
import Attendance from '../attendanceModule/attendance'
import Classactivity from '../classactivityModule/classActivity'
import Guidenote from '../guidenoteModule/Guidenotes'
import Questionbank from '../questionbankModule/QuestionBank'
import Exam from '../examModule/test'
import Notice from '../noticeModule/notice'
class side extends Component {
    constructor(props) {
        super(props);
        this.showProfile = this.showProfile.bind(this);
        this.showDashboard = this.showDashboard.bind(this);
        this.showAssignment = this.showAssignment.bind(this);
        this.showBooks = this.showBooks.bind(this);
        this.showRoutine = this.showRoutine.bind(this);
        this.feed = this.feed.bind(this);
        this.attendance = this.attendance.bind(this);
        this.classactivity = this.classactivity.bind(this);
        this.guidenote = this.guidenote.bind(this);
        this.questionbank = this.questionbank.bind(this);
        this.exam = this.exam.bind(this);
        this.notice = this.notice.bind(this);
    }
    showDashboard() {

        ReactDOM.render(<Dashboard />, document.getElementById('contain1'))

    }
    showProfile() {

        ReactDOM.render(<Profile />, document.getElementById('contain1'))

    }
    showAssignment() {

        ReactDOM.render(<Assignment />, document.getElementById('contain1'))

    }
    showBooks() {

        ReactDOM.render(<Books />, document.getElementById('contain1'))

    }
    showRoutine() {
        ReactDOM.render(<RoutineList />, document.getElementById('contain1'))
    }
    feed() {

        ReactDOM.render(<Feed />, document.getElementById('contain1'))

    }
    attendance() {

        ReactDOM.render(<Attendance />, document.getElementById('contain1'))

    }
    classactivity() {
        ReactDOM.render(<Classactivity />, document.getElementById('contain1'))
    }
    guidenote() {
        ReactDOM.render(<Guidenote />, document.getElementById('contain1'))
    }
    questionbank() {
        ReactDOM.render(<Questionbank />, document.getElementById('contain1'))
    }
    exam() {
        ReactDOM.render(<Exam />, document.getElementById('contain1'))
    }
    notice() {
        ReactDOM.render(<Notice />, document.getElementById('contain1'))
    }

    dropdowntoggler4() {
        if (document.getElementById("nav-dropdown-troggler-4").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-4").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-4").className += " open"
        }
    }
    dropdowntoggler5() {
        if (document.getElementById("nav-dropdown-troggler-5").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-5").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-5").className += " open"
        }
    }
    dropdowntoggler6() {
        if (document.getElementById("nav-dropdown-troggler-6").classList.contains("open") == true) {
            document.getElementById("nav-dropdown-troggler-6").classList.remove("open")
        }
        else {

            document.getElementById("nav-dropdown-troggler-6").className += " open"
        }
    }
 
    render() {

        return ([
            <div className="sidebar">
                <nav className="sidebar-nav">
                    <ul className="nav">
                        <li className="nav-item">
                            <a onClick={this.showProfile} className="nav-link"><i className="icon-user" /> My Profile</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showDashboard} className="nav-link"><i className="icon-user" /> My Dashboard </a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showRoutine} className="nav-link"><i className="icon-notebook" /> Routine</a>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-4">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler4}><i class="icon-paper-clip"></i>Activities </a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.attendance} className="nav-link"><i className="icon-book-open" /> Attendance  </a>
                                </li>

                                <li className="nav-item">
                                    <a onClick={this.classactivity} className="nav-link"><i className="icon-book-open" />Activity Log   </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.showAssignment} className="nav-link"><i className="icon-notebook" /> Assignment</a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.guidenote} className="nav-link"><i className="icon-book-open" /> Guide Note  </a>
                                </li>

                                <li className="nav-item">
                                    <a onClick={this.notice} className="nav-link"><i className="icon-book-open" />Notice Board   </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.feed} className="nav-link"><i className="icon-book-open" /> Feedback  </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-5">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler5}><i class="icon-paper-clip"></i>Performance</a>
                            <ul class="nav-dropdown-items">
                                <li className="nav-item">
                                    <a onClick={this.showRoutine} className="nav-link"><i className="icon-notebook" /> Marks Distribution  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.exam} className="nav-link"><i className="icon-book-open" />Exam  </a>
                                </li>
                                <li className="nav-item">
                                    <a onClick={this.questionbank} className="nav-link"><i className="icon-book-open" /> Question Bank  </a>
                                </li>

                                <li className="nav-item">

                                    <a  className="nav-link"><i className="icon-doc" />Markes  </a>
                                </li>
                                <li className="nav-item">
                                    <a  className="nav-link"><i className="icon-notebook" />Admit Card  </a>
                                </li>
                                <li className="nav-item">
                                    <a  className="nav-link"><i className="icon-user-follow" />Report Card  </a>
                                </li>


                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown" id="nav-dropdown-troggler-6">
                            <a class="nav-link nav-dropdown-toggle" onClick={this.dropdowntoggler6}><i class="icon-paper-clip"></i>Resources </a>
                            <ul class="nav-dropdown-items">

                                <li className="nav-item">
                                    <a onClick={this.showBooks} className="nav-link"><i className="icon-book-open" /> Books</a>
                                </li>


                            </ul>
                        </li>

                       </ul>
                </nav>
                <button className="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>

        ]);


    }
}
export default side