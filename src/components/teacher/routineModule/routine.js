import React, { Component } from 'react';


class routine extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*-===Routine*/}
                <div className="card">
                    <div className="card-header">
                        <b>Session</b>
                        <select name id className="form-control-sm">
                            <option value>2019</option>
                            <option value disabled>2018</option>
                        </select>
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table id="dtHorizontal" className="table table-striped table-bordered" cellSpacing={0}>
                                <thead>
                                    <tr className="text-center text-muted">
                                        <th> Time </th>
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                        <th />
                                    </tr>
                                </thead>
                                <tbody className="text-center">
                                    <tr>
                                        <th className="text-muted text-center text-uppercase" rowSpan={2}>Monday</th>
                                        <th>Physics</th>
                                        <th>Chemistry</th>
                                        <th>--</th>
                                        <th>Mathematics</th>
                                        <th className="bg-secondary"> Break </th>
                                        <th>Electronics</th>
                                        <th>Bengali</th>
                                        <th>--</th>
                                        <th>English</th>
                                    </tr>
                                    <tr>
                                        <th>R3</th>
                                        <th>R2</th>
                                        <th>R3</th>
                                        <th>--</th>
                                        <th className="bg-secondary"> Break </th>
                                        <th>R4</th>
                                        <th>R4</th>
                                        <th>R5</th>
                                        <th>-</th>
                                    </tr>
                                    <tr>
                                        <th className="text-muted text-center text-uppercase" rowSpan={2}>Tuesday</th>
                                        <th>Physics</th>
                                        <th>Chemistry</th>
                                        <th>--</th>
                                        <th>Mathematics</th>
                                        <th className="bg-secondary"> Break </th>
                                        <th>Electronics</th>
                                        <th>Bengali</th>
                                        <th>--</th>
                                        <th>English</th>
                                    </tr>
                                    <tr>
                                        <th>R3</th>
                                        <th>R2</th>
                                        <th>R3</th>
                                        <th>--</th>
                                        <th className="bg-secondary"> Break </th>
                                        <th>R4</th>
                                        <th>R4</th>
                                        <th>R5</th>
                                        <th>-</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {/*--=======/routine====*/}
            </div>

        );


    }
}
export default routine