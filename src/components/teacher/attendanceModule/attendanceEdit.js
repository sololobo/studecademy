import React, { Component } from 'react';


class attendanceEdit extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*-===Body Content===*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Edit Attendance</span>
                        <span className="float-right"><a href="attendance.html" className="btn  btn-primary text-white"><i className="icon-action-undo" /> Back to Attendance</a></span>
                    </div>
                    <div className="card-body">
                        {/*filter*/}
                        <b>Select:</b>
                        <select name id className="form-control-sm">
                            <option value>--Session--</option>
                            <option value>2019</option>
                            <option value>2018</option>
                        </select> &nbsp;
      <b>Select:</b>
                        <select name id className="form-control-sm">
                            <option value>--Class/Department--</option>
                            <option value>Class 4</option>
                            <option value>Class 5</option>
                        </select> &nbsp;
      <b>Select:</b>
                        <select name id className="form-control-sm">
                            <option value>--Choose Semester--</option>
                            <option value>1st Semester</option>
                            <option value>2nd Semester</option>
                        </select> &nbsp;
      <b>Select Month:</b>
                        <select name id className="form-control-sm">
                            <option value>January</option>
                            <option value>February</option>
                            <option value>March</option>
                        </select>
                        {/*/filter*/}
                        <hr />
                        {/*Routine List*/}
                        <table className="table table-bordered table-responsive">
                            <thead className="text-center">
                                <tr>
                                    <th>Roll Number</th>
                                    <th>Name of the Student</th>
                                    <th>01</th>
                                    <th>02</th>
                                    <th>03</th>
                                    <th>04</th>
                                    <th>05</th>
                                    <th>06</th>
                                    <th>07</th>
                                    <th>08</th>
                                    <th>09</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <th>23</th>
                                    <th>24</th>
                                    <th>25</th>
                                    <th>26</th>
                                    <th>27</th>
                                    <th>28</th>
                                    <th>29</th>
                                    <th>30</th>
                                    <th>31</th>
                                </tr>
                            </thead>
                            <tbody className="text-center">
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><input type="checkbox" name id /></th>
                                    <th><input type="checkbox" name id /></th>
                                    <th><input type="checkbox" name id /></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                                <tr>
                                    <th>18705516014</th>
                                    <th>S Sharma</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th>--</th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-danger"><i className="fa fa-times" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th>--</th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                    <th><span className="text-success"><i className="fa fa-check" /></span></th>
                                </tr>
                            </tbody>
                        </table>
                        {/*/Routine List*/}
                    </div>
                    <div className="card-footer">
                        <a href="#" className="float-right btn btn-success text-white"><i className="icon-check" /> Save</a>
                    </div>
                </div>
                {/*-===/Body Content===*/}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for delete */}

            </div>

        );


    }
}
export default attendanceEdit