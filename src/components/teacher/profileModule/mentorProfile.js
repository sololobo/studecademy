import React, { Component } from 'react';
import Cookies from 'js-cookie';
import logo from '../../../media/features/books.png';
import $ from 'jquery';
import swal from 'sweetalert';

class mentorProfile extends Component {



    constructor(props) {
        super(props);


        this.state = {
            teacher: []
        }

    }

    componentDidMount() {


        let url = global.API + "/studapi/public/api/viewateacher";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'teacher_i_d': Cookies.get('teacherid') })
        })
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    teacher: json
                })
            });



    }

    changePass() {
        //let email = document.getElementById('email1').innerHTML;
        //let email = document.getElementById('username').defaultValue;
        let email = Cookies.get('teacherid')
        let pwd = document.getElementById('newpass').defaultValue;
        const fd = new FormData();
        fd.append('email', email);
        fd.append('pwd', pwd);
        swal({
            title: "Are you sure?",
            text: "Sure to change?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/changeuserpassword";
                    fetch(url, {
                        method: 'POST',

                        body: fd
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("password changed", {
                                        icon: "success",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }
                                else {
                                    swal("not saved", {
                                        icon: "warning",
                                    });
                                    $('#resetPassword').modal('hide');

                                    this.componentDidMount();
                                }

                            }));
                }

            });

    }


    submit(teach){

        this.state = {
            teacherID: Cookies.get('teacherid'),
            teacherName: document.getElementById('teacherName').value,
            contactNo: document.getElementById('contactNo').value,
            emailID: document.getElementById('emailID').value,
            //gender: document.getElementById('gender').value,
            //dateOfBirth: document.getElementById('logo').value,
            designation: document.getElementById('designation').value,
            specialization: document.getElementById('specialization').value,
            //Qualification: document.getElementById('orgeditaddress2').value,
            address1: document.getElementById('address1').value,
            address2: document.getElementById('address2').value,
            city: document.getElementById('city').value,
            state: document.getElementById('state').value,
            country: document.getElementById('country').value,
            pinCode: document.getElementById('pinCode').value,
            
            
        }

        let url = global.API + "/studapi/public/api/updateteacher";
        let data = this.state;
        // if (this.state.logo == null)
        //     this.state.logo = this.props.undata.logo;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        if (document.getElementById('profilePic').files.length == 0)
            //console.log(orga.Logo+"hi");
            fd.append('profilePic', teach.profilePic);
        else
            fd.append('profile_pic', document.getElementById('profilePic').files[0]);

        if (document.getElementById('coverPhoto').files.length == 0)
            
            fd.append('coverPhoto', teach.coverPhoto);
        else
            fd.append('coverPhoto', document.getElementById('coverPhoto').files[0]);
            



        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                
                if (resp[0]['result'] == 1) {
                    swal({
                        title: "Done!",
                        text: "Profile Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.componentDidMount();

                }
                else {
                    swal({
                        title: "Error",
                        text: "Profile Not Updated",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })

    }

    render() {

        var { isLoaded, teacher } = this.state;
        //console.log(organisation);

        return (
            <div className="fadeIn animated">
                <span className="h3 font-weight-bold mb-3">My Profile:</span>
                {teacher.map(teach => (
                    <div className="card mt-2">
                        <div className="card-header card-header1 h-100 ">
              
             <img src={global.img + teach.coverPhoto} alt="" className="cover-photo-my-profile" id="cover-photo" />
                             {/* <img src={logo} alt="profile picture" className="profile-photo-my-profile" id="profile-photo" />*/}
                            <img src={global.img + teach.profilePic} alt="profile picture" className="profile-photo-my-profile"
                                id="profile-photo" />
                        </div>
                        <div className="card-body">
                            <div className="pt-2 pb-2 pl-5 pr-5 border mb-5 mt-5">
                                <span className="border-title h3 font-weight-bold">Personal Details:</span>

                                <div className="row mt-3">
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> ID:</label>
                                            <input type="text" name="" id="" defaultValue={teach.teacherID} disabled
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> Name:</label>
                                            <input type="text" name="" id="teacherName" defaultValue={teach.teacherName} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Contact Number:</label>
                                            <input type="text" name="" id="contactNo" defaultValue={teach.contactNo} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Email:</label>
                                            <input type="text" name="" id="emailID" defaultValue={teach.emailID} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Designation:</label>
                                            <input type="text" name="" id="designation" defaultValue={teach.designation}
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Specialization:</label>
                                            <input type="text" name="" id="specialization" defaultValue={teach.specialization}
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Profile
                                        Picture:</label>
                                            <input type="file" name="" id="profilePic" className="form-control bg-white col-md-7"
                                                  />
                                                 
                                        </div>
                              

                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address:</label>
                                            <input type="text" name="" id="address1" defaultValue={teach.address1} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address 2:</label>
                                            <input type="text" name="" id="address2" defaultValue= {teach.address2}  
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">City:</label>
                                            <input type="text" name="" id="city" defaultValue={teach.city} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">State:</label>
                                            <input type="text" name="" id="state" defaultValue={teach.state}
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Country:</label>
                                            <input type="text" name="" id="country" defaultValue={teach.country} 
                                                className="form-control bg-white col-md-7" />
                                        </div>
                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">PIN Code:</label>
                                            <input type="text" name="" id="pinCode" defaultValue={teach.pinCode} 
                                                className="form-control bg-white col-md-7" />
                                        </div>

                                        <div className="form-group row">
                                            <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Cover
                                        Photo:</label>
                                            <input type="file" name="" id="coverPhoto" className="form-control bg-white col-md-7"
                                                />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div className="card-footer">
                            <button type="submit" data-toggle="modal" data-target="#resetPassword" className="btn btn-success float-left text-white"><i className="icon-key"></i> Edit Password</button>
                            <button type="submit" onClick={() => { this.submit(teach) }} className="btn btn-primary float-right text-white">Save and Update</button>
                                         
                        </div>
                                     {/*-modal for reset Password*/}
                <div className="modal fade" id="resetPassword" tabIndex={-1} role="dialog" aria-labelledby="modelTitleresetPassword" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Reset Password</span></h4>
                                    <hr />
                                    <div className="form-group">
                                        <label htmlFor className="font-weight-bold mb-0">Enter New Password:</label>
                                        <input type="password" className="form-control-sm" id="newpass" />
                                    </div>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" id="e" onClick={() => this.changePass()} className="btn btn-warning">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for reset Password */}

                    </div>
                ))}
            </div>
        )

    }
}
export default mentorProfile