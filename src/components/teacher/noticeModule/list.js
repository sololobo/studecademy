import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Viewnotice from './NoticeView'

class NoticeList extends Component {
    constructor(props) {
        super(props);
        this.viewnotice = this.viewnotice.bind(this);
    }

    viewnotice() {

        ReactDOM.render(<Viewnotice />, document.getElementById('contain2'))

    }
    render() {

        return (
            <main className="inbox">
            <div className="toolbar">
                <div className="btn-group">
                    <button type="button" className="btn btn-light">
                        <i className="icon-refresh" />
                    </button>
                </div>
                <button type="button" className="btn btn-light">
                    <i className="icon-trash" />
                </button>
                <div className="btn-group float-right">
                    <button type="button" className="btn btn-light">
                        <i className="icon-action-undo" />
                    </button>
                    <button type="button" className="btn btn-light">
                        <i className="icon-action-redo" />
                    </button>
                </div>
            </div>
            <ul className="messages">
                <li className="message">
                    <div className="row no-gutters">
                        <div className="col-md-1">
                            <input type="checkbox" className="icon-square" />
                        </div>
                        <div className="col-md-11">
                            <a onClick={this.viewnotice}>
                                <div className="header">
                                    <span className="from">Lukasz Holeczek</span>
                                    <span className="date">
                                        <span className="fa fa-paper-clip" /> Today, 3:47 PM</span>
                                </div>
                                <div className="title">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</div>
                                <div className="description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
</div>
                            </a>
                        </div>
                    </div>
                </li>
                <li className="message unread">
                    <div className="row no-gutters">
                        <div className="col-md-1">
                            <input type="checkbox" className="icon-square" />
                        </div>
                        <div className="col-md-11">
                            <a href="view.html">
                                <div className="header">
                                    <span className="from">Lukasz Holeczek</span>
                                    <span className="date">
                                        <span className="fa fa-paper-clip" /> Today, 3:47 PM</span>
                                </div>
                                <div className="title">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
</div>
                                <div className="description">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                    in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
</div>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </main>
    
        );


    }
}
export default NoticeList