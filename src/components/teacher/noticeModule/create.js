import React, { Component } from 'react';


class CreateNotice extends Component {

    render() {

        return (
            <main>
            <p className="text-center">New Message</p>
            <div className="form-row mb-3">
                <label htmlFor="to" className="col-2 col-sm-1 col-form-label">To:</label>
                <div className="col-10 col-sm-11">
                    <fieldset className="form-group">
                        <select id="select2-2" className="form-control select2-multiple" multiple>
                            <option>Option 1</option>
                            <option selected>Option 2</option>
                            <option>Option 3</option>
                            <option>Option 4</option>
                            <option>Option 5</option>
                        </select>
                    </fieldset>
                </div>
            </div>
            <div className="form-row mb-3">
                <label htmlFor="to" className="col-2 col-sm-1 col-form-label">Subject:</label>
                <div className="col-10 col-sm-11">
                    <fieldset className="form-group">
                        <input type="text" className="form-control" />
                    </fieldset>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-11 ml-auto">
                    <div className="toolbar" role="toolbar">
                        <div className="btn-group">
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-bold" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-italic" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-underline" />
                            </button>
                        </div>
                        <div className="btn-group">
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-align-left" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-align-right" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-align-center" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-align-justify" />
                            </button>
                        </div>
                        <div className="btn-group">
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-indent" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-outdent" />
                            </button>
                        </div>
                        <div className="btn-group">
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-list-ul" />
                            </button>
                            <button type="button" className="btn btn-light">
                                <span className="fa fa-list-ol" />
                            </button>
                        </div>
                        <button type="button" className="btn btn-light">
                            <span className="fa fa-trash-o" />
                        </button>
                        <button type="button" className="btn btn-light">
                            <span className="fa fa-paperclip" />
                        </button>
                    </div>
                    <div className="form-group mt-4">
                        <textarea className="form-control" id="message" name="body" rows={12} placeholder="Click here to reply" defaultValue={""} />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-success">Send</button>
                        <button type="submit" className="btn btn-warning">Draft</button>
                        <button type="submit" className="btn btn-danger">Discard</button>
                    </div>
                </div>
            </div>
        </main>
   

        );


    }
}
export default CreateNotice