import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Createnotice from './create'


class ViewNotice extends Component {

    render() {

        return (
            <main className="message">
                <div className="toolbar">
                    <div className="btn-group">
                        <button type="button" className="btn btn-light">
                            <i className="icon-refresh" />
                        </button>
                    </div>
                    <button type="button" className="btn btn-light">
                        <i className="icon-trash" />
                    </button>
                    <div className="btn-group float-right">
                        <button type="button" className="btn btn-light">
                            <i className="icon-action-undo" />
                        </button>
                        <button type="button" className="btn btn-light">
                            <i className="icon-action-redo" />
                        </button>
                    </div>
                </div>
                <div className="details">
                    <div className="title">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</div>
                    <div className="header">
                        <img className="avatar" src="../../img/avatars/7.jpg" />
                        <div className="from">
                            <span>Lukasz Holeczek</span>
lukasz@bootstrapmaster.com
</div>
                        <div className="date">Today, <b>3:47 PM</b></div>
                    </div>
                    <div className="content">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>
                        <blockquote>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</blockquote>
                    </div>
                    <div className="attachments">
                        <div className="attachment">
                            <span className="badge badge-danger">zip</span> <b>bootstrap.zip</b> <i>(2,5MB)</i>
                            <span className="menu">
                                <a href="#" className="fa fa-cloud-download" />
                            </span>
                        </div>
                        <div className="attachment">
                            <span className="badge badge-info">txt</span> <b>readme.txt</b> <i>(7KB)</i>
                            <span className="menu">
                                <a href="#" className="fa fa-cloud-download" />
                            </span>
                        </div>
                        <div className="attachment">
                            <span className="badge badge-success">xls</span> <b>spreadsheet.xls</b> <i>(984KB)</i>
                            <span className="menu">
                                <a href="#" className="fa fa-cloud-download" />
                            </span>
                        </div>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" id="message" name="body" rows={12} placeholder="Click here to reply" defaultValue={""} />
                    </div>
                    <div className="form-group">
                        <button tabIndex={3} type="submit" className="btn btn-success">Send message</button>
                    </div>
                </div>
            </main>

        );


    }
}
export default ViewNotice