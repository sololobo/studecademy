import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Listnotice from './list'
import Createnotice from './create'
import Viewnotice from './NoticeView'
import Sentnotice from './sent'
import Draftnotice from './draft'
import Trashnotice from './trash'

class Noticeboard extends Component {
    constructor(props) {
        super(props);
        this.listnotice = this.listnotice.bind(this);
        this.viewnotice = this.viewnotice.bind(this);
        this.sentnotice = this.sentnotice.bind(this);
        this.draftnotice = this.draftnotice.bind(this);
        this.trashnotice = this.trashnotice.bind(this);
        this.createnotice = this.createnotice.bind(this);
    }
    listnotice() {

        ReactDOM.render(<Listnotice />, document.getElementById('contain2'))

    }
    viewnotice() {

        ReactDOM.render(<Viewnotice />, document.getElementById('contain2'))

    }
    sentnotice() {

        ReactDOM.render(<Sentnotice />, document.getElementById('contain2'))

    }
    draftnotice() {

        ReactDOM.render(<Draftnotice />, document.getElementById('contain2'))

    }
    trashnotice() {

        ReactDOM.render(<Trashnotice />, document.getElementById('contain2'))

    }
    createnotice() {

        ReactDOM.render(<Createnotice />, document.getElementById('contain2'))

    }
    render() {

        return (
            <div className="animated fadeIn">
                <div className="card">
                    <div className="card-header">
                        <h2 className="card-text text-uppercase"><b> Noticeboard </b></h2>
                    </div>
                    <div className="card-body">
                        <div className="email-app mb-4">
                            <nav>
                                <ul className="nav">
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.listnotice} ><i className="icon-envelope-letter" />Notices<span className="badge badge-danger badge-pill">4</span></a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.createnotice}><i className="icon-plus" />Create Notice</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.sentnotice}><i className="icon-logout" /> Sent Notices</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border " onClick={this.draftnotice}><i className="icon-layers" /> Draft Notices</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link border" onClick={this.trashnotice}><i className="icon-trash" /> Trash</a>
                                    </li>
                                </ul>
                            </nav>
                            <div id="contain2">
                         <Listnotice></Listnotice>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );


    }
}
export default Noticeboard