import React, { Component } from 'react';


class guidenotesPreview extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*------------- Article Section---*/}
                <div className="container">
                    <div className="card">
                        <div className="card-header">
                            <span className="h2 font-weight-bold">Title of the Tutorial</span>
                            <br />
                            <span className="text-muted small"><i className="icon-action-redo" /> Uploaded by <i>Author Name</i></span> &nbsp;<span className="badge-pill badge badge-dark"><i className="icon-eye" /> 236 Views</span><span className="badge-pill badge badge-light"><i className="icon-doc" /> Published till: 23-04-2019</span>
                        </div>
                        <div className="card-body">
                            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur enim, quod ratione deleniti et neque quas, sint labore, quia necessitatibus ipsa ad nam! Error id rerum itaque impedit laborum beatae!
      </div>
                        <div className="card-footer">
                            <a href="#" className="float-left btn btn-secondary text-white"><i className="icon-action-undo" /> Previous Guidenotes</a>
                            <a href="#" className="ml-2 btn btn-success text-white"><i className="icon-action-redo" /> Next Guide notes</a>
                            <a href="#" className="float-right btn btn-dark text-white"><i className="icon-doc" /> Take Test</a>
                        </div>
                    </div>
                </div>
                {/*----------/Article Section-----*/}
            </div>

        );


    }
}
export default guidenotesPreview