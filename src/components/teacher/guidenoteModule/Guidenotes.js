import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Guidenoteadd from './createGuidenotes'
import Guidenoteedit from './editGuidenotes'
import Guidenoteview from './guidenotesPreview'

class Guidenotes extends Component {
    
    constructor(props) {
        super(props);
        this.guidenoteadd = this.guidenoteadd.bind(this);
        this.guidenoteedit = this.guidenoteedit.bind(this);
        this.guidenoteview = this.guidenoteview.bind(this);
    }
    guidenoteadd() {

        ReactDOM.render(<Guidenoteadd />, document.getElementById('contain1'))

    }
    guidenoteedit() {

        ReactDOM.render(<Guidenoteedit />, document.getElementById('contain1'))

    }  
    guidenoteview() {

        ReactDOM.render(<Guidenoteview />, document.getElementById('contain1'))

    }

    render() {

        return (
            <div className="fadeIn animated">
                {/*-Filter*/}
                <div className="row no-gutters">
                    <div className="col-md-12 col-xs-12 col-sm-12">
                        <h3><b>Filter:</b></h3>
                        <div className="card">
                            <div className="row">
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold">  Session: </label>
                                        <select className="form-control">
                                            <option> Academic</option>
                                            <option> Competetive</option>
                                            <option> Personal Gromming</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold">  Class/Department:</label>
                                        <select className="form-control">
                                            <option> ME</option>
                                            <option> CE</option>
                                            <option> CSE </option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold">  Semester:</label>
                                        <select className="form-control">
                                            <option> Semester 1</option>
                                            <option> Sec B</option>
                                            <option> Sec C </option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                    <div className="card-body">
                                        <label className="mb-0 font-weight-bold"> Subject:</label>
                                        <select className="form-control">
                                            <option> subject 1</option>
                                            <option>2nd Year</option>
                                            <option> 3rd Year</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                    {/*<div className="card-body">
                                        <label className="mb-0 font-weight-bold"> Select Chapter:</label>
                                        <select className="form-control">
                                            <option> Chap 1</option>
                                            <option> Chap 2</option>
                                            <option> Chap 3</option>
                                        </select>
        </div>*/}
                                </div>
                                <div className="col-md-2 col-xs-6 col-sm-6">
                                <div className="card-body">
                                    <label for="" className="mb-0 font-weight-bold">search:</label>

                                    <input type="text" name="" value="" className="form-control" />

                             
                                </div>
                                </div>
                           </div>
                            {/*--------/row*/}
                        </div>
                        {/*Tutorial List Display*/}
                        <div className="card">
                            <div className="card-header">
                                <span className="card-text h3 font-weight-bold">List of Uploaded Guide Notes <a onClick={this.guidenoteadd} className="btn btn-primary text-white float-right"><i className="icon-cloud-upload" /> Upload new Guide Notes</a></span>
                            </div>
                            <div className="card-body">
                                <div>
                                    {/*-It will be displayed if user select multi check*/}
                                    <b>Choose Action:</b>
                                    <select name id className="form-control-sm">
                                        <option value>Delete</option>
                                    </select>
                                    <button className="btn btn-sm bg-light" data-toggle="modal" data-target="#deleteAlert">Submit</button>
                                    <span className="float-right mb-3">
                                        <b>Filter by:</b>
                                        <select name id className="form-control-sm">
                                            <option value>Published</option>
                                            <option value>Draft</option>
                                        </select>
                                    </span>
                                </div>
                                <table className="table-bordered table">
                                    <thead>
                                        <tr className="bg-light">
                                            <th className="border-0"><input type="checkbox" /></th>
                                            <th className="border-0">Name of the Title</th>
                                            <th className="border-0">Status</th>
                                            <th className="border-0">Views</th>
                                            <th className="border-0">Published on</th>
                                            <th className="border-0">Published Till</th>
                                            <th className="border-0">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="border-0"><input type="checkbox" /></td>
                                            <td className="border-0">Title of the Guide Notes</td>
                                            <td className="border-0"><span className="badge badge-pill bg-light">Published</span></td>
                                            <td className="border-0">236</td>
                                            <th className="border-0">22.02.2019</th>
                                            <th className="border-0">22.04.2019</th>
                                            <td className="border-0">
                                                <span><a onClick={this.guidenoteview} className="text-success"><i className="icon-eye" /></a></span>
                                                <span><a onClick={this.guidenoteedit} className="text-warning"><i className="icon-pencil" /></a></span>
                                                <span><a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" />
                                                </a></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {/*/Tutorial List Display*/}
                    </div>
                </div>
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        );


    }
}
export default Guidenotes