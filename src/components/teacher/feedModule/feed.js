import React, { Component } from 'react';


class feed extends Component {

    render() {

        return (
            <div className="animated fadeIn">
                {/*-row started-*/}
                <div className="row">
                    <div className="col-md-1 col-sm-12" />
                    <div className="col-md-6 col-xl-6 col-sm-12">
                        <div className="card d-block d-md-block d-xl-none d-xs-block">
                            <div className="card-body bg-light">
                                <textarea name id rows={3} className="form-control bg-white" defaultValue={"Share Your think..."} />
                            </div>
                            <div className="card-footer">
                                <label htmlFor="file-input"><i className="icon-camera mr-2" /></label>
                                <input type="file" id="file-input" />
                                <button type="submit" className="btn btn-success"><i className="icon-action-redo" />
            Post</button>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">
                                <div className="float-left">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTCLOBElgDlu1y_DCdN4SciMpFyqiWWxTuU1iRP1nyMjaWmGpN7" alt="..." className="rounded-circle thumbnail feed-logo" /> Bhumi | <span className="text-muted small">Class XI</span>
                                </div>
                                <div className="float-right">
                                    <a href="#"><i className="icon-user-follow" /> Follow</a>
                                </div>
                            </div>
                            <div className="card-body">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Beatae corrupti modi non
                                dolor! Ab consequatur dolore, veritatis excepturi cumque et quisquam molestiae
                                dolores, libero possimus tenetur incidunt id ea laudantium.
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum, eligendi. Cum
                                adipisci officiis ullam temporibus rerum veritatis id! Voluptates minima fuga iusto
                                ipsum nostrum voluptatibus deserunt consequatur sequi vitae illum!
          <hr className="mt-5 mb-2" />
                                <div className>
                                    <span className="mr-4 ml-4"><a href="#"><i className="icon-like" /> Like</a> +64
              Others</span>
                                    <span className="mr-4 ml-4"><a href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea"><i className="icon-bubble" />
                Comment</a></span>
                                    <span className="mr-4 ml-4"><a href="#"><i className="icon-dislike" />
                Dislike</a></span>
                                </div>
                                <hr className="mt-2" />
                                <div className="media comment-area ">
                                    <div className="collapse" id="commentArea">
                                        <img className="d-flex mr-3 rounded-circle feed-commenter" src="http://placehold.it/50x50" alt />
                                        <div className="media-body">
                                            <h5 className="mt-0">Commenter Name</h5>
                Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque
                ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus
                viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.
                Donec lacinia congue felis in faucibus.
                <p className="mt-0 float-right"><a href="#" data-toggle="modal" data-target="#reply"><i className="icon-action-undo" /> Leave
                    Reply</a></p>
                                            <div className="media mt-4">
                                                <img className="d-flex mr-3 rounded-circle feed-commenter" src="http://placehold.it/50x50" alt />
                                                <div className="media-body">
                                                    <h5 className="mt-0">Commenter Name</h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                    scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                    vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                    nisi vulputate fringilla. Donec lacinia congue felis in
                    faucibus.
                  </div>
                                            </div>
                                            <div className="media mt-4">
                                                <img className="d-flex mr-3 rounded-circle feed-commenter" src="http://placehold.it/50x50" alt />
                                                <div className="media-body">
                                                    <h5 className="mt-0">Commenter Name <span className="float-right small"><a href="#" data-target="#deleteAlert" data-toggle="modal" className="text-danger"><i className="icon-trash" />
                          Delete Comment</a></span></h5>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                    scelerisque ante sollicitudin. Cras purus odio, vestibulum in
                    vulputate at, tempus viverra turpis. Fusce condimentum nunc ac
                    nisi vulputate fringilla. Donec lacinia congue felis in
                    faucibus.
                  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <textarea name id rows={2} className="form-control" defaultValue={""} />
                                <div className="float-right mt-1">
                                    <label htmlFor="file-input"><i className="icon-camera mr-3" /></label>
                                    <input type="file" id="file-input" />
                                    <button type="submit" className=" btn btn-success"><i className="icon-paper-plane small" /> Post</button>
                                </div>
                            </div>
                        </div>
                        <div className="card">
                            <div className="card-header">
                                <div className="float-left">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTCLOBElgDlu1y_DCdN4SciMpFyqiWWxTuU1iRP1nyMjaWmGpN7" alt="..." className="rounded-circle thumbnail feed-logo" /> You | <span className="text-muted small">Class XI</span>
                                </div>
                                <div className="float-right">
                                    <a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" /> Delete Post</a>
                                </div>
                            </div>
                            <div className="card-body">
                                <br />
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Beatae corrupti modi non
          dolor! Ab consequatur dolore, veritatis excepturi cumque et quisquam molestiae
          dolores, libero possimus tenetur incidunt id ea laudantium.
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum, eligendi. Cum
          adipisci officiis ullam temporibus rerum veritatis id! Voluptates minima fuga iusto
          ipsum nostrum voluptatibus deserunt consequatur sequi vitae illum!
          <img className="d-flex w-auto feed-picture" src="http://placehold.it/1280x720" alt />
                                <hr className="mt-5 mb-2" />
                                <div className>
                                    <span className="mr-4 ml-4"><a href="#"><i className="icon-like" /> Like</a> +64
              Others</span>
                                    <span className="mr-4 ml-4"><a href="#"><i className="icon-bubble" />
                Comment</a></span>
                                    <span className="mr-4 ml-4"><a href="#"><i className="icon-dislike" />
                Dislike</a></span>
                                </div>
                                <hr className="mt-2 mb-2" />
                            </div>
                            <div className="card-footer">
                                <textarea name id rows={2} className="form-control mb-2" defaultValue={""} />
                                <div className="float-right mt-1">
                                    <label htmlFor="file-input"><i className="icon-camera mr-3" /></label>
                                    <input type="file" id="file-input" />
                                    <button type="submit" className="btn btn-success"><i className="icon-paper-plane small" />&nbsp; Post</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-xl-4 col-xs-12">
                        <div className="card d-md-none d-xl-block d-xs-none">
                            <div className="card-body bg-light">
                                <textarea name id rows={3} className="form-control bg-white" defaultValue={"Share Your think..."} />
                            </div>
                            <div className="card-footer">
                                <label htmlFor="file-input"><i className="icon-camera mr-2" /></label>
                                <input type="file" id="file-input" />
                                <button type="submit" className="btn btn-primary float-right"><i className="icon-action-redo" /> Post</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/row started-*/}

                {/*-modal for Reply*/}
                <div className="modal fade" id="reply" tabIndex={-1} role="dialog" aria-labelledby="modelTitlereply" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Leave a Reply</span></h4>
                                    <hr />
                                    <textarea name id rows={3} className="form-control bg-white mb-3" defaultValue={"Share Your thought..."} />
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-success">Share</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal*/}


            </div>

        );


    }
}
export default feed