import React, { Component } from 'react';


class classActivity extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                {/*-main content*/}
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Class Activity</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-toggle="modal" data-target="#addActivity">+ Add new activity</a></span>
                    </div>
                    <div className="card-body">
                        <div>
                            {/*filter*/}
                            <b>Select Session:</b>
                            <select name id className="form-control-sm">
                                <option value>2019</option>
                                <option value>2018</option>
                            </select>
                            <b>Select Class/Department:</b>
                            <select name id className="form-control-sm">
                                <option value>Class/Deparment</option>
                                <option value />
                            </select>
                            <b>Select Semester:</b>
                            <select name id className="form-control-sm">
                                <option value>1st Semester</option>
                                <option value>2nd Semester</option>
                            </select>
                            <b>Select Subject:</b>
                            <select name id className="form-control-sm">
                                <option value>subject</option>
                                <option value>subject 2</option>
                            </select>
                            <b>Select date:</b>
                            <input type="date" name id className="form-control-sm" />
                            {/*/filter*/}
                        </div>
                        <table className="table mt-2 table-bordered">
                            <thead>
                                <tr className="bg-light">
                                    <th>Name of the Activity</th>
                                    <th>Subject</th>
                                    <th>View</th>
                                    <th />
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="#" data-target="#viewActivity" data-toggle="modal">Activity title</a></td>
                                    <td>Physics</td>
                                    <td>235</td>
                                    <td>
                                        <span className="h4"><a href="#" className="text-danger font-weight-bold" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash" /></a></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                {/*/Main Content*/}

                {/*-modal for view activity*/}
                <div className="modal fade" id="viewActivity" tabIndex={-1} role="dialog" aria-labelledby="modelTitleviewActivity" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold">View Activity</span></h4>
                                    <hr />
                                    <fieldset>
                                        <label htmlFor className="mb-0 font-weight-bold">Published on:</label>
                                        <p>Date</p>
                                        <label htmlFor className="mb-0 font-weight-bold">Title of the Activity</label>
                                        <p>Title of activity</p>
                                        <label htmlFor className="font-weight-bold mb-0">Share Today's Class activity</label>
                                        <p>Today's Activity</p>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for view activity */}
                {/*-modal for add activity*/}
                <div className="modal fade" id="addActivity" tabIndex={-1} role="dialog" aria-labelledby="modelTitleaddActivity" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid">
                                    <h4><span className="font-weight-bold">Add Activity</span></h4>
                                    <hr />
                                    <fieldset>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Session</label>
                                        <select name id className="form-control">
                                            <option value>Session</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Class/Department</label>
                                        <select name id className="form-control">
                                            <option value>Class/Department</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Section</label>
                                        <select name id className="form-control">
                                            <option value>Section</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Subject</label>
                                        <select name id className="form-control">
                                            <option value>Subject</option>
                                        </select>
                                        <label htmlFor className="mb-0 font-weight-bold">Select Date</label>
                                        <input type="date" className="form-control" />
                                        <label htmlFor className="mb-0 font-weight-bold">Title of the Activity</label>
                                        <input type="text" name id className="form-control" />
                                        <label htmlFor className="font-weight-bold mb-0">Share Today's Class activity</label>
                                        <textarea name id rows={3} className="form-control" defaultValue={""} />
                                    </fieldset>
                                    <hr />
                                    <button type="button" className="btn btn-secondary float-left" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning  float-right">Add new activity</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for add activity */}
                {/*-modal for delete*/}
                <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid text-center">
                                    <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                    <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                    <button type="button" className="btn btn-warning">Yes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for delete */}


            </div>

        );


    }
}
export default classActivity