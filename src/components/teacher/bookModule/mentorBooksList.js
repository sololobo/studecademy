import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import BooksView from './mentorBooksView';
import Cookies from 'js-cookie';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';
class mentorBooksList extends Component {
    constructor(props) {
        super(props);
        this.showSessionList = this.showSessionList.bind(this);
        //this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        //this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        //this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        //this.showSectionSubjectAdd = this.showSectionSubjectAdd.bind(this);
        //this.showSectionList = this.showSectionList.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        //this.showSemesterSubject = this.showSemesterSubject.bind(this);
        this.showAllBooks = this.showAllBooks.bind(this);

        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            bookList: [],
            unitList: [],
            sessonList1: [],
            classList: [],
            unitList: [],
            semesterList: [],
            subjectList1: [],
            subjectList: [],



            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            subjectList1: [],
            searchString: "",
            searchParams : []

        }
        this.bookSearchList = []
    }

    showBookview(book) {

        ReactDOM.render(<BooksView bookdata={book} />, document.getElementById('contain1'))

    }

    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }

 /*   showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }*/

    showSession(val) {

      this.updateParamsForSearch('unitID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }


    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

 /*   showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }*/

    showDepartment(val) {

      this.updateParamsForSearch('sessionID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }


    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

  /*  showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }*/

    showSemester(val) {

      this.updateParamsForSearch('departmentID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let semesterURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(semesterURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }

    //Section

   /* showSectionList() {
        let val = document.getElementById("semester1").value;

        this.showSection(val);
    }*/

    showSectionSubjectList(val) {

        //let val = document.getElementById("semester1").value;
        console.log(val);
        

        this.showSection(val);
        this.showSubject(val);

        this.updateParamsForSearch("semesterID", val);
    }

    showSection(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }

    //Subject



    showSubject(val) {



        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            });
    }

/*    showSemesterSubject() {

        let val = document.getElementById("semester1").value
        
        this.updateParamsForSearch('semesterID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList1: json
                })
            });

    }*/

    showAllBooks() {

        let val = document.getElementById("subject1").value
        this.updateParamsForSearch('subjectID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'subjectID': val })

        };


        let sessionURL = global.API + "/studapi/public/api/viewallbooks";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            });

    }





    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallorgabooks";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    bookList: json
                })
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });




    }


    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }


    handleSubjectChange(val){
        this.setState({subjectID : val})
        this.updateParamsForSearch("subjectID", val)
     }
  
     handleSectionChange(val){
        this.setState({ section_i_d: val })
        this.updateParamsForSearch("sectionID", val)
     }

    render() {
        var { isLoaded, bookList, unitList, semesterList, sessonList1, sectionList, classList, subjectList, subjectList1 } = this.state;

        if (this.state.searchParams.length > 0) {
            this.bookSearchList = SearchForObjectsWithParams(bookList, this.state.searchParams)
        }
        else {
            this.bookSearchList = bookList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.bookSearchList = SearchForObjectsWithName(this.bookSearchList, this.state.searchString)
        }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">
                            Books
                            </span>
                    </div>
                    <div className="card-body">

                    <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" >Selected All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>

                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">
                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">

                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" onChange={(data) => this.showSectionSubjectList(data.target.value)} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.handleSubjectChange(data.target.value) }} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {subjectList.map(subject => (
                                                    <option value={subject.subjectID}>{subject.subjectName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>

                                            <select name="section_i_d" value={this.state.section_i_d} onChange={(data) => { this.handleSectionChange(data.target.value)  }} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {sectionList.map(section => (
                                                    <option value={section.sectionID}>{section.sectionName}</option>

                                                ))}

                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>

                        <hr />
                        <div className="row">
                            {this.bookSearchList.map(book => (
                                <div className="col-md-3">
                                    <div className="card">
                                        <div className="card-body book-body">
                                            <img src={global.img + book.bookCoverPhoto} alt="book" className="book-preview" />
                                            <div className="h5 font-weight-bold py-2 px-3">{book.bookName}

                                            </div>
                                        </div>
                                        <div className="card-footer">
                                            <a onClick={() => this.showBookview(book)} className="float-right btn btn-primary text-white">Read Book</a>
                                            <a href="#" className="float-left badge badge-pill bg-light mt-1">- By {book.bookPublication}</a>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        );


    }
}
export default mentorBooksList
