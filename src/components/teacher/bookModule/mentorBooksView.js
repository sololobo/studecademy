import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import Bookview from './mentorBooksList';

class mentorBooksView extends Component {

    constructor(props) {
        super(props);

        this.listBooks = this.listBooks.bind(this);


    }

    listBooks() {

        ReactDOM.render(<Bookview />, document.getElementById('contain1'))

    }
    render() {

        return (
            <div className="fadeIn animated">


                <div className="card">

                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">{this.props.bookdata.bookName}</span>
                        <a onClick={this.listBooks} className="float-right mt-2"><i className="icon-action-undo"></i> Back to Books List</a>
                    </div>
                    <div className="card-body pdf-view">
                        <embed src={global.img + this.props.bookdata.bookURL} type="" width="100%" height="600px" />
                    </div>

                </div>

            </div>
        );


    }
}
export default mentorBooksView