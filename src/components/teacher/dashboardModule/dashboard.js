import React, { Component } from 'react';
import $ from 'jquery';
import './dashboard.css';
import  './todo.css';


class Dashboard extends Component {

    render() {

        return (
            <div className="animated fadeIn">
  {/*--=====Dasboard header Graphs=======*/}
  {/*--Anlaytics*/}
  <div className="card-group mb-4">
    <div className="card">
      <div className="card-body">
        <div className="h1 text-muted text-right mb-4">
          <i className="icon-people" />
        </div>
        <div className="text-value">56 / ∞</div>
        <small className="text-muted text-uppercase font-weight-bold">Unit</small>
        <div className="progress progress-xs mt-3 mb-0">
          <div className="progress-bar bg-info" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
        </div>
      </div>
    </div>
    <div className="card ml-4">
      <div className="card-body">
        <div className="h1 text-muted text-right mb-4">
          <i className="icon-user-follow" />
        </div>
        <div className="text-value">385 / 500</div>
        <small className="text-muted text-uppercase font-weight-bold">Students</small>
        <div className="progress progress-xs mt-3 mb-0">
          <div className="progress-bar bg-success" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
        </div>
      </div>
    </div>
    <div className="card ml-4">
      <div className="card-body">
        <div className="h1 text-muted text-right mb-4">
          <i className="icon-user" />
        </div>
        <div className="text-value">1238 / ∞</div>
        <small className="text-muted text-uppercase font-weight-bold">Mentors</small>
        <div className="progress progress-xs mt-3 mb-0">
          <div className="progress-bar bg-warning" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
        </div>
      </div>
    </div>
    <div className="card ml-4">
      <div className="card-body">
        <div className="h1 text-muted text-right mb-4">
          <i className="icon-speedometer" />
        </div>
        <div className="text-value">56GB / 1TB</div>
        <small className="text-muted text-uppercase font-weight-bold">Disk Usage</small>
        <div className="progress progress-xs mt-3 mb-0">
          <div className="progress-bar bg-danger" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
        </div>
      </div>
    </div>
  </div>
  {/*--/Analytics*/}
  <div className="row mt-4">
    <div className="col-6 col-lg-3">
      <div className="card ">
        <div className="card-body p-3 d-flex align-items-center">
          <i className="icon-docs bg-danger p-3 font-2xl mr-3" />
          <div>
            <div className="text-value-sm text-danger">300</div>
            <div className="text-muted text-uppercase font-weight-bold small">Total Test
              Uploaded</div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-6 col-lg-3">
      <div className="card">
        <div className="card-body p-3 d-flex align-items-center">
          <i className="icon-user bg-warning p-3 font-2xl mr-3" />
          <div>
            <div className="text-value-sm text-warning">300</div>
            <div className="text-muted text-uppercase font-weight-bold small">Current Students
              Online</div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-6 col-lg-3">
      <div className="card">
        <div className="card-body p-3 d-flex align-items-center">
          <i className="icon-bell bg-success text-white p-3 font-2xl mr-3" />
          <div>
            <div className="text-value-sm text-success">150</div>
            <div className="text-muted text-uppercase font-weight-bold small">Test Published
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="col-6 col-lg-3">
      <div className="card">
        <div className="card-body p-3 d-flex align-items-center">
          <i className="icon-docs text-white bg-primary p-3 font-2xl mr-3" />
          <div>
            <div className="text-value-sm text-danger">200</div>
            <div className="text-muted text-uppercase font-weight-bold small">Assignment
              Uploaded</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/*--/row-*/}
  {/*- event section-*/}
  <div className="row">
    <div className="col-md-4 col-xs-12">
      {/*--notice--*/}
      <div className="card">
        <div className="card-header text-muted">
          <b>Notifications</b>
        </div>
        <div className="card-body scrollbar events-scroll">
          <div className="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>New Physics Assignement</strong> has updated. <a href="#"> View Now</a>
            <button type="button btn btn-primary" className="close" data-dismiss="alert" aria-label="Close">
              <i className="icon-close" />
            </button>
          </div>
          <div className="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields
            below.
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <i className="icon-close" />
            </button>
          </div>
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Holy guacamole!</strong> You should check in on some of those fields
            below.
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <i className="icon-close" />
            </button>
          </div>
        </div>
      </div>
      {/*--/to do list--*/}
    </div>
    {/*--/col-*/}
    <div className="col-md-4 col-xs-12">
      {/*--to do list--*/}
      <div className="card">
        <div className="card-header text-muted">
          <b> To Do Lists</b>
        </div>
        <div className="card-body scrollbar events-scroll">
          <div className="add-items d-flex"> <input type="text" className="form-control todo-list-input" style={{width: '75%'}} placeholder="What do you need to do today?" /> <button className="add btn btn-primary font-weight-bold todo-list-add-btn" style={{width: '25%', height: '20%', borderRadius: 'none !important'}}>Add</button>
          </div>
          <div className="list-wrapper">
            <ul className="d-flex flex-column-reverse todo-list">
              <li>
                <div className="form-check "> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> For what reason would it
                    be advisable. <i className="input-helper" /></label> <i className=" remove mdi mdi-close-circle-outline" /></div>
              </li>
              <li className="completed">
                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> For what reason
                    would it be advisable for me to think. <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
              </li>
              <li>
                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> it be advisable for me to
                    think about business content? <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
              </li>
              <li>
                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print Statements all <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
              </li>
              <li className="completed">
                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" defaultChecked /> Call Rampbo <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
              </li>
              <li>
                <div className="form-check"> <label className="form-check-label"> <input className="checkbox" type="checkbox" /> Print bills <i className="input-helper" /></label> </div> <i className="remove mdi mdi-close-circle-outline" />
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/*--/to do list--*/}
    </div>
    {/*--/col-*/}
    <div className="col-md-4 col-xs-12">
      {/*--Events--*/}
      <div className="card">
        <div className="card-header text-muted">
          <b>Upcoming events</b>
          <span className="float-md-right"><a href="#" data-toggle="modal" data-target="#modelEvents">+ Add Events</a></span>
        </div>
        <div className="card-body scrollbar events-scroll">
          <div className="media mt-3 ">
            {/*- event month & time--*/}
            <div className="events align-self-center mr-3">
              <p className="events-month"> DEC <br />
                <b className="events-date">24</b></p>
            </div>
            {/*--event month & time end---*/}
            {/*-- event time and topic start---*/}
            <div className="media-body events-info">
              <h6>04:50 PM</h6>
              <p><b>Physics Exam will be Held</b></p>
            </div>
            {/*-- event time and topic end--*/}
            <a href="#" data-toggle="modal" data-target="#deleteAlert" className="text-danger float-right"><i className="icon-trash" /> Delete Event</a>
          </div>
          {/*- event section end----*/}
          <hr />
        </div>
      </div>
      {/*--/events--*/}
    </div>
    {/*--/col-*/}
  </div>
  {/*-/event section-*/}

  {/* Modal events*/}
  <div className="modal fade" id="modelEvents" tabIndex={-1} role="dialog" aria-labelledby="modelTitlemodelEvents" aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Add New Events</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">
          <div className="container-fluid">
            <div className="form-group">
              <b>Name of the event: &nbsp;</b>
              <input type="text" className="form-control-sm" />
            </div>
            <div className="form-group">
              <b>Date: </b>
              <input type="date" className="form-control-sm" />
              <b>Time: </b>
              <input type="time" className="form-control-sm" />
            </div>
            <div className="form-group">
              <b>Set this event for: &nbsp;</b>
              <input type="checkbox" /> All 
              <input type="checkbox" /> Students
              <input type="checkbox" /> Teachers
            </div>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
  {/*-----/modal events--*/}
  {/*-modal for delete*/}
  <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-body">
          <div className="container-fluid text-center">
            <h4><span className="font-weight-bold">Are You Sure?</span></h4>
            <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
            <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
            <button type="button" className="btn btn-warning">Yes</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/*-/modal for delete */}


</div>

        );


    }
}
export default Dashboard