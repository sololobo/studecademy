import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Asslist from './mentorassignmentList'
import Asscheck from './mentorAssignmentCheck'
import Cookies from 'js-cookie';
import swal from 'sweetalert';
import $ from 'jquery';



class mentorAssignmentView extends Component {
    constructor(props) {
        super(props);
        this.checkAssignment = this.checkAssignment.bind(this);
        this.assList = this.assList.bind(this);
        this.openTab =this.openTab.bind(this);

        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            assignment: [],
            marks_obtained:0


        }
    }

    assList() {

        ReactDOM.render(<Asslist />, document.getElementById('contain1'))

    }
    checkAssignment(assignment) {
        let props = {
            assignmentview:assignment,
            fromAssignmentList:this.props.data
            }

        ReactDOM.render(<Asscheck data={props} />, document.getElementById('contain1'))

    }

    openTab(th)
            {
                window.open(th.name,'_blank');
            }
   

    componentDidMount() {


        console.log(this.props.data.assignmentID)

        let url = global.API + "/studapi/public/api/viewallassignsubmit";
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },

            body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'assignment_i_d': this.props.data.assignmentID })
        })
            .then(res => res.json())
            .then(json => {
                console.log(json)
                this.setState({
                    isLoaded: true,
                    assignment: json,

                })
            });



    }


    updateAssignment(submissionID){

        
        let url = global.API + "/studapi/public/api/updateassignsubmission";
        

        //let marks = document.getElementById('marks').value;
        let data = this.state;
        const fd = new FormData();
        fd.append('submission_i_d', submissionID);
        //fd.append('marks_obtained', marks);
        console.log(fd);
        
       
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Assignment Updated Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    this.componentDidMount();
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Assignment not updated Sucessfully",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })


    }

    render() {
        var { isLoaded, assignment, unitList, sessonList1, classList, semesterList, sectionList, subjectList } = this.state;

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">View Submissions</span>
                        <span className="float-right"><a onClick={this.assList} ><i className="icon-action-undo"></i> &nbsp;Back to Assignments/Homework</a></span>
                    </div>
                    <div className="card-body">
                        <h4 className="font-weight-bold">Assignment 1  | Maximum Marks: {this.props.data.maximumMarks}</h4>
                        <hr />
                        <div className="table-responsive">
                            <table className="table-bordered table">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0">Roll No</th>
                                        <th className="border-0">Name</th>
                                        <th className="border-0">className</th>
                                        <th className="border-0">Submitted on</th>
                                        <th className="border-0">Marks Obtained</th>
                                        <th className="border-0" ></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {assignment.map(assignment => (
                                        <tr>
                                            <td className="border-0">{assignment.rollNo}</td>
                                            <td className="border-0">{assignment.studentName}</td>
                                            <td className="border-0">{this.props.data.departmentName}</td>
                                            <td className="border-0">{assignment.submissionDate}</td>
                                            <td className="border-0"><input type="number" id="marks_obtained" name="marks_obtained" onChange={(data) => { this.setState({ marks_obtained: data.target.value }) }}  defaultValue={assignment.marksObtained} className="form-control-sm" /></td>
                                            <td className="border-0">
                                                <a onClick={()=>this.openTab(this)} href={global.img + assignment.submissionURL} className=" btn btn-primary text-white"><i className="icon-cloud-download"></i> Dowload Submitted Copy</a>
                                                <a onClick={()=>this.checkAssignment(assignment)} className=" btn btn-warning text-white"><i className="icon-cloud-download"></i> Check Copy</a>
                                                <button onClick={()=>this.updateAssignment(assignment.submissionID)} className="btn btn-success" type="submit">Save and Update</button>
                                           </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        );


    }
}
export default mentorAssignmentView