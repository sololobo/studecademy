import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Assview from './mentorAssignmentView';
import Assignmentedit from './assignmentEdit';
import Cookies from 'js-cookie';
import $ from 'jquery';
import swal from 'sweetalert';
import { SearchForObjectsWithName } from '../../searchFunction/searchComponent';
import { SearchForObjectsWithParams } from '../../searchFunction/searchDropdownComponent';

class mentorAssignmentList extends Component {
    constructor(props) {
        super(props);
        this.assignmentEdit = this.assignmentEdit.bind(this);
        this.showSessionList = this.showSessionList.bind(this);
        this.showSessionAdd = this.showSessionAdd.bind(this);
        this.showSession = this.showSession.bind(this);
        this.showDepartment = this.showDepartment.bind(this);
        this.showDepartmentAdd = this.showDepartmentAdd.bind(this);
        this.showDepartmentList = this.showDepartmentList.bind(this);
        this.showSemesterList = this.showSemesterList.bind(this);
        this.showSemesterAdd = this.showSemesterAdd.bind(this);
        this.showSemester = this.showSemester.bind(this);
        //this.showSemesterSection = this.showSemesterSection.bind(this);
        this.showSectionSubjectAdd = this.showSectionSubjectAdd.bind(this);
        this.showSectionList = this.showSectionList.bind(this);
        this.showSection = this.showSection.bind(this);
        this.showSubject = this.showSubject.bind(this);
        this.showSectionSubjectList = this.showSectionSubjectList.bind(this);
        this.assignmentViews = this.assignmentViews.bind(this);
        this.handleSubjectChange = this.handleSubjectChange.bind(this);
        this.handleSectionChange = this.handleSectionChange.bind(this);
        this.state = {
            organisation_i_d: Cookies.get('orgid'),
            teacher_i_d: Cookies.get('teacherid'),
            assignmentList: [],
            chapter_names: "",


            assignment_name: "",

            maximum_marks: "",
            published_on: "",
            last_submission_date: "",
            published_till: "",

            unitList: [],
            sessonList1: [],
            classList: [],
            semesterList: [],
            sectionList: [],
            subjectList: [],
            searchString: "",
            searchParams: [],
            ii: 0

        }
        this.assignmentSearchList = []
    }

    assignmentEdit(assignment) {

        ReactDOM.render(<Assignmentedit  data = {assignment} unitList = {this.state.unitList} organisation = {this.state.organisation_i_d}/>, document.getElementById('updateAssignmentContent'));
    }


    deleteAssignment(id1) {
        console.log(id1);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover  Unit!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let url = global.API + "/studapi/public/api/deleteassin";
                    fetch(url, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json",
                            "Accept": "application/json"
                        },

                        body: JSON.stringify({ 'orgID': Cookies.get('orgid'), 'assignment_i_d': id1 })
                    })
                        .then((resp1) => resp1.json()
                            .then((resp) => {
                                if (resp[0]['result'] == 1) {
                                    swal("Assignment deleted!", {
                                        icon: "success",
                                    });
                                    this.componentDidMount();
                                }

                            }));
                }

            });
    }
    assignmentViews(assignment) {

        ReactDOM.render(<Assview data={assignment} />, document.getElementById('contain1'))

    }

    showSessionList() {
        let val = document.getElementById("unit1").value;

        this.showSession(val);


    }

    showSessionAdd() {
        let val = document.getElementById("unit2").value;
        this.showSession(val)

    }

    showSession(val) {


      this.updateParamsForSearch('unitID', val)
        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'unit_i_d': val })

        };


        let sessionURL = global.API + "/studapi/public/api/getallsessionidname";
        fetch(sessionURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sessonList1: json
                })
            });
    }

    //Department

    showDepartmentList() {
        let val = document.getElementById("session1").value;

        this.showDepartment(val);
    }

    showDepartmentAdd() {
        let val = document.getElementById("session2").value;

        this.showDepartment(val);

    }

    showDepartment(val) {

      this.updateParamsForSearch('sessionID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'session_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getalldepartmentidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    classList: json
                })
            });
    }

    //Semester

    showSemesterList() {
        let val = document.getElementById("department1").value;

        this.showSemester(val);
    }

    showSemesterAdd() {
        let val = document.getElementById("department2").value;

        this.showSemester(val);

    }

    showSemester(val) {

      this.updateParamsForSearch('departmentID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'department_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsemidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    semesterList: json
                })
            });
    }


    //Section

    showSectionList() {
        let val = document.getElementById("semester1").value;

        this.showSection(val);
    }

    showSectionSubjectList() {

        let val = document.getElementById("semester1").value;
        this.updateParamsForSearch('semesterID', val);

        this.showSection(val);
        this.showSubject(val);

    }


    showSectionSubjectAdd() {

        let val = document.getElementById("semester2").value;


        this.showSection(val);
        this.showSubject(val);

    }

    showSection(val) {

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }

 /*   showSemesterSection(val) {

      this.updateParamsForSearch('semesterID', val)

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: { 'semester_i_d': val }

        };


        let classURL = global.API + "/studapi/public/api/getallsectionidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    sectionList: json
                })
            });
    }*/

    //Subject



    showSubject(val) {

        const requestOptions1 = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'semester_i_d': val })

        };


        let classURL = global.API + "/studapi/public/api/getallsubjectidname";
        fetch(classURL, requestOptions1)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    subjectList: json
                })
            });
    }

    submit() {
        var i = 0;

        if (i < 10) {
            if (document.getElementById("unit2").value == "") {
                document.getElementById("unit2Error").innerHTML = "*Please fill this field";
                document.getElementById("unit2").focus();
                document.getElementById("unit2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("unit2").value != "") {
                document.getElementById("unit2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("unit2").style.borderColor = "";
            }

            if (document.getElementById("session2").value == "") {
                document.getElementById("session2Error").innerHTML = "*Please fill this field";
                document.getElementById("session2").focus();
                document.getElementById("session2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("session2").value != "") {
                document.getElementById("session2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("session2").style.borderColor = "";
            }

            if (document.getElementById("department2").value == "") {
                document.getElementById("department2Error").innerHTML = "*Please fill this field";
                document.getElementById("department2").focus();
                document.getElementById("department2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("department2").value != "") {
                document.getElementById("department2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("department2").style.borderColor = "";
            }

            if (document.getElementById("semester2").value == "") {
                document.getElementById("semester2Error").innerHTML = "*Please fill this field";
                document.getElementById("semester2").focus();
                document.getElementById("semester2").style.borderColor = "#FF0000";
            }

            if (document.getElementById("semester2").value != "") {
                document.getElementById("semester2Error").innerHTML = "";
                i = i + 1;
                document.getElementById("semester2").style.borderColor = "";
            }
            if (document.getElementById("section").value == "") {
                document.getElementById("sectionError").innerHTML = "*Please fill this field";
                document.getElementById("section").focus();
                document.getElementById("section").style.borderColor = "#FF0000";
            }

            if (document.getElementById("section").value != "") {
                document.getElementById("sectionError").innerHTML = "";
                i = i + 1;
                document.getElementById("section").style.borderColor = "";
            }

            if (document.getElementById("subject_i_d").value == "") {
                document.getElementById("subject_i_dError").innerHTML = "*Please fill this field";
                document.getElementById("subject_i_d").focus();
                document.getElementById("subject_i_d").style.borderColor = "#FF0000";
            }

            if (document.getElementById("subject_i_d").value != "") {
                document.getElementById("subject_i_dError").innerHTML = "";
                i = i + 1;
                document.getElementById("subject_i_d").style.borderColor = "";
            }


            if (document.getElementById("published_on").value == "") {
                document.getElementById("published_onError").innerHTML = "*Please fill this field";
                document.getElementById("published_on").focus();
                document.getElementById("published_on").style.borderColor = "#FF0000";
            }

            if (document.getElementById("published_on").value != "") {
                document.getElementById("published_onError").innerHTML = "";
                i = i + 1;
                document.getElementById("published_on").style.borderColor = "";
            }


            if (document.getElementById("last_submission_date").value == "") {
                document.getElementById("last_submission_dateError").innerHTML = "*Please fill this field";
                document.getElementById("last_submission_date").focus();
                document.getElementById("last_submission_date").style.borderColor = "#FF0000";
            }

            if (document.getElementById("last_submission_date").value != "") {
                document.getElementById("last_submission_dateError").innerHTML = "";
                i = i + 1;
                document.getElementById("last_submission_date").style.borderColor = "";
            }


             if (document.getElementById("assignment_name").value == "") {
                document.getElementById("assignment_nameError").innerHTML = "*Please fill this field";
                document.getElementById("assignment_name").focus();
                document.getElementById("assignment_name").style.borderColor = "#FF0000";
            }

            if (document.getElementById("assignment_name").value != "") {
                document.getElementById("assignment_nameError").innerHTML = "";
                i = i + 1;
                document.getElementById("assignment_name").style.borderColor = "";
            }
            if (document.getElementById("assignmenturl").value == "") {
                document.getElementById("assignment_u_r_lError").innerHTML = "*Please fill this field";
                document.getElementById("assignmenturl").focus();
                document.getElementById("assignmenturl").style.borderColor = "#FF0000";
            }

            if (document.getElementById("assignmenturl").value != "") {
                document.getElementById("assignment_u_r_lError").innerHTML = "";
                i = i + 1;
                document.getElementById("assignmenturl").style.borderColor = "";
            }



            if (i == 10) {
                console.log(i);
                this.state.ii = 10;
            }
        }
        if (this.state.ii == 10) {

        console.log(this.state);
        let url = global.API + "/studapi/public/api/addassignment";
        let data = this.state;
        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
        $.each(data, function (key, value) {
            fd.append(key, value);
        })
        fd.append('assignment_u_r_l', document.getElementById("assignmenturl").files[0]);
        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp == 1) {
                    swal({
                        title: "Done!",
                        text: "Assignment Added Sucessfully",
                        icon: "success",
                        button: "OK",
                    });
                    $('#uploadAssignment').modal('hide');
                    this.componentDidMount();
                }
                else {
                    swal({
                        title: "Error!",
                        text: "Assignment not  Added",
                        icon: "warning",
                        button: "OK",
                    });
                }
            })
        })
    }}

    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }


    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 'orgID': Cookies.get('orgid') })

        };


        let url = global.API + "/studapi/public/api/viewallassignments";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    assignmentList: json
                })
            });

        let unitURL = global.API + "/studapi/public/api/getallunitidname";
        fetch(unitURL, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    unitList: json
                })
            });




    }



    updateParamsForSearch(columnName, columnValue) {
        let alreadyPresent = false
        let index = -1
        var array = [...this.state.searchParams]
        columnValue = columnValue.toString()
        //iterating over array to find if columnName is alreadyPresent or not
        array.forEach(function (param, i) {
            if (columnName == param[0]) {
                alreadyPresent = true;
                index = i;
            }
        })
        //if the columnName is not present push it in the array
        if (!alreadyPresent) {
            array.push([columnName, columnValue])
        } else {
            //if the value at index is empty delete it
            let temp = []
            for (let i = 0; i <= index; i++) {
                temp.push(array[i])
            }
            array = temp
            if (columnValue == "") {
                array.splice(index, 1)
            } else {
                //other wise update it to columnValue
                array[index][1] = columnValue;
            }

        }
        //setting searchParams to be equal to the modified array
        this.setState({ searchParams: array })
    }


    handleSubjectChange(val){
      this.setState({subjectID : val})
      this.updateParamsForSearch("subjectID", val)
   }

   handleSectionChange(val){
      this.setState({ section_i_d: val })
      this.updateParamsForSearch("sectionID", val)
   }
   //this function toggles the state of all the options
   selectAllOptions() {
    if (!$('.checkBoxForDeletion:not(:checked)').length) {
        $(".checkBoxForDeletion").prop('checked', false)
    } else {
        $(".checkBoxForDeletion").prop('checked', true)
    }

}
//Refresh Page
   refreshPage(){
      this.componentDidMount();
   }

    render() {
        var { isLoaded, assignmentList, unitList, sessonList1, classList, semesterList, sectionList, subjectList } = this.state;
        if (this.state.searchParams.length > 0) {
            this.assignmentSearchList = SearchForObjectsWithParams(assignmentList, this.state.searchParams)
        }
        else {
            this.assignmentSearchList = assignmentList;
        }
        if (this.state.searchString.replace(/\s/g, '') != '') {
            this.assignmentSearchList = SearchForObjectsWithName(this.assignmentSearchList, this.state.searchString)
        }
        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="h3 font-weight-bold">Assignments/Homework</span>
                        <span className="float-right"><a href="#" className="btn btn-primary text-white" data-target="#uploadAssignment" data-toggle="modal">+ Add New Assignments</a></span>
                    </div>
                    <div className="card-body">

                    <div className="card">
                            <div className="card-body pt-0 pb-0">

                                <button className="btn btn-warning float-left" onClick = {() => this.selectAllOptions()}>Select All</button>

                                <button className="btn btn-primary float-left" onClick={() => this.deleteSelectedOptions()}>Delete Selected Options</button>
                                <button className="btn btn-primary float-left" onClick={() => this.refreshPage()}>Refresh</button>
                              
                                <button className="btn btn-success float-left" href="#" data-toggle="collapse" data-target="#commentArea" aria-expanded="false" aria-controls="commentArea" >Filter</button>
                                <input type="text" className="float-right mt-2 form-control-sm" placeholder="Type to search" onChange={(e) => this.setState({ searchString: e.target.value })} ></input>

                            </div>
                        </div>
                        {/*-modal filter*/}
                        <div className="  ">
                            <div className="collapse" id="commentArea">
                                <div className="media-body">

                                    <div className="row">
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select UNIT:</b></label>
                                            <select name="" id="unit1" className=" form-control" onChange={this.showSessionList}>
                                                <option value="" selected>Select</option>
                                                {unitList.map(unit => (
                                                    <option value={unit.unitID}>{unit.unitName}</option>

                                                ))}
                                            </select>

                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Session:</b></label>
                                            <select name="" id="session1" className=" form-control" onChange={this.showDepartmentList}>
                                                <option value="" selected>Select</option>
                                                {sessonList1.map(session => (
                                                    <option value={session.sessionID}>{session.session}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b>Select Class/Department:</b></label>
                                            <select name="" id="department1" onChange={this.showSemesterList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {classList.map(classes => (
                                                    <option value={classes.departmentID}>{classes.departmentName}</option>

                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row">

                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Semester:</b></label>
                                            <select name="" id="semester1" onChange={this.showSectionSubjectList} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {semesterList.map(semester => (
                                                    <option value={semester.semesterID}>{semester.semesterName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Subject:</b></label>
                                            <select name="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.handleSubjectChange(data.target.value) }} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {subjectList.map(subject => (
                                                    <option value={subject.subjectID}>{subject.subjectName}</option>

                                                ))}
                                            </select>
                                        </div>
                                        <div className="col-md-4 form-group">
                                            <label htmlFor className="mb-0 font-weight-bold"> <b> Select Section:</b></label>

                                            <select name="section_i_d" value={this.state.section_i_d} onChange={(data) => { this.handleSectionChange(data.target.value) }} className=" form-control">
                                                <option value="" selected>Select</option>
                                                {sectionList.map(section => (
                                                    <option value={section.sectionID}>{section.sectionName}</option>

                                                ))}

                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/*-/filter*/}


                        </div>

                        <hr />
                        <div className="table-responsive">
                            <table className="table table-bordered">
                                <thead>
                                    <tr className="bg-light">
                                        <th className="border-0"><input type="checkbox" onClick={() => this.selectAllOptions()} /></th>
                                        <th className="border-0">Session</th>
                                        <th className="border-0">className/Department</th>
                                        <th className="border-0">Semester</th>
                                        <th className="border-0">Subject</th>
                                        <th className="border-0">Name</th>
                                        <th className="border-0">Maximum Marks</th>
                                        <th className="border-0">Published on</th>
                                        <th className="border-0">Last Submission Date</th>
                                        <th className="border-0">Published Till</th>
                                        <th className="border-0">No. Users Submission</th>
                                        <th className="border-0">Current Status</th>
                                        <th className="border-0">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.assignmentSearchList.map(assignment => (

                                        <tr>
                                          <td className="border-0"><input type="checkbox" className = "checkBoxForDeletion" value = {assignment.assignmentID} name="" id="" /></td>
                                             <td className="border-0"> {assignment.session}</td>
                                            <td className="border-0"> {assignment.departmentName}</td>
                                            <td className="border-0"> {assignment.semesterName}</td>
                                            <td className="border-0"> {assignment.subjectName}</td>
                                            <td className="border-0"><a href={global.img + assignment.assignmentURL} download=""  target="_blank">{assignment.assignmentName}</a></td>
                                            <td className="border-0"> {assignment.maximumMarks}</td>
                                            <td className="border-0">{assignment.publishedOn}</td>
                                            <td className="border-0">{assignment.lastSubmissionDate}</td>
                                            <td className="border-0">{assignment.publishedTill}</td>
                                            <td className="border-0">{assignment.numOfSubmission}</td>
                                            <th className="border-0"><span className="badge badge-pill bg-light">{assignment.status}</span></th>
                                            <td className="border-0">
                                                <span><a href="#" data-toggle="modal" onClick={()=>this.assignmentEdit(assignment)} data-target="#updateAssignment" data-placement="top" title="Update Assignment"><i className="icon-pencil"></i> </a></span>
                                                <span><a  onClick={()=>this.assignmentViews(assignment)} className="text-primary" data-placement="top" title="View Submission"><i className="icon-eye"></i></a></span>
                                                <span><a onClick={() => this.deleteAssignment(assignment.assignmentID)} className="text-danger"><i className="icon-trash" data-placement="top" title="Delete Assignments/Homework"></i></a></span>

                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                {/*modal for upload  Assignment*/}
                <div className="modal fade" id="uploadAssignment" tabIndex={-1} role="dialog" aria-labelledby="modelTitleuploadAssignment" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid ">
                                    <h4><span className="font-weight-bold text-center">Upload Homework/Assignment</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit<span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>
                                                <span id="unit2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="semester2" id="semester2" onChange={this.showSectionSubjectAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                                <span id="semester2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>
                                                <span id="session2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Section <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="section_i_d" id="section" value={this.state.section_i_d} onChange={(data) => { this.setState({ section_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {sectionList.map(section => (
                                                        <option value={section.sectionID}>{section.sectionName}</option>

                                                    ))}

                                                </select>
                                                <span id="sectionError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">ClassName/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                                <span id="department2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject <span className="text-danger"><strong>*</strong></span></label>
                                                <select name="subject_i_d" id="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.setState({ subject_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                                <span id="subject_i_dError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label className="font-weight-bold"> Chapter :</label>
                                                <input type="text" name="chapter_names" value={this.state.chapter_names} onChange={(data) => { this.setState({ chapter_names: data.target.value }) }} className="form-control" />

                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Published on <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="date" name="published_on" id="published_on" value={this.state.published_on} onChange={(data) => { this.setState({ published_on: data.target.value }) }} className="form-control" />
                                            <span id="published_onError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Published Till :</label>
                                            <input type="date" name="published_till" value={this.state.published_till} onChange={(data) => { this.setState({ published_till: data.target.value }) }} className="form-control" />

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Homework Active till <span className="text-danger"><strong>*</strong></span></label>
                                            <input type="date" name="last_submission_date" id="last_submission_date" value={this.state.last_submission_date} onChange={(data) => { this.setState({ last_submission_date: data.target.value }) }} className="form-control" />
                                            <span id="last_submission_dateError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Maximum Marks :</label>
                                            <input type="text" name="maximum_marks" value={this.state.maximum_marks} onChange={(data) => { this.setState({ maximum_marks: data.target.value }) }} className="form-control" />

                                        </div>
                                    </div>
                                    <div className="form-group row mt-3">
                                        <div className="col-md-12">
                                            <label htmlFor className="mb-0 form-group  font-weight-bold"> Name of the  Assignment <span className="text-danger"><strong>*</strong></span></label> &nbsp;
                                             <input type="text" name="assignment_name" id="assignment_name" value={this.state.assignment_name} onChange={(data) => { this.setState({ assignment_name: data.target.value }) }} className="form-control " />
                                             <span id="assignment_nameError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        </div>
                                        <div className="form-group row">
                                        <div className="col-md-6">
                                            <label htmlFor className="mb-0 form-group font-weight-bold">Upload Homework (Only .docx, .pdf allowed)<span className="text-danger"><strong>*</strong></span></label>
                                            <input type="file" name="assignment_u_r_l" id="assignmenturl" className="form-control " />
                                            <span id="assignment_u_r_lError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor className="mb-0 form-group font-weight-bold">Upload Homework (Only .docx, .pdf allowed)</label>
                                            <input type="file" name="solution_u_r_l" id="solutionurl" className="form-control " />

                                        </div>
                                    </div>
                                    <hr />
                                    <div className="float-right ">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button" onClick={() => { this.submit() }} className="btn btn-primary text-white">Upload Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-/modal for upload assignment*/}
                {/*-modal for upload  Result*/}
                <div className="modal fade" id="updateAssignment" tabIndex={-1} role="dialog" aria-labelledby="modelTitleuploadAssignment" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-100" role="document">
                     <div id="updateAssignmentContent"></div>
                     {/*
                        <div className="modal-content">
                            <div className="modal-body">
                                <div className="container-fluid ">
                                    <h4><span className="font-weight-bold text-center">Upload Homework/Assignment</span></h4>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Unit<span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="" id="unit2" className="form-control" onChange={this.showSessionAdd}>
                                                    <option value="" selected>Select</option>
                                                    {unitList.map(unit => (
                                                        <option value={unit.unitID}>{unit.unitName}</option>

                                                    ))}
                                                </select>
                                                <span id="unit2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Semester <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="semester2" id="semester2" onChange={this.showSectionSubjectAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {semesterList.map(semester => (
                                                        <option value={semester.semesterID}>{semester.semesterName}</option>

                                                    ))}
                                                </select>
                                                <span id="semester2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="form-group">

                                                <label for="" className="mb-0 font-weight-bold">Session <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="session2" className="form-control" onChange={this.showDepartmentAdd}>
                                                    <option value="" selected>Select</option>
                                                    {sessonList1.map(session => (
                                                        <option value={session.sessionID}>{session.session}</option>

                                                    ))}
                                                </select>
                                                <span id="session2Error" class="text-danger font-weight-bold"></span>


                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Section <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select name="section_i_d" value={this.state.section_i_d} onChange={(data) => { this.setState({ section_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {sectionList.map(section => (
                                                        <option value={section.sectionID}>{section.sectionName}</option>

                                                    ))}

                                                </select>
                                                <span id="sectionError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                        <div className="col-md-4">

                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">ClassName/Department <span className="text-danger"><strong>*</strong></span>:</label>
                                                <select id="department2" onChange={this.showSemesterAdd} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {classList.map(classes => (
                                                        <option value={classes.departmentID}>{classes.departmentName}</option>

                                                    ))}
                                                </select>
                                                <span id="department2Error" class="text-danger font-weight-bold"></span>

                                            </div>
                                            <div className="form-group">
                                                <label for="" className="mb-0 font-weight-bold">Subject <span className="text-danger"><strong>*</strong></span></label>
                                                <select name="subject_i_d" id="subject_i_d" value={this.state.subject_i_d} onChange={(data) => { this.setState({ subject_i_d: data.target.value }) }} className="form-control">
                                                    <option value="" selected>Select</option>
                                                    {subjectList.map(subject => (
                                                        <option value={subject.subjectID}>{subject.subjectName}</option>

                                                    ))}
                                                </select>
                                                <span id="subject_i_dError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label className="font-weight-bold"> Chapter :</label>
                                                <input type="text" name="chapter_names" id="chapter_names" value={this.state.chapter_names} onChange={(data) => { this.setState({ chapter_names: data.target.value }) }} className="form-control" />
                                                <span id="chapter_namesError" class="text-danger font-weight-bold"></span>

                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Published on <span className="text-danger"><strong>*</strong></span>:</label>
                                            <input type="date" name="published_on" id="published_on" value={this.state.published_on} onChange={(data) => { this.setState({ published_on: data.target.value }) }} className="form-control" />
                                            <span id="published_onError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Published Till :</label>
                                            <input type="date" name="published_till" value={this.state.published_till} onChange={(data) => { this.setState({ published_till: data.target.value }) }} className="form-control" />
                                            <span id="published_tillError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Homework Active till <span className="text-danger"><strong>*</strong></span></label>
                                            <input type="date" name="last_submission_date" id="last_submission_date" value={this.state.last_submission_date} onChange={(data) => { this.setState({ last_submission_date: data.target.value }) }} className="form-control" />
                                            <span id="last_submission_dateError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-3">
                                            <label htmlFor className="mb-0 font-weight-bold">Maximum Marks :</label>
                                            <input type="text" name="maximum_marks" id="maximum_marks" value={this.state.maximum_marks} onChange={(data) => { this.setState({ maximum_marks: data.target.value }) }} className="form-control" />
                                            <span id="maximum_marksError" class="text-danger font-weight-bold"></span>

                                        </div>
                                    </div>
                                    <div className="form-group row mt-3">
                                        <div className="col-md-12">
                                            <label htmlFor className="mb-0 form-group  font-weight-bold"> Name of the  Assignment <span className="text-danger"><strong>*</strong></span></label> &nbsp;
                                             <input type="text" name="assignment_name" id="assignment_name" value={this.state.assignment_name} onChange={(data) => { this.setState({ assignment_name: data.target.value }) }} className="form-control " />
                                             <span id="assignment_nameError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        </div>
                                        <div className="form-group row">
                                        <div className="col-md-6">
                                            <label htmlFor className="mb-0 form-group font-weight-bold">Upload Homework (Only .docx, .pdf allowed)<span className="text-danger"><strong>*</strong></span></label>
                                            <input type="file" name="assignment_u_r_l" id="assignmenturl" className="form-control " />
                                            <span id="assignment_u_r_lError" class="text-danger font-weight-bold"></span>

                                        </div>
                                        <div className="col-md-6">
                                            <label htmlFor className="mb-0 form-group font-weight-bold">Upload Homework (Only .docx, .pdf allowed)</label>
                                            <input type="file" name="solution_u_r_l" id="solutionurl" className="form-control " />

                                        </div>
                                    </div>
                                    <hr />
                                    <div className="float-right ">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button"  onClick={() => { this.onEditAssignmentFormSubmit() }}  className="btn btn-primary text-white">Upload Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    */}
                    </div>
                </div>

                {/*-/modal for upload Result*/}

            </div>
        );


    }
}
export default mentorAssignmentList
