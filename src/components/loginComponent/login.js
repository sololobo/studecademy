import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import logo from '../includes/media/tm-logo.png';
import swal from 'sweetalert';
import Cookies from 'js-cookie';
import $ from 'jquery';
import Changepass from '../forgetPassword/forgetPassword'
import   '../includes/global1';
class loginComponent extends Component{

    constructor(){
        super();
        this.state={
    
            email_i_d:"",
           pwd:"",
           ii: 0
    
        }
       this.pass=this.pass.bind(this)
    }
pass(){
    var email1 = document.getElementById("email1").value;
    var i = 0;
    var emailCheck = /^[A-Za-z0-9_.]{3,60}@[A-Za-z0-9]{3,60}[.]{1}[A-Za-z.]{2,6}$/;
    if (i < 1) {
    if (document.getElementById("email1").value == "") {
        document.getElementById("email1Error").innerHTML = "*Please fill this field";
        document.getElementById("email1").focus();
        document.getElementById("email1").style.borderColor = "#FF0000";
    }
    if (document.getElementById("email1").value != "") {
        if (!emailCheck.test(email1)) {
            document.getElementById("email1Error").innerHTML = "*Invalid Email Id";
            document.getElementById("email1").focus();
            document.getElementById("email1").style.borderColor = "#FF0000";
        }

        if (emailCheck.test(email1)) {
            document.getElementById("email1Error").innerHTML = "";
            i = i + 1;
            document.getElementById("email1").style.borderColor = "";
        }
    }
    if (i == 1) {
        console.log(i);
        this.state.ii = 1;
    }
}
if (this.state.ii == 1) {
    let em=document.getElementById("email1").value;
    console.log(em)
    

       

        let url = global.API + "/studapi/public/api/changepass1";
        let data = this.state;

        const fd = new FormData();
        //formData.append('file', this.state.org_logo);
            fd.append('email', em);

        console.log(fd);
        fetch(url, {
            method: 'POST',

            body: fd
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                if (resp[0]['result'] == 1) {
                    ReactDOM.render(<Changepass  email={em} />,document.getElementById('root'))
                    $('#modelId').hide()
                
                    $('.modal-backdrop').hide();

                }
                else {
                    swal({
                        title: "Error",
                        text: "No such user found",
                        icon: "warning",
                        button: "OK",
                    });
                   // $('#modalId').hide();
                    $('#modelId').hide()
                
$('.modal-backdrop').hide();

                }
            })
        })
        console.log(data);
    

}}y
componentDidMount(){
    if((Cookies.get('username'))!=undefined){
        let ut=Cookies.get('usertype');
        if(ut==11){
            window.location = "/organisation/dashboard"
        }
        else  if(ut==12){
            window.location = "/teacher/dashboard"
        }
        else  if(ut==13){
            window.location = "/student/dashboard"
        }
        else {
            window.location = "/admin/dashboard"
        }
       }
}



submit()
{
    
    global.user2="changed";
    if(document.getElementById("user1").value=="" || document.getElementById("pass1").value=="" )
    {
        swal({
            title: "Oops",
            text: "Please enter both username and password!",
            icon: "warning",
          });
    }
    else {
       var  cvalue=  document.getElementById("user1").value;
    console.log(this.state);
    let url = global.API + "/studapi/public/api/loginusers";
    //console.warn(window.API);
    let data=this.state;
    fetch(url,{
        method:'POST',
        headers:{
            "Content-Type":"application/json",
            "Accept":"application/json"
        },
        body:JSON.stringify(data)
    }).then((result)=>{
        result.json().then((resp)=>{
            console.warn("resp",resp)
            if(resp==11){
                var d = new Date();
  d.setTime(d.getTime() + (1*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "username" + "=" + cvalue +  ";path=/";
  let cvalue1=11;
  document.cookie = "usertype" + "=" + cvalue1 + ";path=/";
            
                               window.location = "/organisation/dashboard";
            }
            else if(resp==12){
                var d = new Date();
  d.setTime(d.getTime() + (1*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "username" + "=" + cvalue +  ";path=/";
  let cvalue1=12;
  document.cookie = "usertype" + "=" + cvalue1 +  ";path=/";
            
                               window.location = "/teacher/dashboard";
            }
            else if(resp==13){
                var d = new Date();
  d.setTime(d.getTime() + (1*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "username" + "=" + cvalue + ";path=/";
  let cvalue1=13;
  document.cookie = "usertype" + "=" + cvalue1 + ";path=/";
            
                               window.location = "/student/dashboard";
            }
            else if(resp==14){
                var d = new Date();
  d.setTime(d.getTime() + (1*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "username" + "=" + cvalue + ";path=/";
  let cvalue1=14;
  document.cookie = "usertype" + "=" + cvalue1 +  ";path=/";
            
                               window.location = "/admin/dashboard";
            }
            else if(resp==2){
                swal({
                    title: "Warning",
                    text: "Wrong Username and/or Password!",
                    icon: "warning",
                  });
            }
            else if(resp==3){
                swal({
                    title: "Warning",
                    text: "You are not registered!",
                    icon: "warning",
                  });
            }
            else{
                swal({
                    title: "OOPs",
                    text: "Please Verify your email!",
                    icon: "warning",
                  });
            }
              }
        )
    });
}
}

    render(){

        return (
            
            <div className="container-modal">
        <div className="row h-100 align-items-center">
            <div className="col-md-4"></div>
            <div className="col-md-4 px-5 py-5 bg-white login">
                <img src={logo} alt="" className="login-logo" />
                <div className="mt-4">
                    <div className="alert alert-success" role="alert" style={{display:"none"}} id="login1">
                        <i className="fa fa-check-circle"></i>&nbsp; Successfully Logged in ! Redirecting...
                    </div>
                    <div className="alert alert-warning" role="alert" style={{display:"none"}} id="wrong">
                        <i className="fa fa-warning"></i>&nbsp; Wrong Username or Password ! Please try again
                    </div>
                </div>
                <h2 className="font-weight-bold mb-0">Login</h2>
                <hr />
                <div className="form-group login-form-inputs">
                    <label for="" className="mb-0">Enter your Username/Email:</label>
                    <input type="text" id="user1" name="email_i_d" value={this.state.email_i_d} onChange={(data) => {this.setState({email_i_d:data.target.value})}} className="form-control mb-2" required />
                    <label for="" className="mb-0">Enter your Password:</label>
                    <input type="password" id="pass1" name="pwd" value={this.state.pwd} onChange={(data) => {this.setState({pwd:data.target.value})}} className="form-control mb-2" />
                    
                    <a href="#" className="text-danger float-right" data-target="#modelId" data-toggle="modal">Forgot Password?</a>
                    <br />
                    <button type="submit" onClick={()=>{this.submit()}} className="mt-2 btn btn-primary form-control">Login</button>
                </div>
            </div>
             <div className="col-md-4"></div>
         </div>
    

   
    <div className="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
                    <div className="modal-header">
                            <h5 className="modal-title">Forgot Password </h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                <div className="modal-body">
                    <div className="container">
                            <label for="" className="mb-0">Enter Your Email ID: </label>
                            <input type="text" className="form-control mb-2" id="email1" />
                            <span id="email1Error" class="text-danger font-weight-bold"></span>

                            <button type="submit" className="btn btn-primary form-control mb-4" onClick={()=>{this.pass()}}>Next</button>
                            

                           
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


        );


    }
}
export default loginComponent