import React, { Component } from 'react'
class Footer extends Component{
    render(){

        return (
     <footer class="app-footer bg-white p-2">
        <span><a href="#">Learning Management System</a> | V-0.01</span>
        <span class="ml-auto"> © <a href="#">Studecademy</a></span>
    </footer>
        );


    }
}
export default Footer