import React, { Component } from 'react';


class organizationDashboard extends Component {

  render() {

    return (
      <div className="fadeIn animated">
        <div className="container">
          <div className="row" style={{ margin: '0 !important' }}>
            <div className="col-md-2">
              <div className="card">
                <a href="#">
                  <div className="card-body text-center">
                    <i className="icon-speedometer" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Dashboard</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="#">
                  <div className="card-body text-center">
                    <i className="icon-graph" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Analytics</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="#" data-toggle="modal" data-target="#modelId">
                  <div className="card-body text-center">
                    <i className="icon-paper-clip" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Packages</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="organization-view.html">
                  <div className="card-body text-center">
                    <i className="icon-organization" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Organization</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="#">
                  <div className="card-body text-center">
                    <i className="icon-home" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Dashboard</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="#">
                  <div className="card-body text-center">
                    <i className="icon-home" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Dashboard</span>
                  </div>
                </a>
              </div>
            </div>
            <div className="col-md-2">
              <div className="card">
                <a href="#">
                  <div className="card-body text-center">
                    <i className="icon-home" style={{ fontSize: 36 }} /> <br />
                    <span className="h5 font-weight-bold">Dashboard</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

    );


  }
}
export default organizationDashboard