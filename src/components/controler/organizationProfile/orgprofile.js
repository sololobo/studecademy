import React, { Component } from 'react';


class mentorProfile extends Component {

    render() {

        return (
            <div className="fadeIn animated">
                <span className="h3 font-weight-bold mb-3">My Profile:</span>
                <div className="card mt-2">
                    <div className="card-header h-100 ">
                        <img src="../../img/background.jpg" alt="" className="cover-photo-my-profile" id="cover-photo" />
                        <img src="../../img/avatars/6.jpg" alt="profile picture" className="profile-photo-my-profile"
                            id="profile-photo" />
                    </div>
                    <div className="card-body">
                        <div className="pt-2 pb-2 pl-5 pr-5 border mb-5 mt-5">
                            <span className="border-title h3 font-weight-bold">Personal Details:</span>
                            <div className="row mt-3">
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> ID:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2"> Name:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Contact Number:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Email:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Designation:</label>
                                        <input type="text" name="" id="" placeholder="18705526015"
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Specialization:</label>
                                        <input type="text" name="" id="" placeholder="18705526015"
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Profile
                                        Picture:</label>
                                        <input type="file" name="" id="" className="form-control col-md-7"
                                            onchange="readURL(this);" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Change Password:</label>
                                        <input type="password" name="" id="" className="form-control col-md-7" />
                                    </div>


                                </div>
                                <div className="col-md-6">
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Address 2:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">City:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">State:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Country:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>
                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">PIN Code:</label>
                                        <input type="text" name="" id="" placeholder="18705526015" disabled
                                            className="form-control col-md-7" />
                                    </div>

                                    <div className="form-group row">
                                        <label for="" className="mb-2 font-weight-bold col-md-5 mt-2">Upload Cover
                                        Photo:</label>
                                        <input type="file" name="" id="" className="form-control col-md-7"
                                            onchange="readURL(this);" />
                                    </div>
                                    <button className="btn btn-primary float-right text-white">Save and Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        )

    }
}
export default mentorProfile