import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Org from '../organizationList/organizationList'

class createOrganization extends Component {
    constructor(props) {
        super(props);
        this.showOrg = this.showOrg.bind(this);


        this.state = {





        }
    }
    showOrg() {

        ReactDOM.render(<Org />, document.getElementById('contain1'))

    }
    submit() {
        console.log(this.state);
        let url = global.API + "/studapi/public/api/addstudent";
        let data = this.state;
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        }).then((result) => {
            result.json().then((resp) => {
                console.warn("resp", resp)
                alert("Data is submitted")
            })
        })
    }

    render() {

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">Add Organization</span>
                        <a onClick={this.showOrg} className="float-right mt-2">+ List of Organizations</a>
                    </div>
                    <div className="card-body">
                        <div className="border pt-2 pb-2 pl-3 pr-3">
                            <span className=" font-weight-bold h3 border-title">Organization Details:</span>
                            <hr />
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Organization*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Address*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">PIN*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">URL of the Organization*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Subsciption Starts From*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="date" className="form-control" />
                                    </div>

                                </div>

                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Organization Type*:</label>
                                        <select name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control">
                                            <option value={this.state.unit_i_d}>Organization Type</option>
                                            <option value={this.state.unit_i_d}>Training Institute</option>
                                            <option value={this.state.unit_i_d}>College</option>
                                            <option value={this.state.unit_i_d}>School</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">City*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">State*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>

                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Telephone Number*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="text" className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Subsciption Ends on*:</label>
                                        <input name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} type="date" className="form-control" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group mb-1">
                                        <label for="" className="mb-0 font-weight-bold">Upload Logo*:</label> <br />
                                        <img id="blah" src="http://placehold.it/180" alt="your image" className="organization-logo" />
                                        <input type="file" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control-sm" onchange="readURL(this);" />
                                    </div>
                                    <div className="form-group mt-4">
                                        <label for="" className="mb-0 font-weight-bold">Country*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Organization Email Address*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Current Status*:</label>
                                        <select name="unit_i_d" onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control">
                                            <option value={this.state.unit_i_d}>Pending</option>
                                            <option value={this.state.unit_i_d}>Active</option>
                                            <option value={this.state.unit_i_d}>Deactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Name of Contact Person*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Designation*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Email Address*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Contact Number*:</label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">No. of User Role Allocation*:</label>
                                        <input type="number" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Maximum Number of Students*:</label>
                                        <input type="number" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="form-group">
                                        <label for="" className="mb-0 font-weight-bold">Maximum number of uploading File Size(MB)*: </label>
                                        <input type="text" name="unit_i_d" value={this.state.unit_i_d} onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} className="form-control" />

                                    </div>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-success"><i className="icon-check"></i> Save &amp; Proceed</button>
                        </div>
                        <hr />
                        <div className="border pt-2 pr-4 pl-4 pb-3">
                            <span className="font-weight-bold h3 border-title">Package Details:</span>
                            <hr />
                            <div className="input-group">
                                <label for="" className="mb-0 font-weight-bold">Select Package:</label>
                                <select name="unit_i_d" onChange={(data) => { this.setState({ unit_i_d: data.target.value }) }} id="" className="form-contol ml-2">
                                    <option value={this.state.unit_i_d}>Select Package</option>
                                    <option value={this.state.unit_i_d}>Organization</option>
                                    <option value={this.state.unit_i_d}>Training Institute</option>
                                    <option value={this.state.unit_i_d}>Customize Package</option>
                                </select>
                                <button className="btn btn-dark ml-2 btn">Proceed</button>
                            </div>
                            <table className="mt-3 table table-bordered table-responsive-md">
                                <thead className="bg-light">
                                    <tr>
                                        <th rowspan="2" className="border"><input type="checkbox" onclick="toggle(this);" /></th>
                                        <th rowspan="2" className="border">Name of Modules</th>
                                        <th colspan="3" className="text-center border">Action</th>
                                    </tr>
                                    <tr className="border">
                                        <th className="border">Read</th>
                                        <th className="border">Write</th>
                                        <th className="border">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Unit</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Subject</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Session</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Group/Department/className</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>className Activity</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Attendance</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Examination</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Home work/Assignment</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Guide Notes</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Report Card</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Books</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Question Bank</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Routine</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Holiday List</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Performance of Teacher</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Performance of Students</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Payments/Fees Collection</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Forum</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Noticeboard</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="checkbox" /></td>
                                        <td>Group Chat</td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                        <td><input type="checkbox" /></td>
                                    </tr>
                                </tbody>
                            </table>
                            <button type="submit" className="btn btn-success"><i className="icon-check"></i> Save &amp; Proceed</button>
                        </div>
                    </div>
                </div>


            </div>

        );


    }
}
export default createOrganization