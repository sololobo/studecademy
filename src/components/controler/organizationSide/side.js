import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Org from '../organizationList/organizationList'
import Profile from '../organizationProfile/orgprofile'
import Dashboard from '../organizationDashboardview/organizationDashboard'


class organizationSide extends Component {
    constructor(props) {
        super(props);
        this.showOrg = this.showOrg.bind(this);
        this.showProfile = this.showProfile.bind(this);
        this.showDashboard = this.showDashboard.bind(this);
    }
    showOrg() {

        ReactDOM.render(<Org />, document.getElementById('contain1'))

    }
    showProfile() {

        ReactDOM.render(<Profile />, document.getElementById('contain1'))

    }
    showDashboard() {

        ReactDOM.render(<Dashboard />, document.getElementById('contain1'))

    }

    render() {

        return (
            <div className="sidebar">
                <nav className="sidebar-nav">
                    <ul className="nav">

                        <li className="nav-item">
                            <a onClick={this.showDashboard} className="nav-link"><i className="icon-notebook"></i> Dashboard</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showOrg} className="nav-link"><i className="icon-user-follow" /> Organizations</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={this.showProfile} className="nav-link"><i className="icon-user"></i> My Profile</a>
                        </li>


                    </ul>
                </nav>
                <button className="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>

        );


    }
}
export default organizationSide