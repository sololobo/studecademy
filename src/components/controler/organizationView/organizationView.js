import React, { Component } from 'react';
import './organizationView.css';

class organizationView extends Component {

  render() {

    return (
      <div className="fadeIn animated">
        <div className="card">
          <div className="card-header">
            <span className="font-weight-bold text-primary mb-0 h3">Organization</span>
            <a href="organization-view.html" className="float-right mt-2"><i className="icon-action-undo" /> Back to list Organizations</a>
          </div>
          <div className="card-body">
            <h2>
              <img id="blah" src="http://placehold.it/180" alt="your image" className="organization-logo" />
              <span className="font-weight-bold">Constellation Training &amp; Placement</span>
              <span className="float-right small"><a href="#" className="btn  btn-primary text-white mt-4"> Login to control Panel</a></span>
            </h2>
            <hr />
            <div className="row">
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-people" />
                    </div>
                    <div className="h4 mb-0">8500</div>
                    <small className="text-muted text-uppercase font-weight-bold">Active Users</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar bg-info" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-user-follow" />
                    </div>
                    <div className="h4 mb-0">10</div>
                    <small className="text-muted text-uppercase font-weight-bold">Active Units</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar bg-success" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-user" />
                    </div>
                    <div className="h4 mb-0">20</div>
                    <small className="text-muted text-uppercase font-weight-bold">Registered Mentor</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar bg-warning" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-pie-chart" />
                    </div>
                    <div className="h4 mb-0">2800</div>
                    <small className="text-muted text-uppercase font-weight-bold">Registered Students</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-speedometer" />
                    </div>
                    <div className="h4 mb-0">54GB</div>
                    <small className="text-muted text-uppercase font-weight-bold">Resource USes</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar bg-danger" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-md-2">
                <div className="card alert-blue">
                  <div className="card-body">
                    <div className="h1 text-muted text-right mb-4">
                      <i className="icon-speech" />
                    </div>
                    <div className="h4 mb-0">Done</div>
                    <small className="text-muted text-uppercase font-weight-bold">Payment Status</small>
                    <div className="progress progress-xs mt-3 mb-0">
                      <div className="progress-bar bg-info" role="progressbar" style={{ width: '25%' }} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="border pt-2 pb-2 pl-3 pr-3">
              <span className=" font-weight-bold h3 border-title">Organization Details:</span>
              <div className="row">
                <div className="col-md-2" />
                <div className="col-md-8 text-center">
                  <p><b>Name of the organization:</b> &nbsp; Constellation Training and Placement Private Limited</p>
                  <p><b>Organization type:</b> &nbsp;Training Institute</p>
                  <p><b>Address:</b> &nbsp;Kolkata</p>
                  <p><b>City:</b> &nbsp;</p>
                  <p><b>State:</b> &nbsp;</p>
                  <p><b>PIN Code:</b> &nbsp;</p>
                  <p><b>Country:</b> &nbsp;</p>
                  <p><b>Telephone Number:</b> &nbsp;</p>
                  <p><b>Website URL:</b> &nbsp;</p>
                  <p><b>Email Address:</b> &nbsp;</p>
                  <hr />
                  <p><b>Subscription Starts on:</b> &nbsp;</p>
                  <p><b>Subscription Ends on:</b> &nbsp;</p>
                  <p><b>Name of Contact Person:</b> &nbsp;</p>
                  <p><b>Email Address:</b> &nbsp;</p>
                  <p><b>Contact Number:</b> &nbsp;</p>
                  <p><b>Designation:</b> &nbsp;</p>
                </div>
                <div className="col-md-2" />
              </div>
            </div>
            <hr />
            <div className="border pt-2 pr-4 pl-4 pb-3">
              <table className="mt-3 table table-bordered table-responsive-md">
                <thead className="bg-light">
                  <tr>
                    <th rowSpan={2} className="border">Name of Modules</th>
                    <th colSpan={3} className="text-center border">Action</th>
                  </tr>
                  <tr className="border">
                    <th className="border">Read</th>
                    <th className="border">Write</th>
                    <th className="border">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Unit</td>
                    <td><input type="checkbox" defaultChecked disabled /></td>
                    <td><input type="checkbox" defaultChecked disabled /></td>
                    <td><input type="checkbox" defaultChecked disabled /></td>
                  </tr>
                  <tr>
                    <td>Exam Modules</td>
                    <td><input type="checkbox" disabled /></td>
                    <td><input type="checkbox" defaultChecked disabled /></td>
                    <td><input type="checkbox" defaultChecked disabled /></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div className="modal fade" id="deleteAlert" tabIndex="{-1}" role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="container-fluid text-center">
                  <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                  <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                  <button type="button" className="btn btn-warning">Yes</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal fade" id="passwordAlert" tabIndex="{-1}" role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <div className="container-fluid text-center">
                  <h4><span className="font-weight-bold">Change Password</span></h4>
                  <hr />
                  <p>
                  </p><div className="input-group">
                    <b className="mt-1">New Password:</b> &nbsp;
              <input type="password" className="form-control" />
                  </div>
                  <p>
                    <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                    <button type="button" className="btn btn-warning">Change Password</button>
                  </p></div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );


  }
}
export default organizationView