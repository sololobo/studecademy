import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Organizationview from '../organizationView/organizationView'
import Organization from '../createOrganizatio/createOrganization'

class organizationList extends Component {


    constructor(props) {
        super(props);
        this.showOrgview = this.showOrgview.bind(this);
        this.showOrganization = this.showOrganization.bind(this);
        this.state = {
            organisationList: [],

        }

    }
    showOrgview() {

        ReactDOM.render(<Organizationview />, document.getElementById('contain1'))

    }
    showOrganization() {

        ReactDOM.render(<Organization />, document.getElementById('contain1'))

    }
    componentDidMount() {

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },

        };


        let url = global.API + "/studapi/public/api/viewallteachers";
        fetch(url, requestOptions)
            .then(res => res.json())
            .then(json => {
                this.setState({
                    organisationList: json
                })
            });



    }

    render() {
        var { isLoaded, organisationList } = this.state;

        return (
            <div className="fadeIn animated">
                <div className="card">
                    <div className="card-header">
                        <span className="font-weight-bold text-primary mb-0 h3">View Organization</span>
                        <a onClick={this.showOrganization} className="float-right">+ Create New Organization</a>
                    </div>
                    <div className="card-body">
                        <table className="datatable table table-bordered table-responsive-md ">
                            <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>User Name</th>
                                    <th>Name of Organization</th>
                                    <th>Organization Type</th>
                                    <th>Name of the Contact Person</th>
                                    <th>Contact Number</th>
                                    <th>Contact Mail</th>
                                    <th>Subscription Date</th>
                                    <th>Package Name</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {organisationList.map(organisation => (
                                    <tr>
                                        <td>STU001</td>
                                        <td>TINT1</td>
                                        <td>Techno Internation New Town</td>
                                        <td>College</td>
                                        <td>Mr. Raj Kapoor</td>
                                        <td>+91-9876543210</td>
                                        <td>raj@tint.com</td>
                                        <td>23-02-2019</td>
                                        <td>
                                            <badge className="bg-light badge badge-pill">Active</badge>
                                        </td>
                                        <td>Organization</td>
                                        <td className="text-center">
                                            <a className="icon-options-vertical" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <a className="dropdown-item text-dark" onClick={this.showOrgview} ><i
                                                    className="icon-eye"></i> View Organization</a>
                                                <a className="dropdown-item text-dark" href="#" data-target="#passwordAlert" data-toggle="modal"><i
                                                    className="icon-settings"></i> Change Password</a>
                                            </div>
                                            <span><a href="organization-edit.html" className="text-warning"><i className="icon-pencil"></i></a></span>
                                            <span><a href="#" className="text-danger" data-toggle="modal" data-target="#deleteAlert"><i className="icon-trash"></i></a></span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div>
                    {/*-modal*/}
                    <div className="modal fade" id="deleteAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="container-fluid text-center">
                                        <h4><span className="font-weight-bold">Are You Sure?</span></h4>
                                        <p><i> Once You confirm, Data Will not be recovered/restored</i></p>
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button" className="btn btn-warning">Yes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*-/modal*/}
                    {/*-modal for forget password*/}
                    <div className="modal fade" id="passwordAlert" tabIndex={-1} role="dialog" aria-labelledby="modelTitldeleteAlert" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <div className="container-fluid text-center">
                                        <h4><span className="font-weight-bold">Change Password</span></h4>
                                        <hr />
                                        <p>
                                        </p><div className="input-group">
                                            <b className="mt-1">New Password:</b> &nbsp;
                                            <input type="password" className="form-control" />
                                        </div>
                                        <p />
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">No! Cancel it</button>
                                        <button type="button" className="btn btn-warning">Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );


    }
}
export default organizationList