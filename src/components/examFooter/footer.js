import React, { Component } from 'react'
class Footer extends Component {
    render() {

        return (
            <footer className="footer">
                <span><a href="#">Learning Management System</a> | V-0.01</span>
                <span className="ml-auto"> © <a href="#">Studecademy</a></span>
            </footer>

        );


    }
}
export default Footer