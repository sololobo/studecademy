import React, { Component } from 'react';
import logo from '../includes/media/tm-logo.png';
import swal from 'sweetalert';
import Login from '../loginComponent/login';
import ReactDOM from 'react-dom';

import Cookies from 'js-cookie';
import '../includes/global1';
class forgetComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {

            pwd: "",
            otp: "",
            ii: 0,
            email: this.props.email

        }


    }



    submit() {
        var i = 0;
        var pass = document.getElementById('pass');
        var pass1 = document.getElementById('pass1');
        if (i < 4) {
            if (document.getElementById("otp").value == "") {
                document.getElementById("otpError").innerHTML = "*Please fill this field";
                document.getElementById("otp").focus();
                document.getElementById("otp").style.borderColor = "#FF0000";
            }

            if (document.getElementById("otp").value != "") {
                document.getElementById("otpError").innerHTML = "";
                i = i + 1;
                document.getElementById("otp").style.borderColor = "";
            }
            if (document.getElementById("pass").value == "") {
                document.getElementById("passError").innerHTML = "*Please fill this field";
                document.getElementById("pass").focus();
                document.getElementById("pass").style.borderColor = "#FF0000";
            }

            if (document.getElementById("pass").value != "") {
                document.getElementById("passError").innerHTML = "";
                i = i + 1;
                document.getElementById("pass").style.borderColor = "";
            }
            if (document.getElementById("pass1").value == "") {
                document.getElementById("pass1Error").innerHTML = "*Please fill this field";
                document.getElementById("pass1").focus();
                document.getElementById("pass1").style.borderColor = "#FF0000";
            }

            if (document.getElementById("pass1").value != "") {
                document.getElementById("pass1Error").innerHTML = "";
                i = i + 1;
                document.getElementById("pass1").style.borderColor = "";
            }
            if (pass.value != pass1.value) {
                document.getElementById("pass1Error").innerHTML = "Passwords must match";
                document.getElementById("pass1").focus();
                document.getElementById("pass1").style.borderColor = "#FF0000";

            }
            if (pass.value == pass1.value) {
                i = i + 1;
                document.getElementById("pass1Error").innerHTML = "";
                document.getElementById("pass1").style.borderColor = "";
            }

            if (i == 4) {
                console.log(i);
                this.state.ii = 4;
            }
        }
        if (this.state.ii == 4) {
            console.log(this.state);
            let url = global.API + "/studapi/public/api/changepass2";
            let data = this.state;
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }).then((result) => {
                result.json().then((resp) => {
                    console.warn("resp", resp)
                    if (resp[0]['result'] == 1) {
                        swal({
                            title: "Done!",
                            text: "Password Changed Sucessfully",
                            icon: "success",
                            button: "OK",
                        });
                        ReactDOM.render(<Login />, document.getElementById('root'))
                    }

                    else if (resp[0]['result'] == 4) {
                        swal({
                            title: "Error!",
                            text: "Wrong OTP",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                    else if (resp[0]['result'] == 2) {
                        swal({
                            title: "Error!",
                            text: "No such user, Please check your username",
                            icon: "warning",
                            button: "OK",
                        });
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "OTP expired",
                            icon: "warning",
                            button: "OK",
                        });
                    }



                })
            })
        }
    }

    render() {

        return (

            <div className="container-modal">
                <div className="row h-100 align-items-center">
                    <div className="col-md-4"></div>
                    <div className="col-md-4 px-5 py-5 bg-white login">
                        <img src={logo} alt="" className="login-logo" />
                        <h2 className="font-weight-bold mb-0">Change Password</h2>
                        <hr />
                        <div className="form-group login-form-inputs">

                            <div className="alert alert-success" role="alert">
                                <i className="fa fa-check-circle"></i> Please check your mail for otp:
                            </div>
                            <div className="form-group">
                                <label for="" className="mb-0">Enter Your OTP :</label>
                                <input type="password" id="otp" name="" value={this.state.otp} className="form-control mb-2" onChange={(data) => { this.setState({ otp: data.target.value }) }} required />
                                <span id="otpError" class="text-danger font-weight-bold"></span>
                            </div>
                            <div className="form-group">
                                <label for="" className="mb-0">Enter Your New Password:</label>
                                <input type="password" id="pass" name="" className="form-control mb-2" required />
                                <span id="passError" class="text-danger font-weight-bold"></span>
                            </div>
                            <div className="form-group">
                                <label for="" className="mb-0">Confirm your New Password:</label>
                                <input type="password" id="pass1" name="pwd" value={this.state.pwd} onChange={(data) => { this.setState({ pwd: data.target.value }) }} className="form-control mb-2" />
                                <span id="pass1Error" class="text-danger font-weight-bold"></span>
                            </div>
                            <button type="submit" onClick={() => { this.submit() }} className=" btn btn-primary form-control">Reset password</button>
                        </div>
                    </div>
                    <div className="col-md-4"></div>
                </div>
            </div>

        );


    }
}
export default forgetComponent