
    function startTime(){
    var dt = new Date();

    // ensure date comes as 01, 09 etc
    var DD = ("0" + dt.getDate()).slice(-2);

    // getMonth returns month from 0
    var MM = ("0" + (dt.getMonth() + 1)).slice(-2);
    var YYYY = dt.getFullYear();
    var hh = ("0" + dt.getHours()).slice(-2);
    var mm = ("0" + dt.getMinutes()).slice(-2);
    var ss = ("0" + dt.getSeconds()).slice(-2);
    var ampm = hh >= 12 ? 'PM' : 'AM';
      hh = hh % 12;
      hh = hh ? hh : 12; // the hour '0' should be '12'
      mm = mm < 10 ? ""+mm : mm;

    var date_string =  hh + ":" + mm + ":" + ss +" "+ ampm +" "+ DD + "-" + MM + "-" + YYYY ;
    document.getElementById('dateTime').innerHTML=date_string;
    var t = setTimeout(startTime, 500);
   }
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }